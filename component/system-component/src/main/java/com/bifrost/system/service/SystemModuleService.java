package com.bifrost.system.service;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.bifrost.common.banner.ModuleInfo;
import com.bifrost.common.service.GenericService;
import com.bifrost.system.model.SystemModule;
import com.bifrost.system.repository.SystemModuleRepository;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class SystemModuleService extends GenericService<SystemModuleRepository, SystemModule, String> {

	private static final String KEY = "system:module";
	
	public SystemModuleService() {
		super(KEY);
	}
	
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		if(this.findById(ModuleInfo.HAZELCAST_SERVER_CODE)==null)
			this.save(new SystemModule(ModuleInfo.HAZELCAST_SERVER_CODE, ModuleInfo.HAZELCAST_SERVER_DESC));
		if(this.findById(ModuleInfo.CORE_ORCHESTRATOR_CODE)==null)
			this.save(new SystemModule(ModuleInfo.CORE_ORCHESTRATOR_CODE, ModuleInfo.CORE_ORCHESTRATOR_DESC));
		if(this.findById(ModuleInfo.CORE_LOGGING_CODE)==null)
			this.save(new SystemModule(ModuleInfo.CORE_LOGGING_CODE, ModuleInfo.CORE_LOGGING_DESC));
		if(this.findById(ModuleInfo.CORE_GUI_CODE)==null)
			this.save(new SystemModule(ModuleInfo.CORE_GUI_CODE, ModuleInfo.CORE_GUI_DESC));
		if(this.findById(ModuleInfo.REST_ENDPOINT_CODE)==null)
			this.save(new SystemModule(ModuleInfo.REST_ENDPOINT_CODE, ModuleInfo.REST_ENDPOINT_DESC));
		if(this.findById(ModuleInfo.SOAP_ENDPOINT_CODE)==null)
			this.save(new SystemModule(ModuleInfo.SOAP_ENDPOINT_CODE, ModuleInfo.SOAP_ENDPOINT_DESC));
		if(this.findById(ModuleInfo.KAFKA_ENDPOINT_CODE)==null)
			this.save(new SystemModule(ModuleInfo.KAFKA_ENDPOINT_CODE, ModuleInfo.KAFKA_ENDPOINT_DESC));
		if(this.findById(ModuleInfo.ISO8583_ENDPOINT_CODE)==null)
			this.save(new SystemModule(ModuleInfo.ISO8583_ENDPOINT_CODE, ModuleInfo.ISO8583_ENDPOINT_DESC));
		if(this.findById(ModuleInfo.DATA_SOURCE_ENDPOINT_CODE)==null)
			this.save(new SystemModule(ModuleInfo.DATA_SOURCE_ENDPOINT_CODE, ModuleInfo.DATA_SOURCE_ENDPOINT_DESC));
	}
}
