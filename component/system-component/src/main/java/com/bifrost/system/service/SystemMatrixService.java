package com.bifrost.system.service;

import java.net.UnknownHostException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.system.Heartbeat;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class SystemMatrixService {
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private ThreadPoolTaskScheduler scheduler;
	
	@Value("${"+ParamConstant.Bifrost.HEARTBEAT_DELAY+":3000}")
	private Long heartbeatDelay;
	
	// A process to add hostname to heartbeat cache
	@PostConstruct
	private void init() throws UnknownHostException {
		scheduler.scheduleWithFixedDelay(new Heartbeat(context).withTtl(heartbeatDelay), heartbeatDelay);
	}
}
