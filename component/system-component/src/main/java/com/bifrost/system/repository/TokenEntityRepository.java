package com.bifrost.system.repository;

import com.bifrost.system.model.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TokenEntityRepository extends JpaRepository<TokenEntity, Long>, JpaSpecificationExecutor<TokenEntity> {

}