package com.bifrost.system.model;

import com.bifrost.system.dto.TokenServerDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "token_jwt")
public class TokenEntity {

    private int id;
    private String username;
    private String token;
    private String secretKey;
    private String ipPublic;
    private String merchantName;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "username", nullable = false)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Column(name = "secret_key", nullable = false)
    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @Column(name = "ip_public")
    public String getIpPublic() {
        return ipPublic;
    }

    public void setIpPublic(String ipPublic) {
        this.ipPublic = ipPublic;
    }

    @Column(name = "merchant_name")
    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public TokenServerDto parseDto() {
        TokenServerDto tokenServerDto = new TokenServerDto();
        tokenServerDto.setId(String.valueOf(this.id));
        tokenServerDto.setUsername(this.username);
        tokenServerDto.setToken(this.token);
        tokenServerDto.setSecretKey(this.secretKey);
        tokenServerDto.setIpPublic(this.ipPublic);
        tokenServerDto.setMerchantName(this.merchantName);
        return tokenServerDto;
    }

}