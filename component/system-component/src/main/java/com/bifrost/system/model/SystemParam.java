package com.bifrost.system.model;

import java.io.IOException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.system.dto.SystemParamDto;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 */
@Entity
@Table(name = "system_param")
public class SystemParam implements AbstractModel {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "category", nullable = false, length = 40)
    private String category;

    @Column(name = "name", nullable = false, unique = true, length = 255)
    private String name;

    @Column(name = "value", nullable = true, length = 255)
    private String value;

    @Column(name = "description")
    private String description;

    @Column(name = "application", nullable = true, length = 40, columnDefinition = "varchar(40) default 'bifrost'")
    private String application;

    @Column(name = "profile", nullable = true, length = 40, columnDefinition = "varchar(40) default 'default'")
    private String profile;

    @Column(name = "label", nullable = true, length = 40, columnDefinition = "varchar(40) default 'master'")
    private String label;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getPk() {
        return id;
    }

    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeLong(id);
        out.writeUTF(category);
        out.writeUTF(name);
        out.writeUTF(value);
        out.writeUTF(application);
        out.writeUTF(profile);
        out.writeUTF(label);
        out.writeUTF(description);
    }

    public void readData(ObjectDataInput in) throws IOException {
        id = in.readLong();
        category = in.readUTF();
        name = in.readUTF();
        value = in.readUTF();
        application = in.readUTF();
        profile = in.readUTF();
        label = in.readUTF();
        description = in.readUTF();
    }

    public SystemParamDto parseDto(){
        SystemParamDto systemParamDto = new SystemParamDto();
        systemParamDto.setId(this.id.toString());
        systemParamDto.setCategory(this.category);
        systemParamDto.setName(this.name);
        systemParamDto.setValue(this.value);
        systemParamDto.setDescription(this.description);
        systemParamDto.setApplication(this.application);
        systemParamDto.setProfile(this.profile);
        systemParamDto.setLabel(this.label);
        return systemParamDto;
    }
}