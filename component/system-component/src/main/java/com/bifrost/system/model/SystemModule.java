package com.bifrost.system.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "system_module")
public class SystemModule implements AbstractModel {
	
	@Id
	@Column(name = "code", unique = true, nullable = false, length = 10)
	private String code;

	@Column(name = "description", nullable = false)
	private String description;

	public SystemModule() {
	}

	public SystemModule(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getPk() {
		return code;
	}

	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(code);
		out.writeUTF(description);
	}

	public void readData(ObjectDataInput in) throws IOException {
		code = in.readUTF();
		description = in.readUTF();
	}

}
