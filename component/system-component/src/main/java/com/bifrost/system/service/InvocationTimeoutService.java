package com.bifrost.system.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.common.service.GenericService;
import com.bifrost.system.model.InvocationTimeout;
import com.bifrost.system.repository.InvocationTimeoutRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class InvocationTimeoutService extends GenericService<InvocationTimeoutRepository, InvocationTimeout, Long> {

	private static final String KEY = "invocation:timeout";
	
	@Value("${" + ParamConstant.Bifrost.GENERIC_DEFAULT_TIMEOUT + ":25000}")
	private String defaultTimeout;

	public InvocationTimeoutService() {
		super(KEY);
	}
	
	@Cacheable(cacheNames = KEY+":findByEndpointCodeAndInvocationCode")
	public InvocationTimeout findByEndpointCodeAndInvocationCode(String endpointCode, String invocationCode) {
		return null;
	}

	@Override
	@CachePut(
			cacheNames = KEY+":findByEndpointCodeAndInvocationCode", 
			key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpointCode, #t.invocationCode)"
	)
	public InvocationTimeout save(InvocationTimeout t) {
		return super.save(t);
	}

	@Override
	@CacheEvict(
			cacheNames = KEY+":findByEndpointCodeAndInvocationCode", 
			key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpointCode, #t.invocationCode)"
	)
	public void delete(InvocationTimeout t) {
		super.delete(t);
	}

	@Override
	@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndInvocationCode", allEntries = true)
	public void evictAll() {
		super.evictAll();
	}

	@Override
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<SimpleKey, InvocationTimeout> findByEndpointCodeAndInvocationCode = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndInvocationCode");
		for(InvocationTimeout t : findAll()) {
			SimpleKey key = new SimpleKey(t.getEndpointCode(), t.getInvocationCode());
			if(findByEndpointCodeAndInvocationCode.get(key)==null)
				findByEndpointCodeAndInvocationCode.set(key, findByEndpointCodeAndInvocationCode(t.getEndpointCode(), t.getInvocationCode()));
		}
	}
}
