package com.bifrost.system;

import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.springframework.context.ApplicationContext;

import com.bifrost.common.constant.CacheName;
import com.bifrost.system.configuration.AbstractSystemIdentity;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */

public class Heartbeat implements Runnable {

	private Long timeToLive;
	private IMap<String, String> systemMatrix;
	private AbstractSystemIdentity systemIdentity;
	// private static final Log LOGGER = LogFactory.getLog(Heartbeat.class);
	
	public Heartbeat(ApplicationContext context) throws UnknownHostException {
		if(context!=null) {
			this.systemMatrix = context.getBean(HazelcastInstance.class).getMap(CacheName.SYSTEM_MATRIX);
			this.systemIdentity = context.getBean(AbstractSystemIdentity.class);
		}
	}
	
	public Heartbeat withTtl(Long timeToLive) {
		this.timeToLive = timeToLive;
		return this;
	}
	
	public void run() {
		try {
			systemMatrix.putAsync(systemIdentity.getHost(), systemIdentity.getModuleCode(), timeToLive, TimeUnit.MILLISECONDS);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
