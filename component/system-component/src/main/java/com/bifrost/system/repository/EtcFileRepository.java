package com.bifrost.system.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.system.model.EtcFile;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EtcFileRepository extends JpaRepository<EtcFile, Long> {

	public List<EtcFile> findByName(String name);
	public List<EtcFile> findByExt(String ext);
	public EtcFile findByNameAndExt(String name, String ext);
	
}
