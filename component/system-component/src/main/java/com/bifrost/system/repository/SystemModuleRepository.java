package com.bifrost.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.system.model.SystemModule;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface SystemModuleRepository extends JpaRepository<SystemModule, String> {
	
}
