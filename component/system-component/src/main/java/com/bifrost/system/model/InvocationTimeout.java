package com.bifrost.system.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(
		name = "invocation_timeout", 
		uniqueConstraints = {
				@UniqueConstraint(columnNames = { "endpoint_code", "invocation_code" }) 
		})
public class InvocationTimeout implements AbstractModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "endpoint_code", nullable = false, length = 20)
	private String endpointCode;

	@Column(name = "invocation_code", nullable = false, length = 255)
	private String invocationCode;

	@Column(name = "timeout_millis", nullable = true, columnDefinition = "integer default 0")
	private Long timeoutMillis;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getInvocationCode() {
		return invocationCode;
	}

	public void setInvocationCode(String invocationCode) {
		this.invocationCode = invocationCode;
	}

	public Long getTimeoutMillis() {
		return timeoutMillis;
	}

	public void setTimeoutMillis(Long timeoutMillis) {
		this.timeoutMillis = timeoutMillis;
	}

	public Object getPk() {
		return id;
	}

	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(endpointCode);
		out.writeUTF(invocationCode);
		out.writeLong(timeoutMillis);
	}

	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		endpointCode = in.readUTF();
		invocationCode = in.readUTF();
		timeoutMillis = in.readLong();
	}

}
