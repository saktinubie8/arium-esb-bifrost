package com.bifrost.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.system.model.InvocationTimeout;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface InvocationTimeoutRepository extends JpaRepository<InvocationTimeout, Long> {

	public InvocationTimeout findByEndpointCodeAndInvocationCode(String endpointCode, String invocationCode);
	
}
