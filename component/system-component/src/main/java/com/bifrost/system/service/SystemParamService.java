package com.bifrost.system.service;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.system.model.SystemParam;
import com.bifrost.system.repository.SystemParamRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class SystemParamService extends GenericService<SystemParamRepository, SystemParam, Long>{

	private static final String KEY = "system:param";
	
	public SystemParamService() {
		super(KEY);
	}
	
	@Cacheable(cacheNames = KEY+":findByName")
	public SystemParam findByName(String name) {
		return null;
	}

	@Override
	@CachePut(cacheNames = KEY+":findByName", key = "#t.name")
	public SystemParam save(SystemParam t) {
		return super.save(t);
	}

	@Override
	@CacheEvict(cacheNames = KEY+":findByName", key = "#t.name")
	public void delete(SystemParam t) {
		super.delete(t);
	}

	@Override
	@CacheEvict(cacheNames = KEY+":findByName", allEntries = true)
	public void evictAll() {
		super.evictAll();
	}

	@Override
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, SystemParam> findByName = hazelcastInstance.getMap(KEY+":findByName");
		for(SystemParam t : findAll())
			if(findByName.get(t.getName())==null)
				findByName.set(t.getName(), t);
	}
}
