package com.bifrost.system.configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;

import com.bifrost.common.constant.ParamConstant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public abstract class AbstractSystemIdentity {

	@Value("${"+ParamConstant.Bifrost.IDENTITY_USING_HOSTNAME+":true}")
	private Boolean identityUsingHostname;
	
	public abstract String getModuleCode();
	public abstract int getDefaultPort();
	
	public String getHost() throws UnknownHostException {
		if(identityUsingHostname)
			return InetAddress.getLocalHost().getHostName();
		else
			return InetAddress.getLocalHost().getHostAddress();
	}
}
