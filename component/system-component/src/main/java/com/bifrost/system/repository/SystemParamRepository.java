package com.bifrost.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bifrost.system.model.SystemParam;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author rosaekapratama@gmail.com
 */
public interface SystemParamRepository extends JpaRepository<SystemParam, Long>, JpaSpecificationExecutor<SystemParam> {

    SystemParam findByName(String name);

    List<SystemParam> findByCategory(String category);

}