package com.bifrost.system.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "etc_file", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "ext"})})
public class EtcFile implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;
	
	@Column(name = "name", nullable = false, length = 255)
	private String name;
	
	@Column(name = "ext", nullable = false, length = 10)
	private String ext;
	
	@Column(name = "data", nullable = false)
	private byte[] data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(name);
		out.writeUTF(ext);
		out.writeByteArray(data);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		name = in.readUTF();
		ext = in.readUTF();
		data = in.readByteArray();
	}

	@Override
	public Object getPk() {
		return id;
	}
	
	
	
	
}
