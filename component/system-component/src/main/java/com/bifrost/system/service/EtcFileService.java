package com.bifrost.system.service;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.system.model.EtcFile;
import com.bifrost.system.repository.EtcFileRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EtcFileService extends GenericService<EtcFileRepository, EtcFile, Long>{

	private static final String KEY = "system:file";
	
	public EtcFileService() {
		super(KEY);
	}

	@Cacheable(cacheNames = KEY+":findByName")
	public List<EtcFile> findByName(String name) {
		return null;
	}

	@Cacheable(cacheNames = KEY+":findByExt")
	public List<EtcFile> findByExt(String ext) {
		return null;
	}

	@Cacheable(cacheNames = KEY+":findByNameAndExt")
	public EtcFile findByNameAndExt(String name, String ext) {
		return null;
	}
	
	@CachePut(cacheNames = KEY+":findByNameAndExt", key = "new org.springframework.cache.interceptor.SimpleKey(#t.name, #t.ext)")
	public EtcFile save(EtcFile t) {
		EtcFile retVal = super.save(t); 
		String name = retVal.getName();
		String ext = retVal.getExt();
		IMap<String, List<EtcFile>> findByName = hazelcastInstance.getMap(KEY+":findByName");
		IMap<String, List<EtcFile>> findByExt = hazelcastInstance.getMap(KEY+":findByExt");
		findByName.setAsync(name, repository.findByName(name));
		findByExt.setAsync(ext, repository.findByExt(ext));
		return retVal;
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByName", key = "#t.name"),
			@CacheEvict(cacheNames = KEY+":findByExt", key = "#t.ext"),
			@CacheEvict(cacheNames = KEY+":findByNameAndExt", key = "new org.springframework.cache.interceptor.SimpleKey(#t.name, #t.ext)")
	})
	public void delete(EtcFile t) {
		super.delete(t);
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByName", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findByExt", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findByNameAndExt", allEntries = true)
	})
	public void evictAll() {
		super.evictAll();
	}

	public void populate() throws Exception {
		super.populate();
		IMap<String, List<EtcFile>> findByName = hazelcastInstance.getMap(KEY+":findByName");
		IMap<String, List<EtcFile>> findByExt = hazelcastInstance.getMap(KEY+":findByExt");
		IMap<SimpleKey, EtcFile> findByNameAndExt = hazelcastInstance.getMap(KEY+":findByNameAndExt");
		for(EtcFile t : findAll()) {
			String name = t.getName();
			String ext = t.getExt();
			findByName.setAsync(name, repository.findByName(name));
			findByExt.setAsync(ext, repository.findByExt(ext));
			findByNameAndExt.setAsync(new SimpleKey(name, ext), t);
		}
	}
	
	

}
