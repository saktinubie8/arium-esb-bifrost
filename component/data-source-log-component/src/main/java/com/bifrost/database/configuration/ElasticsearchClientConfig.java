package com.bifrost.database.configuration;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.core.ElasticsearchEntityMapper;
import org.springframework.data.elasticsearch.core.EntityMapper;

import com.bifrost.common.constant.ParamConstant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class ElasticsearchClientConfig extends AbstractElasticsearchConfiguration {
	
	private static final Log LOGGER = LogFactory.getLog(ElasticsearchClientConfig.class);
	
	@Value("${"+ParamConstant.Elasticsearch.CLUSTER_ADDRESSES+"}")
	private String clusterAddresses;
	
	@Value("${"+ParamConstant.Elasticsearch.CONNECT_TIMEOUT+":5000}")
	private int connectTimeout;
	
	@Value("${"+ParamConstant.Elasticsearch.SOCKET_TIMEOUT+":10000}")
	private int socketTimeout;
	
	@Bean
	@Override
	public RestHighLevelClient elasticsearchClient() {

		String[] addresses = clusterAddresses.split(",");
		ClientConfiguration clientConfiguration = ClientConfiguration.builder() 
				.connectedTo(addresses)
				.withConnectTimeout(connectTimeout)
				.withSocketTimeout(socketTimeout)
				.build();

		LOGGER.trace("[System][Elasticsearch] Creating high level rest with configuration below");
		LOGGER.trace("[System][Elasticsearch] Addresses: "+Arrays.toString(addresses));
		LOGGER.trace("[System][Elasticsearch] Connect timeout: "+connectTimeout+" ms");
		LOGGER.trace("[System][Elasticsearch] Socket timeout: "+socketTimeout+" ms");
		return RestClients.create(clientConfiguration).rest();  
	}
	
	@Bean
	@Override
	public EntityMapper entityMapper() {                     
		ElasticsearchEntityMapper entityMapper = new ElasticsearchEntityMapper(elasticsearchMappingContext(),
				new DefaultConversionService());
		entityMapper.setConversions(elasticsearchCustomConversions());
		return entityMapper;
	}
}
