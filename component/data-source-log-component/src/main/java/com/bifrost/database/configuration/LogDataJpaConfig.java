package com.bifrost.database.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.bifrost.common.constant.ParamConstant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration("logDataJpaConfig")
@EnableJpaRepositories(
		basePackages = "com.bifrost", 
		entityManagerFactoryRef = "logEntityManagerFactory",
		transactionManagerRef = "logTransactionManager",
		includeFilters = {@Filter(type = FilterType.REGEX, pattern = "com.bifrost.logging.repository.*")})
public class LogDataJpaConfig {

	@Value("${" + ParamConstant.Database.LOG_HOST + "}")
	private String host;

	@Value("${" + ParamConstant.Database.LOG_PORT + "}")
	private String port;

	@Value("${" + ParamConstant.Database.LOG_NAME + "}")
	private String database;

	@Value("${" + ParamConstant.Database.LOG_USER + "}")
	private String user;

	@Value("${" + ParamConstant.Database.LOG_PASS + "}")
	private String pass;
	
	@Value("${spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation:true}")
	private String noCreateClob;

	@Value("${spring.jpa.hibernate.ddl-auto:none}")
	private String ddlAuto;

	@Bean("logDataSource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().url("jdbc:postgresql://" + host + ":" + port + "/" + database).username(user)
				.password(pass).build();
	}

	@Bean("logEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("logDataSource")DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource);
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setPackagesToScan("com.bifrost.logging.model");
		Properties properties = new Properties();
		properties.setProperty("hibernate.jdbc.lob.non_contextual_creation", noCreateClob);
		properties.setProperty("hibernate.hbm2ddl.auto", ddlAuto);
		em.setJpaProperties(properties);

		return em;
	}

    @Bean("logTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("logEntityManagerFactory")LocalContainerEntityManagerFactoryBean entityManagerFactory) {
    	JpaTransactionManager transactionManager = new JpaTransactionManager();
    	transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
    	return transactionManager;
    }
}
