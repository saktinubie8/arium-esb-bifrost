package com.bifrost.logic;

import javax.sql.DataSource;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface DataSourceStarter {

	public void setDataSource(DataSource dataSource);
	
}
