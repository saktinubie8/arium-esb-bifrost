package com.bifrost.database.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.bifrost.common.constant.ParamConstant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration("cfgDataJpaConfig")
@EnableJpaRepositories(
		basePackages = "com.bifrost", 
		entityManagerFactoryRef = "cfgEntityManagerFactory",
		transactionManagerRef = "cfgTransactionManager",
		excludeFilters = {@Filter(type = FilterType.REGEX, pattern = {"com.bifrost.logging.*", "com.bifrost.gui.*"})})
public class CfgDataJpaConfig {

	@Value("${" + ParamConstant.Database.CONFIG_HOST + "}")
	private String host;

	@Value("${" + ParamConstant.Database.CONFIG_PORT + "}")
	private String port;

	@Value("${" + ParamConstant.Database.CONFIG_NAME + "}")
	private String database;

	@Value("${" + ParamConstant.Database.CONFIG_USER + "}")
	private String user;

	@Value("${" + ParamConstant.Database.CONFIG_PASS + "}")
	private String pass;
	
	@Value("${spring.jpa.hibernate.dialect:org.hibernate.dialect.PostgreSQLDialect}")
	private String hibernateDialect;
	
	@Value("${spring.jpa.hibernate.jdbc.lob.non_contextual_creation:true}")
	private String noCreateClob;

	@Value("${spring.jpa.hibernate.ddl-auto:none}")
	private String ddlAuto;

	@Value("${spring.jpa.hibernate.format_sql:true}")
	private String formatSql;
	
	@Bean("cfgDataSource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().url("jdbc:postgresql://" + host + ":" + port + "/" + database).username(user)
				.password(pass).build();
	}

	@Bean("cfgEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("cfgDataSource")DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource);
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setPackagesToScan(
				new String[] { 
						"com.bifrost.orchestrator.model.modifier",
						"com.bifrost.orchestrator.model.router",
						"com.bifrost.orchestrator.model.mapper",
						"com.bifrost.system.model",
						"com.bifrost.endpoint.model",
						"com.bifrost.endpoint.*.model"
				});
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", hibernateDialect);
		properties.setProperty("hibernate.jdbc.lob.non_contextual_creation", noCreateClob);
		properties.setProperty("hibernate.hbm2ddl.auto", ddlAuto);
		properties.setProperty("hibernate.format_sql", formatSql);
		em.setJpaProperties(properties);

		return em;
	}
	
    @Bean("cfgTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("cfgEntityManagerFactory")LocalContainerEntityManagerFactoryBean entityManagerFactory) {
    	JpaTransactionManager transactionManager = new JpaTransactionManager();
    	transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
    	return transactionManager;
    }
}
