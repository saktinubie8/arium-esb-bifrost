package com.bifrost.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.serialization.ObjectDeserializer;
import com.bifrost.serialization.ObjectSerializer;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@EnableKafka
@Configuration
public class KafkaConfig {

	@Value("${" + ParamConstant.Kafka.SERVER_ADDRESS_LIST + "}")
	private String addressList;

	@Value("${" + ParamConstant.Kafka.CONSUMER_AUTO_OFFSET_RESET + ":earliest}")
	private String autoOffsetReset;

	@Value("${" + ParamConstant.Kafka.TOPIC_GROUP_ID + "}")
	private String groupId;

	@Value("${" + ParamConstant.Kafka.PARTITION_ASSIGNMENT_STRATEGY + ":org.apache.kafka.clients.consumer.RoundRobinAssignor}")
	private String assignmentStrategy;
	
	@Bean
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> configs = new HashMap<String, Object>();
		configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, addressList);
		return new KafkaAdmin(configs);
	}
	
	@Bean()
	public ConsumerFactory<String, Object> consumerFactory() {
		Map<String, Object> configs = new HashMap<String, Object>();
		configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, addressList);
		configs.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
		configs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		configs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ObjectDeserializer.class);
		configs.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, assignmentStrategy);
		configs.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		configs.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "100");
		configs.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "15000");
		return new DefaultKafkaConsumerFactory<String, Object>(configs);
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory(
			ConsumerFactory<String, Object> consumerFactory) {
		ConcurrentKafkaListenerContainerFactory<String, Object> factory = new ConcurrentKafkaListenerContainerFactory<String, Object>();
		factory.setConsumerFactory(consumerFactory);
		return factory;
	}

	@Bean
	public ProducerFactory<String, Object> producerFactory() {
		Map<String, Object> configProps = new HashMap<String, Object>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, addressList);
		configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ObjectSerializer.class);
		configProps.put(ProducerConfig.RETRIES_CONFIG, 0);
		configProps.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
		configProps.put(ProducerConfig.LINGER_MS_CONFIG, 1);
		configProps.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
		return new DefaultKafkaProducerFactory<String, Object>(configProps);
	}

	@Bean
	public KafkaTemplate<String, Object> kafkaTemplate() {
		return new KafkaTemplate<String, Object>(producerFactory());
	}
}
