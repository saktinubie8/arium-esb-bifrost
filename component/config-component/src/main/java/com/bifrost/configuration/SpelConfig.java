package com.bifrost.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class SpelConfig {
	
	/**
	 * Turn on:
	 *  - auto null reference initialization
	 *  - auto collection growing
	 * @return ExpressionParser single instance
	 */
	@Bean
	public ExpressionParser expressionParser() {
		SpelParserConfiguration config = new SpelParserConfiguration(true, true);
		ExpressionParser parser = new SpelExpressionParser(config);
		return parser;
	}

}
