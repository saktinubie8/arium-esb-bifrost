package com.bifrost.configuration;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class CommonConfig {
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
	@Bean
	public ObjectWriter objectWriter(ObjectMapper objectMapper) {
		return objectMapper.writer().forType(Map.class);
	}
	
	@Bean
	public ObjectReader objectReader(ObjectMapper objectMapper) {
		return objectMapper.reader().forType(Map.class);
	}
	
	@Bean
	public AtomicLong atomicLong() {
		return new AtomicLong();
	}
	
}
