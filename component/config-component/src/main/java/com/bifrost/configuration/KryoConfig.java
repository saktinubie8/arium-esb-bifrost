package com.bifrost.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.serialization.KryoService;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class KryoConfig {

	@Value("${"+ParamConstant.Kryo.MAX_POOL_SIZE+":200}")
	private int maxPoolSize;
	
	@PostConstruct
	public void init() {
		KryoService.init(maxPoolSize);
	}
}
