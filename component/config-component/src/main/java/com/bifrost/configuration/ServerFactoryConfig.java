package com.bifrost.configuration;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.system.configuration.AbstractSystemIdentity;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
@EnableRetry
public class ServerFactoryConfig implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {

	@Autowired
	private AbstractSystemIdentity systemIdentity;

	@Value("${"+ParamConstant.Bifrost.MODULE_PORT_MIN+"}")
	private int minPort;

	@Value("${"+ParamConstant.Bifrost.MODULE_PORT_MAX+"}")
	private int maxPort;

	public void customize(ConfigurableServletWebServerFactory server) {
		server.setPort(getAvailablePort());
	}

	/**
	 * @return available port between minPort and maxPort
	 */
	private int getAvailablePort() {
		int port = systemIdentity.getDefaultPort();
		if (available(port)) {
			return port;
		}
		for (port = minPort; port <= maxPort; port++) {
			if (available(port)) {
				return port;
			}
		}
		return port;
	}

	/**
	 * if you have some range for denied ports you can also check it
	 * here just add proper checking and return
	 * @param port
	 * @return false if port checked within that range
	 */
	private boolean available(int port) {
		ServerSocket ss = null;
		DatagramSocket ds = null;
		try {
			ss = new ServerSocket(port);
			ss.setReuseAddress(true);
			ds = new DatagramSocket(port);
			ds.setReuseAddress(true);
			return true;
		} catch (IOException e) {
		} finally {
			if (ds != null) {
				ds.close();
			}

			if (ss != null) {
				try {
					ss.close();
				} catch (IOException e) {
					/* should not be thrown */
				}
			}
		}
		return false;
	}
}
