package com.bifrost.configuration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
@EnableCaching
public class CacheConfig {

}
