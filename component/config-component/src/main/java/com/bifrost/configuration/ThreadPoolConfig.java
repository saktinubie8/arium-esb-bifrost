package com.bifrost.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.bifrost.common.constant.ParamConstant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class ThreadPoolConfig {

	@Value("${"+ParamConstant.Thread.POOL_TASK_CORE_SIZE+"}")
	private int taskCoreSize;
	
	@Value("${"+ParamConstant.Thread.POOL_TASK_MAX_SIZE+"}")
	private int taskMaxSize;
	
	@Value("${"+ParamConstant.Thread.POOL_TASK_KEEP_ALIVE_SECONDS+"}")
	private int taskKeepAliveSeconds;
	
	@Value("${"+ParamConstant.Thread.POOL_TASK_QUEUE_CAPACITY+"}")
	private int taskQueueCapacity;
	
	@Value("${"+ParamConstant.Thread.POOL_TASK_WAIT_TASK_ON_SHUTDOWN+"}")
	private Boolean taskWaitTaskOnShutdown;
	
    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(taskCoreSize);
        threadPoolTaskExecutor.setMaxPoolSize(taskMaxSize);
        threadPoolTaskExecutor.setKeepAliveSeconds(taskKeepAliveSeconds);
        threadPoolTaskExecutor.setQueueCapacity(taskQueueCapacity);
        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(taskWaitTaskOnShutdown);
        return threadPoolTaskExecutor;
    }
	
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(3);
        scheduler.initialize();
        return scheduler;
    }
	
}
