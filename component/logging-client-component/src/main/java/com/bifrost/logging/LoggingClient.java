package com.bifrost.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.model.MessageLog;
import com.bifrost.logging.model.RouteLog;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class LoggingClient {

	private static final Log LOGGER = LogFactory.getLog(LoggingClient.class);
	
	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate;
	
	public void write(EndpointLog log) {
		LOGGER.trace("[System][Logger] Send to kafka broker. "+log.toString());
		kafkaTemplate.send("logging.endpoint.in", log);
	}
	
	public void write(RouteLog log) {
		LOGGER.trace("[System][Logger] Send to kafka broker. "+log.toString());
		kafkaTemplate.send("logging.route.in", log);
	}
	
	public void write(MessageLog log) {
		LOGGER.trace("[System][Logger] Send to kafka broker. "+log.toString());
		kafkaTemplate.send("logging.message.in", log);
	}
}
