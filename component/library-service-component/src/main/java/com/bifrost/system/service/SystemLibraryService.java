package com.bifrost.system.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.objenesis.ObjenesisSerializer;
import org.springframework.objenesis.instantiator.ObjectInstantiator;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.system.library.LibraryClassLoader;
import com.bifrost.system.library.LibraryMap;
import com.bifrost.system.model.SystemLibrary;
import com.bifrost.system.repository.SystemLibraryRepository;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class SystemLibraryService extends GenericService<SystemLibraryRepository, SystemLibrary, Long> {

	private static final Log LOGGER = LogFactory.getLog(SystemLibraryService.class); 
	private static final String KEY = "system:library";

	@Autowired
	private LibraryClassLoader libraryLoader;
	
	@Autowired
	private ObjenesisSerializer objenesisSerializer;
	
	@Autowired
	private LibraryMap libraries;
	
	public SystemLibraryService() {
		super(KEY);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostConstruct
	public void init() {
		for(SystemLibrary systemLibrary : findAll()) {
			
			// Write lib bytes to file
			File file = new File("temp/"+systemLibrary.getFileName()+"."+systemLibrary.getExt());
			FileOutputStream fos;
			if(file.exists())
				file.delete();
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
				fos = new FileOutputStream(file);
				fos.write(systemLibrary.getData());
				fos.close();
			} catch (IOException e) {
				LOGGER.error("[System][Library] Failed to write lib bytes of '"+file.getName()+"', caused by: "+e.getMessage());
				e.printStackTrace();
				continue;
			}

			try {
				LOGGER.trace("[System][Library] Adding '"+file.getName()+"' to system class loader...");
				libraryLoader.addFile(file.getPath());
				LOGGER.debug("[System][Library] '"+file.getName()+"' is added SUCCESSFULLY");
			} catch (MalformedURLException e) {
				LOGGER.error("[System][Library] FAILED to add '"+file.getName()+"' to system class loader, caused by: "+e.getMessage());
				e.printStackTrace();
				continue;
			}
			
			// Put loaded class to cache
			JarFile jarFile = null;
			try {
				jarFile = new JarFile(file);
				Enumeration<JarEntry> en = jarFile.entries();
				while(en.hasMoreElements()) {
					JarEntry jarEntry = en.nextElement();
					if(!jarEntry.getName().endsWith(".class"))
						continue;
					String jarName = jarFile.getName().substring(0,jarFile.getName().length()-4).split("\\\\")[1];
					String fullClsName = jarEntry.getName().replace("/", ".").substring(0, jarEntry.getName().length()-6);
					String clsName = fullClsName.substring(fullClsName.lastIndexOf(".")+1);
					String key = jarName+"."+clsName;
					try {
						Class cls = libraryLoader.loadClass(fullClsName);
						if(!Arrays.asList(cls.getInterfaces()).contains(Serializable.class)) {
							LOGGER.error("[System][Library] FAILED to load bean '"+fullClsName+"' to cache, caused by: "+fullClsName+" not serializable");
							continue;
						}
						ObjectInstantiator instantiator = objenesisSerializer.getInstantiatorOf(cls);
						Object obj = libraries.get(key);
						if(obj!=null && obj.getClass().equals(cls))
							LOGGER.info("[System][Library] Bean '"+fullClsName+"' is already exist, overwritting...");
						libraries.put(key, instantiator.newInstance());
						LOGGER.info("[System][Library] Bean '"+fullClsName+"' is loaded SUCCESSFULLY to cache with key '"+key+"'");
					} catch (ClassNotFoundException e) {
						LOGGER.error("[System][Library] FAILED to load bean '"+fullClsName+"' to cache, caused by: "+e.getMessage());
						e.printStackTrace();
						continue;
					}
				}
			} catch (IOException e) {
				LOGGER.error("[System][Library] FAILED to load '"+file.getName()+"' as jar file, caused by: "+e.getMessage());
				e.printStackTrace();
				continue;
			} finally {
				try {
					jarFile.close();
					file.delete();
					File tempDir = file.getParentFile();
					if(tempDir.isDirectory() && tempDir.getName().equals("temp") && tempDir.list().length==0)
						tempDir.delete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		try {
			libraryLoader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		LOGGER.trace("===================================================================================================================================================================================================");
	}

}
