package com.bifrost.system.library;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.springframework.stereotype.Component;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class LibraryClassLoader extends URLClassLoader {

	public LibraryClassLoader() {
		super(new URL[] {});
	}
	
	public void addFile (String path) throws MalformedURLException {
        String urlPath = "jar:file://" + path + "!/";
        addURL (new URL (urlPath));
    }
	
}
