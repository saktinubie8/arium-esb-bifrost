package com.bifrost.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.system.model.SystemLibrary;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface SystemLibraryRepository extends JpaRepository<SystemLibrary, Long> {
	
}
