package com.bifrost.system.model;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "system_library")
public class SystemLibrary implements AbstractModel {

	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "file_name", nullable = false)
	private String fileName;

	@Column(name = "ext", nullable = false)
	private String ext;

	@Column(name = "date_modified", nullable = false)
	private Date dateModified;

	@Lob
	@Column(name = "data", nullable = false)
	private byte[] data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public Object getPk() {
		return id;
	}

	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(fileName);
		out.writeUTF(ext);
		out.writeObject(dateModified);
		out.writeByteArray(data);
	}

	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		fileName = in.readUTF();
		ext = in.readUTF();
		dateModified = in.readObject();
		data = in.readByteArray();
	}

}
