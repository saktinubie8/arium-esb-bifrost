package com.bifrost.system.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.objenesis.ObjenesisSerializer;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class ObjenesisConfig {
	
	@Bean
	public ObjenesisSerializer objenesisSerializer() {
		return new ObjenesisSerializer(true);
	}

}
