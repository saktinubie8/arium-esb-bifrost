package com.bifrost.system.library;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class LibraryMap extends ConcurrentHashMap<String, Object> {

	private static final long serialVersionUID = 8377014083349735939L;

}
