package org.nucleus8583.core.xml;

import java.util.List;

public class MessageDefinition {

    private List<FieldDefinition> fields;

    public List<FieldDefinition> getFields() {
        return fields;
    }

    public void setFields(List<FieldDefinition> fields) {
        this.fields = fields;
    }
}
