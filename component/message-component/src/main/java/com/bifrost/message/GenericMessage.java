package com.bifrost.message;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class GenericMessage {

	protected static final String TRX_ID = "transactionId";
	protected static final String IS_REQUEST = "isRequest";
	protected static final String ORIGIN_NODE = "originNode";
	protected static final String ROUTE_COMPLETED = "routeCompleted";
	protected static final String ORIGIN_ASSOC_KEY = "originAssocKey";
	protected static final String MAPPED_RESPONSE_CODE = "map.responseCode";
	protected static final String MAPPED_RESPONSE_DESC = "map.responseDesc";
	protected static final String SYSTEM_RESPONSE_CODE = "sys.responseCode";
	protected static final String SYSTEM_RESPONSE_DESC = "sys.responseDesc";
	protected static final String ORIGIN_ENDPOINT_CODE = "originEndpointCode";
	protected static final String ORIGIN_INVOCATION_CODE = "originInvocationCode";
	
	protected Map<String, Object> root = new TreeMap<String, Object>();
	protected Map<String, Object> headers = new TreeMap<String, Object>();
	protected Map<String, Object> body = new TreeMap<String, Object>();
	
	public String getTransactionId() {
		return (String) this.root.get(TRX_ID);
	}
	
	public Boolean routeCompleted() {
		if(this.root.get(ROUTE_COMPLETED)==null)
			return false;
		return (Boolean) this.root.get(ROUTE_COMPLETED);
	}
	
	public void completeRoute() {
		this.root.put(ROUTE_COMPLETED, true);
		this.setResponse();
	}
	
	public Object getRoot(String rootName) {
		return this.root.get(rootName);
	}

	@SuppressWarnings("unchecked")
	public <T> T getRoot(String rootName, Class<T> cls) {
		return (T)this.root.get(rootName);
	}
	
	public Map<String, Object> getRoots() {
		return root;
	}
	
	public void putRoot(String rootName, Object value) {
		this.root.put(rootName, value);
	}

	public Object getHeader(String headerName) {
		return this.headers.get(headerName);
	}

	@SuppressWarnings("unchecked")
	public <T> T getHeader(String headerName, Class<T> cls) {
		return (T)this.headers.get(headerName);
	}

	public Map<String, Object> getHeaders() {
		return headers;
	}
	
	public void putHeader(String headerName, Object value) {
		this.headers.put(headerName, value);
	}
	
	public Object removeHeader(String headerName) {
		return headers.remove(headerName);
	}
	
	public void clearHeaders() {
		this.headers.clear();
	}
	
	public void putHeaders(Map<String, Object> m) {
		this.headers.putAll(m);
	}
	
	public void setHeaders(Map<String, Object> m) {
		this.headers = m;
	}
	
	public Object get(String key) {
		return this.body.get(key);
	}
	
	@SuppressWarnings("unchecked")
	public Object getRecursive(String... keys) {
		Object retVal = this.body;
		for(String key : keys)
			if(retVal==null)
				return retVal;
			else if(retVal instanceof Map) {
				Map<String, Object> map = (Map<String, Object>) retVal;
				retVal = map.get(key);
			}else
				return retVal;
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String key, Class<T> cls) {
		return (T)this.body.get(key);
	}

	public Map<String, Object> getBody() {
		return body;
	}
	
	public void setBody(Map<String, Object> body) {
		this.body = body;
	}
	
	public void put(String key, Object value) {
		this.body.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public void putRecursive(Object value, String... keys) {
		Map<String, Object> map = this.body;
		for(int i=0;i<keys.length;i++)
			if((map.get(keys[i])==null && i<keys.length-1) || (map.get(keys[i])!=null && !(map.get(keys[i]) instanceof Map)  && i<keys.length-1)) {
				map.put(keys[i], new TreeMap<String, Object>());
				map = (Map<String, Object>) map.get(keys[i]);
			}else if(i<keys.length-1) {
				map = (Map<String, Object>) map.get(keys[i]);
			}else {
				map.put(keys[i], value);
			}
	}
	
	public void putBody(Map<? extends String, ? extends Object> m) {
		this.body.putAll(m);
	}
	
	public Object remove(String key) {
		return this.body.remove(key);
	}
	
	public void clear() {
		this.body.clear();
	}

	public void setRequest() {
		this.root.put(IS_REQUEST, true);
	}

	public void setResponse() {
		this.root.put(IS_REQUEST, false);
	}
	
	public Boolean isRequest() {
		return (Boolean) this.root.get(IS_REQUEST);
	}
	
	public Boolean isResponse() {
		return !isRequest();
	}
	
	public String getOriginNode() {
		return (String) this.root.get(ORIGIN_NODE);
	}
	
	public String getOriginAssocKey() {
		return (String) this.root.get(ORIGIN_ASSOC_KEY);
	}
	
	public String getOriginEndpointCode() {
		return (String) this.root.get(ORIGIN_ENDPOINT_CODE);
	}
	
	public String getOriginInvocationCode() {
		return (String) this.root.get(ORIGIN_INVOCATION_CODE);
	}

	public void setSystemResponseCode(String responseCode) {
		this.root.put(SYSTEM_RESPONSE_CODE, responseCode);
	}

	public Boolean systemResponseCodeIsNull() {
		if(this.root.get(SYSTEM_RESPONSE_CODE)==null)
			return true;
		else
			return false;
	}

	public Boolean systemResponseCodeIsExists() {
		return systemResponseCodeIsNull();
	}
	
	public String getSystemResponseCode() {
		return (String) this.root.get(SYSTEM_RESPONSE_CODE);
	}
	
	public void setSystemResponseDesc(String responseDesc) {
		this.root.put(SYSTEM_RESPONSE_DESC, responseDesc);
	}
	
	public String getSystemResponseDesc() {
		return (String) this.root.get(SYSTEM_RESPONSE_DESC);
	}

	public void setMappedResponseCode(String responseCode) {
		this.root.put(MAPPED_RESPONSE_CODE, responseCode);
	}
	
	public String getMappedResponseCode() {
		return (String) this.root.get(MAPPED_RESPONSE_CODE);
	}

	public void setMappedResponseDesc(String responseDesc) {
		this.root.put(MAPPED_RESPONSE_DESC, responseDesc);
	}
	
	public String getMappedResponseDesc() {
		return (String) this.root.get(MAPPED_RESPONSE_DESC);
	}
	
	public void putAll(GenericMessage message) {
		this.headers.putAll(message.getHeaders());
		this.body.putAll(message.getBody());
	}
	
	public void setAll(GenericMessage message) {
		setHeaders(message.getHeaders());
		setBody(message.getBody());
	}
	
	public String getLogId() {
		return getOriginEndpointCode()+":"+getOriginAssocKey();
	}
}
