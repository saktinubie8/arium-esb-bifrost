package com.bifrost.message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bifrost.common.util.DateUtils;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class ExternalMessage extends GenericMessage implements KryoSerializable, DataSerializable {

	private static final Log LOGGER = LogFactory.getLog(ExternalMessage.class);
	private String responseCodeField = "responseCode";
	private String responseDescField = "responseDesc";
	private Boolean returnImmediately = false;
	private String assocKeyField = "assocKey";
	private Boolean responseCodeInHeaders;
	private Boolean responseDescInHeaders;
	private Boolean assocKeyInHeaders;
	private String invocationCode;
	private String endpointCode;
	private String node;

	public ExternalMessage() {
	}
	
	public ExternalMessage(String endpointCode, String invocationCode, InternalMessage internalMessage) {
		this.endpointCode = endpointCode;
		this.invocationCode = invocationCode;
		root.put(TRX_ID, internalMessage.getTransactionId());
		root.put(ORIGIN_ASSOC_KEY, internalMessage.getOriginAssocKey());
		root.put(MAPPED_RESPONSE_CODE, internalMessage.getMappedResponseCode());
		root.put(MAPPED_RESPONSE_DESC, internalMessage.getMappedResponseDesc());
		if(internalMessage.isRequest())
			setRequest();
		else
			setResponse();
	}

	public ExternalMessage(String endpointCode, String node, Long assocKey, String invocationCode) {
		this(endpointCode, node, String.valueOf(assocKey), invocationCode);
	}

	public ExternalMessage(String endpointCode, String node, String assocKey, String invocationCode) {
		root.put(TRX_ID, constructTrxId(endpointCode, node, assocKey));
		root.put(ORIGIN_INVOCATION_CODE, invocationCode);
		root.put(ORIGIN_ENDPOINT_CODE, endpointCode);
		root.put(ORIGIN_ASSOC_KEY, assocKey);
		root.put(ORIGIN_NODE, node);
		root.put(assocKeyField, assocKey);
		this.endpointCode = endpointCode;
		this.invocationCode = invocationCode;
		this.node = node;
	}
	
	public static String constructTrxId(String endpointCode, String node, String assocKey) {
		return endpointCode + ":" + node + ":" + assocKey + ":" + DateUtils.generateFormattedInDateTime();
	}

	public String getNode() {
		return this.node;
	}
	
	public ExternalMessage setNode(String node) {
		this.node = node;
		return this;
	}

	public String getEndpointCode() {
		return this.endpointCode;
	}

	public String getInvocationCode() {
		return this.invocationCode;
	}

	public ExternalMessage setInvocationCode(String invocationCode) {
		this.invocationCode = invocationCode;
		return this;
	}

	public String getAssocKeyField() {
		return this.assocKeyField;
	}

	public ExternalMessage setAssocKeyField(String assocKeyField) {
		this.assocKeyField = assocKeyField;
		return this;
	}
	
	public String getAssocKeyPos() {
		if(assocKeyInHeaders==null)
			return "root";
		else if(assocKeyInHeaders)
			return "header";
		else
			return "body"; 
	}
	
	public ExternalMessage setAssocKeyInHeaders(Boolean inHeaders) {
		this.assocKeyInHeaders = inHeaders;
		return this;
	}

	public String getResponseCodeField() {
		return this.responseCodeField;
	}
	
	public ExternalMessage setResponseCodeField(String responseCodeField) {
		if(responseCodeField!=null && !responseCodeField.isEmpty())
			this.responseCodeField = responseCodeField;
		return this;
	}
	
	public String getResponseCodePos() {
		if(responseCodeInHeaders==null)
			return "root";
		else if(responseCodeInHeaders)
			return "header";
		else
			return "body"; 
	}
	
	public ExternalMessage setResponseCodeInHeaders(Boolean inHeaders) {
		if(inHeaders!=null)
			this.responseCodeInHeaders = inHeaders;
		return this;
	}

	public String getResponseDescField() {
		return this.responseDescField;
	}

	public ExternalMessage setResponseDescField(String responseDescField) {
		if(responseDescField!=null && !responseDescField.isEmpty())
			this.responseDescField = responseDescField;
		return this;
	}
	
	public String getResponseDescPos() {
		if(responseDescInHeaders==null)
			return "root";
		else if(responseDescInHeaders)
			return "header";
		else
			return "body"; 
	}
	
	public ExternalMessage setResponseDescInHeaders(Boolean inHeaders) {
		if(inHeaders!=null)
			this.responseDescInHeaders = inHeaders;
		return this;
	}

	public String getAssocKey() {
		if(assocKeyInHeaders==null)
			return (String) root.get(assocKeyField);
		else if(assocKeyInHeaders)
			return (String) headers.get(assocKeyField);
		else 
			return (String) body.get(assocKeyField);
	}
	
	public String getResponseCode() {
		Object rc;
		if(responseCodeInHeaders==null)
			rc = root.get(responseCodeField);
		else if(responseCodeInHeaders)
			rc = headers.get(responseCodeField);
		else
			rc = body.get(responseCodeField);

		if(rc==null)
			return null;
		else if(rc instanceof String)
			return (String) rc;
		else
			return String.valueOf(rc);
	}
	
	public String getResponseDesc() {
		Object rc;
		if(responseDescInHeaders==null)
			rc = root.get(responseDescField);
		else if(responseDescInHeaders)
			rc = headers.get(responseDescField);
		else
			rc = body.get(responseDescField);

		if(rc==null)
			return null;
		else if(rc instanceof String)
			return (String) rc;
		else
			return String.valueOf(rc);
	}

	public ExternalMessage setAssocKey(String assocKey) {
		if(assocKeyInHeaders==null)
			root.put(assocKeyField, assocKey);
		else if(assocKeyInHeaders)
			headers.put(assocKeyField, assocKey);
		else
			body.put(assocKeyField, assocKey);
		return this;
	}

	public ExternalMessage setAssocKey(Long assocKey) {
		return setAssocKey(String.valueOf(assocKey));
	}

	public ExternalMessage setResponseCode(String responseCode) {
		if(responseCodeInHeaders==null)
			root.put(responseCodeField, responseCode);
		else if(responseCodeInHeaders)
			headers.put(responseCodeField, responseCode);
		else
			body.put(responseCodeField, responseCode);
		return this;
	}

	public ExternalMessage setResponseDesc(String responseDesc) {
		if(responseDescInHeaders==null)
			root.put(responseDescField, responseDesc);
		else if(responseDescInHeaders)
			headers.put(responseDescField, responseDesc);
		else
			body.put(responseDescField, responseDesc);
		return this;
	}
	
	public Boolean isReturnImmediately() {
		return this.returnImmediately;
	}
	
	public ExternalMessage setReturnImmediately() {
		this.returnImmediately = true;
		return this;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("ROOT    : "+root);
		result.append("\nHEADERS : "+headers);
		result.append("\nBODY    : "+body);
		return result.toString();
	}

	
	@SuppressWarnings("unchecked")
	public static void writeObject(Kryo kryo, Output output, Object obj) {
		if(obj!=null) {
			output.writeBoolean(true);
			if(obj instanceof List) {
				output.writeInt(0);
				List<Object> list = (List<Object>) obj;
				output.writeInt(list.size());
				for(Object value : list)
					writeObject(kryo, output, value);
			}else if(obj instanceof Map) {
				output.writeInt(1);
				Map<String, Object> map = (Map<String, Object>) obj;
				output.writeInt(map.size());
				for(Entry<String, Object> entry : map.entrySet()) {
					String key = entry.getKey();
					Object value = entry.getValue();
					output.writeString(key);
					writeObject(kryo, output, value);
				}
			}else if(obj instanceof String){
				output.writeInt(2);
				output.writeString((String)obj);
			}else if(obj instanceof Integer){
				output.writeInt(3);
				output.writeInt((int) obj);
			}else if(obj instanceof Long){
				output.writeInt(4);
				output.writeLong((Long) obj);
			}else if(obj instanceof Boolean){
				output.writeInt(5);
				output.writeBoolean((Boolean) obj);
			}else {
				output.writeInt(-1);
				output.writeString(obj.getClass().getName());
				kryo.writeObject(output, obj);;
			}
		}else
			output.writeBoolean(false);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object readObject(Kryo kryo, Input input) {
		if(input.readBoolean()) {
			int classId = input.readInt();
			if(classId==0) {
				int size = input.readInt();
				List<Object> list = new ArrayList<Object>(size);
				for(int i=0;i<size;i++)
					list.add(i, readObject(kryo, input));
				return list;
			}else if(classId==1) {
				int size = input.readInt();
				Map<String, Object> map = new HashMap<String, Object>(size);
				for(int i=0;i<size;i++) {
					String key = input.readString();
					Object value = readObject(kryo, input);
					map.put(key, value);
				}
				return map;
			}else if(classId==2)
				return input.readString();
			else if(classId==3)
				return input.readInt();
			else if(classId==4)
				return input.readLong();
			else if(classId==5)
				return input.readBoolean();
			else if(classId==-1) {
				String className = input.readString();
				Class clss = null;
				try {
					clss = Class.forName(className);
				} catch (ClassNotFoundException e) {
					LOGGER.error("[System] Failed when trying to get class '"+className+"' during deserialization process, cause by: "+e.getMessage());
					e.printStackTrace();
				}
				return kryo.readObject(input, clss);
			}
		}
		return null;
	}
	
	@Override
	public void write(Kryo kryo, Output output) {
		output.writeInt(0);
		output.writeString(responseCodeField);
		output.writeString(responseDescField);
		output.writeString(assocKeyField);
		output.writeBoolean(returnImmediately);
		if(responseCodeInHeaders!=null) {
			output.writeBoolean(true);
			output.writeBoolean(responseCodeInHeaders);
		}else
			output.writeBoolean(false);
		if(responseDescInHeaders!=null) {
			output.writeBoolean(true);
			output.writeBoolean(responseDescInHeaders);
		}else
			output.writeBoolean(false);
		if(assocKeyInHeaders!=null) {
			output.writeBoolean(true);
			output.writeBoolean(assocKeyInHeaders);
		}else
			output.writeBoolean(false);
		output.writeString(invocationCode);
		output.writeString(endpointCode);
		if(node!=null) {
			output.writeBoolean(true);
			output.writeString(node);
		}else
			output.writeBoolean(false);
		
		writeObject(kryo, output, root);
		writeObject(kryo, output, headers);
		writeObject(kryo, output, body);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void read(Kryo kryo, Input input) {
		responseCodeField = input.readString(); //1 length 32 empty
		responseDescField = input.readString(); //16  3
		assocKeyField = input.readString(); //28 0
		returnImmediately = input.readBoolean(); //36
		if(input.readBoolean()) //37
			responseCodeInHeaders = input.readBoolean();
		if(input.readBoolean()) //38
			responseDescInHeaders = input.readBoolean();
		if(input.readBoolean())
			assocKeyInHeaders = input.readBoolean();
		invocationCode = input.readString().trim(); //40
		endpointCode = input.readString().trim(); //79
		if(input.readBoolean())
			node = input.readString().trim();
		
		root = (Map<String, Object>) readObject(kryo, input);
		headers = (Map<String, Object>) readObject(kryo, input);
		body = (Map<String, Object>) readObject(kryo, input);
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(responseCodeField);
		out.writeUTF(responseDescField);
		out.writeUTF(assocKeyField);
		out.writeBoolean(returnImmediately);
		if(responseCodeInHeaders!=null) {
			out.writeBoolean(true);
			out.writeBoolean(responseCodeInHeaders);
		}else
			out.writeBoolean(false);
		if(responseDescInHeaders!=null) {
			out.writeBoolean(true);
			out.writeBoolean(responseDescInHeaders);
		}else
			out.writeBoolean(false);
		if(assocKeyInHeaders!=null) {
			out.writeBoolean(true);
			out.writeBoolean(assocKeyInHeaders);
		}else
			out.writeBoolean(false);
		out.writeUTF(invocationCode);
		out.writeUTF(endpointCode);
		out.writeUTF(node);
		
		out.writeInt(root.size());
		for(Entry<String, Object> entry : root.entrySet()) {
			out.writeUTF(entry.getKey());
			out.writeObject(entry.getValue());
		}
		out.writeInt(headers.size());
		for(Entry<String, Object> entry : headers.entrySet()) {
			out.writeUTF(entry.getKey());
			out.writeObject(entry.getValue());
		}
		out.writeInt(body.size());
		for(Entry<String, Object> entry : body.entrySet()) {
			out.writeUTF(entry.getKey());
			out.writeObject(entry.getValue());
		}
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		responseCodeField = in.readUTF();
		responseDescField = in.readUTF();
		assocKeyField = in.readUTF();
		returnImmediately = in.readBoolean();
		if(in.readBoolean())
			responseCodeInHeaders = in.readBoolean();
		if(in.readBoolean())
			responseDescInHeaders = in.readBoolean();
		if(in.readBoolean())
			assocKeyInHeaders = in.readBoolean();
		invocationCode = in.readUTF();
		endpointCode = in.readUTF();
		node = in.readUTF();
		
		int size = in.readInt();
		root.clear();
		for(int i=0;i<size;i++) {
			String key = in.readUTF();
			Object obj = in.readObject();
			root.put(key, obj);
		}

		size = in.readInt();
		headers.clear();
		for(int i=0;i<size;i++) {
			String key = in.readUTF();
			Object obj = in.readObject();
			headers.put(key, obj);
		}

		size = in.readInt();
		body.clear();
		for(int i=0;i<size;i++) {
			String key = in.readUTF();
			Object obj = in.readObject();
			body.put(key, obj);
		}
	}
}
