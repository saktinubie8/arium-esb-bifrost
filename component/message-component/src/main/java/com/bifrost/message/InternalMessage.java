package com.bifrost.message;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InternalMessage extends GenericMessage {

	private static final String ROUTE_CODE = "routeCode";
	
	private List<Track> tracks = new ArrayList<Track>();

	public InternalMessage(ExternalMessage externalMessage) {
		this.root = externalMessage.getRoots();
	}
	
	public InternalMessage addTrack(String endpointCode, String node, String invocationCode, Long invocationId, String responseCode) {
		tracks.add(new Track(endpointCode, node, invocationCode, invocationId, responseCode));
		return this;
	}
	
	public Boolean trackIsEmpty() {
		if(tracks.size()==0)
			return true;
		else
			return false;
	}
	
	public Track getFirstTrack() {
		if(tracks.size()==0)
			return null;
		return tracks.get(0);
	}
	
	public Track getLastTrack() {
		if(tracks.size()==0)
			return null;
		return tracks.get(tracks.size()-1);
	}

	public String getLastEndpointCode() {
		Track lastTrack = getLastTrack();
		if(lastTrack==null)
			return null;
		else
			return lastTrack.getEndpointCode()==null?"":lastTrack.getEndpointCode();
	}
	
	public String getLastResponseCode() {
		Track lastTrack = getLastTrack();
		if(lastTrack==null)
			return null;
		else
			return lastTrack.getResponseCode()==null?"":lastTrack.getResponseCode();
	}

	public InternalMessage setLastTrackResponseCode(String responseCode) {
		Track lastTrack = getLastTrack();
		if(lastTrack==null)
			throw new NullPointerException("Track is empty");
		else
			lastTrack.setResponseCode(responseCode);
		return this;		
	}
	
	public InternalMessage setRouteCode(String routeCode) {
		putRoot(ROUTE_CODE, routeCode);
		return this;
	}
	
	public String getRouteCode() {
		return (String) getRoot(ROUTE_CODE);
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("ROOT: "+root);
		result.append("\nHEADERS: "+getHeaders());
		result.append("\nBODY: "+getBody());
		result.append("\nTRACKS: "+tracks);
		return result.toString();
	}
}
