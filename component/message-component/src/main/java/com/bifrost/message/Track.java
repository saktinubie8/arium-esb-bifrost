package com.bifrost.message;

import java.io.Serializable;

/**
 * @author rosaekapratama@gmail.com
 *
 */

public class Track implements Serializable {

	private static final long serialVersionUID = 7946507650508825863L;
	private String node;
	private Long invocationId;
	private String endpointCode;
	private String responseCode;
	private String invocationCode;

	public Track(String endpointCode, String node, String invocationCode, Long invocationId, String responseCode) {
		super();
		this.node = node;
		this.invocationId = invocationId;
		this.endpointCode = endpointCode;
		this.responseCode = responseCode;
		this.invocationCode = invocationCode;
	}

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getInvocationCode() {
		return invocationCode;
	}

	public void setInvocationCode(String invocationCode) {
		this.invocationCode = invocationCode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	

	public Long getInvocationId() {
		return invocationId;
	}

	public void setInvocationId(Long invocationId) {
		this.invocationId = invocationId;
	}

	@Override
	public String toString() {
		return endpointCode+":"+node+":"+invocationId+":"+invocationCode+":"+responseCode;
	}
}
