package com.bifrost.logging.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EndpointLogId implements Serializable {

	private static final long serialVersionUID = -6601723364056861799L;

	@Column(name = "date_time", nullable = false)
	private Date dateTime;

	@Column(name = "endpoint_code", nullable = false, length = 20)
	private String endpointCode;

	@Column(name = "node", nullable = false, length = 40)
	private String node;

	@Column(name = "status", nullable = false, length = 20)
	private String status;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
