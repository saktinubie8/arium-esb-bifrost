package com.bifrost.logging.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint_status", uniqueConstraints = @UniqueConstraint(columnNames = {"endpoint_code", "node"}))
public class EndpointStatus implements AbstractModel {
	public static final String ONLINE = "ONLINE";
	public static final String OFFLINE = "OFFLINE";
	public static final String CONNECTED = "CONNECTED";
	public static final String DISCONNECTED = "DISCONNECTED";
	public static final String ERROR = "ERROR";

	@EmbeddedId
	private EndpointStatusId endpointStatusId;
	
	@Column(name = "status", nullable = false, length = 20)
	private String status;

	public EndpointStatus() {
	}

	public EndpointStatus(String endpointCode, String node, String status) {
		EndpointStatusId id = new EndpointStatusId();
		id.setEndpointCode(endpointCode);
		id.setNode(node);
		this.endpointStatusId = id;
		this.status = status;
	}
	
	public EndpointStatusId getEndpointStatusId() {
		return endpointStatusId;
	}

	public void setEndpointStatusId(EndpointStatusId endpointStatusId) {
		this.endpointStatusId = endpointStatusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static String constructKey(String endpointCode, String node) {
		return endpointCode.concat(":").concat(node);
	}
	
	public static String[] parseKey(String key) {
		return key.split(":");
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(endpointStatusId.getEndpointCode());
		out.writeUTF(endpointStatusId.getNode());
		out.writeUTF(status);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		endpointStatusId = new EndpointStatusId();
		endpointStatusId.setEndpointCode(in.readUTF());
		endpointStatusId.setNode(in.readUTF());
		status = in.readUTF();
	}

	@Override
	public Object getPk() {
		return endpointStatusId;
	}

}
