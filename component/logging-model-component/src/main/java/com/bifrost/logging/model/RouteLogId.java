package com.bifrost.logging.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Embeddable
public class RouteLogId implements Serializable {

	private static final long serialVersionUID = -2512632088873667362L;

	@Column(name = "transaction_id", nullable = true, length = 255)
	private String transactionId;

	@Column(name = "date_time", nullable = false)
	private Date dateTime;

	@Column(name = "endpoint_code", nullable = false, length = 20)
	private String endpointCode;

	@Column(name = "invocation_code", nullable = true, length = 255)
	private String invocationCode;

	@Column(name = "is_request", nullable = false)
	private Boolean isRequest;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getInvocationCode() {
		return invocationCode;
	}

	public void setInvocationCode(String invocationCode) {
		this.invocationCode = invocationCode;
	}

	public Boolean getIsRequest() {
		return isRequest;
	}

	public void setIsRequest(Boolean isRequest) {
		this.isRequest = isRequest;
	}

}
