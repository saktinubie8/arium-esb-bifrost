package com.bifrost.logging.model;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.common.util.HazelcastUtils;
import com.bifrost.logging.model.dto.MessageLogDto;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "message_log")
public class MessageLog implements AbstractModel, KryoSerializable {

	@Id
	@Column(name = "transaction_id", nullable = false, length = 255)
	private String transactionId;

	@Column(name = "received", nullable = false)
	private Date received;

	@Column(name = "process_time", nullable = false)
	private Long processTime;

	@Column(name = "origin_endpoint", nullable = false, length = 20)
	private String originEndpoint;

	@Column(name = "invocation_code", nullable = false, length = 255)
	private String invocationCode;

	@Column(name = "route_code", nullable = false, length = 40)
	private String routeCode;

	@Column(name = "response_code", nullable = false, length = 20)
	private String responseCode;

	@Column(name = "response_desc", nullable = false, length = 255)
	private String responseDesc;

	@Column(name = "internal_message", nullable = false, columnDefinition = "TEXT")
	private String internalMessage;

	public MessageLog() {
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getReceived() {
		return received;
	}

	public void setReceived(Date received) {
		this.received = received;
	}

	public Long getProcessTime() {
		return processTime;
	}

	public void setProcessTime(Long processTime) {
		this.processTime = processTime;
	}

	public String getInvocationCode() {
		return invocationCode;
	}

	public void setInvocationCode(String invocationCode) {
		this.invocationCode = invocationCode;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getOriginEndpoint() {
		return originEndpoint;
	}

	public void setOriginEndpoint(String originEndpoint) {
		this.originEndpoint = originEndpoint;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public String getInternalMessage() {
		return internalMessage;
	}

	public void setInternalMessage(String internalMessage) {
		this.internalMessage = internalMessage;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(transactionId);
		out.writeLong(received.getTime());
		out.writeUTF(originEndpoint);
		out.writeUTF(invocationCode);
		HazelcastUtils.writeLong(processTime, out);
		HazelcastUtils.writeUTF(routeCode, out);
		HazelcastUtils.writeUTF(responseCode, out);
		HazelcastUtils.writeUTF(responseDesc, out);
		HazelcastUtils.writeUTF(internalMessage, out);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		transactionId = in.readUTF();
		received = new Date(in.readLong());
		originEndpoint = in.readUTF();
		invocationCode = in.readUTF();
		processTime = HazelcastUtils.readLong(in);
		routeCode = HazelcastUtils.readUTF(in);
		responseCode = HazelcastUtils.readUTF(in);
		responseDesc = HazelcastUtils.readUTF(in);
		internalMessage = HazelcastUtils.readUTF(in);
	}

	@Override
	public void write(Kryo kryo, Output output) {
		output.writeInt(2);
		output.writeString(transactionId);
		output.writeLong(received.getTime());
		output.writeLong(processTime);
		output.writeString(originEndpoint);
		output.writeString(invocationCode);
		output.writeString(routeCode);
		output.writeString(responseCode);
		output.writeString(responseDesc);
		output.writeString(internalMessage);
	}

	@Override
	public void read(Kryo kryo, Input input) {
		transactionId = input.readString();
		received = new Date(input.readLong());
		processTime = input.readLong();
		originEndpoint = input.readString();
		invocationCode = input.readString();
		routeCode = input.readString();
		responseCode = input.readString();
		responseDesc = input.readString();
		internalMessage = input.readString();
	}

	@Override
	public Object getPk() {
		return transactionId;
	}

    /*****************************
     * - DTO -
     ****************************/
	
	public MessageLog(MessageLogDto messageLog) {
        setReceived(messageLog.getReceived());
        setProcessTime(messageLog.getProcessTime());
        setOriginEndpoint(messageLog.getOriginEndpoint());
        setInvocationCode(messageLog.getInvocationCode());
        setRouteCode(messageLog.getRouteCode());
        setResponseCode(messageLog.getResponseCode());
      
        setResponseDesc(messageLog.getResponseDesc());
        setInternalMessage(messageLog.getInternalMessage());
   }

    public MessageLogDto parseDto() {
    	MessageLogDto messageLogDto = new MessageLogDto();
    	messageLogDto.setTransactionId(this.transactionId);
    	messageLogDto.setReceived(this.received);
    	messageLogDto.setProcessTime(this.processTime);
    	messageLogDto.setOriginEndpoint(this.originEndpoint);
    	messageLogDto.setInvocationCode(this.invocationCode);
    	messageLogDto.setRouteCode(this.routeCode);
    	messageLogDto.setResponseCode(this.responseCode);
    	messageLogDto.setResponseDesc(this.responseDesc);
    	messageLogDto.setInternalMessage(this.internalMessage);
    	messageLogDto.setReceivedString();
        return messageLogDto;
    }

	@Override
	public String toString() {
		return "MessageLog [transactionId=" + transactionId + ", received=" + received + ", processTime=" + processTime
				+ ", originEndpoint=" + originEndpoint + ", invocationCode=" + invocationCode + ", routeCode="
				+ routeCode + ", responseCode=" + responseCode + ", responseDesc=" + responseDesc +"]";
	}
}
