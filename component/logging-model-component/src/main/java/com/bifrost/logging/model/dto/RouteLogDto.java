package com.bifrost.logging.model.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class RouteLogDto extends GenericDto {

    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final long serialVersionUID = 403221500388340375L;
    private Long id;
    private String transactionId;
    private String endpointCode;
    private Date dateTime;
    private String invocationCode;
    private Boolean isRequest;
    private String raw;
    private String formattedRaw;
    private List<String> button;
    private String dateTimeString;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public List<String> getButton() {
        return button;
    }

    public void setButton(List<String> button) {
        this.button = button;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getInvocationCode() {
        return invocationCode;
    }

    public void setInvocationCode(String invocationCode) {
        this.invocationCode = invocationCode;
    }

    public Boolean getRequest() {
        return isRequest;
    }

    public void setRequest(Boolean request) {
        isRequest = request;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getFormattedRaw() {
        return formattedRaw;
    }

    public void setFormattedRaw(String formattedRaw) {
        this.formattedRaw = formattedRaw;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public void setDateTimeString() {
        this.dateTimeString = formatDateTime.format(this.dateTime);
    }
}