package com.bifrost.logging.model.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EndpointLogDto extends GenericDto {

    /**
     *
     */
    private static final long serialVersionUID = 403221500388340375L;
    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Long id;
    private String endpointCode;
    private Date dateTime;
    private String node;
    private String status;
    private String description;
    private List<String> button;
    private String dateTimeString;

    public List<String> getButton() {
        return button;
    }

    public void setButton(List<String> button) {
        this.button = button;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime2) {
        this.dateTime = dateTime2;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public void setDateTimeString() {
        this.dateTimeString = formatDateTime.format(this.dateTime);
    }
}