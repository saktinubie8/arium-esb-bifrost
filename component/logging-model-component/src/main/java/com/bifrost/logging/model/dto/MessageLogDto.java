package com.bifrost.logging.model.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MessageLogDto extends GenericDto {

    /**
     *
     */
    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final long serialVersionUID = 3395329079365812286L;
    private List<String> button;
    private String transactionId;
    private Date received;
    private Long processTime;
    private String originEndpoint;
    private String invocationCode;
    private String routeCode;
    private String responseCode;
    private String responseDesc;
    private String internalMessage;
    private String receivedString;

    public List<String> getButton() {
        return button;
    }

    public void setButton(List<String> button) {
        this.button = button;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public Long getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Long processTime) {
        this.processTime = processTime;
    }

    public String getOriginEndpoint() {
        return originEndpoint;
    }

    public void setOriginEndpoint(String originEndpoint) {
        this.originEndpoint = originEndpoint;
    }

    public String getInvocationCode() {
        return invocationCode;
    }

    public void setInvocationCode(String invocationCode) {
        this.invocationCode = invocationCode;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String getInternalMessage() {
        return internalMessage;
    }

    public void setInternalMessage(String internalMessage) {
        this.internalMessage = internalMessage;
    }

    public String getReceivedString() {
        return receivedString;
    }

    public void setReceivedString() {
        this.receivedString = formatDateTime.format(this.received);
    }
}