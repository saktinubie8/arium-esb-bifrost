package com.bifrost.logging.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * @author rosaekapratama@gmail.com
 *
 */

@Embeddable()
public class EndpointStatusId implements Serializable {

	private static final long serialVersionUID = -6258745055096568114L;
	
	@Column(name = "node", nullable = false, length = 20)
	private String node;

	@Column(name = "endpoint_code", nullable = false, length = 20)
	private String endpointCode;

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	@Override
	public String toString() {
		return "EndpointStatusId [endpointCode=" + endpointCode + ", node=" + node + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endpointCode == null) ? 0 : endpointCode.hashCode());
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EndpointStatusId other = (EndpointStatusId) obj;
		if (endpointCode == null) {
			if (other.endpointCode != null)
				return false;
		} else if (!endpointCode.equals(other.endpointCode))
			return false;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		return true;
	}
}
