package com.bifrost.logging.model;

import java.io.IOException;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.logging.model.dto.EndpointLogDto;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 */
@Entity
@Table(name = "endpoint_log")
public class EndpointLog implements AbstractModel, KryoSerializable {
	
	@EmbeddedId
    private EndpointLogId endpointLogId;

    public EndpointLog() {
        endpointLogId = new EndpointLogId();
    }

    public EndpointLog(Date dateTime, String endpointCode, String node, String status, String description) {
        endpointLogId = new EndpointLogId();
        endpointLogId.setDateTime(dateTime);
        endpointLogId.setEndpointCode(endpointCode);
        endpointLogId.setNode(node);
        endpointLogId.setStatus(status);
        endpointLogId.setDescription(description);
    }

    public Date getDateTime() {
        return endpointLogId.getDateTime();
    }

    public void setDateTime(Date dateTime) {
        this.endpointLogId.setDateTime(dateTime);
    }

    public String getEndpointCode() {
        return endpointLogId.getEndpointCode();
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointLogId.setEndpointCode(endpointCode);
    }

    public String getNode() {
        return endpointLogId.getNode();
    }

    public void setNode(String node) {
        this.endpointLogId.setNode(node);
    }

    public String getStatus() {
        return endpointLogId.getStatus();
    }

    public void setStatus(String status) {
        this.endpointLogId.setStatus(status);
    }

    public String getDescription() {
        return endpointLogId.getDescription();
    }

    public void setDescription(String description) {
        this.endpointLogId.setDescription(description);
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeLong(endpointLogId.getDateTime().getTime());
        out.writeUTF(endpointLogId.getEndpointCode());
        out.writeUTF(endpointLogId.getEndpointCode());
        out.writeUTF(endpointLogId.getNode());
        out.writeUTF(endpointLogId.getDescription());
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        if (endpointLogId == null)
            endpointLogId = new EndpointLogId();
        setDateTime(new Date(in.readLong()));
        setEndpointCode(in.readUTF());
        setNode(in.readUTF());
        setStatus(in.readUTF());
        setDescription(in.readUTF());
    }

    @Override
    public void write(Kryo kryo, Output output) {
        output.writeInt(1);
        output.writeLong(endpointLogId.getDateTime().getTime());
        output.writeString(endpointLogId.getEndpointCode());
        output.writeString(endpointLogId.getEndpointCode());
        output.writeString(endpointLogId.getNode());
        output.writeString(endpointLogId.getDescription());
    }

    @Override
    public void read(Kryo kryo, Input input) {
        if (endpointLogId == null)
            endpointLogId = new EndpointLogId();
        setDateTime(new Date(input.readLong()));
        setEndpointCode(input.readString());
        setNode(input.readString());
        setStatus(input.readString());
        setDescription(input.readString());
    }

    @Override
    public Object getPk() {
        return endpointLogId;
    }

	@Override
	public String toString() {
		return "EndpointLog [endpointCode=" + getEndpointCode() + ", dateTime=" + getDateTime() + ", status=" + getStatus() + ", node=" + getNode() + ", description=" + getDescription()+"]";
	}

    public EndpointLogDto parseDto() {
        EndpointLogDto endpointLogDto = new EndpointLogDto();
        endpointLogDto.setDateTime(this.getDateTime());
        endpointLogDto.setEndpointCode(this.getEndpointCode());
        endpointLogDto.setNode(this.getNode());
        endpointLogDto.setStatus(this.getStatus());
        endpointLogDto.setDescription(this.getDescription());
        endpointLogDto.setDateTimeString();
        return endpointLogDto;
    }

}