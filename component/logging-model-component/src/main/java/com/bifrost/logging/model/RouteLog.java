package com.bifrost.logging.model;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.logging.model.dto.RouteLogDto;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "route_log")
public class RouteLog implements AbstractModel, KryoSerializable {

	@EmbeddedId
	private RouteLogId routeLogId;
	
	@Column(name = "raw", nullable = false, columnDefinition = "TEXT")
	private String raw;

	@Column(name = "formatted_raw", nullable = true, columnDefinition = "TEXT")
	private String formattedRaw;

	public RouteLog() {
		routeLogId = new RouteLogId();
	}
	
	public String getTransactionId() {
		return routeLogId.getTransactionId();
	}

	public void setTransactionId(String transactionId) {
		this.routeLogId.setTransactionId(transactionId);
	}

	public Date getDateTime() {
		return routeLogId.getDateTime();
	}

	public void setDateTime(Date dateTime) {
		this.routeLogId.setDateTime(dateTime);
	}

	public String getEndpointCode() {
		return routeLogId.getEndpointCode();
	}

	public void setEndpointCode(String endpointCode) {
		this.routeLogId.setEndpointCode(endpointCode);
	}

	public String getInvocationCode() {
		return routeLogId.getInvocationCode();
	}

	public void setInvocationCode(String invocationCode) {
		this.routeLogId.setInvocationCode(invocationCode);
	}

	public Boolean isRequest() {
		return routeLogId.getIsRequest();
	}

	public Boolean isResponse() {
		return !routeLogId.getIsRequest();
	}

	public void setRequest(Boolean isRequest) {
		this.routeLogId.setIsRequest(isRequest);
	}
	
	public void setRequest() {
		this.routeLogId.setIsRequest(true);
	}
	
	public void setResponse() {
		this.routeLogId.setIsRequest(false);
	}

	public String getRaw() {
		return raw;
	}

	public void setRaw(String raw) {
		this.raw = raw;
	}

	public String getFormattedRaw() {
		return formattedRaw;
	}

	public void setFormattedRaw(String formattedRaw) {
		this.formattedRaw = formattedRaw;
	}

	public void setFormattedRaw(Object formattedRaw) {
		this.formattedRaw = formattedRaw.toString();
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(routeLogId.getTransactionId());
		out.writeLong(routeLogId.getDateTime().getTime());
		out.writeUTF(routeLogId.getEndpointCode());
		out.writeUTF(routeLogId.getInvocationCode());
		out.writeBoolean(routeLogId.getIsRequest());
		out.writeUTF(raw);
		out.writeUTF(formattedRaw);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		if(routeLogId==null)
			routeLogId = new RouteLogId();
		setTransactionId(in.readUTF());
		setDateTime(new Date(in.readLong()));
		setEndpointCode(in.readUTF());
		setInvocationCode(in.readUTF());
		setRequest(in.readBoolean());
		setRaw(in.readUTF());
		setFormattedRaw(in.readUTF());
	}

	@Override
	public void write(Kryo kryo, Output output) {
		output.writeInt(3);
		output.writeString(routeLogId.getTransactionId());
		output.writeLong(routeLogId.getDateTime().getTime());
		output.writeString(routeLogId.getEndpointCode());
		output.writeString(routeLogId.getInvocationCode());
		output.writeBoolean(routeLogId.getIsRequest());
		output.writeString(raw);
		output.writeString(formattedRaw);
	}

	@Override
	public void read(Kryo kryo, Input input) {
		if(routeLogId==null)
			routeLogId = new RouteLogId();
		setTransactionId(input.readString());
		setDateTime(new Date(input.readLong()));
		setEndpointCode(input.readString());
		setInvocationCode(input.readString());
		setRequest(input.readBoolean());
		setRaw(input.readString());
		setFormattedRaw(input.readString());
	}

	@Override
	public Object getPk() {
		return routeLogId;
	}

	public RouteLogDto parseDto() {
		RouteLogDto routeLogDto = new RouteLogDto();
		routeLogDto.setDateTime(this.getDateTime());
		routeLogDto.setEndpointCode(this.getEndpointCode());
		routeLogDto.setTransactionId(this.getTransactionId());
		routeLogDto.setInvocationCode(this.getInvocationCode());
		routeLogDto.setRequest(this.isRequest());
		routeLogDto.setRaw(this.getRaw());
		routeLogDto.setFormattedRaw(this.getFormattedRaw());
		routeLogDto.setDateTimeString();
		return routeLogDto;
	}
	
	@Override
	public String toString() {
		return "RouteLog [transactionId=" + getTransactionId() +", endpointCode=" + getEndpointCode() + 
				", invocationCode=" + getInvocationCode() + ", isRequest=" + isRequest() +"]";
	}
}
