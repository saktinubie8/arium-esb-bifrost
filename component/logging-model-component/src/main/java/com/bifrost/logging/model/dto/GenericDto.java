package com.bifrost.logging.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class GenericDto implements Serializable {

    private static final long serialVersionUID = 4010133534085767972L;
    public static final String SYSTEM_DATE_PATTERN = "dd-MM-yyyy HH:mm:ss";
    public static final String SYSTEM_DATE_PATTERN2 = "dd/MM/yyyy";
    public static final String SYSTEM_DATE_PATTERN3 = "yyyy-MM-dd HH:mm:ss";
    protected Boolean isActive = true;
    protected Boolean isUsed;
    protected String strIsActive;
    protected String createdBy;
    protected String createdDate;
    protected String modifiedBy;
    protected String modifiedDate;
    protected String lockedStatus;
    protected String lockedBy;
    protected String lastApprovedBy;
    protected String lastApprovedDate;
    protected int number;

    @JsonIgnore
    public Date getCreatedDateParse() {
        try {
            if (createdDate != null)
                return new SimpleDateFormat(SYSTEM_DATE_PATTERN).parse(this.createdDate);
        } catch (ParseException e) {
            return new Date();
        }
        return new Date();
    }

    public void setCreatedDate(Date date) {
        if (date == null)
            this.createdDate = "";
        else
            this.createdDate = new SimpleDateFormat(SYSTEM_DATE_PATTERN).format(date);
    }

    @JsonIgnore
    public Date getModifiedDateParse() {
        try {
            if (modifiedDate != null)
                return new SimpleDateFormat(SYSTEM_DATE_PATTERN).parse(this.modifiedDate);
        } catch (ParseException e) {
            return new Date();
        }
        return new Date();
    }

    public void setModifiedDate(Date date) {
        if (date == null)
            this.modifiedDate = "";
        else
            this.modifiedDate = new SimpleDateFormat(SYSTEM_DATE_PATTERN).format(date);
    }

    @JsonIgnore
    public Date getLastApprovedDate() {
        try {
            if (lastApprovedDate != null)
                return new SimpleDateFormat(SYSTEM_DATE_PATTERN).parse(this.lastApprovedDate);
        } catch (ParseException e) {
            return new Date();
        }
        return new Date();
    }

    public void setLastApprovedDate(Date date) {
        if (date == null)
            this.lastApprovedDate = "";
        else
            this.lastApprovedDate = new SimpleDateFormat(SYSTEM_DATE_PATTERN).format(date);
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
        setStrIsActive();
    }

    public void setStrIsActive() {
        this.strIsActive = (this.isActive) ? "Active" : "Inactive";
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getLockedStatus() {
        return lockedStatus;
    }

    public void setLockedStatus(String lockedStatus) {
        this.lockedStatus = lockedStatus;
    }

    public String getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy) {
        this.lockedBy = lockedBy;
    }

    public String getLastApprovedBy() {
        return lastApprovedBy;
    }

    public void setLastApprovedBy(String lastApprovedBy) {
        this.lastApprovedBy = lastApprovedBy;
    }

    public void setLastApprovedDate(String lastApprovedDate) {
        this.lastApprovedDate = lastApprovedDate;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}