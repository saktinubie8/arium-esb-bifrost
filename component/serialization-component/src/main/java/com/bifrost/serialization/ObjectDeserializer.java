package com.bifrost.serialization;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;

import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.model.MessageLog;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.message.ExternalMessage;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.unsafe.UnsafeInput;

public class ObjectDeserializer implements Deserializer<Object> {

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {
		
	}

	@Override
	public Object deserialize(String topic, byte[] data) {
		Kryo kryo = KryoService.pool.obtain();
		UnsafeInput input = new UnsafeInput(data);
		int type = input.readInt();
		Object retVal = null;
		if(type==0)
			retVal = kryo.readObject(input, ExternalMessage.class);
		else if(type==1)
			retVal = kryo.readObject(input, EndpointLog.class);
		else if(type==2)
			retVal = kryo.readObject(input, MessageLog.class);
		else if(type==3)
			retVal = kryo.readObject(input, RouteLog.class);
		input.close();
		KryoService.pool.free(kryo);
		return retVal;
	}

	@Override
	public void close() {
		
	}

}
