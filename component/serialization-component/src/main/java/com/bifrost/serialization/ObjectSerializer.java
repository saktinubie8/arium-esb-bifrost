package com.bifrost.serialization;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.unsafe.UnsafeOutput;

public class ObjectSerializer implements Serializer<Object> {

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {
		
	}

	@Override
	public byte[] serialize(String topic, Object data) {
		Kryo kryo = KryoService.pool.obtain();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		UnsafeOutput output = new UnsafeOutput(baos, 1024);
		output.setVariableLengthEncoding(false);
		kryo.writeObject(output, data);
		output.flush();
		byte[] retVal = baos.toByteArray();
		output.close();
		try {
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			KryoService.pool.free(kryo);	
		}
		return retVal;
	}

	@Override
	public void close() {
		
	}
}
