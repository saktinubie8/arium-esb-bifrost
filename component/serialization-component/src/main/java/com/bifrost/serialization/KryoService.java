package com.bifrost.serialization;

import java.lang.reflect.InvocationHandler;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.util.Pool;

import de.javakaffee.kryoserializers.ArraysAsListSerializer;
import de.javakaffee.kryoserializers.CollectionsEmptyListSerializer;
import de.javakaffee.kryoserializers.CollectionsEmptyMapSerializer;
import de.javakaffee.kryoserializers.CollectionsEmptySetSerializer;
import de.javakaffee.kryoserializers.CollectionsSingletonListSerializer;
import de.javakaffee.kryoserializers.CollectionsSingletonMapSerializer;
import de.javakaffee.kryoserializers.CollectionsSingletonSetSerializer;
import de.javakaffee.kryoserializers.GregorianCalendarSerializer;
import de.javakaffee.kryoserializers.JdkProxySerializer;
import de.javakaffee.kryoserializers.SynchronizedCollectionsSerializer;
import de.javakaffee.kryoserializers.UnmodifiableCollectionsSerializer;

public class KryoService {

	public static Pool<Kryo> pool;
	public static final int LIST_CLASS_ID = 1;
	public static final int MAP_CLASS_ID = 2;
	public static int poolSize;
	
	public static void init(int size) {
		poolSize = size;
		pool = new Pool<Kryo>(true, false, poolSize) {

			@Override
			protected Kryo create() {
				Kryo kryo = new Kryo();
				kryo.setRegistrationRequired(false);
				kryo.register( Arrays.asList( "" ).getClass(), new ArraysAsListSerializer());
				kryo.register( Collections.EMPTY_LIST.getClass(), new CollectionsEmptyListSerializer());
				kryo.register( Collections.EMPTY_MAP.getClass(), new CollectionsEmptyMapSerializer());
				kryo.register( Collections.EMPTY_SET.getClass(), new CollectionsEmptySetSerializer());
				kryo.register( Collections.singletonList( "" ).getClass(), new CollectionsSingletonListSerializer());
				kryo.register( Collections.singleton( "" ).getClass(), new CollectionsSingletonSetSerializer());
				kryo.register( Collections.singletonMap( "", "" ).getClass(), new CollectionsSingletonMapSerializer());
				kryo.register( GregorianCalendar.class, new GregorianCalendarSerializer());
				kryo.register( InvocationHandler.class, new JdkProxySerializer());
				UnmodifiableCollectionsSerializer.registerSerializers( kryo );
				SynchronizedCollectionsSerializer.registerSerializers( kryo );
				return kryo;
			}
		};
	}
	
	public static Boolean poolIsNotExist() {
		return poolIsExist();
	}
	
	public static Boolean poolIsExist() {
		if(pool==null)
			return false;
		else
			return true;
	}
	
	public static int getPoolSize() {
		return poolSize;
	}
}
