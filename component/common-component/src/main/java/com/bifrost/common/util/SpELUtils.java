package com.bifrost.common.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.http.HttpHeaders;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.TopicPartitionOffset;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class SpELUtils {

	private static final String TOPIC_PARTITION_OFFSET_CLASS_PACKAGE = TopicPartitionOffset.class.getPackage().getName() + ".TopicPartitionOffset";
	private static final String KAFKA_HEADERS_CLASS_PACKAGE = KafkaHeaders.class.getPackage().getName()+ ".KafkaHeaders";
	private static final String CONSUMER_CONFIG_CLASS_PACKAGE = ConsumerConfig.class.getPackage().getName()+ ".ConsumerConfig";
	private static final String PRODUCER_CONFIG_CLASS_PACKAGE = ProducerConfig.class.getPackage().getName()+ ".ProducerConfig";
	private static final String HTTP_HEADERS_CLASS_PACKAGE = HttpHeaders.class.getPackage().getName()+ ".HttpHeaders";
	private static final Map<String, String> detectedClass = new HashMap<String, String>(){
		private static final long serialVersionUID = -7588882710232936255L;
	{
		put("TopicPartitionOffset", TOPIC_PARTITION_OFFSET_CLASS_PACKAGE);
		put("KafkaHeaders", KAFKA_HEADERS_CLASS_PACKAGE);
		put("ConsumerConfig", CONSUMER_CONFIG_CLASS_PACKAGE);
		put("ProducerConfig", PRODUCER_CONFIG_CLASS_PACKAGE);
		put("HttpHeaders", HTTP_HEADERS_CLASS_PACKAGE);
	}};

	/**
	 * @param className name of a class
	 * @return class name full with package path
	 */
	public static String addPackagerOnClass(String className) {
		return detectedClass.get(className);
	}

	/**
	 * @param expression SpEL string expression
	 * @param addTOperator wether add T in front of class name or not
	 * @return expression with package added
	 * 
	 * addTOperator true:
	 * KafkaHeaders ===> new T(org.springframework.kafka.support.KafkaHeaders)
	 * 
	 * addTOperator false:
	 * KafkaHeaders ===> new org.springframework.kafka.support.KafkaHeaders
	 */
	public static String addPackagerOnSpEL(String expression, Boolean addTOperator) {
		for(Map.Entry<String, String> entry : detectedClass.entrySet()) {
			if(expression.contains(entry.getKey()) && addTOperator)
				expression = expression.replace(entry.getKey(), "T("+entry.getValue()+")");
			else if(expression.contains(entry.getKey()) && !addTOperator)
				expression = expression.replace(entry.getKey(), entry.getValue());
		}
		return expression;
	}
}
