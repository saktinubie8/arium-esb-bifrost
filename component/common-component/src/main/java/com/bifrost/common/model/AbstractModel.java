package com.bifrost.common.model;

import com.hazelcast.nio.serialization.DataSerializable;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface AbstractModel extends DataSerializable {

	public Object getPk();
	
}
