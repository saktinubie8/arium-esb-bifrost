package com.bifrost.common.model;

import org.springframework.data.annotation.*;
import org.springframework.data.annotation.Version;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseAuditTrailEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "CREATED_BY", updatable = false)
    @CreatedBy
    protected String createdBy;

    @Column(name = "CREATED_DATE", updatable = false)
    @CreatedDate
    protected Date createdDate;

    @Column(name = "MODIFIED_BY")
    @LastModifiedBy
    protected String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    @LastModifiedDate
    protected Date modifiedDate;

    @Column(name = "IS_ACTIVE")
    protected Boolean isActive = true;

    @Column(name = "IS_USED")
    protected Boolean isUsed = true;

    @Column(name = "VERSION")
    @Version
    protected Integer version = 0;

    @javax.persistence.Transient
    protected String overrideModifiedBy;

    @javax.persistence.Transient
    protected String overrideCreatedBy;

    @PrePersist
    @PreUpdate
    public void overrideModifiedBy() {
        if (overrideModifiedBy != null)
            modifiedBy = overrideModifiedBy;
        if (overrideCreatedBy != null)
            createdBy = overrideCreatedBy;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}