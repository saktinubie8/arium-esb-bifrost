package com.bifrost.common.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public class ApprovalAuditTrailEntity extends BaseAuditTrailEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "LOCKED_STATUS")
    protected String lockedStatus;

    @Column(name = "LOCKED_BY")
    protected String lockedBy;

    @Column(name = "LAST_APPROVED_BY")
    protected String lastApprovedBy;

    @Column(name = "LAST_APPROVED_DATE")
    protected Date lastApprovedDate;

    public String getLockedStatus() {
        return lockedStatus;
    }

    public void setLockedStatus(String lockedStatus) {
        this.lockedStatus = lockedStatus;
    }

    public String getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy) {
        this.lockedBy = lockedBy;
    }

    public String getLastApprovedBy() {
        return lastApprovedBy;
    }

    public void setLastApprovedBy(String lastApprovedBy) {
        this.lastApprovedBy = lastApprovedBy;
    }

    public Date getLastApprovedDate() {
        return lastApprovedDate;
    }

    public void setLastApprovedDate(Date lastApprovedDate) {
        this.lastApprovedDate = lastApprovedDate;
    }

}