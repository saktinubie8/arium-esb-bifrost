package com.bifrost.common.banner;

import java.io.PrintStream;

import org.springframework.boot.Banner;
import org.springframework.core.env.Environment;

/**
 * Class with function to print Bifrost banner
 * base on module name and version which are provided
 * from constructor parameter
 * 
 * @author rosaekapratama@gmail.com
 */
public class BannerInfo implements Banner {

	private final String VERSION = "1.0.0-RELEASE";
	private String moduleName;

	/**
	 * Constructor to print Bifrost banner
	 * @param moduleName name of the module which will start in this boot app
	 * @param version the version of the module
	 */
	public BannerInfo(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
		out.append("  ____     __               _   \n");
		out.append(" |  _ \\   / _|             | |  \n");
		out.append(" | |_) |_| |_ _ __ ___  ___| |_ \n");
		out.append(" |  _ <| |  _| '__/ _ \\/ __| __|\n");
		out.append(" | |_) | | | | | | (_) \\__ \\ |_ \n");
		out.append(" |____/|_|_| |_|  \\___/|___/\\__|\n");
		out.append(" ===============================\n");
		out.append("  :: Bifrost :: v"+VERSION+"\n");
		out.append("  :: "+moduleName+" Module\n");
		out.append("\n");
	}

}
