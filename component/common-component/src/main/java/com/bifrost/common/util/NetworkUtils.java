package com.bifrost.common.util;

/**
 * @author rosaekapratama@gmail.com
 *
 */

public class NetworkUtils {

	/**
	 * @param remoteIp Remote IP which wants to be checked
	 * @param whiteList List of IP which can proceed
	 * @return
	 */
	public static Boolean validateIp(String remoteIp, String whiteList) {
		if(whiteList==null)
			return true;
		else if(whiteList.equals("*"))
			return true;
		else if(whiteList.equals("*"))
			return true;
		else
			return validateIp(remoteIp, whiteList.split("\\."));
	}
	
	/**
	 * @param remoteIp Remote IP which wants to be checked
	 * @param whiteList List of IP which can proceed
	 * @return
	 */
	public static Boolean validateIp(String remoteIp, String[] whiteList) {
		String[] remoteFragment = remoteIp.split("\\.");
		for(int i=0; i<whiteList.length; i++) {
			if(whiteList[i].equals("*"))
				continue;
			else if(!whiteList[i].equals(remoteFragment[i])) {
				return false;
			}
		}
		return true;
	}
	
}
