package com.bifrost.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class DateUtils {

	public static final String SYSTEM_DATE_FORMAT = "yyyyMMdd";
	public static final String SYSTEM_DATE_TIME_FORMAT = "yyyyMMdd'T'HHmmss.SSS";
	public static final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat(SYSTEM_DATE_TIME_FORMAT);
	public static final SimpleDateFormat dateFormatter = new SimpleDateFormat(SYSTEM_DATE_FORMAT);
	
	/**
	 * @return date in string formatted in yyyyMMdd'T'HHmmss.SSS pattern
	 */
	public static String generateFormattedInDateTime() {
		return formatInDateTime(new Date());
	}
	
	/**
	 * @return date in string formatted in yyyyMMdd pattern
	 */
	public static String generateFormattedInDate() {
		return formatInDate(new Date());
	}

	/**
	 * @param date input date which want to be reformatted
	 * @return date in yyyyMMdd'T'HHmmss.SSS pattern
	 */
	public static String formatInDateTime(Date date) {
		return dateTimeFormatter.format(date);
	}

	/**
	 * @param date input date which want to be reformatted
	 * @return date in yyyyMMdd pattern
	 */
	public static String formatInDate(Date date) {
		return dateFormatter.format(date);
	}
	
	/**
	 * @param pattern date pattern
	 * @return date in corresponding pattern
	 */
	public static String formatDate(String pattern) {
		return formatDate(new Date(), pattern);
	}

	/**
	 * @param date input date which want to be reformatted
	 * @param pattern date pattern
	 * @return date in corresponding pattern
	 */
	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.format(date);
	}

	/**
	 * @param source formatted date
	 * @return date in SYSTEM_DATE_TIME_FORMAT pattern 
	 */
	public static Date parseDateTimeString(String source) throws ParseException {
		return dateTimeFormatter.parse(source);
	}

	/**
	 * @param source formatted date
	 * @return date in SYSTEM_DATE_FORMAT pattern 
	 */
	public static Date parseDateString(String source) throws ParseException {
		return dateFormatter.parse(source);
	}

	/**
	 * @param source formatted date
	 * @param pattern date pattern
	 * @return date in corresponding pattern
	 */
	public static Date parseString(String source, String pattern) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.parse(source);
	}
	
}
