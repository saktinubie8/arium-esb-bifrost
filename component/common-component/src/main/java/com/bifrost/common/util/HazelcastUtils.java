package com.bifrost.common.util;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class HazelcastUtils {

	public static void writeUTF(String value, ObjectDataOutput out) throws IOException {
		if(value!=null) {
			out.writeBoolean(true);
			out.writeUTF(value);
		}else
			out.writeBoolean(false);
	}

	public static void writeLong(Long value, ObjectDataOutput out) throws IOException {
		if(value!=null) {
			out.writeBoolean(true);
			out.writeLong(value);
		}else
			out.writeBoolean(false);
	}

	public static void writeBoolean(Boolean value, ObjectDataOutput out) throws IOException {
		if(value!=null) {
			out.writeBoolean(true);
			out.writeBoolean(value);
		}else
			out.writeBoolean(false);
	}
	
	public static String readUTF(ObjectDataInput in) throws IOException {
		if(in.readBoolean())
			return in.readUTF();
		else
			return null;
	}
	
	public static Long readLong(ObjectDataInput in) throws IOException {
		if(in.readBoolean())
			return in.readLong();
		else
			return null;
	}
	
	public static Boolean readBoolean(ObjectDataInput in) throws IOException {
		if(in.readBoolean())
			return in.readBoolean();
		else
			return null;
	}
	
}
