package com.bifrost.common.constant;

public class GUIConstant {

    private GUIConstant() {
        throw new IllegalStateException("Utility class");
    }

    public static final String IS_ACTIVE = "isActive";
    public static final String IS_HOLIDAY = "isHoliday";
    public static final String FORMAT_DATE = "dd/MM/yyyy";

    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String FAILURE = "failure";

}