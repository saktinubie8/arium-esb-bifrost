package com.bifrost.common.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

/**
 * Class which provide standar CRUD method for its entity model.
 * 
 * @param <R> entity repository which extends JpaRepository class
 * @param <T> entity model
 * @param <ID> entity primary key type
 * 
 * @author rosaekapratama@gmail.com
 */
public class GenericService<R extends JpaRepository<T, ID>, T extends AbstractModel, ID> {

	private static final Log LOGGER = LogFactory.getLog(GenericService.class);
	
	@Autowired
	protected R repository;

	@Autowired
	protected HazelcastInstance hazelcastInstance;
	
	private String prefixCacheName;
	IMap<SimpleKey, List<T>> findAll;
	IMap<Object, T> findById;

	/*
	 * Cache name must be provided to create an instance.
	 * It is used as a prefix cache name when saving return result
	 * of called CRUD method. 
	 */
	public GenericService(String cacheName) {
		this.prefixCacheName = cacheName;
	}
	
	/**
	 * Cache map with key SimpleyKey.EMPTY is called first,
	 * if its value is not null then return that cache value.
	 * If its value is null then called method from repository,
	 * save the result to cache first, then  return it.
	 * 
	 * @return list of all entity records
	 */
	public List<T> findAll() {
		if(findAll==null)
			findAll = hazelcastInstance.getMap(prefixCacheName + ":findAll");
		List<T> list = findAll.get(SimpleKey.EMPTY);
		if (list != null)
			return list;
		else 
			return null;
	}

	/**
	 * Cache map with @id param as its key is called first,
	 * if its value is not null then return that cache value.
	 * If its value is null then called method from repository,
	 * save the result to cache first, then  return it.
	 * 
	 * @param id primary key of entity which want to be fetched
	 * @return entity with the corresponding primary key
	 */
	public T findById(ID id) {
		if(findById==null)
			findById = hazelcastInstance.getMap(prefixCacheName + ":findById");
		T t = findById.get(id);
		if (t != null)
			return t;
		else 
			return null;
	}
	
	/**
	 * If child class has another dao-related method other than above
	 * then the child class must override this method 
	 * and handle the cache value via @CacheEvict or @CachePut
	 * 
	 * @param t entity which want to be saved
	 * @return entity which is saved
	 */
	public T save(T t) {
		if(findAll==null)
			findAll = hazelcastInstance.getMap(prefixCacheName + ":findAll");
		T obj = repository.save(t);
		findAll.evictAll();
		refreshFindByIdCache(t, false);
		return obj;
	}

	/**
	 * If child class has another dao-related method other than above
	 * then the child class must override this method 
	 * and handle the cache value via @CacheEvict
	 * 
	 * @param t entity which want to be deleted
	 */
	public void delete(T t) {
		if(findAll==null)
			findAll = hazelcastInstance.getMap(prefixCacheName + ":findAll");
		repository.delete(t);
		findAll.evictAll();
		refreshFindByIdCache(t, true);
	}
	
	/**
	 * If child class has another dao-related method other than above
	 * then the child class must override this method 
	 * and handle the cache value via @CacheEvict\
	 */
	public void evictAll() {
		findAll.evictAll();
		findById.evictAll();
	}
	
	/**
	 * Add or remove a value in method findById
	 * 
	 * @param t entity which want to be saved or removed from cache
	 * @param isRemove true if want to remove entity, false if want to save entity
	 */
	@SuppressWarnings("unchecked")
	protected void refreshFindByIdCache(T t, Boolean isRemove) {
		if(findById==null)
			findById = hazelcastInstance.getMap(prefixCacheName + ":findById");
		ID pk = (ID) t.getPk();
		T obj = findById.get(pk);
		if(obj!=null && !isRemove) {
			findById.set(pk, t);
			LOGGER.trace("[System][Hazelcast] MANAGED to REPLACE object with key: "+pk+", value: "+t+" in cache: "+prefixCacheName + ":findById");
		}else if(obj!=null && isRemove) {
			findById.evict(pk);
			LOGGER.trace("[System][Hazelcast] MANAGED to REMOVE object with key: "+pk+" in cache: "+prefixCacheName + ":findById");
		}else if(obj==null && isRemove)
			LOGGER.trace("[System][Hazelcast] FAILED to REMOVE object which not exist in cache: "+prefixCacheName + ":findById");
	}
	
	/**
	 * Populating inital cache data 
	 * @throws Exception 
	 */
	@PostConstruct
	public void populate() throws Exception {
		if(findAll==null)
			findAll = hazelcastInstance.getMap(prefixCacheName + ":findAll");
		if(findById==null)
			findById = hazelcastInstance.getMap(prefixCacheName + ":findById");
		List<T> tList = repository.findAll();
		if(findAll.get(SimpleKey.EMPTY)==null)
			findAll.set(SimpleKey.EMPTY, tList);
		for(T t : tList)
			if(findById.get(t.getPk())==null)
				findById.set(t.getPk(), t);
	}
}
