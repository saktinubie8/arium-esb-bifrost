package com.bifrost.common.constant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class PathConstant {

	// LIBRARY FOLDER
	public static class Lib {
		public static final String BASE_PATH = "lib";
	}

	// PACKAGER RELATED CONSTANT
	public static class Packager {
		// ENDPOINT PACKAGER PATH FOLDER LOCATION
		public static final String BASE_PATH = "packager";
		public static final String KAFKA_PATH = BASE_PATH+"/kafka/";
		public static final String ISO8583_PATH = BASE_PATH+"/iso8583/";
		
		// DEFAULT NAME (ALL MAPPING WHICH DOESNT HAVE PACKAGER WILL USE PACKAGER WITH THIS NAME INSTEAD)
		public static final String DEFAULT_NAME = "default";
		
		// GENERICPACKAGER.DTD NAME
		public static final String GENERIC_PACKAGER_DTD = "genericpackager.dtd";
		
		// PACKAGER EXTENSION TYPE
		public static final String ISO8583_EXT = "iso";
		public static final String FIXED_LENGTH_EXT = "fln";
		public static final String XSD_EXT = "xsd";
		public static final String WSDL_EXT = "wsdl";
		
		// XML PACKAGER TAG TYPE
		public static final String FIXED_LENGTH_PARENT_TAG = "schema";
		public static final String ISO8583_PARENT_TAG = "iso-message";
	}
	
	// SYSTEM REST PATH
	public static class Rest {
		public static final String SYSTEM_PREFIX = "/system";
		public static final String SYSTEM_CACHE = SYSTEM_PREFIX+"/cache";
		public static final String SYSTEM_PACKAGER = SYSTEM_PREFIX+"/packager";
		public static final String SYSTEM_SERVICE = SYSTEM_PREFIX+"/service";
		public static final String SERVICE_ENDPOINT_PREFIX = SYSTEM_SERVICE+"/endpoint";
		public static final String SERVICE_MODIFIER_PREFIX = SYSTEM_SERVICE+"/modifier";
		public static final String SERVICE_ROUTER_PREFIX = SYSTEM_SERVICE+"/router";
		public static final String SERVICE_MAPPER_PREFIX = SYSTEM_SERVICE+"/mapper";
		public static final String SERVICE_ORCHESTRATOR_PREFIX = SYSTEM_SERVICE+"/orchestrator";
		public static final String SYSTEM_LIBRARY = SYSTEM_PREFIX+"/lib";
	}
}
