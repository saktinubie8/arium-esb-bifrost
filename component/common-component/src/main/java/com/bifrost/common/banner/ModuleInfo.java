package com.bifrost.common.banner;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class ModuleInfo {


	public static final String HAZELCAST_SERVER_CODE = "HZL_SVR";
	public static final String CORE_ORCHESTRATOR_CODE = "COR_ORC";
	public static final String CORE_LOGGING_CODE = "COR_LOG";
	public static final String CORE_GUI_CODE = "COR_GUI";
	public static final String KAFKA_ENDPOINT_CODE = "KFK_ENP";
	public static final String REST_ENDPOINT_CODE = "RST_ENP";
	public static final String SOAP_ENDPOINT_CODE = "SOP_ENP";
	public static final String ISO8583_ENDPOINT_CODE = "ISO_ENP";
	public static final String DATA_SOURCE_ENDPOINT_CODE = "DSR_ENP";

	public static final String CONFIG_SERVER_DESC = "Central Configuration Server";
	public static final String HAZELCAST_SERVER_DESC = "Hazelcast Server";
	public static final String CORE_ORCHESTRATOR_DESC = "Core Orchestrator";
	public static final String CORE_LOGGING_DESC = "Core Logging";
	public static final String CORE_GUI_DESC = "Core GUI";
	public static final String KAFKA_ENDPOINT_DESC = "Kafka Generic Endpoint";
	public static final String SOAP_ENDPOINT_DESC = "SOAP Generic Endpoint";
	public static final String REST_ENDPOINT_DESC = "REST Generic Endpoint";
	public static final String ISO8583_ENDPOINT_DESC = "ISO8583 Generic Endpoint";
	public static final String DATA_SOURCE_ENDPOINT_DESC = "Data Source Generic Endpoint";

}