package com.bifrost.common.json.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class GenericResponse {

	public static final GenericResponse DEFAULT = new GenericResponse("SYSDFL", "Default");
	public static final GenericResponse SUCCESS = new GenericResponse("SYSSCS", "Success");
	public static final GenericResponse FAILED = new GenericResponse("SYSFLD", "Failed");
	public static final GenericResponse ERROR = new GenericResponse("SYSERR", "Error");
	public static final GenericResponse OFFLINE = new GenericResponse("SYSOFF", "Offline");
	public static final GenericResponse TIMEOUT = new GenericResponse("SYSTMO", "Timeout");
	public static final GenericResponse UNAVAILBALE_ROUTE = new GenericResponse("SYSUNR", "Unavailable route");
	public static final GenericResponse UNKNOWN_INVOCATION_CODE = new GenericResponse("SYSUIC", "Unknown invocation code");
	public static final GenericResponse UNAUTHORIZED = new GenericResponse("SYSAUT", "Unauthorized");
	public static final GenericResponse NO_MATCHING_RESPONSE = new GenericResponse("SYSNMR", "No matching response");
	
	@JsonProperty("responseCode")
	private String code;

	@JsonProperty("responseDesc")
	private String desc;

	public GenericResponse() {
	}

	public GenericResponse(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return code+":"+desc;
	}
	
	
}
