package com.bifrost.common.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.common.json.response.CacheResponse;
import com.bifrost.common.json.response.GenericResponse;

public abstract class AbstractCacheController {
	
	@RequestMapping(path = PathConstant.Rest.SYSTEM_CACHE+"/all/refresh", method = RequestMethod.GET)
	public GenericResponse refreshAllCache() {
		refreshAll();
		return CacheResponse.SUCCESS;
	}
	
	public abstract void evictAll();
	public abstract void populateAll();
	public void refreshAll() {
		evictAll();
		populateAll();
	}
	
}
