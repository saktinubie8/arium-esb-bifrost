package com.bifrost.common.constant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class HazelcastConstant {

	public static class INSTANCE {
		public static final String NAME = "bifrost";
	}
	
	public static class JOIN {
		public static final String MULTICAST = "multicast";
		public static final String TCPIP = "tcpip";
		public static final String SWARM = "swarm";
	}
}
