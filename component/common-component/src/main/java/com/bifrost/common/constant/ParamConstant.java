package com.bifrost.common.constant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class ParamConstant {

	// BIFROST
	public static class Bifrost {
		public static final String MODULE_PORT_MIN = "bifrost.module.port.min";
		public static final String MODULE_PORT_MAX = "bifrost.module.port.max";
		public static final String MODULE_HAZELCAST_SERVER_PORT = "bifrost.module.hazelcast-server.port";
		public static final String MODULE_CORE_ORCHESTRATOR_PORT = "bifrost.module.core-orchestrator.port";
		public static final String MODULE_CORE_GUI_PORT = "bifrost.module.core-gui.port";
		public static final String MODULE_CORE_LOGGING_PORT = "bifrost.module.core-logging.port";
		public static final String MODULE_ENDPOINT_KAFKA_PORT = "bifrost.module.endpoint-kafka.port";
		public static final String MODULE_ENDPOINT_REST_PORT = "bifrost.module.endpoint-rest.port";
		public static final String MODULE_ENDPOINT_SOAP_PORT = "bifrost.module.endpoint-soap.port";
		public static final String MODULE_ENDPOINT_ISO8583_PORT = "bifrost.module.endpoint-iso8583.port";
		public static final String GENERIC_DEFAULT_TIMEOUT = "bifrost.config.timeout.default";
		public static final String HEARTBEAT_DELAY = "bifrost.config.heartbeat.delay";
		public static final String IDENTITY_USING_HOSTNAME = "bifrost.config.identity.host.show-name";
		public static final String ENDPOINT_STATUS_CACHE_TTL = "bifrost.config.cache.endpoint.status.ttl";
	}

	// DATABASE
	public static class Database {
		public static final String CONFIG_HOST = "database.cfg.host";
		public static final String CONFIG_PORT = "database.cfg.port";
		public static final String CONFIG_NAME = "database.cfg.name";
		public static final String CONFIG_USER = "database.cfg.user";
		public static final String CONFIG_PASS = "database.cfg.pass";
		public static final String LOG_HOST = "database.log.host";
		public static final String LOG_PORT = "database.log.port";
		public static final String LOG_NAME = "database.log.name";
		public static final String LOG_USER = "database.log.user";
		public static final String LOG_PASS = "database.log.pass";
		public static final String GUI_HOST = "database.gui.host";
		public static final String GUI_PORT = "database.gui.port";
		public static final String GUI_NAME = "database.gui.name";
		public static final String GUI_USER = "database.gui.user";
		public static final String GUI_PASS = "database.gui.pass";
	}

	// ELASTICSEARCH
	public static class Elasticsearch {
		public static final String CLUSTER_ADDRESSES = "elasticsearch.cluster.addresses";
		public static final String CONNECT_TIMEOUT = "elasticsearch.client.connect-timeout";
		public static final String SOCKET_TIMEOUT = "elasticsearch.client.socket-timeout";
	}
	
	// THREAD
	public static class Thread {
		public static final String POOL_TASK_CORE_SIZE = "thread.pool.task.size.core";
		public static final String POOL_TASK_MAX_SIZE = "thread.pool.task.size.max";
		public static final String POOL_TASK_KEEP_ALIVE_SECONDS = "thread.pool.task.keep-alive-seconds";
		public static final String POOL_TASK_QUEUE_CAPACITY = "thread.pool.task.queue-capacity";
		public static final String POOL_TASK_WAIT_TASK_ON_SHUTDOWN = "thread.pool.task.wait-task-on-shutdown";
		public static final String POOL_SCHEDULER_CORE_SIZE = "thread.pool.scheduler.size.core";
	}

	// HAZELCAST
	public static class Hazelcast {
		public static final String SOCKET_BIND_ANY = "hazelcast.socket.bind.any";
		public static final String SOCKET_SERVER_BIND_ANY = "hazelcast.socket.server.bind.any";
		public static final String SOCKET_CLIENT_BIND_ANY = "hazelcast.socket.client.bind.any";
		public static final String SERVER_INSTANCE_NAME = "hazelcast.server.instance.name";
		public static final String SERVER_GROUP_NAME = "hazelcast.server.group.name";
		public static final String SERVER_MANAGEMENT_CENTER_ENABLED = "hazelcast.server.management-center.enabled";
		public static final String SERVER_MANAGEMENT_CENTER_URL = "hazelcast.server.management-center.url";
		public static final String SERVER_INTERFACE_ENABLED = "hazelcast.server.interface.enabled";
		public static final String SERVER_INTERFACE_LIST = "hazelcast.server.interface.list";
		public static final String SERVER_JOIN_MODE = "hazelcast.server.join.mode";
		public static final String SERVER_MULTICAST_GROUP = "hazelcast.server.multicast.group";
		public static final String SERVER_MULTICAST_PORT = "hazelcast.server.multicast.port";
		public static final String SERVER_TCPIP_MEMBERS = "hazelcast.server.tcpip.members";
		public static final String SERVER_SWARM_DOCKER_NETWORK_NAMES = "hazelcast.server.swarm.docker-network-names";
		public static final String SERVER_SWARM_DOCKER_SERVICE_NAMES = "hazelcast.server.swarm.docker-service-names";
		public static final String SERVER_SWARM_DOCKER_SERVICE_LABELS = "hazelcast.server.swarm.docker-service-labels";
		public static final String SERVER_SWARM_SWARM_MGR_URI = "hazelcast.server.swarm.swarm-mgr-uri";
		public static final String SERVER_SWARM_SKIP_VERIFY_SSL = "hazelcast.server.swarm.skip-verify-ssl";
		public static final String SERVER_SWARM_LOG_ALL_SERVICE_NAMES_ON_FAILED_DISCOVERY = "hazelcast.server.swarm.log-all-service-names-on-failed-discovery";
		public static final String SERVER_SWARM_STRICT_SERVICE_NAME_COMPARISON = "hazelcast.server.swarm.strict-docker-service-name-comparison";
		public static final String SERVER_NETWORK_PORT = "hazelcast.server.network.port";
		public static final String SERVER_NETWORK_PORT_AUTO_INCREMENT = "hazelcast.server.network.port.auto-increment";
		public static final String SERVER_NETWORK_PORT_COUNT = "hazelcast.server.network.port.count";
		public static final String SERVER_CP_MEMBER_COUNT = "hazelcast.server.cp.member.count";
		public static final String CLIENT_SERVER_MEMBERS = "hazelcast.client.server.members";
		public static final String CLIENT_SMART_ROUTING = "hazelcast.client.smart.routing";
		public static final String CLIENT_RECONNECT_DELAY = "hazelcast.client.reconnect.delay";
	}

	// KAFKA
	public static class Kafka {
		public static final String SERVER_ADDRESS_LIST = "kafka.server.address-list";
		public static final String CONSUMER_AUTO_OFFSET_RESET = "kafka.consumer.auto-offset-reset";
		public static final String CONSUMER_PARTITION_ASSIGNMENT_STRATEGY = "kafka.consumer.partition-assignment-strategy";
		public static final String TOPIC_GROUP_ID = "kafka.topic.group-id";
		public static final String TOPIC_REPLICATION = "kafka.topic.replication";
		public static final String TOPIC_PARTITIONS = "kafka.topic.partitions";
		public static final String PARTITION_ASSIGNMENT_STRATEGY = "kafka.partition.assignment-strategy";
		public static final String MODIFIER_KAFKA_CONCURRENCY = "kafka.container.concurreny.modifier";
		public static final String ROUTER_KAFKA_CONCURRENCY = "kafka.container.concurreny.router";
		public static final String MAPPER_KAFKA_CONCURRENCY = "kafka.container.concurreny.mapperr";
		public static final String ENDPOINT_LOG_KAFKA_CONCURRENCY = "kafka.container.concurreny.endpoint-log";
		public static final String ROUTE_LOG_KAFKA_CONCURRENCY = "kafka.container.concurreny.route-log";
		public static final String MESSAGE_LOG_KAFKA_CONCURRENCY = "kafka.container.concurreny.message-log";
	}
	
	// KRYO
	public static class Kryo {
		public static final String MAX_POOL_SIZE = "kryo.max-pool-size";
	}
	
	// HOUSEKEEPING
	public static class Housekeeping {
		public static final String SCHEDULE = "housekeeping.schedule";
		public static final String DAYS_TO_KEEP = "housekeeping.days-to-keep";
	}

	// RESPONSE CODE SUCCESS GROUP
	public static class ResponseCode {
		public static final String RESPONSE_CODE_SUCCESS_GROUP = "response.code.success.group";
	}
}
