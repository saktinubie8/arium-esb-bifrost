package com.bifrost.common.json.response;

import com.bifrost.common.json.response.GenericResponse;

public class CacheResponse extends GenericResponse {

	public static final GenericResponse CACHE_NOT_FOUND = new GenericResponse("SYSCNF", "CACHE NOT FOUND");
	public static final GenericResponse ITEM_NOT_FOUND = new GenericResponse("SYSINF", "ITEM WITH REQUESTED KEY CANNOT BE FOUND");
	
}
