package com.bifrost.common.constant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class CacheName {
	
	public static final String SYSTEM_MATRIX = "system:matrix";
	public static final String ENDPOINT_STATUS = "endpoint:status";
	public static String ENDPOINT_PACKAGER(String endpointCode) {
		return "endpoint:"+endpointCode+":packager";
	}
	public static final String ENDPOINT_STATUS_DETAIL_CACHE = "endpoint:status:detail";
	public static String ENDPOINT_ASSOC_KEY_COUNT(String endpointCode) {
		return "endpoint:"+endpointCode+":assockey:count";
	}
	public static final String DEST_ENDPOINT_MSG = "endpoint:dest:message";
	public static final String DEST_ENDPOINT_LATCH = "endpoint:dest:latch";
	public static final String MESSAGE_LOGS = "log:message";
}
