package com.bifrost;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Base class for spring boot main method class
 * which have standard operation to start, stop,
 * and restart springboot application 
 * 
 * @author rosaekapratama@gmail.com
 */
public class BaseApplication {

    protected static ConfigurableApplicationContext context;
    protected static SpringApplication app;
    protected static String[] args;
	
    public void stop() {
    	context.close();
    }
    
    public void start() {
    	context = app.run(args);
    }
    
    public void restart() {
    	context.close();
    	context = app.run(args);
    }
}
