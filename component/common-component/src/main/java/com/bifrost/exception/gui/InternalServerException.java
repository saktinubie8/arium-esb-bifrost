package com.bifrost.exception.gui;

public class InternalServerException extends Exception {

    private static final long serialVersionUID = 8972880939809113925L;

    public InternalServerException() {
        super();
    }

    public InternalServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InternalServerException(String message) {
        super(message);
    }

    public InternalServerException(Throwable cause) {
        super(cause);
    }

}