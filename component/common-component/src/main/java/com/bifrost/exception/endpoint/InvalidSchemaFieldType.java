package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InvalidSchemaFieldType extends Exception {

	private static final long serialVersionUID = -5072423576611981484L;

	public InvalidSchemaFieldType(String message) {
		super(message);
	}
}
