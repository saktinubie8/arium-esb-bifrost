package com.bifrost.exception.system;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InvalidDataException extends Exception {

	private static final long serialVersionUID = 3682764426076478013L;
	
	public InvalidDataException(String message) {
		super(message);
	}

}
