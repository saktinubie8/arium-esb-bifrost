package com.bifrost.exception.gui;

import com.bifrost.common.util.ErrorCode;

public class SystemErrorException extends Exception {

    private static final long serialVersionUID = 8972880939809113925L;
    private final ErrorCode errorCode;

    public SystemErrorException() {
        super();
        this.errorCode = ErrorCode.ERR_SYS0500;
    }

    public SystemErrorException(String message, Throwable cause) {
        super(message, cause);
        this.errorCode = ErrorCode.ERR_SYS0500;
    }

    public SystemErrorException(String message) {
        super(message);
        this.errorCode = ErrorCode.ERR_SYS0500;
    }

    public SystemErrorException(ErrorCode errorCode) {
        super(errorCode.name());
        this.errorCode = errorCode;
    }

    public SystemErrorException(Throwable cause) {
        super(cause);
        this.errorCode = ErrorCode.ERR_SYS0500;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

}