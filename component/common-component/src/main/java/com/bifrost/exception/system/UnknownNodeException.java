package com.bifrost.exception.system;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class UnknownNodeException extends Exception {

	private static final long serialVersionUID = -4694921113073751966L;

	public UnknownNodeException(String message) {
		super(message);
	}
}
