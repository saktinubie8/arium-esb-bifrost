package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InvalidContainerPropertiesClassException extends Exception {

	private static final long serialVersionUID = 1133632754458276411L;

	public InvalidContainerPropertiesClassException(String message) {
		super(message);
	}
}
