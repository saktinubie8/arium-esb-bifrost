package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InvalidSchemaTagType extends Exception {

	private static final long serialVersionUID = -9108396755091792252L;

	public InvalidSchemaTagType(String message) {
		super(message);
	}
}
