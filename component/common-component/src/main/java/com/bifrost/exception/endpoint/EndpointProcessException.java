package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class EndpointProcessException extends Exception {

	private static final long serialVersionUID = 5355988882483507438L;

	public EndpointProcessException(String message) {
		super(message);
	}
}
