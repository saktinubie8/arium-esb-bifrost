package com.bifrost.exception.modifier;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class UnknownInvocationException extends Exception {

	private static final long serialVersionUID = 1926658244552029713L;

	public UnknownInvocationException(String message) {
		super(message);
	}
}
