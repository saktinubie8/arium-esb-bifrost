package com.bifrost.exception.modifier;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class SpELProcessException extends Exception {

	private static final long serialVersionUID = 1926658244552029713L;

	public SpELProcessException(String message) {
		super(message);
	}
}
