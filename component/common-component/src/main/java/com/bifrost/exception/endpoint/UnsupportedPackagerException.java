package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class UnsupportedPackagerException extends Exception {

	private static final long serialVersionUID = -3694274805902179676L;

	public UnsupportedPackagerException(String message) {
		super(message);
	}
}
