package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InvalidInputClassType extends Exception {

	private static final long serialVersionUID = -8926711276251653882L;

	public InvalidInputClassType(String message) {
		super(message);
	}
}
