package com.bifrost.exception.endpoint;

public class EndpointInitiationException extends Exception {

	private static final long serialVersionUID = 2268964498546467472L;

	public EndpointInitiationException(String message) {
		super(message);
	}
	
}
