package com.bifrost.exception.gui;

public class DataNotFoundException extends Exception {

    private static final long serialVersionUID = 8972880939809113925L;

    public DataNotFoundException() {
        super();
    }

    public DataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException(Throwable cause) {
        super(cause);
    }

}