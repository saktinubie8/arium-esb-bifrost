package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InvalidPackager extends Exception {

	private static final long serialVersionUID = -8926711276251653882L;

	public InvalidPackager(String message) {
		super(message);
	}
}
