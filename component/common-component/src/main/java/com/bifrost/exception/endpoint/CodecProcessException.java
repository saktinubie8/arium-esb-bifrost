package com.bifrost.exception.endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */

public class CodecProcessException extends Exception {

	private static final long serialVersionUID = -1360819474921243579L;
	private Object faultObject;
	
	public CodecProcessException(String message) {
		super(message);
	}

	public CodecProcessException(String message, Object faultObject) {
		super(message);
		this.faultObject = faultObject;
	}
	
	public Object getFault() {
		return this.faultObject;
	}

}
