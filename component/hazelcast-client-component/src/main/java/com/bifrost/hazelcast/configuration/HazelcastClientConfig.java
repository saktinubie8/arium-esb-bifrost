package com.bifrost.hazelcast.configuration;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bifrost.common.constant.ParamConstant;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientConnectionStrategyConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.client.config.ConnectionRetryConfig;
import com.hazelcast.client.config.ClientConnectionStrategyConfig.ReconnectMode;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class HazelcastClientConfig {

	@Value("${"+ParamConstant.Hazelcast.SERVER_GROUP_NAME+"}")
	private String groupName;
	
	@Value("${" + ParamConstant.Hazelcast.CLIENT_SERVER_MEMBERS + "}")
	private String serverMembers;

	@Value("${" + ParamConstant.Hazelcast.CLIENT_SMART_ROUTING + "}")
	private String smartRouting;

	@Value("${" + ParamConstant.Hazelcast.CLIENT_RECONNECT_DELAY + ":2500}")
	private int maxBackoffMillis;
	
	@Bean
	public HazelcastInstance hazelcastInstance() {

		// Group configuration
		GroupConfig groupConfig = new GroupConfig();
		groupConfig.setName(groupName);
		
		// Network configuration
		ClientNetworkConfig clientNetworkConfig = new ClientNetworkConfig();
		clientNetworkConfig.setAddresses(Arrays.asList(serverMembers.split(",")));
		clientNetworkConfig.setSmartRouting(Boolean.parseBoolean(smartRouting));

		// Connection retry configuration
		ConnectionRetryConfig connectionRetryConfig = new ConnectionRetryConfig();
		connectionRetryConfig.setEnabled(true);
		connectionRetryConfig.setFailOnMaxBackoff(false);
		connectionRetryConfig.setMaxBackoffMillis(maxBackoffMillis);
		
		// Connection strategy configuration
		ClientConnectionStrategyConfig clientConnectionStrategyConfig = new ClientConnectionStrategyConfig();
		clientConnectionStrategyConfig.setAsyncStart(true);
		clientConnectionStrategyConfig.setConnectionRetryConfig(connectionRetryConfig);
		clientConnectionStrategyConfig.setReconnectMode(ReconnectMode.ASYNC);
		
		// Initialize hazelcast instance with configuration above
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setGroupConfig(groupConfig);
		clientConfig.setNetworkConfig(clientNetworkConfig);
		clientConfig.setConnectionStrategyConfig(clientConnectionStrategyConfig);

		return HazelcastClient.newHazelcastClient(clientConfig);
	}

	@Bean
	public CacheManager cacheManager(HazelcastInstance hazelcastInstance) {
		CacheManager cacheManager = new HazelcastCacheManager(hazelcastInstance);
		return cacheManager;
	}
}
