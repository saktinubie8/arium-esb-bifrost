package com.bifrost.endpoint.model;

import java.io.IOException;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint_type")
public class EndpointType implements AbstractModel {
	
	public static final String ISO8583 = "iso8583";
	public static final String REST = "rest";
	public static final String SOAP = "soap";
	public static final String KAFKA = "kafka";
	public static final String DATABASE = "database";
	
	@Id
	@Column(name = "type", nullable = false, length = 10)
	private String type;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "type")
	private Set<Endpoint> endpoints;

	public EndpointType() {
	}

	public EndpointType(String type, String description) {
		this.type = type;
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEndpoints(Set<Endpoint> endpoints) {
		this.endpoints = endpoints;
	}

	@Override
	public Object getPk() {
		return this.type;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(type);
		out.writeUTF(description);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		type = in.readUTF();
		description = in.readUTF();
	}

}
