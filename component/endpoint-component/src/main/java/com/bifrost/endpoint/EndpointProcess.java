package com.bifrost.endpoint;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.camel.Exchange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.bifrost.common.constant.CacheName;
import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.endpoint.dispatcher.AsynchronousDispatcher;
import com.bifrost.endpoint.dispatcher.SynchronousDispatcher;
import com.bifrost.endpoint.logger.AbstractDispatcherLogger;
import com.bifrost.endpoint.logger.AbstractReceiverLogger;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.packager.GenericPackageLoader;
import com.bifrost.exception.endpoint.CodecProcessException;
import com.bifrost.exception.modifier.UnknownInvocationException;
import com.bifrost.logging.LoggingClient;
import com.bifrost.logging.model.MessageLog;
import com.bifrost.message.ExternalMessage;
import com.bifrost.system.model.InvocationTimeout;
import com.bifrost.system.service.InvocationTimeoutService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICountDownLatch;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 * 
 * Endpoint must invoke receive(Object, String, true) method when receiving request message, or
 * invoke receive(Object, String, false) method when receiving response message from outer system.
 * There are 2 types endpoint messaging:
 * 
 * ASYNCHRONOUS
 * - for endpoint which receive request, its response will send via asynchronous dispatcher
 * - for endpoint which send request, its response will come from method receive(Object, String, false) invocation
 * 
 * SYNCHRONOUS
 * - for endpoint which receive request, its response will return directly from invoking method receive(Object, String, true)
 * - for endpoint which send request, its response will come from return value of invoking syncDispatcher.send
 */
public class EndpointProcess {

	private String node;
	private String endpointCode;
	private String endpointType;
	private AbstractCodec codec;
	private LoggingClient loggingClient;
	private ContainerProperties containerProps;
	private HazelcastInstance hazelcastInstance;
	private GenericPackageLoader packageLoader;
	private SynchronousDispatcher syncDispatcher;
	private AbstractReceiverLogger receiverLogger;
	private AsynchronousDispatcher asyncDispatcher;
	private AbstractDispatcherLogger dispatcherLogger;
	private KafkaTemplate<String, Object> kafkaTemplate;
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	private AbstractMessageConfigurator messageConfigurator;
	private ConsumerFactory<String, Object> consumerFactory;
	private InvocationTimeoutService invocationTimeoutService;
	private static final Log LOGGER = LogFactory.getLog(EndpointProcess.class);
	private ConcurrentMessageListenerContainer<String, Object> internalListener;
	private Map<Long, CountDownLatch> latches = new ConcurrentHashMap<Long, CountDownLatch>();
	private Map<Long, ExternalMessage> oriMsg = new ConcurrentHashMap<Long, ExternalMessage>();
	private static final Map<String, AtomicLong> atomicLongs = new ConcurrentHashMap<String, AtomicLong>();
	private IMap<String, ExternalMessage> destMsg;
	private IMap<String, MessageLog> messageLogs;
	private int concurrency = 10;
	
	@SuppressWarnings("unchecked")
	public EndpointProcess(Endpoint endpoint, String node, ApplicationContext context) {
		// Injecting necessery spring bean if application context is exists
		if(context!=null) {
			this.invocationTimeoutService = context.getBean(InvocationTimeoutService.class);
			this.messageConfigurator = context.getBean(AbstractMessageConfigurator.class);
			this.threadPoolTaskExecutor = context.getBean(ThreadPoolTaskExecutor.class);
			this.dispatcherLogger = context.getBean(AbstractDispatcherLogger.class);
			this.receiverLogger = context.getBean(AbstractReceiverLogger.class);
			this.packageLoader = context.getBean(GenericPackageLoader.class);
			this.hazelcastInstance = context.getBean(HazelcastInstance.class);
			this.consumerFactory = context.getBean(ConsumerFactory.class);
			this.kafkaTemplate = context.getBean(KafkaTemplate.class);
			this.loggingClient = context.getBean(LoggingClient.class);
			this.codec = context.getBean(AbstractCodec.class);
			messageLogs = hazelcastInstance.getMap(CacheName.MESSAGE_LOGS);
			this.destMsg = hazelcastInstance.getMap(CacheName.DEST_ENDPOINT_MSG);
		}
		
		this.node = node;
		endpointCode = endpoint.getCode();
		endpointType = endpoint.getType().getType();
		LOGGER.trace("["+endpointCode+"] ============================================================================================================================================");
		LOGGER.info("["+endpointCode+"] INITIATING "+endpointType+" endpoint...");
		LOGGER.trace("["+endpointCode+"] ============================================================================================================================================");
		
		/*
		 * Consumer init for request or respone from inside of the system
		 * latch.countDown for request from outside system is called here
		 * Generate 2 consumer for each endpoint based on its code
		 * exp : test01.out for destination endpoint, consuming request from modifier
		 * exp : test01.{node}.out for origin endpoint, consuming response from modifier
		 * Means endpoint actually can act both as destination and origin
		 */
		containerProps = new ContainerProperties(endpointCode+".out", endpointCode + "." + node+".out");
		containerProps.setMissingTopicsFatal(false);
		containerProps.setMessageListener(new MessageListener<String, ExternalMessage>() {

			public void onMessage(ConsumerRecord<String, ExternalMessage> data) {
				final ExternalMessage externalMessage = data.value();
				final String invocationCode = externalMessage.getInvocationCode();
				
				/*
				 * If message is a response then
				 * countDown the latch for origin endpoint request latch wait,
				 * and override request data content with response data for the same key.
				 * If latches not found, means origin endpoint request already timed out,
				 * and if return immediately is true means it is the timeout response message.
				 */
				if(externalMessage.isResponse()) {
					LOGGER.trace("["+endpointCode+"] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
					LOGGER.debug("["+endpointCode+"] Receiving response from modifier: \n"+externalMessage);
					Long assocKey = Long.valueOf(externalMessage.getOriginAssocKey());
					if(latches.containsKey(assocKey)) {
						oriMsg.put(assocKey, externalMessage);
						latches.remove(assocKey).countDown();
					}else if(externalMessage.isReturnImmediately()){
						oriMsg.put(assocKey, externalMessage);
						try {
							Thread.sleep(2000L);
						} catch (InterruptedException e) {
							e.printStackTrace();
						} finally {
							oriMsg.remove(assocKey);
						}
					}
						
					
				/*
				 * If it is a request then 
				 * sent message to outer target system,
				 * and wait as long as timeoutMillis.
				 * There's a possibility response will come from a different node,
				 * so rather than use ordinary Map and Latches, 
				 * i use hazelcast instead for its node sync capability
				 */
				}else {
					LOGGER.trace("["+endpointCode+"] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
					LOGGER.debug("["+endpointCode+"] Receiving request from modifier: \n"+externalMessage);
					Long assocKey = hazelcastInstance.getCPSubsystem().getAtomicLong(CacheName.ENDPOINT_ASSOC_KEY_COUNT(endpointCode)).getAndIncrement();
					externalMessage.setAssocKey(assocKey);
					externalMessage.setNode(node);
					final String key = constructKey(endpointCode, assocKey);
					IMap<String, ExternalMessage> message = hazelcastInstance.getMap(CacheName.DEST_ENDPOINT_MSG);
					ICountDownLatch latch = hazelcastInstance.getCPSubsystem().getCountDownLatch(key);
					Long timeoutMillis = 25000L;
					InvocationTimeout invocationTimeout = invocationTimeoutService.findByEndpointCodeAndInvocationCode(endpointCode, invocationCode);
					if(invocationTimeout!=null)
						timeoutMillis = invocationTimeout.getTimeoutMillis();

					//Dispatcher is responsible for sending request message to outer system
					final Object reqObj;
					try {
						reqObj = codec.encode(externalMessage, packageLoader.getEncodePackager(endpointCode, invocationCode));
					} catch (UnknownInvocationException e1) {
						LOGGER.error("["+endpointCode+"] Got error while trying to send message to endpoint, cause by: "+e1.getMessage());
						e1.printStackTrace();
						externalMessage.setSystemResponseCode(GenericResponse.UNKNOWN_INVOCATION_CODE.getCode());
						externalMessage.setSystemResponseDesc("Failed sending message to endpoint, cause by: "+e1.getMessage());
						LOGGER.trace("["+endpointCode+"] Returning to modifier with system response code "+GenericResponse.UNKNOWN_INVOCATION_CODE.getCode());
						kafkaTemplate.send("modifier."+endpointCode+"."+invocationCode.replaceAll("[^\\dA-Za-z ]", "_")+".in", externalMessage);
						return;
					} catch (Exception e2) {
						// Error encode, send to router, and show the error
						LOGGER.error("["+endpointCode+"] Failed encoding message, cause by: "+e2.getMessage());
						e2.printStackTrace();
						externalMessage.setSystemResponseCode(GenericResponse.ERROR.getCode());
						externalMessage.setSystemResponseDesc("Failed encoding message, cause by: "+e2.getMessage());
						externalMessage.setResponse();
						LOGGER.trace("["+endpointCode+"] Returning to modifier with system response code "+GenericResponse.ERROR.getCode());
						kafkaTemplate.send("modifier."+endpointCode+"."+invocationCode.replaceAll("[^\\dA-Za-z ]", "_")+".in", externalMessage);
						return;
					}

					message.set(key, externalMessage);
					LOGGER.debug("["+endpointCode+"] Sending request to endpoint: \n"+dispatcherLogger.writeAsString(reqObj));
					LOGGER.trace("["+endpointCode+"] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
					
					//For Asynchronous destination endpoint
					if(asyncDispatcher!=null)
						threadPoolTaskExecutor.execute(new Runnable() {

							@Override
							public void run() {
								try {
									threadPoolTaskExecutor.execute(new Runnable() {
										
										@Override
										public void run() {
											dispatcherLogger.writeRequest(reqObj, externalMessage);
										}
									});
									asyncDispatcher.send(reqObj, externalMessage);
								}catch(Exception e) {
									LOGGER.error("["+endpointCode+"] Got error while trying to send message to endpoint, cause by: "+e.getMessage());
									e.printStackTrace();
									externalMessage.setSystemResponseCode(GenericResponse.ERROR.getCode());
									externalMessage.setSystemResponseDesc("Failed sending message to endpoint, cause by: "+e.getMessage());
									externalMessage.setResponse();
									message.set(key, externalMessage);
									latch.countDown();
									LOGGER.trace("["+endpointCode+"] Returning to modifier with system response code "+GenericResponse.ERROR.getCode());
								}
							}
						});
						

					//For Synchronous destination endpoint
					else {
						final Object finalReq = reqObj;
						threadPoolTaskExecutor.execute(new Runnable() {
							
							@Override
							public void run() {
								IMap<String, ExternalMessage> message = hazelcastInstance.getMap(CacheName.DEST_ENDPOINT_MSG);
								ExternalMessage request = message.get(key);
								ExternalMessage response = null;
								Object resObj = null;
								try {
									resObj = syncDispatcher.send(finalReq, externalMessage);

								// Error sending to endpoint, send to router, and show the error
								} catch (Exception e) {
									LOGGER.error("["+endpointCode+"] Got error while trying to send message to endpoint, cause by: "+e.getMessage());
									e.printStackTrace();
									request.clearHeaders();
									request.clear();
									request.setSystemResponseCode(GenericResponse.ERROR.getCode());
									request.setSystemResponseDesc("Failed sending message to endpoint, cause by: "+e.getMessage());
									LOGGER.trace("["+endpointCode+"] Returning to modifier with system response code "+GenericResponse.ERROR.getCode());
								}

								if(resObj!=null) {
									LOGGER.trace("["+endpointCode+"] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
									LOGGER.debug("["+endpointCode+"] Receiving response from endpoint: \n"+dispatcherLogger.writeAsString(resObj));
									try {
										response = codec.decode(resObj, packageLoader.getDecodePackager(endpointCode, invocationCode));
									} catch (Exception e) {
										// Error decode, send to router, and show the error
										LOGGER.error("["+endpointCode+"] Failed decoding message, cause by: "+e.getMessage());
										e.printStackTrace();
										request.clearHeaders();
										request.clear();
										request.setSystemResponseCode(GenericResponse.ERROR.getCode());
										request.setSystemResponseDesc("Failed decoding message, cause by: "+e.getMessage());
										LOGGER.trace("["+endpointCode+"] Returning to modifier with system response code "+GenericResponse.ERROR.getCode());
									}
								}

								if(response!=null)
									request.setAll(response);
								
								request.setResponse();
								message.set(key, request);
								latch.countDown();
							}
						});
					}
					
					try {
						latch.trySetCount(1);
						latch.await(timeoutMillis, TimeUnit.MILLISECONDS);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						latch.destroy();
					}
					
					ExternalMessage response = message.remove(key);
					if(response.isResponse()) {
						messageConfigurator.apply(response);
						LOGGER.debug("["+endpointCode+"] Sending response to modifier: \n"+response);
						kafkaTemplate.send("modifier."+endpointCode+"."+invocationCode.replaceAll("[^\\dA-Za-z ]", "_")+".in", response);
					}else {
						// Send external message with timeout response to modifier
						externalMessage.setSystemResponseCode(GenericResponse.TIMEOUT.getCode());
						externalMessage.setSystemResponseDesc(GenericResponse.TIMEOUT.getDesc());
						externalMessage.setResponse();
						messageConfigurator.apply(externalMessage);
						LOGGER.trace("["+endpointCode+"] Endpoint not responding, Returning to modifier with system response code "+GenericResponse.TIMEOUT.getCode());
						kafkaTemplate.send("modifier."+endpointCode+"."+invocationCode.replaceAll("[^\\dA-Za-z ]", "_")+".in", externalMessage);
					}
				}
			}
		});
		internalListener = new ConcurrentMessageListenerContainer<String, Object>(consumerFactory, containerProps);
		internalListener.setConcurrency(concurrency);
		start();
	}
	
	/*
	 * Process for receiving message from outer system
	 * if endpoint messaging type is asynchronous, 
	 * then response object will be send by its dispatcher 
	 * and this method will return null, else
	 * if endpoint messaging type is synchronous, 
	 * then response object will return in this method
	 */
	@SuppressWarnings("rawtypes")
	public Object receive(Object obj, String invocationCode, Date receivedTime, Boolean isRequest) {
		/*
		 * This cache will be populated when object modifier module is started.
		 * Until then the map will always has null value, and return result of 
		 * encode unregistered invocation response
		 */
		
		IMap map = hazelcastInstance.getMap("modifier:invocation:findByEndpointCodeAndInvocationCode");
		if(!map.containsKey(new SimpleKey(endpointCode, invocationCode))) {
			// If no invocation code found, return unknown invocation message
			try {
				return codec.encodeUnknownInvocation(packageLoader.getEncodePackager(endpointCode, invocationCode));
			} catch (Exception e) {
				// DROP MESSAGE
				e.printStackTrace();
			}
		}
		AtomicLong atomicLong;
		if(atomicLongs.get(endpointCode)==null) {
			atomicLong = new AtomicLong();
			atomicLongs.put(endpointCode, atomicLong);
		}else
			atomicLong = atomicLongs.get(endpointCode);
		Long assocKey = atomicLong.incrementAndGet();
		String prettyRaw = receiverLogger.writeAsString(obj);
		
		final ExternalMessage payload;
		try {
			payload = codec.decode(obj, packageLoader.getDecodePackager(endpointCode, invocationCode));
		} catch (CodecProcessException e) {
			// Fault decode, show the error, and return fault object
			LOGGER.error("["+endpointCode+"] Got fault when trying to decode message, cause by: "+e.getMessage());
			e.printStackTrace();
			return e.getFault();
		} catch (Exception e) {
			// Error decode, drop the message, and show the error
			LOGGER.error("["+endpointCode+"] Failed when trying to decode message, cause by: "+e.getMessage());
			e.printStackTrace();
			return null;
		}

		if(isRequest) {
			LOGGER.trace("["+endpointCode+"] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			LOGGER.debug("["+endpointCode+"] Receiving request from endpoint: \n"+prettyRaw+"");
			ExternalMessage externalMessage = new ExternalMessage(endpointCode, node, assocKey, invocationCode);
			externalMessage.setRequest();
			externalMessage.putAll(payload);

			/*
			 * Remove CamelHttpServletRequest because its value is org.apache.catalina.connector.RequestFacade which is not serializeable
			 * Remove CamelHttpServletResponse because its value is org.apache.catalina.connector.ResponseFacade which is not serializeable
			 */
			externalMessage.removeHeader(Exchange.HTTP_SERVLET_REQUEST);
			externalMessage.removeHeader(Exchange.HTTP_SERVLET_RESPONSE);
			
			final ExternalMessage emsg = externalMessage;
			threadPoolTaskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					MessageLog messageLog = new MessageLog();
					messageLog.setTransactionId(emsg.getTransactionId());
					messageLog.setReceived(receivedTime);
					messageLog.setOriginEndpoint(endpointCode);
					messageLog.setInvocationCode(invocationCode);
					messageLogs.setAsync(emsg.getTransactionId(), messageLog, 1, TimeUnit.HOURS);		
					receiverLogger.writeRequest(receivedTime, obj, emsg);
				}
			});
			
			InvocationTimeout invocationTO = invocationTimeoutService.findByEndpointCodeAndInvocationCode(endpointCode, invocationCode);
			Long timeoutMillis = invocationTO!=null?invocationTO.getTimeoutMillis():null;
			CountDownLatch latch = new CountDownLatch(1);
			latches.put(assocKey, latch);
			oriMsg.put(assocKey, externalMessage);
			messageConfigurator.apply(externalMessage);
			LOGGER.debug("["+endpointCode+"] Sending request to modifier: \n"+externalMessage+"");
			LOGGER.trace("["+endpointCode+"] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			kafkaTemplate.send("modifier."+endpointCode+"."+invocationCode.replaceAll("[^\\dA-Za-z ]", "_")+".in", externalMessage);	
			
			/*
			 * Latch is waiting until latch.countDown is called or latch.await runs out
			 * Override default waiting time to 60 seconds from 25 seconds
			 */
			try {
				latch.await((timeoutMillis!=null && timeoutMillis>25000)?timeoutMillis:60000, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				latches.remove(assocKey);
			}
			
			/*
			 * If latch.countDown is called then eMsg must and will be a response.
			 * If latch.await runs out then eMsg will stay be a request
			 * Send to channel with corresponding response if it is a response
			 */
			externalMessage = oriMsg.remove(assocKey);
			if(externalMessage.isResponse()) {
				return processResponse(receivedTime, invocationCode, externalMessage);
			}else {
				// Send external message with timeout response to response mapper
				externalMessage.setSystemResponseCode(GenericResponse.TIMEOUT.getCode());
				externalMessage.setSystemResponseDesc(GenericResponse.TIMEOUT.getDesc());
				externalMessage.setResponse();
				externalMessage.setBody(null);
				LOGGER.trace("["+endpointCode+"] Waiting time is up, send system response code "+GenericResponse.TIMEOUT.getCode()+" to modifier");
				kafkaTemplate.send("mapper.response.in", externalMessage);
				try {
					Thread.sleep(1000L);
					if(oriMsg.get(assocKey)!=null) {
						return processResponse(receivedTime, invocationCode, oriMsg.remove(assocKey));
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					oriMsg.remove(assocKey);
				}
			}
			
		/*
		 * For incoming response from outer system,
		 * put all body response to request emsg from cache and
		 * set keep wait flag to false make the while loop to break.
		 * This block of code will NEVER be executed for destination synchronous endpoint 
		 */
		}else {
			LOGGER.trace("["+endpointCode+"] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			LOGGER.debug("["+endpointCode+"] Receiving response from endpoint: "+prettyRaw);
			IMap<String, ExternalMessage> message = hazelcastInstance.getMap(CacheName.DEST_ENDPOINT_MSG);
			String key = constructKey(invocationCode, payload.getAssocKey());
			ICountDownLatch latch = hazelcastInstance.getCPSubsystem().getCountDownLatch(key);
			ExternalMessage externalMessage = message.get(key);
			externalMessage.putAll(payload);
			message.set(key, externalMessage);

			threadPoolTaskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					receiverLogger.writeResponse(receivedTime, obj, externalMessage);
				}
			});
			
			latch.countDown();
		}
		return null;
	}
	
	private Object processResponse(Date receivedTime, String invocationCode, ExternalMessage externalMessage) {
		messageConfigurator.apply(externalMessage);
		if(externalMessage.getMappedResponseCode()!=null && externalMessage.getResponseCode()==null) {
			LOGGER.trace("["+endpointCode+"] Set response code with mapped value '"+externalMessage.getMappedResponseCode()+"' to field '"+externalMessage.getResponseCodeField()+"' in "+externalMessage.getResponseCodePos());
			externalMessage.setResponseCode(externalMessage.getMappedResponseCode());
		}else
			LOGGER.trace("["+endpointCode+"] Response code exist with value '"+externalMessage.getResponseCode()+"' on field '"+externalMessage.getResponseCodeField()+"' in "+externalMessage.getResponseCodePos());
		if(externalMessage.getMappedResponseDesc()!=null && externalMessage.getResponseDesc()==null) {
			LOGGER.trace("["+endpointCode+"] Set response description with mapped value '"+externalMessage.getMappedResponseDesc()+"' to field '"+externalMessage.getResponseDescField()+"' in "+externalMessage.getResponseDescPos());
			externalMessage.setResponseDesc(externalMessage.getMappedResponseDesc());
		}else
			LOGGER.trace("["+endpointCode+"] Response description exist with value '"+externalMessage.getResponseDesc()+"' on field '"+externalMessage.getResponseDescField()+"' in "+externalMessage.getResponseDescPos());
			
		final Object objRes;
		try {
			objRes = codec.encode(externalMessage, packageLoader.getEncodePackager(endpointCode, invocationCode));
		} catch (Exception e) {
			// Error encode, drop the message, and show the error
			LOGGER.error("["+endpointCode+"] Failed when trying to encode message, cause by: "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		LOGGER.debug("["+endpointCode+"] Sending response to endpoint: \n"+receiverLogger.writeAsString(objRes));
		LOGGER.trace("["+endpointCode+"] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		threadPoolTaskExecutor.execute(new Runnable() {
			
			@Override
			public void run() {
				receiverLogger.writeResponse(new Date(), objRes, externalMessage);

				MessageLog messageLog = messageLogs.remove(externalMessage.getTransactionId());
				Long processTime = new Date().getTime() - receivedTime.getTime();
				messageLog.setProcessTime(processTime);
				messageLog.setResponseCode(externalMessage.getResponseCode());
				messageLog.setResponseDesc(externalMessage.getResponseDesc());
				loggingClient.write(messageLog);
			}
		});
		//For Asynchronous origin endpoint
		if(asyncDispatcher!=null)
			try {
				asyncDispatcher.send(objRes, externalMessage);
			} catch (Exception e) {
				// Got problem while trying to reply to origin endpoint, drop message
				e.printStackTrace();
			}
		//For Synchronous origin endpoint
		else {
			return objRes;
		}
		return null;
	}
	
	public String constructKey(String endpointCode, String assocKey) {
		return endpointCode+"-"+assocKey;
	}
	
	public String constructKey(String endpointCode, Long assocKey) {
		return constructKey(endpointCode, String.valueOf(assocKey));
	}

	public EndpointProcess setHazelcastInstance(HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
		return this;
	}

	public EndpointProcess setInvocationTimeoutService(InvocationTimeoutService invocationTimeoutService) {
		this.invocationTimeoutService = invocationTimeoutService;
		return this;
	}

	public EndpointProcess setAsyncDispatcher(AsynchronousDispatcher asyncDispatcher) {
		this.asyncDispatcher = asyncDispatcher;
		return this;
	}

	public EndpointProcess setSyncDispatcher(SynchronousDispatcher syncDispatcher) {
		this.syncDispatcher = syncDispatcher;
		return this;
	}

	public EndpointProcess setKafkaTemplate(KafkaTemplate<String, Object> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
		return this;
	}
	
	public EndpointProcess setConcurrency(int concurrency) {
		this.concurrency = concurrency;
		return this;
	}

	public EndpointProcess setCodec(AbstractCodec codec) {
		this.codec = codec;
		return this;
	}
	
	public EndpointProcess start() {
		if(!isRunning())
			this.internalListener.start();
		return this;
	}
	
	public EndpointProcess stop() {
		if(isRunning())
			this.internalListener.stop();
		return this;
	}
	
	public EndpointProcess pause() {
		if(!isPaused() && !this.internalListener.isPauseRequested())
			this.internalListener.pause();
		return this;
	}
	
	public EndpointProcess resume() {
		if(isPaused())
			this.internalListener.resume();
		return this;
	}
	
	public Boolean isPaused() {
		return this.internalListener.isContainerPaused();
	}
	
	public Boolean isRunning() {
		return this.internalListener.isRunning();
	}
	
	public EndpointProcess setListenerFactory(ConsumerFactory<String, Object> consumerFactory) {
		this.consumerFactory = consumerFactory;
		return this;
	}
	
	public ConcurrentMessageListenerContainer<String, Object> getListener() {
		return this.internalListener;
	}
	
	public EndpointProcess setPackageLoader(GenericPackageLoader packageLoader) {
		this.packageLoader = packageLoader;
		return this;
	}
	
	/*
	 * ThreadPoolTaskExecutor need to be set for asynchronous endpoint
	 */
	public EndpointProcess setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
		return this;
	}
	
	public int trafficSize() {
		return oriMsg.size()+destMsg.size();
	}
	
	public Boolean isIdle() {
		if(trafficSize()<1)
			return true;
		else
			return false;
	}
}
