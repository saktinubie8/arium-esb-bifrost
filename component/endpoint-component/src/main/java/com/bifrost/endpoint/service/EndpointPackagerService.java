package com.bifrost.endpoint.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.model.EndpointPackager;
import com.bifrost.endpoint.repository.EndpointPackagerRepository;
import com.bifrost.system.model.EtcFile;
import com.bifrost.system.repository.EtcFileRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Service
public class EndpointPackagerService extends GenericService<EndpointPackagerRepository, EndpointPackager, Long>{

	private static final String KEY = "endpoint:packager";
	
	@Autowired
	private EtcFileRepository fileRepository;
	
	public EndpointPackagerService() {
		super(KEY);
	}

	@SuppressWarnings("unchecked")
	public List<EndpointPackager> findByEndpointCode(String endpointCode) {
		return (List<EndpointPackager>) hazelcastInstance.getMap(KEY+":findByEndpointCode").get(endpointCode);
	}

	public EndpointPackager findByEndpointCodeAndFileName(String endpointCode, String fileName) {
		return (EndpointPackager) hazelcastInstance.getMap(KEY+":findByEndpointCodeAndFileName").get(new SimpleKey(endpointCode, fileName));
	}
	
	@CachePut(cacheNames = KEY+":findByEndpointCodeAndFileName", key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpoint.code, #t.fileName)")
	public EndpointPackager save(EndpointPackager t) {
		EndpointPackager retVal = super.save(t);
		String endpointCode = retVal.getEndpoint().getCode();
		IMap<String, List<EndpointPackager>> findByEndpointCode = hazelcastInstance.getMap(KEY+":findByEndpointCode");
		findByEndpointCode.set(endpointCode, repository.findByEndpoint_Code(endpointCode));
		return retVal;
	}

	@Caching(evict = {
		@CacheEvict(cacheNames = KEY+":findByEndpointCode", key = "#t.endpoint.code"),
		@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndFileName", key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpoint.code, #t.fileName)")
	})
	public void delete(EndpointPackager t) {
		super.delete(t);
	}
	
	@Caching(evict = {
		@CacheEvict(cacheNames = KEY+":findByEndpointCode", allEntries = true),
		@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndFileName", allEntries = true)
	})
	public void evictAll() {
		super.evictAll();
	}

	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, List<EndpointPackager>> findByEndpointCode = hazelcastInstance.getMap(KEY+":findByEndpointCode");
		IMap<SimpleKey, EndpointPackager> findByEndpointCodeAndFileName = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndFileName");
		for(EndpointPackager t : findAll()) {
			if(findByEndpointCode.get(t.getEndpoint().getCode())==null)
				findByEndpointCode.set(t.getEndpoint().getCode(), repository.findByEndpoint_Code(t.getEndpoint().getCode()));
			EtcFile file = fileRepository.findById(t.getFile().getId()).get();
			SimpleKey key = new SimpleKey(t.getEndpoint().getCode(), file.getName());
			if(findByEndpointCodeAndFileName.get(key)==null)
				findByEndpointCodeAndFileName.set(key, t);
		}
	}
}
