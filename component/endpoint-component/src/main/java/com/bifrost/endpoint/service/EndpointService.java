package com.bifrost.endpoint.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.CacheName;
import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.repository.EndpointRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointService extends GenericService<EndpointRepository, Endpoint, String>{

	private static final String KEY = "endpoint";
	
	public EndpointService() {
		super(KEY);
	}

	@SuppressWarnings("unchecked")
	public List<Endpoint> findByType(String type) {
		return (List<Endpoint>) hazelcastInstance.getMap(KEY+":findByType").get(type);
	}
	
	public Endpoint save(Endpoint t) {
		Endpoint retVal = super.save(t); 
		String type = retVal.getType().getType();
		IMap<String, List<Endpoint>> findByType = hazelcastInstance.getMap(KEY+":findByType");
		findByType.set(type, repository.findByType_type(type));
		return retVal;
	}

	@CacheEvict(cacheNames = KEY+":findByType", key = "#t.type.type")
	public void delete(Endpoint t) {
		super.delete(t);
	}
	
	@CacheEvict(cacheNames = KEY+":findByType", allEntries = true)
	public void evictAll() {
		super.evictAll();
	}

	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, List<Endpoint>> findByType = hazelcastInstance.getMap(KEY+":findByType");
		for(Endpoint t : findAll())
			if(findByType.get(t.getType().getType())==null)
				findByType.set(t.getType().getType(), repository.findByType_type(t.getType().getType()));
	}

	public Long getAtomicLong(String endpointCode) {
		return hazelcastInstance.getCPSubsystem().getAtomicLong(CacheName.ENDPOINT_ASSOC_KEY_COUNT(endpointCode)).getAndIncrement();
	}
	
	// Reset one endpoint association key accumulation
	public void resetAssocKey(String endpointCode) {
		hazelcastInstance.getCPSubsystem().getAtomicLong(CacheName.ENDPOINT_ASSOC_KEY_COUNT(endpointCode)).set(1);
	}
	
	// Reset all endpoint association key accumulation
	@Scheduled(cron = "0 0 24 * * ?")
	public void resetAssocKey() {
		for(Endpoint endpoint : findAll())
			resetAssocKey(endpoint.getCode());
	}
}
