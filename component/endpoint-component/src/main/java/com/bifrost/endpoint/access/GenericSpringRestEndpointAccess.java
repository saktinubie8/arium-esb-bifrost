package com.bifrost.endpoint.access;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.endpoint.AbstractController;
import com.bifrost.endpoint.json.response.EndpointPackagerResponse;
import com.bifrost.endpoint.json.response.EndpointServiceResponse;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.model.EndpointPackager;
import com.bifrost.endpoint.service.EndpointPackagerService;
import com.bifrost.endpoint.service.EndpointService;

/**
 * A class which open up services to start, stop, and reload endpoint.
 * Use this class if your program uses springboot rest as its servlet
 * 
 * @author rosaekapratama@gmail.com
 */
public class GenericSpringRestEndpointAccess {

	@Autowired
	private AbstractController controller;
	
	@Autowired
	private EndpointService endpointService;
	
	@Autowired
	private EndpointPackagerService endpointPackagerService;
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/start", method = RequestMethod.GET)
	public List<EndpointServiceResponse> startAll() {
		return controller.startAll();
	}
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/start", method = RequestMethod.GET)
	public List<EndpointServiceResponse> start(@PathVariable("endpointCode")String endpointCode) {
		return Arrays.asList(new EndpointServiceResponse[] {controller.start(endpointCode)});
	}

	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/stop", method = RequestMethod.GET)
	public List<EndpointServiceResponse> stopAll() {
		return controller.stopAll();
	}
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/stop", method = RequestMethod.GET)
	public List<EndpointServiceResponse> stop(@PathVariable("endpointCode")String endpointCode) {
		return Arrays.asList(new EndpointServiceResponse[] {controller.stop(endpointCode)});
	}


	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/restart", method = RequestMethod.GET)
	public List<EndpointServiceResponse> restartAll() {
		return controller.restartAll();
	}
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/restart", method = RequestMethod.GET)
	public List<EndpointServiceResponse> restart(@PathVariable("endpointCode")String endpointCode) {
		return Arrays.asList(new EndpointServiceResponse[] {controller.restart(endpointCode)});
	}


	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/reload", method = RequestMethod.GET)
	public List<EndpointServiceResponse> reloadAll() {
		return controller.reloadAll();
	}
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/reload", method = RequestMethod.GET)
	public List<EndpointServiceResponse> reload(@PathVariable("endpointCode")String endpointCode) {
		return Arrays.asList(new EndpointServiceResponse[] {controller.reload(endpointCode)});
	}
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/packager/save", method = RequestMethod.POST)
	public GenericResponse savePackager(@PathVariable("endpointCode")String endpointCode, @RequestBody EndpointPackager request) {
		Endpoint endpoint = endpointService.findById(endpointCode);
		if(endpoint == null)
			return EndpointPackagerResponse.UNKNOWN_ENDPOINT_CODE;
		EndpointPackager packager = endpointPackagerService.findByEndpointCodeAndFileName(endpointCode, request.getFile().getName());
		if(packager != null)
			return EndpointPackagerResponse.PACKAGER_EXISTS;
		endpointPackagerService.save(request);
		return EndpointPackagerResponse.SUCCESS;
	}
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/packager/update", method = RequestMethod.POST)
	public GenericResponse updatePackager(@PathVariable("endpointCode")String endpointCode, @RequestBody EndpointPackager request) {
		Endpoint endpoint = endpointService.findById(endpointCode);
		if(endpoint == null)
			return EndpointPackagerResponse.UNKNOWN_ENDPOINT_CODE;
		endpointPackagerService.save(request);
		return EndpointPackagerResponse.SUCCESS;
	}
}
