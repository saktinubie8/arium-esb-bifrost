package com.bifrost.endpoint.json.response;

import com.bifrost.common.json.response.GenericResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class EndpointServiceResponse extends GenericResponse {

	@JsonInclude(value = Include.NON_NULL)
	private String endpointCode;

	@JsonInclude(value = Include.NON_NULL)
	private String node;

	public EndpointServiceResponse(String endpointCode, String node, String responseCode, String responseDesc) {
		super(responseCode, responseDesc);
		this.endpointCode = endpointCode;
		this.node = node;
	}

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public EndpointServiceResponse(String code, String desc) {
		super(code, desc);
	}
}
