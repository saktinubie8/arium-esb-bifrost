package com.bifrost.endpoint.logger;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.beans.factory.annotation.Autowired;

import com.bifrost.endpoint.logger.AbstractReceiverLogger;
import com.bifrost.logging.LoggingClient;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class ServletReceiverLogger implements AbstractReceiverLogger {

	@Autowired
	private LoggingClient loggingClient;
	
	@Override
	public void writeRequest(Date writeTime, Object request, ExternalMessage externalMessage) {
		Message message = (Message) request;
		RouteLog logIn = new RouteLog();
		logIn.setTransactionId(externalMessage.getTransactionId());
		logIn.setDateTime(writeTime);
		logIn.setEndpointCode(externalMessage.getEndpointCode());
		logIn.setInvocationCode(externalMessage.getInvocationCode());
		logIn.setRequest();
		logIn.setRaw(writeAsString(message));
		logIn.setFormattedRaw(externalMessage);
		loggingClient.write(logIn);
	}

	@Override
	public void writeResponse(Date writeTime, Object response, ExternalMessage externalMessage) {
		Message message = (Message) response;
		RouteLog logOut = new RouteLog();
		logOut.setTransactionId(externalMessage.getTransactionId());
		logOut.setDateTime(writeTime);
		logOut.setEndpointCode(externalMessage.getEndpointCode());
		logOut.setInvocationCode(externalMessage.getInvocationCode());
		logOut.setResponse();
		logOut.setRaw(writeAsString(message));
		logOut.setFormattedRaw(externalMessage);
		loggingClient.write(logOut);
	}

	@Override
	public String writeAsString(Object obj) {
		Message message = (Message) obj;
		HttpServletRequest request = null;
		if(message.getHeader(Exchange.HTTP_SERVLET_REQUEST)!=null)
			request = (HttpServletRequest) message.getHeader(Exchange.HTTP_SERVLET_REQUEST);
		StringBuffer result = new StringBuffer();
		if(request!=null) {
			result.append("Client: ");
			result.append(request.getRemoteAddr()==null?"null":request.getRemoteAddr());
			result.append("\nAddress: ");
			result.append(request.getRequestURL());
			result.append("\nEncoding: ");
			result.append(request.getCharacterEncoding());
			result.append("\nHttp-Method: ");
			result.append(request.getMethod());
			result.append("\nContent-Type: ");
			result.append(request.getContentType());	
		}else {
			result.append("Http-Code: ");
			result.append(message.getHeader(Exchange.HTTP_RESPONSE_CODE));
		}
		result.append("\nHeaders: {");
		Map<String, Object> headers = message.getHeaders();
		for(Entry<String, Object> entry : headers.entrySet()) {
			result.append(entry.getKey());
			result.append("=[");
			result.append(entry.getValue());
			result.append("]");
			result.append(", ");
		}
		if(headers.size()>0) {
			result.delete(result.length()-2, result.length());
			result.append("}");
		}
		result.append("\nPayload: ");
		Object body = message.getBody();
		if(body!=null && body instanceof String && !((String)body).isBlank())
			result.append(message.getBody());
		else if(body!=null)
			result.append(new String((byte[])message.getBody(), Charset.forName("UTF8")));
		return result.toString();
	}
}
