package com.bifrost.endpoint.dispatcher;

import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface AsynchronousDispatcher {

	public void send(Object obj, ExternalMessage externalMessage) throws Exception;
	
}
