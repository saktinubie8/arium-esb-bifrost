package com.bifrost.endpoint.packager;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class PackagerMap extends ConcurrentHashMap<String, Object> {
	
	private static final long serialVersionUID = 251177951893028782L;
	
	public String constructKey(String endpointCode, String invocationCode) {
		return endpointCode+"/"+invocationCode;
	}

}
