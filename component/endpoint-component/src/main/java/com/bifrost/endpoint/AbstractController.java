package com.bifrost.endpoint;

import java.net.UnknownHostException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.expression.ExpressionParser;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.endpoint.json.response.EndpointServiceResponse;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.service.EndpointService;
import com.bifrost.endpoint.service.EndpointStatusManager;
import com.bifrost.system.configuration.AbstractSystemIdentity;
import com.bifrost.system.service.EtcFileService;

/**
 * An interface which has standard service operation
 * 
 * @author rosaekapratama@gmail.com
 */
public abstract class AbstractController implements SchedulingConfigurer {
	
	protected static final String RECEIVED_TIME = "receivedTime";
	protected static String node;
	
	@Autowired
	protected EtcFileService fileService;
	
	@Autowired
	protected EndpointService endpointService;
	
	@Autowired
	protected ExpressionParser expressionParser;
	
	@Autowired
	protected ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	protected ThreadPoolTaskScheduler taskScheduler;
	
	@Autowired
	protected AbstractSystemIdentity systemIdentity;

	@Autowired
	protected EndpointStatusManager endpointStatusManager;
	
	@Value("${"+ParamConstant.Bifrost.ENDPOINT_STATUS_CACHE_TTL+":3600000}")
	protected Long cacheTtl;
	
	public abstract EndpointServiceResponse start(String endpointCode);
	public abstract EndpointServiceResponse stop(String endpointCode);
	public abstract EndpointServiceResponse restart(String endpointCode);
	public abstract EndpointServiceResponse reload(String endpointCode);
	public abstract List<EndpointServiceResponse> startAll();
	public abstract List<EndpointServiceResponse> stopAll();
	public abstract List<EndpointServiceResponse> restartAll();
	public abstract List<EndpointServiceResponse> reloadAll();
	public abstract List<Endpoint> getAllEndpoint();
	
	@PostConstruct
	public void init() throws UnknownHostException {
		node = systemIdentity.getHost();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					reload(endpoint.getCode());
				}
			});
	}
	
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(taskScheduler);
		taskRegistrar.addFixedRateTask(new Runnable() {
			
			@Override
			public void run() {
				for(Endpoint endpoint : getAllEndpoint())
					endpointStatusManager.refresh(endpoint.getCode(), node);
			}
		}, cacheTtl);
	}
}
