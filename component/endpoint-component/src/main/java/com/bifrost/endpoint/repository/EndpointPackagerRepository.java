package com.bifrost.endpoint.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.model.EndpointPackager;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Transactional
public interface EndpointPackagerRepository extends JpaRepository<EndpointPackager, Long>{

	public List<EndpointPackager> findByEndpoint_Code(String endpointCode);
	public EndpointPackager findByEndpoint_CodeAndFile_Name(String endpointCode, String fileName);
	
}
