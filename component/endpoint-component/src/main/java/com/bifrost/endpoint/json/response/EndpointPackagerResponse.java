package com.bifrost.endpoint.json.response;

import com.bifrost.common.json.response.GenericResponse;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class EndpointPackagerResponse extends GenericResponse {

	public static final GenericResponse UNKNOWN_ENDPOINT_CODE = new GenericResponse("SYSUEC", "UNKNOWN ENDPOINT CODE");
	public static final GenericResponse PACKAGER_EXISTS = new GenericResponse("SYSPEX", "PACKAGER EXISTS");
	
}
