package com.bifrost.endpoint.rest;

import java.io.ByteArrayInputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class RestTemplateFactory extends ConcurrentHashMap<String, RestTemplate> {

	private static final long serialVersionUID = 1048115224782678022L;
	private static final Log LOGGER = LogFactory.getLog(RestTemplateFactory.class);
	private static final Map<String, PoolingHttpClientConnectionManager> connectionManagers = new ConcurrentHashMap<String, PoolingHttpClientConnectionManager>();
	private static final Map<String, Integer> closeIdleMillisMap = new ConcurrentHashMap<String, Integer>();
	
	private CloseableHttpClient createHttpClient(String endpointCode, SSLConnectionSocketFactory sslsf, int maxCon, int keepAliveMillis, int reqTOMillis, int conTOMillis, int socTOMillis) {
	    Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
	    	.register("https", sslsf)
	    	.register("http", new PlainConnectionSocketFactory())
	    	.build();
	    
		PoolingHttpClientConnectionManager poolingConnectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        poolingConnectionManager.setMaxTotal(maxCon);
        
        ConnectionKeepAliveStrategy conKeepAliveStrategy = new ConnectionKeepAliveStrategy() {
			
			@Override
			public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
				HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
                while (it.hasNext()) {
                    HeaderElement he = it.nextElement();
                    String param = he.getName();
                    String value = he.getValue();
 
                    if (value != null && param.equalsIgnoreCase("timeout")) {
                        return Long.parseLong(value) * 1000;
                    }
                }
                return keepAliveMillis;
			}
		};
		
		RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(reqTOMillis)
                .setConnectTimeout(conTOMillis)
                .setSocketTimeout(socTOMillis).build();
 
        CloseableHttpClient httpClient =  HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(poolingConnectionManager)
                .setKeepAliveStrategy(conKeepAliveStrategy)
                .build();

        connectionManagers.put(endpointCode, poolingConnectionManager);
        return httpClient;
	}
	
	public void createTrustAllRestTemplate(String endpointCode, int maxCon, int keepAliveMillis, int closeIdleMillis, int reqTOMillis, int conTOMillis, int socTOMillis) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		closeIdleMillisMap.put(endpointCode, closeIdleMillis);
		TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
	    SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
	    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
	    HttpComponentsClientHttpRequestFactory requestFactory =  new HttpComponentsClientHttpRequestFactory(createHttpClient(endpointCode, sslsf, maxCon, keepAliveMillis, reqTOMillis, conTOMillis, socTOMillis));
		put(endpointCode, new RestTemplate(requestFactory));
		LOGGER.trace("["+endpointCode+"] Created trust all rest template");
	}
	
	public void createTrustCertRestTemplate(String endpointCode, byte[] cert, int maxCon, int keepAliveMillis, int closeIdleMillis, int reqTOMillis, int conTOMillis, int socTOMillis) throws Exception {
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		keyStore.load(null, null);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		ByteArrayInputStream bais = new ByteArrayInputStream(cert);
		Certificate certs =  cf.generateCertificate(bais);
		keyStore.setCertificateEntry(endpointCode, certs);

	    TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
	    KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
	    tmf.init(keyStore);
	    TrustManager[] tm = tmf.getTrustManagers();		
	    kmf.init(keyStore, null);
	    KeyManager[] km = kmf.getKeyManagers();
	    SSLContext sslContext = SSLContext.getInstance("TLS");
	    sslContext.init(km, tm, null);
	    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext);
	    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(createHttpClient(endpointCode, sslsf, maxCon, keepAliveMillis, reqTOMillis, conTOMillis, socTOMillis));
	    put(endpointCode, new RestTemplate(factory));
		LOGGER.trace("["+endpointCode+"] Created trust cert rest template");
	}
	
	@Scheduled(fixedDelay = 10000)
	private void closeIdleConnection() {
		for(Entry<String, PoolingHttpClientConnectionManager> entry : connectionManagers.entrySet()) {
			String endpointCode = entry.getKey();
			PoolingHttpClientConnectionManager connectionManager = entry.getValue();
			try {
				LOGGER.trace("["+endpointCode+"] Closing expired and idle connections...");
				connectionManager.closeExpiredConnections();
				connectionManager.closeIdleConnections(closeIdleMillisMap.get(endpointCode), TimeUnit.MILLISECONDS);
            } catch (Exception e) {
                LOGGER.trace("["+endpointCode+"] Exception occured in idle connection monitor, cause by: "+e.getMessage());
                e.printStackTrace();
            }
		}
	}
}
