package com.bifrost.endpoint.access;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.endpoint.AbstractController;
import com.bifrost.endpoint.json.response.EndpointPackagerResponse;
import com.bifrost.endpoint.json.response.EndpointServiceResponse;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.model.EndpointPackager;
import com.bifrost.endpoint.service.EndpointPackagerService;
import com.bifrost.endpoint.service.EndpointService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A class which open up services to start, stop, and reload endpoint.
 * Use this class if your program uses camel rest as its servlet
 * 
 * @author rosaekapratama@gmail.com
 */
public class GenericCamelRestEndpointAccess {

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private AbstractController controller;
	
	@Autowired
	private EndpointService endpointService;
	
	@Autowired
	private EndpointPackagerService endpointPackagerService;
	
	@PostConstruct
	public void init() throws Exception {
		if(camelContext.getStatus().isStopped())
			camelContext.start();
		RouteBuilder route = new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				restConfiguration("servlet");
				rest()
					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/start")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
					
						@Override
						public void process(Exchange exchange) throws Exception {
							exchange.getMessage().setBody(controller.startAll());
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()
				
					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/start")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							String endpointCode = (String) exchange.getMessage().getHeader("endpointCode");
							exchange.getMessage().setBody(Arrays.asList(new EndpointServiceResponse[] {controller.start(endpointCode)}));
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()

					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/stop")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							exchange.getMessage().setBody(controller.stopAll());
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()
					
					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/stop")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							String endpointCode = (String) exchange.getMessage().getHeader("endpointCode");
							exchange.getMessage().setBody(Arrays.asList(new EndpointServiceResponse[] {controller.stop(endpointCode)}));
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()

					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/restart")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							exchange.getMessage().setBody(controller.restartAll());
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()
					
					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/restart")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							String endpointCode = (String) exchange.getMessage().getHeader("endpointCode");
							exchange.getMessage().setBody(Arrays.asList(new EndpointServiceResponse[] {controller.restart(endpointCode)}));
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()

					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/reload")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							exchange.getMessage().setBody(controller.reloadAll());
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()
					
					.get(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/reload")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							String endpointCode = (String) exchange.getMessage().getHeader("endpointCode");
							exchange.getMessage().setBody(Arrays.asList(new EndpointServiceResponse[] {controller.reload(endpointCode)}));
							exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest()
				
					.post(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/packager/save")
					.consumes(MediaType.APPLICATION_JSON_VALUE)
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
					
						@Override
						public void process(Exchange exchange) throws Exception {
							String endpointCode = (String) exchange.getMessage().getHeader("endpointCode");
							Endpoint endpoint = endpointService.findById(endpointCode);
							if(endpoint == null) {
								exchange.getMessage().setBody(EndpointPackagerResponse.UNKNOWN_ENDPOINT_CODE);
								exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
								exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
							}else {
								EndpointPackager request = (EndpointPackager) exchange.getMessage().getBody();
								EndpointPackager packager = endpointPackagerService.findByEndpointCodeAndFileName(endpointCode, request.getFile().getName());
								if(packager != null) {
									exchange.getMessage().setBody(EndpointPackagerResponse.PACKAGER_EXISTS);
									exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
									exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
								}else {
									endpointPackagerService.save(request);
									exchange.getMessage().setBody(EndpointPackagerResponse.SUCCESS);
									exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
									exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
								}
							}
						}
					}).endRest()

					.post(PathConstant.Rest.SERVICE_ENDPOINT_PREFIX+"/{endpointCode}/packager/update")
					.consumes(MediaType.APPLICATION_JSON_VALUE)
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
					
						@Override
						public void process(Exchange exchange) throws Exception {
							String endpointCode = (String) exchange.getMessage().getHeader("endpointCode");
							Endpoint endpoint = endpointService.findById(endpointCode);
							if(endpoint == null) {
								exchange.getMessage().setBody(EndpointPackagerResponse.UNKNOWN_ENDPOINT_CODE);
								exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
								exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
							}else {
								EndpointPackager request = (EndpointPackager) exchange.getMessage().getBody();
								endpointPackagerService.save(request);
								exchange.getMessage().setBody(EndpointPackagerResponse.SUCCESS);
								exchange.getMessage().setBody(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(exchange.getMessage().getBody()));
								exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
							}
						}
					}).endRest().setId("endpoint:access");
				
			}
		};
		camelContext.addRoutes(route);
	}
}
