package com.bifrost.endpoint.model;

import java.io.IOException;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.system.model.EtcFile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint_packager")
public class EndpointPackager implements AbstractModel {

	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "endpoint_code", nullable = false)
	private Endpoint endpoint;

	@Column(name = "invocation_code", nullable = true, length = 255)
	private String invocationCode;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "file", nullable = false)
	private EtcFile file;

	@Column(name = "note", nullable = true, length = 255)
	private String note;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}
	
	public EtcFile getFile() {
		return file;
	}

	public void setFile(EtcFile file) {
		this.file = file;
	}
	
	public String getInvocationCode() {
		return invocationCode;
	}

	public void setInvocationCode(String invocationCode) {
		this.invocationCode = invocationCode;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public Object getPk() {
		return id;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(endpoint.getCode());
		if(invocationCode!=null) {
			out.writeBoolean(true);
			out.writeUTF(invocationCode);	
		}else
			out.writeBoolean(false);
		out.writeLong(file.getId());
		if(note!=null) {
			out.writeBoolean(true);
			out.writeUTF(note);
		}else
			out.writeBoolean(false);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		endpoint = new Endpoint();
		endpoint.setCode(in.readUTF());
		if(in.readBoolean())
			invocationCode = in.readUTF();
		if(file==null)
			file = new EtcFile();
		file.setId(in.readLong());
		if(in.readBoolean())
			note = in.readUTF();
	}

}
