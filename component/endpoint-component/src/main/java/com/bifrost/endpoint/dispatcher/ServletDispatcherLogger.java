package com.bifrost.endpoint.dispatcher;

import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import com.bifrost.endpoint.logger.AbstractDispatcherLogger;
import com.bifrost.logging.LoggingClient;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class ServletDispatcherLogger implements AbstractDispatcherLogger {

	@Autowired
	private LoggingClient loggingClient;

	public void writeRequest(Object request, ExternalMessage externalMessage) {
		RequestEntity<?> entity = (RequestEntity<?>) request;
		RouteLog logOut = new RouteLog();
		logOut.setTransactionId(externalMessage.getTransactionId());
		logOut.setDateTime(new Date());
		logOut.setEndpointCode(externalMessage.getEndpointCode());
		logOut.setInvocationCode(externalMessage.getInvocationCode());
		logOut.setRequest();
		logOut.setRaw(writeAsString(entity));
		logOut.setFormattedRaw(externalMessage);
		loggingClient.write(logOut);
	}

	public void writeResponse(Object response, ExternalMessage externalMessage) {
		RouteLog logIn = new RouteLog();
		logIn.setTransactionId(externalMessage.getTransactionId());
		logIn.setDateTime(new Date());
		logIn.setEndpointCode(externalMessage.getEndpointCode());
		logIn.setInvocationCode(externalMessage.getInvocationCode());
		logIn.setResponse();
		if(response instanceof String)
			logIn.setRaw((String) response);
		else 
			logIn.setRaw(writeAsString((ResponseEntity<?>) response));
		logIn.setFormattedRaw(externalMessage);
		loggingClient.write(logIn);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String writeAsString(Object message) {
		HttpEntity<?> entity = (HttpEntity<?>) message;
		HttpHeaders headers = entity.getHeaders();
		StringBuffer result = new StringBuffer();
		if(entity instanceof RequestEntity) {
			result.append("Address: ");
			result.append(((RequestEntity) entity).getUrl().toString().replace("//", "/").replace(":/", "://"));
			result.append("\nHttp-Method: ");
			result.append(((RequestEntity) entity).getMethod().name());	
		}else if(entity instanceof ResponseEntity) {
			result.append("Http-Code: ");
			result.append(((ResponseEntity) entity).getStatusCode());
		}
		if(headers!=null) {
			if(headers.getContentType()!=null) {
				result.append("\nContent-Type: ");
				result.append(headers.getContentType().toString());	
			}
			result.append("\nHeaders: {");
			for(Entry<String, List<String>> entry : headers.entrySet()) {
				result.append(entry.getKey());
				result.append("=");
				result.append(entry.getValue());
				result.append("");
				result.append(", ");
			}
			if(headers.size()>0)
				result.delete(result.length()-2, result.length());
			result.append("}");
		}
		if(entity!=null && entity.getBody()!=null) {
			result.append("\nPayload: ");
			Object body = entity.getBody();
			if(body!=null && body instanceof byte[])
				result.append(new String((byte[]) entity.getBody()));
			else if(body!=null)
				result.append(entity.getBody());
		}
		return result.toString();
	}

}
