package com.bifrost.endpoint.dispatcher;

import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface SynchronousDispatcher {

	public abstract Object send(Object obj, ExternalMessage externalMessage) throws Exception;

}
