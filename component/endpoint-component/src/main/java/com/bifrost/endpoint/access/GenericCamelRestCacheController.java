package com.bifrost.endpoint.access;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.common.json.response.CacheResponse;
import com.bifrost.common.json.response.GenericResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A class which open up services to start, stop, and reload endpoint.
 * Use this class if your program uses camel rest as its servlet
 * 
 * @author rosaekapratama@gmail.com
 */
public class GenericCamelRestCacheController extends GenericSpringRestCacheController {

	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Override
	@PostConstruct
	public GenericResponse refreshAllCache() {
		if(camelContext.getStatus().isStopped())
			try {
				camelContext.start();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
		RouteBuilder route = new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				restConfiguration("servlet");
				rest()
					.get(PathConstant.Rest.SYSTEM_CACHE+"/all/refresh")
					.produces(MediaType.APPLICATION_JSON_VALUE)
					.route().process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							refreshAll();
							exchange.getMessage().setBody(mapper.writeValueAsString(CacheResponse.SUCCESS));
							exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
						}
					}).endRest();	
			}
		};
		try {
			camelContext.addRoutes(route);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
}
