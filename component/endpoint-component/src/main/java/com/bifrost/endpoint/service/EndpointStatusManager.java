package com.bifrost.endpoint.service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.CacheName;
import com.bifrost.common.constant.ParamConstant;
import com.bifrost.logging.LoggingClient;
import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.model.EndpointStatus;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */

// Key of endpoint status cache format is as follow '[endpoint code]:[node]'
@Service
public class EndpointStatusManager {
	
	private IMap<String, String> statusMap;
	
	@Autowired
	private HazelcastInstance instance;
	
	@Autowired
	private LoggingClient loggingClient;
	
	@Value("${"+ParamConstant.Bifrost.ENDPOINT_STATUS_CACHE_TTL+":3600000}")
	private Long cacheTtl;
	
	@PostConstruct
	public void init() {
		statusMap = instance.getMap(CacheName.ENDPOINT_STATUS);
	}

	public void setOnline(String endpointCode, String node, String info) {
		if(isOnline(endpointCode, node))
			return;
		statusMap.setAsync(EndpointStatus.constructKey(endpointCode, node), EndpointStatus.ONLINE, cacheTtl, TimeUnit.MILLISECONDS);
		loggingClient.write(new EndpointLog(new Date(), endpointCode, node, EndpointStatus.ONLINE, info));
	}	

	public void setOffline(String endpointCode, String node, String info) {
		if(isOffline(endpointCode, node))
			return;
		statusMap.setAsync(EndpointStatus.constructKey(endpointCode, node), EndpointStatus.OFFLINE, cacheTtl, TimeUnit.MILLISECONDS);
		loggingClient.write(new EndpointLog(new Date(), endpointCode, node, EndpointStatus.OFFLINE, info));
	}

	public void setConnected(String endpointCode, String node, String info) {
		if(isOnline(endpointCode, node))
			return;
		statusMap.setAsync(EndpointStatus.constructKey(endpointCode, node), EndpointStatus.CONNECTED, cacheTtl, TimeUnit.MILLISECONDS);
		loggingClient.write(new EndpointLog(new Date(), endpointCode, node, EndpointStatus.CONNECTED, info));
	}	

	public void setDisconnected(String endpointCode, String node, String info) {
		if(isOnline(endpointCode, node))
			return;
		statusMap.setAsync(EndpointStatus.constructKey(endpointCode, node), EndpointStatus.DISCONNECTED, cacheTtl, TimeUnit.MILLISECONDS);
		loggingClient.write(new EndpointLog(new Date(), endpointCode, node, EndpointStatus.DISCONNECTED, info));
	}	

	public void setError(String endpointCode, String node, String info) {
		if(isError(endpointCode, node))
			return;
		statusMap.setAsync(EndpointStatus.constructKey(endpointCode, node), EndpointStatus.ERROR, cacheTtl, TimeUnit.MILLISECONDS);
		loggingClient.write(new EndpointLog(new Date(), endpointCode, node, EndpointStatus.ERROR, info));
	}
	
	public Boolean isOnline(String endpointCode, String node) {
		String status = statusMap.get(EndpointStatus.constructKey(endpointCode, node));
		if(status!=null && status.equals(EndpointStatus.ONLINE))
			return true;
		else
			return false;
	}
	
	public Boolean isOffline(String endpointCode, String node) {
		String status = statusMap.get(EndpointStatus.constructKey(endpointCode, node));
		if(status!=null && status.equals(EndpointStatus.OFFLINE))
			return true;
		else
			return false;
	}
	
	public Boolean isConnected(String endpointCode, String node) {
		String status = statusMap.get(EndpointStatus.constructKey(endpointCode, node));
		if(status!=null && status.equals(EndpointStatus.CONNECTED))
			return true;
		else
			return false;
	}
	
	public Boolean isDisconnected(String endpointCode, String node) {
		String status = statusMap.get(EndpointStatus.constructKey(endpointCode, node));
		if(status!=null && status.equals(EndpointStatus.DISCONNECTED))
			return true;
		else
			return false;
	}
	
	public Boolean isError(String endpointCode, String node) {
		String status = statusMap.get(EndpointStatus.constructKey(endpointCode, node));
		if(status!=null && status.equals(EndpointStatus.ERROR))
			return true;
		else
			return false;
	}
	
	public void refresh(String endpointCode, String node) {
		String key = EndpointStatus.constructKey(endpointCode, node);
		String value = statusMap.get(key);
		statusMap.setAsync(key, value, cacheTtl, TimeUnit.MILLISECONDS);
	}
}
