package com.bifrost.endpoint.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint")
public class Endpoint implements AbstractModel {

	@Id
	@Column(name = "code", nullable = false, length = 20)
	private String code;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "type", nullable = false)
	private EndpointType type;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public EndpointType getType() {
		return type;
	}

	public void setType(EndpointType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Object getPk() {
		return code;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(code);
		out.writeUTF(description);
		out.writeUTF(type.getType());
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		code = in.readUTF();
		description = in.readUTF();
		type = new EndpointType();
		type.setType(in.readUTF());
	}
}
