package com.bifrost.endpoint.packager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.nucleus8583.core.MessageSerializer;
import org.springframework.beans.factory.annotation.Autowired;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.endpoint.model.EndpointPackager;
import com.bifrost.endpoint.service.EndpointPackagerService;
import com.bifrost.exception.endpoint.UnsupportedPackagerException;
import com.bifrost.system.model.EtcFile;
import com.bifrost.system.service.EtcFileService;

/**
 * @author rosaekapratama@gmail.com
 *
 */

public class GenericPackageLoader {
	
	protected Log LOGGER = LogFactory.getLog(GenericPackageLoader.class);

	@Autowired
	protected PackagerMap packagers;
	
	@Autowired
	protected EtcFileService fileService;
	
	@Autowired
	protected EndpointPackagerService packagerService;

	/**
	 * @param endpointCode
	 * @param invocationCode
	 * @return packager object for encoding
	 */
	public Object getEncodePackager(String endpointCode, String invocationCode) {
		return packagers.get(packagers.constructKey(endpointCode, invocationCode));
	}
	
	/**
	 * @param endpointCode
	 * @param invocationCode
	 * @return packager object for decoding
	 */
	public Object getDecodePackager(String endpointCode, String invocationCode) {
		return packagers.get(packagers.constructKey(endpointCode, invocationCode));
	}
	
	/**
	 * Load and process packager byte[] to map
	 * 
	 * @param endpointType
	 * @param endpointCode
	 * @param node
	 * @throws UnsupportedPackagerException
	 */
	public void load(String endpointCode) throws Exception {
		List<EndpointPackager> packagers = packagerService.findByEndpointCode(endpointCode);
		if(packagers==null)
			return;
		for(EndpointPackager packager : packagers) {
			EtcFile file = fileService.findById(packager.getFile().getId());
			String invocationCode = packager.getInvocationCode();
			String ext = file.getExt();
			byte[] bytes = file.getData();
			if(ext.endsWith(PathConstant.Packager.FIXED_LENGTH_EXT))
				processFixedLength(endpointCode, invocationCode, bytes);
			else if(ext.endsWith(PathConstant.Packager.ISO8583_EXT))
				processISO8583(endpointCode, invocationCode, bytes);
			else
				throw new UnsupportedPackagerException("Unsupported extension file, '" + file.getName()+"."+file.getExt() + "'");
		}
	}
	
	protected void processFixedLength(String endpointCode, String invocationCode, byte[] bytes) throws UnsupportedPackagerException, JDOMException, IOException {
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		Element schema = new SAXBuilder().build(bais).getRootElement();
		String parentTagName = schema.getName();
		bais.close();
			
		// Will make packager object as org.jdom2.Element object if first tag is '<schema>'
		if(parentTagName.equalsIgnoreCase(PathConstant.Packager.FIXED_LENGTH_PARENT_TAG))
			packagers.put(packagers.constructKey(endpointCode, invocationCode), schema);
 	
		// Skip packager fetching process if unknown parent tag exists
		else {
			LOGGER.error("[" + endpointCode + "] Unknown parent tag, '<" + parentTagName + ">'");
			throw new UnsupportedPackagerException("Unknown parent tag, '<" + parentTagName + ">'");
		}
	}
	
	protected void processISO8583(String endpointCode, String invocationCode, byte[] bytes) throws UnsupportedPackagerException, JDOMException, IOException {
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		Element schema = new SAXBuilder().build(bais).getRootElement();
		String parentTagName = schema.getName();
		bais.close();

		// Will make packager object as org.nucleus8583.core.Iso8583MessageSerializer object if first tag is '<iso-message>'
		if(schema.getName().equalsIgnoreCase(PathConstant.Packager.ISO8583_PARENT_TAG))
			packagers.put(packagers.constructKey(endpointCode, invocationCode), MessageSerializer.create(bais));
		
		// Skip packager fetching process if unknown parent tag exists
		else {
			LOGGER.error("[" + endpointCode + "] Unknown parent tag, '<" + parentTagName + ">'");
			throw new UnsupportedPackagerException("Unknown parent tag, '<" + parentTagName + ">'");
		}
	}
}
