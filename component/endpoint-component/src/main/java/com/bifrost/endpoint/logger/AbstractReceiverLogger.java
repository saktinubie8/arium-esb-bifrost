package com.bifrost.endpoint.logger;

import java.util.Date;

import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */

public interface AbstractReceiverLogger {

	public void writeRequest(Date writeTime, Object request, ExternalMessage externalMessage);
	public void writeResponse(Date writeTime, Object response, ExternalMessage externalMessage);
	public String writeAsString(Object obj); 
	
}
