package com.bifrost.endpoint.service;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.model.EndpointType;
import com.bifrost.endpoint.repository.EndpointTypeRepository;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointTypeService extends GenericService<EndpointTypeRepository, EndpointType, String>{

	private static final String KEY = "endpoint:type";
	
	public EndpointTypeService() {
		super(KEY);
	}

	@Override
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		if(findById(EndpointType.KAFKA)==null)
			save(new EndpointType(EndpointType.KAFKA, "Kafka message interchange adapter"));
		if(findById(EndpointType.SOAP)==null)
			save(new EndpointType(EndpointType.SOAP, "SOAP message interchange adapter"));
		if(findById(EndpointType.REST)==null)
			save(new EndpointType(EndpointType.REST, "REST service interchange adapter"));
		if(findById(EndpointType.ISO8583)==null)
			save(new EndpointType(EndpointType.ISO8583, "ISO 8583 message interchange adapter"));
		if(findById(EndpointType.DATABASE)==null)
			save(new EndpointType(EndpointType.DATABASE, "Database access adapter"));
	}

}
