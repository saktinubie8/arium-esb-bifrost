package com.bifrost.endpoint;

import com.bifrost.message.ExternalMessage;

public interface AbstractMessageConfigurator {

	public void apply(ExternalMessage externalMessage);
	
}
