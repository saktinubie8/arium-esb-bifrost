package com.bifrost.endpoint.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.model.EndpointType;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointTypeRepository extends JpaRepository<EndpointType, String>{

}
