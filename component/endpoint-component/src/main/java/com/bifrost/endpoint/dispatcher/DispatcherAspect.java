package com.bifrost.endpoint.dispatcher;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.bifrost.endpoint.logger.AbstractDispatcherLogger;
import com.bifrost.message.ExternalMessage;

@Aspect
@Component
public class DispatcherAspect {
	
	@Autowired
	private AbstractDispatcherLogger logger;
	
	@Autowired
	private ThreadPoolTaskExecutor thread;
	
	@Before(value = "execution(* SynchronousDispatcher.send(..))")
	public void requestLogAsync(JoinPoint joinPoint) {
		
		thread.execute(new Runnable() {
			
			@Override
			public void run() {
			    logger.writeRequest(joinPoint.getArgs()[0], (ExternalMessage) joinPoint.getArgs()[1]);	
			}
		});
	}
	
	@AfterReturning(value = "execution(* SynchronousDispatcher.send(..))", returning = "retVal")
	public void responseLogAsync(JoinPoint joinPoint, Object retVal) {
		thread.execute(new Runnable() {
			
			@Override
			public void run() {
				logger.writeResponse(retVal, (ExternalMessage) joinPoint.getArgs()[1]);
			}
		});
	}
}
