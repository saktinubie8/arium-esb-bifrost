package com.bifrost.endpoint.logger;

import com.bifrost.message.ExternalMessage;

public interface AbstractDispatcherLogger {
	
	public abstract void writeRequest(Object request, ExternalMessage externalMessage);
	public abstract void writeResponse(Object response, ExternalMessage externalMessage);
	public abstract String writeAsString(Object message);
	
}
