package com.bifrost.endpoint.access;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bifrost.common.controller.AbstractCacheController;
import com.bifrost.endpoint.service.EndpointPackagerService;
import com.bifrost.endpoint.service.EndpointService;
import com.bifrost.endpoint.service.EndpointTypeService;
import com.bifrost.system.service.EtcFileService;

public class GenericSpringRestCacheController extends AbstractCacheController {

	private static final Log LOGGER = LogFactory.getLog(GenericSpringRestCacheController.class);
	
	@Autowired
	private EtcFileService fileService;
	
	@Autowired
	private EndpointService endpointService;
	
	@Autowired
	private EndpointTypeService endpointTypeService;
	
	@Autowired
	private EndpointPackagerService endpointPackagerService;

	@Override
	public void evictAll() {
		fileService.evictAll();
		endpointService.evictAll();
		endpointTypeService.evictAll();
		endpointPackagerService.evictAll();
	}

	@Override
	public void populateAll() {
		try {
			fileService.populate();
			endpointService.populate();
			endpointTypeService.populate();
			endpointPackagerService.populate();
		} catch (Exception e) {
			LOGGER.error("Failed when trying to populate cache, cause by: "+e.getMessage());
			e.printStackTrace();
		}
	}
}
