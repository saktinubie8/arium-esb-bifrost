package com.bifrost.endpoint.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.model.Endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointRepository extends JpaRepository<Endpoint, String>{

	public List<Endpoint> findByType_type(String type);
	
}
