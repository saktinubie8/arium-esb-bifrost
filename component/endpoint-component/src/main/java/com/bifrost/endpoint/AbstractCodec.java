package com.bifrost.endpoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Element;
import org.nucleus8583.core.MessageSerializer;
import org.springframework.beans.factory.annotation.Autowired;

import com.bifrost.exception.endpoint.CodecProcessException;
import com.bifrost.exception.endpoint.InvalidInputClassType;
import com.bifrost.exception.endpoint.InvalidSchemaFieldType;
import com.bifrost.exception.endpoint.InvalidSchemaTagType;
import com.bifrost.message.ExternalMessage;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public abstract class AbstractCodec {

	public static final String FXL_TYPE_STRING = "str";
	public static final String FXL_TYPE_NUMERIC = "num";
	public static final String FXL_FIELD = "field";
	public static final String FXL_ARRAY = "array";

	@SuppressWarnings("unused")
	@Autowired 
	private ObjectMapper objectMapper;

	@Autowired 
	private ObjectReader objectReader;

	@Autowired 
	private ObjectWriter objectWriter;
	
	/**
	 * @param obj object which will be decoded
	 * @param packager an object which will be decoded againts
	 * @return decoded result as ExternalMessage object
	 * @throws Exception
	 */
	public abstract ExternalMessage decode(Object obj, Object packager) throws Exception;
	
	/**
	 * @param payload object which will be encoded
	 * @param packager an object which will be encoded againts
	 * @return encoded result as Object
	 * @throws Exception
	 */
	public abstract Object encode(ExternalMessage message, Object packager) throws Exception;

	/**
	 * Executed when unknown invocation code is called
	 * 
	 * @param payload object which will be encoded
	 * @param packager an object which will be encoded againts
	 * @return encoded result as Object
	 * @throws Exception
	 */
	public abstract Object encodeUnknownInvocation(Object packager) throws Exception;
	
	protected Map<String, Object> decodeJson(byte[] bytes) throws JsonParseException, JsonMappingException, IOException {
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.putAll(objectReader.readValue(bytes));
		return payload;
	}
	
	protected String encodeJson(Map<String, Object> payload) throws JsonProcessingException {
		return new String(objectWriter.writeValueAsBytes(payload));
	}

	protected Map<String, Object> decodeFixedLength(String raw, Element schema, Boolean isRequest) throws InvalidInputClassType, InvalidSchemaFieldType, InvalidSchemaTagType, CodecProcessException  {
		Map<String, Object> payload = new HashMap<String, Object>();
		List<Element> list = null;
		if(isRequest)
			list = schema.getChildren("request");
		else
			list = schema.getChildren("response");
		if(list==null && isRequest)
			throw new CodecProcessException("Tag 'request' is not found");
		else if(list==null && !isRequest)
			throw new CodecProcessException("Tag 'response' is not found");
		for(Element child : list) {
			switch(child.getName()) {
			case FXL_FIELD : {
				parseField(child, raw, payload);
				break;
			}
			case FXL_ARRAY : {
				String id = child.getAttributeValue("id").trim();
				int size = (child.getAttributeValue("size")!=null)?Integer.parseInt(child.getAttributeValue("size")):0;
				if(size<1 && child.getAttributeValue("size-ref")!=null)
					size = Integer.parseInt((String)payload.get(child.getAttributeValue("size-ref")));
				if(size<1)
					continue;
				List<Map<String, Object>> arrayList = new ArrayList<Map<String, Object>>();
				for(int i=0;i<size;i++) {
					Map<String, Object> arrayValue = new HashMap<String, Object>(); 
					for(Element children : child.getChildren())
						parseField(children, raw, arrayValue);
					arrayList.add(i, arrayValue);
				}
				payload.put(id, arrayList);
				break;
			}
			default : throw new InvalidSchemaTagType(child.getName());
			}
		}
		return payload;
	}

	protected Map<String, Object> decodeFixedLength(byte[] bytes, Element schema, Boolean isRequest) throws InvalidInputClassType, InvalidSchemaFieldType, InvalidSchemaTagType, CodecProcessException  {
		return decodeFixedLength(new String(bytes), schema, isRequest);
	}
	
	protected String encodeFixedLength(Map<String, Object> payload, Element schema, Boolean isRequest) throws InvalidSchemaFieldType, InvalidSchemaTagType, CodecProcessException {
		String retVal = "";
		List<Element> list = null;
		if(isRequest)
			list = schema.getChildren("request");
		else
			list = schema.getChildren("response");
		if(list==null && isRequest)
			throw new CodecProcessException("Tag 'request' is not found");
		else if(list==null && !isRequest)
			throw new CodecProcessException("Tag 'response' is not found");
		for(Element child : list) {
			switch(child.getName()) {
			case FXL_FIELD : {
				writeField(child, payload, retVal);
				break;
			}
			case FXL_ARRAY : {
				String id = child.getAttributeValue("id").trim();
				int size = (child.getAttributeValue("size")!=null)?Integer.parseInt(child.getAttributeValue("size")):0;
				if(size<1 && child.getAttributeValue("size-ref")!=null)
					size = Integer.parseInt((String)payload.get(child.getAttributeValue("size-ref")));
				if(size<1)
					continue;
				
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> arrayList = (List<Map<String, Object>>) payload.get(id);
				for(int i=0;i<size;i++)
					for(Element children : child.getChildren())
						writeField(children, arrayList.get(i), retVal);
				break;
			}
			default : throw new InvalidSchemaTagType(child.getName());
			}
		}
		return retVal;
	}

	
	private static void parseField(Element elm, String raw, Map<String, Object> retVal) throws InvalidSchemaFieldType {
		String id = elm.getAttributeValue("id").trim();
		int length = Integer.parseInt (elm.getAttributeValue("length"));
		String type = elm.getAttributeValue("type").trim().toLowerCase();
		Boolean trim = (elm.getAttributeValue("trim")!=null)?Boolean.valueOf(elm.getAttributeValue("trim")):false;
		String value = raw.substring(0, length);
		raw = raw.substring(length);
		if(trim && type.equals(FXL_TYPE_NUMERIC))
			value = value.replaceFirst("^0+(?!$)", "");
		else if(trim && type.equals(FXL_TYPE_STRING))
			value = value.replaceAll("\\s+$", "");
		else
			throw new InvalidSchemaFieldType(id);
		retVal.put(id, value);
	}

	private static void writeField(Element elm, Map<String, Object> data, String retVal) throws InvalidSchemaFieldType {
		String id = elm.getAttributeValue("id").trim();
		int length = Integer.parseInt (elm.getAttributeValue("length"));
		String type = elm.getAttributeValue("type").trim().toLowerCase();
		String value = "";
		if(data.get(id)!=null)
			value = String.valueOf(data.get(id));
		if(type.equals(FXL_TYPE_NUMERIC))
			retVal += StringUtils.leftPad(value, length, '0').substring(value.length()-length, value.length());
		else if(type.equals(FXL_TYPE_STRING))
			retVal += StringUtils.leftPad(value, length, ' ').substring(value.length()-length, value.length());
		else
			throw new InvalidSchemaFieldType(id);
	}
	
	protected Map<String, Object> decodeISO8583(Object obj, MessageSerializer packager) {
		return null;
	}
	
	protected Object encodeISO8583(Map<String, Object> payload, MessageSerializer packager) {
		return null;
	}
}
