package com.bifrost.endpoint.soap;

import org.springframework.stereotype.Component;

import com.bifrost.endpoint.logger.ServletReceiverLogger;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class ReceiverLogger extends ServletReceiverLogger {

}
