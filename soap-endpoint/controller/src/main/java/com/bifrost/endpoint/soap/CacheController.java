package com.bifrost.endpoint.soap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.bifrost.endpoint.access.GenericCamelRestCacheController;
import com.bifrost.endpoint.soap.service.EndpointSoapActionService;
import com.bifrost.endpoint.soap.service.EndpointSoapService;

@RestController
public class CacheController extends GenericCamelRestCacheController {

	private static final Log LOGGER = LogFactory.getLog(CacheController.class);

	@Autowired
	private EndpointSoapService soapService;
	
	@Autowired
	private EndpointSoapActionService actionService;

	@Override
	public void evictAll() {
		super.evictAll();
		soapService.evictAll();
		actionService.evictAll();
	}

	@Override
	public void populateAll() {
		super.populateAll();
		try {
			soapService.populate();
			actionService.populate();
		} catch (Exception e) {
			LOGGER.error("Failed when trying to populate endpoint cache, cause by: "+e.getMessage());
			e.printStackTrace();
		}
	}

}
