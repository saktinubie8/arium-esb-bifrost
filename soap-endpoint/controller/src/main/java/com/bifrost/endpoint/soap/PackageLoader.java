package com.bifrost.endpoint.soap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Input;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.wsdl.Types;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.extensions.soap.SOAPOperation;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaCollection;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.DOMOutputter;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.endpoint.model.EndpointPackager;
import com.bifrost.endpoint.packager.GenericPackageLoader;
import com.bifrost.endpoint.soap.model.EndpointSoap;
import com.bifrost.endpoint.soap.model.EndpointSoapAction;
import com.bifrost.endpoint.soap.service.EndpointSoapActionService;
import com.bifrost.endpoint.soap.service.EndpointSoapService;
import com.bifrost.exception.endpoint.InvalidPackager;
import com.bifrost.exception.endpoint.UnsupportedPackagerException;
import com.bifrost.system.model.EtcFile;


/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class PackageLoader extends GenericPackageLoader {

	@Autowired
	private WSDLContainer wsdlContainer;
	
	@Autowired
	private EndpointSoapService soapService;
	
	@Autowired
	private EndpointSoapActionService soapActionService;
	
	@SuppressWarnings({ "unchecked", "unused" })
	public void load(String endpointCode) throws Exception {
		for(EndpointPackager packager : packagerService.findByEndpointCode(endpointCode)) {
			EtcFile file = fileService.findById(packager.getFile().getId());
			String fileName = file.getName();
			String ext = file.getExt();
			byte[] bytes = file.getData();
			
			// Append every XSD element in one schema
			if (ext.equals(PathConstant.Packager.XSD_EXT)) {
				Element child = new SAXBuilder().build(new ByteArrayInputStream(bytes)).getRootElement();
				Namespace oldTns = null;
				for(Namespace ns : child.getAdditionalNamespaces())
					if(ns.getURI().equals(child.getAttribute("targetNamespace").getValue()))
						oldTns = ns;
				Element root = (Element) packagers.get(endpointCode+":xsd");
				if(root==null) {
					EndpointSoap endpointSoap = soapService.findById(endpointCode);
					root = new Element("schema", Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema"));
					root.setAttribute("targetNamespace", endpointSoap.getTargetNameSpace());
					root.addNamespaceDeclaration(Namespace.getNamespace("tns", endpointSoap.getTargetNameSpace()));
				}
				for(Element element : child.getChildren()) {
					changeNamespace(element, oldTns, child.getNamespace(), root.getNamespace("tns"), root.getNamespace());
					root.addContent(element.clone());
				}
				
				packagers.put(endpointCode+":xsd", root);
				XMLOutputter writer = new XMLOutputter();
				writer.setFormat(Format.getPrettyFormat());
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				writer.output(root, baos);
				ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
				StreamSource ss = new StreamSource(bais);
				
				XmlSchemaCollection schemaCol = new XmlSchemaCollection();
				XmlSchema xmlSchema = schemaCol.read(ss);
				packagers.put(endpointCode+":schema", xmlSchema);

				DOMOutputter domOutputter = new DOMOutputter();
				packagers.put(endpointCode+":element", domOutputter.output(root));
				
				bais = new ByteArrayInputStream(baos.toByteArray());
				ss = new StreamSource(bais);
				Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(ss);
				packagers.put(endpointCode+":validator", schema);
				baos.close();
				bais.close();

			/*
			 * If WSDL is provided, then system will try to populate
			 * endpoint_soap table and endpoint_soap_system table based on its WSDL
			 */
			} else if(ext.equals(PathConstant.Packager.WSDL_EXT)) {
				// WSDL reader setting
				WSDLFactory factory = WSDLFactory.newInstance();
				WSDLReader reader = factory.newWSDLReader();
				reader.setFeature("javax.wsdl.verbose", true);
				reader.setFeature("javax.wsdl.importDocuments", true);
				
				/*
				 * Merge WSDL if more than one are exist
				 * Currently can't handle 2 or more WSDL with the same QName for its element
				 * It simply just merge two WSDL into one, it applies on namespaces too,
				 * so if different uri namespace has same prefix, then it will be replaced by the later one
				 */
				Definition savedDef = wsdlContainer.get(endpointCode+":provided");
				Definition currentDef = reader.readWSDL(null, new InputSource(new ByteArrayInputStream(bytes)));
				if(savedDef!=null) {
					Map<String, String> savednms = savedDef.getNamespaces();
					Iterator<Entry<String, String>> curnms = currentDef.getNamespaces().entrySet().iterator(); 
					while(curnms.hasNext()) {
						Entry<String, String> namespace = curnms.next();
						String prefix = namespace.getKey();
						String uri = namespace.getValue();
					}
				}
				if(savedDef!=null) {
					Iterator<Service> srvcIterator = currentDef.getServices().values().iterator();
					while(srvcIterator.hasNext()) {
						Service service = srvcIterator.next();
						service.setQName(new QName(savedDef.getTargetNamespace(), service.getQName().getLocalPart()));
						savedDef.addService(service);
					}
					
					Iterator<Binding> bindIterator = currentDef.getBindings().values().iterator();
					while(bindIterator.hasNext()) {
						Binding binding = bindIterator.next();
						binding.setQName(new QName(savedDef.getTargetNamespace(), binding.getQName().getLocalPart()));
						savedDef.addBinding(binding);
					}

					Iterator<PortType> portTypeIterator = currentDef.getPortTypes().values().iterator();
					while(portTypeIterator.hasNext()) {
						PortType portType = portTypeIterator.next();
						portType.setQName(new QName(savedDef.getTargetNamespace(), portType.getQName().getLocalPart()));
						List<Operation> opTemp = new ArrayList<Operation>();
						opTemp.addAll(portType.getOperations());
						Iterator<Operation> opIterator = opTemp.iterator();
						while(opIterator.hasNext()) {
							Operation op = opIterator.next();
							Input input = op.getInput();
							Message inputMessage = op.getInput().getMessage();
							inputMessage.setQName(new QName(savedDef.getTargetNamespace(), inputMessage.getQName().getLocalPart()));
							Iterator<Part> partIterator = inputMessage.getParts().values().iterator();
							while(partIterator.hasNext()) {
								Part part = partIterator.next();
								inputMessage.removePart(part.getName());
								part.setElementName(new QName(savedDef.getTargetNamespace(), part.getElementName().getLocalPart()));
								inputMessage.addPart(part);
							}
							op.setInput(input);

							Output output = op.getOutput();
							Message outputMessage = op.getInput().getMessage();
							outputMessage.setQName(new QName(savedDef.getTargetNamespace(), outputMessage.getQName().getLocalPart()));
							partIterator = outputMessage.getParts().values().iterator();
							while(partIterator.hasNext()) {
								Part part = partIterator.next();
								outputMessage.removePart(part.getName());
								part.setElementName(new QName(savedDef.getTargetNamespace(), part.getElementName().getLocalPart()));
								outputMessage.addPart(part);
							}
							op.setOutput(output);
							portType.removeOperation(op.getName(), input.getName(), output.getName());
							portType.addOperation(op);
						}
						savedDef.addPortType(portType);
					}

					Iterator<Message> msgIterator = currentDef.getMessages().values().iterator();
					while(msgIterator.hasNext()) {
						Message message = msgIterator.next();
						message.setQName(new QName(savedDef.getTargetNamespace(), message.getQName().getLocalPart()));
						Iterator<Part> partIterator = message.getParts().values().iterator();
						while(partIterator.hasNext()) {
							Part part = partIterator.next();
							message.removePart(part.getName());
							part.setElementName(new QName(savedDef.getTargetNamespace(), part.getElementName().getLocalPart()));
							message.addPart(part);
						}
						savedDef.addMessage(message);
					}
					
					Types savedTypes = savedDef.getTypes();
					Iterator<javax.wsdl.extensions.schema.Schema> curSchema = currentDef.getTypes().getExtensibilityElements().iterator();
					while(curSchema.hasNext()) {
						javax.wsdl.extensions.schema.Schema schema = curSchema.next();
						schema.getElement().setAttribute("targetNamespace", savedDef.getTargetNamespace());
						savedTypes.addExtensibilityElement(schema);
					}
					
					String curTns = currentDef.getTargetNamespace();
					Map<String, String> savednms = savedDef.getNamespaces();
					Iterator<Entry<String, String>> curnms = currentDef.getNamespaces().entrySet().iterator(); 
					while(curnms.hasNext()) {
						Entry<String, String> namespace = curnms.next();
						String prefix = namespace.getKey();
						String uri = namespace.getValue();
						if(uri.equals(curTns))
							savedDef.addNamespace(prefix, savedDef.getTargetNamespace());
						else
							savedDef.addNamespace(prefix, uri);
					}
				}else
					savedDef = currentDef;
				wsdlContainer.put(endpointCode+":provided", savedDef);
				
				// Read wsdl file then dismantle its value
				String tns = currentDef.getTargetNamespace();
				Iterator<Service> srvcItr = currentDef.getServices().values().iterator();
				while(srvcItr.hasNext()) {
					Service service = srvcItr.next();
					String serviceName = service.getQName().getLocalPart();
					Iterator<Port> portItr = service.getPorts().values().iterator(); 
					while(portItr.hasNext()) {
						Port port = portItr.next();
						SOAPAddress address = (SOAPAddress) port.getExtensibilityElements().get(0);
						String locationURI = address.getLocationURI();
						EndpointSoap endpointSoap = null;
						endpointSoap = soapService.findById(endpointCode);
							
						// If records not exists then create a new one based on dismantled wsdl value
						if(endpointSoap==null) {
							endpointSoap = new EndpointSoap();
							endpointSoap.setEndpointCode(endpointCode);
							endpointSoap.setAddressLocation(locationURI);
							endpointSoap.setAsServer(true);
							endpointSoap.setSsl(false);
							endpointSoap.setServiceName(serviceName);
							endpointSoap.setTargetNameSpace(tns);
							endpointSoap.setMaxCon(50);
							endpointSoap.setKeepAliveMillis(20000);
							endpointSoap.setCloseIdleMillis(30000);
							endpointSoap.setConTOMillis(30000);
							endpointSoap.setReqTOMillis(30000);
							endpointSoap.setSocTOMillis(60000);
							endpointSoap = soapService.save(endpointSoap);
						}
						List<BindingOperation> bindOpList = port.getBinding().getBindingOperations(); 
						for(BindingOperation bindOp : bindOpList) {
							List<SOAPOperation> soapOpList = bindOp.getExtensibilityElements();
							SOAPOperation soapOp = soapOpList.get(0);
							String actionName = soapOp.getSoapActionURI();
							EndpointSoapAction esa = soapActionService.findByEndpointCodeAndActionName(endpointSoap.getEndpointCode(), actionName);

							// If records not exists then create a new one based on dismantled wsdl value
							if(esa==null) {
								String eleInput;
								Message msg = bindOp.getOperation().getInput().getMessage();
								Map<String, Part> inputMap = msg.getParts();
								if(inputMap.size()>1)
									eleInput = msg.getQName().getLocalPart();
								else {
									Iterator<Part> iterInput = inputMap.values().iterator();
									Part partInput = iterInput.next();
									if(partInput.getTypeName()==null)
										throw new InvalidPackager(fileName+".wsdl has null type at part:"+partInput.getName()+" on message:"+msg.getQName().getLocalPart());
									else {
										QName type = partInput.getTypeName();
										if(type.getNamespaceURI().toLowerCase().equals("http://www.w3.org/2001/XMLSchema"))
											eleInput = msg.getQName().getLocalPart();
										else
											eleInput = partInput.getTypeName().getLocalPart();
									}
								}
								
								Iterator<Part> iterOutput = bindOp.getOperation().getOutput().getMessage().getParts().values().iterator();
								Part partOutput = iterOutput.next();
								String eleOutput;
								if(partOutput.getTypeName()==null)
									eleOutput = partOutput.getElementName().getLocalPart();
								else
									eleOutput = partOutput.getTypeName().getLocalPart();
								
								esa = new EndpointSoapAction();
								esa.setActionName(actionName);
								esa.setInputElement(eleInput);
								esa.setOutputElement(eleOutput);
								esa.setSoap(endpointSoap);
								soapActionService.save(esa);
							}
						}
					}
				}

				/*
				 * Create schema validation for SOAP XML againts its WSDL
				 * Dismantle only its all schema element in tag <types>
				 */
				Element root = new SAXBuilder().build(new ByteArrayInputStream(bytes)).getRootElement();
				Element mainSchema = (Element) packagers.get(endpointCode+":xsd");
				Element schemaL = null;
				for(Element elem : root.getChildren()) {
					if(elem.getName().toLowerCase().equals("types")) {
						schemaL = elem.getChild("schema");
						schemaL.detach();
						for(Namespace namespace : root.getAdditionalNamespaces())
							schemaL.addNamespaceDeclaration(namespace);
						schemaL.addNamespaceDeclaration(root.getNamespace());
					}else if(elem.getName().toLowerCase().equals("message")) {
						List<Element> children = elem.getChildren();
						Element part = children.get(0);
						Namespace ns = Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
						String xmlns = root.getNamespace(part.getAttributeValue("type").split(":")[0]).getURI();
						if(children.size()>1 || (children.size()==1 && xmlns.equals(ns.getURI()))) {
							schemaL = new Element("schema", ns);
							Element sequence = schemaL.addContent(new Element("sequence", ns));
							for(Element child : children) {
								child.setName("element");
								child.setNamespace(ns);
								sequence.addContent(child);
							}
						}
					}
					
					for(Namespace namespace : root.getAdditionalNamespaces())
						schemaL.addNamespaceDeclaration(namespace);
					schemaL.addNamespaceDeclaration(root.getNamespace());
					if(mainSchema!=null) {
						// Namespace with same prefix will be replaced with later one, not handled yet 
						for(Namespace ns : schemaL.getNamespacesInScope())
							mainSchema.addNamespaceDeclaration(ns);
						for(Element child : schemaL.getChildren())
							mainSchema.addContent(child.clone());
					}else
						mainSchema = schemaL;
				}

				packagers.put(endpointCode+":xsd", mainSchema);
				Document doc = new Document(mainSchema);
				XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				outputter.output(doc, baos);
				
				/*
				 * Write schema to byte array input stream so we don't have to create a file physically
				 * after that we just need to read from byte array input stream
				 */
				ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
				Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new StreamSource(bais));

				bais = new ByteArrayInputStream(baos.toByteArray());
				XmlSchemaCollection schemaCol = new XmlSchemaCollection();
				XmlSchema xmlSchema = schemaCol.read(new StreamSource(bais));
				packagers.put(endpointCode+":schema", xmlSchema);
				
				List<Schema> schemaList = (List<Schema>) packagers.get(endpointCode+":validator");
				if(schemaList==null)
					schemaList = new ArrayList<Schema>();
				schemaList.add(schema);
				packagers.put(endpointCode+":validator", schemaList);
				bais.close();
			} else {
				LOGGER.error("[" + endpointCode + "] Unsupported extension file, '" + fileName +"."+ext+ "'");
				throw new UnsupportedPackagerException("Unsupported extension file, '" + fileName +"."+ext+ "'");
			}
			LOGGER.trace("[" + endpointCode + "] '" + fileName +"."+ext+ "' is loaded");
		}
	}
	
	private void changeNamespace(Element element, Namespace oldTns, Namespace oldXmlns, Namespace newTns, Namespace newXmlns) {
		element.setNamespace(newXmlns);
		Attribute type = element.getAttribute("type");
		if(oldTns!=null && type!=null && type.getValue().startsWith(oldTns.getPrefix()+":"))
			type.setValue(type.getValue().replace(oldTns.getPrefix()+":", newTns.getPrefix()+":"));
		else if(type!=null && type.getValue().startsWith(oldXmlns.getPrefix()+":"))
				type.setValue(type.getValue().replace(oldXmlns.getPrefix()+":", newXmlns.getPrefix()+":"));
		for(Element child : element.getChildren())
			changeNamespace(child, oldTns, oldXmlns, newTns, newXmlns);
	}

	@Override
	public Object getEncodePackager(String endpointCode, String invocationCode) {
		return packagers.get(endpointCode+":schema");
	}

	@Override
	public Object getDecodePackager(String endpointCode, String invocationCode) {
		return packagers.get(endpointCode+":validator");
	}
}
