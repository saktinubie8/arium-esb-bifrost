package com.bifrost.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bifrost.endpoint.AbstractMessageConfigurator;
import com.bifrost.endpoint.soap.model.EndpointSoapAction;
import com.bifrost.endpoint.soap.service.EndpointSoapActionService;
import com.bifrost.message.ExternalMessage;

@Component
public class MessageConfigurator implements AbstractMessageConfigurator {

	@Autowired
	private EndpointSoapActionService service;
	
	@Override
	public void apply(ExternalMessage externalMessage) {
		EndpointSoapAction action = service.findByEndpointCodeAndActionName(externalMessage.getEndpointCode(), externalMessage.getInvocationCode());
		externalMessage.setResponseCodeInHeaders(action.getRcInHeaders());
		externalMessage.setResponseCodeField(action.getRcFieldName());
		externalMessage.setResponseDescInHeaders(action.getRdInHeaders());
		externalMessage.setResponseDescField(action.getRdFieldName());
	}

}
