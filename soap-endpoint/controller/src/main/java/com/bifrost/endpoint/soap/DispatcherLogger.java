package com.bifrost.endpoint.soap;

import org.springframework.stereotype.Component;

import com.bifrost.endpoint.dispatcher.ServletDispatcherLogger;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class DispatcherLogger extends ServletDispatcherLogger {

}
