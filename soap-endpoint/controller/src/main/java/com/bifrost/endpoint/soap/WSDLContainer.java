package com.bifrost.endpoint.soap;

import java.util.concurrent.ConcurrentHashMap;

import javax.wsdl.Definition;

import org.springframework.stereotype.Component;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class WSDLContainer extends ConcurrentHashMap<String, Definition> {

	private static final long serialVersionUID = -7491315371451332476L;

}
