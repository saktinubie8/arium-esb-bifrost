package com.bifrost.endpoint.soap;

import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.wsdl.Binding;
import javax.wsdl.BindingInput;
import javax.wsdl.BindingOperation;
import javax.wsdl.BindingOutput;
import javax.wsdl.Definition;
import javax.wsdl.Input;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.OperationType;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.wsdl.Types;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.extensions.soap.SOAPBinding;
import javax.wsdl.extensions.soap.SOAPBody;
import javax.wsdl.extensions.soap.SOAPOperation;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLWriter;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaElement;
import org.apache.ws.commons.schema.XmlSchemaSerializer.XmlSchemaSerializerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.util.NetworkUtils;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.endpoint.AbstractController;
import com.bifrost.endpoint.EndpointProcess;
import com.bifrost.endpoint.json.response.EndpointServiceResponse;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.model.EndpointType;
import com.bifrost.endpoint.packager.PackagerMap;
import com.bifrost.endpoint.rest.RestTemplateFactory;
import com.bifrost.endpoint.soap.model.EndpointSoap;
import com.bifrost.endpoint.soap.model.EndpointSoapAction;
import com.bifrost.endpoint.soap.service.EndpointSoapActionService;
import com.bifrost.endpoint.soap.service.EndpointSoapService;
import com.bifrost.exception.endpoint.EndpointInitiationException;
import com.bifrost.exception.endpoint.InvalidContainerPropertiesClassException;
import com.bifrost.logging.LoggingClient;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.message.ExternalMessage;
import com.bifrost.serialization.KryoService;
import com.bifrost.system.model.EtcFile;
import com.ibm.wsdl.BindingInputImpl;
import com.ibm.wsdl.BindingOutputImpl;
import com.ibm.wsdl.extensions.schema.SchemaImpl;
import com.ibm.wsdl.extensions.soap.SOAPAddressImpl;
import com.ibm.wsdl.extensions.soap.SOAPBindingImpl;
import com.ibm.wsdl.extensions.soap.SOAPBodyImpl;
import com.ibm.wsdl.extensions.soap.SOAPOperationImpl;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class Controller extends AbstractController {

	/*
	 * Status true means ONLINE
	 * Status false means OFFLINE
	 */
	private static final Map<String, Boolean> status = new ConcurrentHashMap<String, Boolean>();
	private static final Map<String, EndpointProcess> processes = new ConcurrentHashMap<String, EndpointProcess>();
	private static final Log LOGGER = LogFactory.getLog(Controller.class);
	private static String node;
	
	@Autowired
	private Dispatcher dispatcher;
	
	@Autowired
	private PackagerMap packagers;
	
	@Autowired	
	private CamelContext camelContext;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private WSDLContainer wsdlContainer;
	
	@Autowired
	private LoggingClient loggingClient;
	
	@Autowired
	private PackageLoader packageLoader;
	
	@Autowired
	private ReceiverLogger receiverLogger;
	
	@Autowired
	private EndpointSoapService soapService;
	
	@Autowired
	private CacheController cacheController;
	
	@Autowired
	private RestTemplateFactory restTemplateFactory;
	
	@Autowired
	private EndpointSoapActionService soapActionService;
	
	@Override
	public EndpointServiceResponse start(String endpointCode) {
		EndpointSoap soap = soapService.findById(endpointCode);
		if(soap==null) {
			endpointStatusManager.setOffline(endpointCode, node, "No service can be found");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", no service can be found"));
		}else {
			status.put(soap.getEndpointCode(), true);
			endpointStatusManager.setOnline(endpointCode, node, "Endpoint started successfully");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());	
		}
	}

	@Override
	public EndpointServiceResponse stop(String endpointCode) {
		EndpointSoap soap = soapService.findById(endpointCode);
		if(soap==null) {
			endpointStatusManager.setOffline(endpointCode, node, "No service can be found");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), ", no service can be found");
		}else {
			status.put(soap.getEndpointCode(), false);
			endpointStatusManager.setOffline(endpointCode, node, "Endpoint stopped successfully");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());	
		}
	}

	@Override
	public EndpointServiceResponse restart(String endpointCode) {
		EndpointServiceResponse response = stop(endpointCode);
		if(response.getCode().equals(GenericResponse.SUCCESS.getCode()))
			response = start(endpointCode);
		else
			return response;
		return response;
	}

	public EndpointServiceResponse reload(String endpointCode) {
		
		// refresh all cache
		cacheController.refreshAll();
		
		// Load packager
		try {
			wsdlContainer.remove(endpointCode+":provided");
			packageLoader.load(endpointCode);
			packagers.remove(endpointCode+":xsd");
		} catch (Exception e) {
			LOGGER.error("["+endpointCode+"] Error when trying to load packager, cause by: "+e.getMessage());
			endpointStatusManager.setError(endpointCode, node, "Error when trying to load packager, cause by: "+e.getMessage());
			e.printStackTrace();
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", cause by: "+e.getMessage()));
		}
		
		EndpointServiceResponse response = stop(endpointCode);
		if(!response.getCode().equals(GenericResponse.SUCCESS.getCode()))
			return response;
		
		// Stopping internal kafka endpoint consumer
		EndpointProcess process = processes.get(endpointCode); 
		if(process!=null)
			process.stop();
		
		/*
		 * Remove route from camel context,
		 * to make sure no message consumed and be processed
		 */
		EndpointSoap soap = soapService.findById(endpointCode);
		try {
			status.remove(endpointCode);
			camelContext.removeRoute(endpointCode);
		} catch (Exception e) {
			LOGGER.error("["+endpointCode+"] Error when trying to remove route\"+constructKey(endpointCode, soap.getId())+\", cause by: "+e.getMessage());
			endpointStatusManager.setError(endpointCode, node, "Error when trying to remove route "+endpointCode+", cause by: "+e.getMessage());
			e.printStackTrace();
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", cause by: "+e.getMessage()));
		}
		
		// Construct WSDL
		try {
			Definition def = wsdlContainer.get(endpointCode+":provided");
			if(def==null)
				wsdlContainer.put(endpointCode+":generated", constructWSDL(soap));
		} catch(Exception e) {
			LOGGER.error("["+endpointCode+"] Failed to construct WSDL, cause by: "+e.getMessage());
			endpointStatusManager.setError(endpointCode, node, "Failed to construct WSDL, cause by: , cause by: "+e.getMessage());
			e.printStackTrace();
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", cause by: "+e.getMessage()));
		}
		
		// Load endpoint configuration
		try {
			loadEndpoint(endpointCode);
		} catch (Exception e) {
			status.put(endpointCode, false);
			LOGGER.error("["+endpointCode+"] Error when trying to load configuration, cause by: "+e.getMessage());
			endpointStatusManager.setError(endpointCode, node, "Error when trying to reload configuration, cause by: "+e.getMessage());
			e.printStackTrace();
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", cause by: "+e.getMessage()));
		}
		return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());
	}
	
	@Override
	public List<EndpointServiceResponse> startAll() {
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(start(endpoint.getCode()));
				}
			});
		return listResponse;
	}

	@Override
	public List<EndpointServiceResponse> stopAll() {
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(stop(endpoint.getCode()));
				}
			});
		return listResponse;
	}

	@Override
	public List<EndpointServiceResponse> restartAll() {
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(restart(endpoint.getCode()));
				}
			});
		return listResponse;
	}

	@Override
	public List<EndpointServiceResponse> reloadAll() {
		CountDownLatch latch = new CountDownLatch(getAllEndpoint().size());
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(reload(endpoint.getCode()));
					latch.countDown();
				}
			});
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return listResponse;
	}

	@PostConstruct
	public void init() throws UnknownHostException {
		node = systemIdentity.getHost();
		if(camelContext.getStatus().isStopped())
			camelContext.start();
		super.init();
	}
	
	private void loadEndpoint(String endpointCode) throws InvalidContainerPropertiesClassException, InterruptedException, EndpointInitiationException {
		EndpointSoap soap = soapService.findById(endpointCode);
		EndpointProcess process = new EndpointProcess(endpointService.findById(endpointCode), node, context);
		process.setSyncDispatcher(dispatcher);
		processes.put(endpointCode, process);
		while(!process.isRunning())
			Thread.sleep(1000L);
		if(soap.isClient() && (soap.getAddressLocation()==null || soap.getAddressLocation().isEmpty()))
			throw new EndpointInitiationException("Address location cannot be null if endpoint is a client");
		try {
			loadSOAP(soap, process);
			status.put(soap.getEndpointCode(), true);
			endpointStatusManager.setOnline(endpointCode, node, "Endpoint loaded and started successfully");
			LOGGER.trace("["+endpointCode+"] ====================================================================================================================================================================================");
			LOGGER.info("["+endpointCode+"] Endpoint IS REGISTERED successfully");
			LOGGER.trace("["+endpointCode+"] ====================================================================================================================================================================================");
		} catch (Exception e) {
			e.printStackTrace();
			status.put(soap.getEndpointCode(), false);
			LOGGER.error("["+endpointCode+"] Error when trying to load configuration, cause by: "+e.getMessage());
			endpointStatusManager.setError(endpointCode, node, "Error when trying to load configuration, cause by: "+e.getMessage());
			LOGGER.trace("["+endpointCode+"] ====================================================================================================================================================================================");
			LOGGER.info("["+endpointCode+"] FAILED to register endpoint");
			LOGGER.trace("["+endpointCode+"] ====================================================================================================================================================================================");
		}
	}
	
	public void loadSOAP(EndpointSoap endpointSoap, EndpointProcess process) throws Exception {
		String endpointCode = endpointSoap.getEndpointCode();
		String soapWhiteList = endpointSoap.getWhiteList();

		/*
		 * SpEL example for http headers
		 * set(HttpHeaders.CONTENT_TYPECONTENT_TYPE, 'application/json');
		 * set(HttpHeaders.AUTHORIZATION, 'Basic: XXXXXXXXXXXX');
		 * deilimited by semicolon
		 */
		final HttpHeaders httpHeaders = new HttpHeaders();
		for(EndpointSoapAction action : soapActionService.findByEndpointCode(endpointCode)) {
			if(action.getHeaders()!=null)
				for (String exp : action.getHeaders().split(";"))
					if(exp != null && !exp.trim().isEmpty())
						expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(exp, true)).getValue(httpHeaders);	
		}
		
		String address;
		try {
			address = new URL(endpointSoap.getAddressLocation()).getPath();	
		}catch(MalformedURLException e) {
			address = endpointSoap.getAddressLocation().trim();
		}
		final String path = address;
		Boolean isServer = endpointSoap.isServer();
		if(isServer) {
			RouteBuilder routeBuilder = new RouteBuilder() {
				
				@Override
				public void configure() throws Exception {
					restConfiguration("servlet");
					
					rest()
					.post(path)
					
						.consumes(MediaType.TEXT_XML_VALUE)
						.produces(MediaType.TEXT_XML_VALUE)
						.route()
						.routeId(endpointCode)
						.convertBodyTo(byte[].class, "UTF8")
						.choice()
							.when(new Predicate() {
								
								public boolean matches(Exchange exchange) {
									exchange.getMessage().setHeader(RECEIVED_TIME, new Date());
									
									// Checking is service status ONLINE or OFFLINE
									Boolean result = status.get(endpointCode);
									if(result==null || !result || KryoService.poolIsNotExist()) {
										// Return service offline
										//exchange.getMessage().setBody(null);
										exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 503);
										return result;	
									}

									// Checking remote IP within white list or not
									result = false;
									if(soapWhiteList==null || soapWhiteList.isEmpty() || soapWhiteList.equals("*"))
										result = true;
									else {
										HttpServletRequest message = exchange.getMessage().getHeader(Exchange.HTTP_SERVLET_REQUEST, HttpServletRequest.class);
										if(message.getRemoteAddr()==null)
											// Should not be null
											result = false;
										else if(!result)
											result = NetworkUtils.validateIp(message.getRemoteAddr(), soapWhiteList);
									}

									if(!result) {
										// Return unauthorized
										//exchange.getMessage().setBody(null);
										exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 401);
										return result;
									}

									// Checking is content type supported or not
									MediaType mediaTypeRequest = MediaType.parseMediaType((String) exchange.getMessage().getHeader(Exchange.CONTENT_TYPE));
									result = mediaTypeRequest.includes(MediaType.TEXT_XML);
								
									if(!result) {
										// Return unsupported content type
										//exchange.getMessage().setBody(null);
										exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 415);
									}
									return result;
								}
							})
								.process(new Processor() {
									
									@SuppressWarnings("null")
									@Override
									public void process(Exchange exchange) throws Exception {
										String actionName = extractActionName(exchange);
										EndpointSoapAction soapAction = soapActionService.findByEndpointCodeAndActionName(endpointCode, actionName);		
										if(soapAction==null) {
											LOGGER.info("["+endpointCode+"] SOAP action with name '"+actionName+"' cannot be found");
											SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
											ByteArrayOutputStream baos = new ByteArrayOutputStream();  
											soapMessage = MessageFactory.newInstance().createMessage();
											soapMessage.getSOAPBody().addFault(new QName(GenericResponse.ERROR.getDesc()), "Unknown SOAP action");
											soapMessage.writeTo(baos);
											exchange.getMessage().setBody(new String(baos.toByteArray(), Charset.defaultCharset()));
											exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 501);
											baos.close();
										}else {
											Date receivedTime = (Date) exchange.getMessage().removeHeader(RECEIVED_TIME);
											org.apache.camel.Message responseMessage = (org.apache.camel.Message) process.receive(exchange.getMessage(), actionName, receivedTime, true);
											if(responseMessage == null)
												exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
											else if(responseMessage.getHeader(Exchange.HTTP_RESPONSE_CODE)==null) {
												LOGGER.info("["+endpointCode+"] Http response code is null, set with 200");
												responseMessage.setHeader(Exchange.HTTP_RESPONSE_CODE, 200);
												exchange.setMessage(responseMessage);
											}else {
												try {
													Object httpResponseCode = responseMessage.getHeader(Exchange.HTTP_RESPONSE_CODE);
													if(httpResponseCode instanceof String)
														Integer.valueOf((String) httpResponseCode);
													exchange.setMessage(responseMessage);
												}catch(NumberFormatException e) {	
													LOGGER.info("["+endpointCode+"] Http response code '"+responseMessage.getHeader(Exchange.HTTP_RESPONSE_CODE)+"' is not a number, cannot reply!");
												}
											}
										}
									}
								})
							// Write failed message route log
							.otherwise()
								.process(new Processor() {
									public void process(Exchange exchange) throws Exception {
										writeFailedLog(endpointCode, exchange);
									}
								})
					.endRest()
					
					.get(path)
						.produces(MediaType.TEXT_XML_VALUE)
						.route().process(new Processor() {
							
							@Override
							public void process(Exchange exchange) throws Exception {
								WSDLFactory factory = WSDLFactory.newInstance();
								WSDLWriter writer = factory.newWSDLWriter();
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								Definition def = wsdlContainer.get(endpointCode+":provided");
								if(def==null)
									def = wsdlContainer.get(endpointCode+":generated");
								writer.writeWSDL(def, baos);
								exchange.getMessage().setBody(new String(baos.toByteArray(), Charset.defaultCharset()));
								exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 200);
								baos.close();
							}
						});
				}
			};
			camelContext.addRoutes(routeBuilder);
		}else if(!isServer && (!endpointSoap.isSslEnabled() || (endpointSoap.isSslEnabled() && endpointSoap.getCert()==null)))
			restTemplateFactory.createTrustAllRestTemplate(endpointCode, endpointSoap.getMaxCon(), endpointSoap.getKeepAliveMillis(), endpointSoap.getCloseIdleMillis(), endpointSoap.getReqTOMillis(), endpointSoap.getConTOMillis(), endpointSoap.getSocTOMillis());
		else if(!isServer && endpointSoap.isSslEnabled()) {
			EtcFile cert = fileService.findById(endpointSoap.getCert().getId());
			restTemplateFactory.createTrustCertRestTemplate(endpointCode, cert.getData(), endpointSoap.getMaxCon(), endpointSoap.getKeepAliveMillis(), endpointSoap.getCloseIdleMillis(), endpointSoap.getReqTOMillis(), endpointSoap.getConTOMillis(), endpointSoap.getSocTOMillis());
		}
	}
	
	private String extractActionName(Exchange exchange) {
		String actionName = (String) exchange.getMessage().getHeader("SOAPAction");
		actionName = actionName.replace("\"", "");
		return actionName;
	}

	private void writeFailedLog(String endpointCode, Exchange exchange) {
		String trxId;
		try {
			trxId = ExternalMessage.constructTrxId(endpointCode, systemIdentity.getHost(), exchange.getExchangeId());
		} catch (UnknownHostException e) {
			trxId = ExternalMessage.constructTrxId(endpointCode, "UnknownHost", exchange.getExchangeId());
			e.printStackTrace();
		}

		Date receivedTime = (Date) exchange.getMessage().removeHeader(RECEIVED_TIME);
		String actionName = extractActionName(exchange);
		RouteLog logIn = new RouteLog();
		logIn.setTransactionId(trxId);
		logIn.setDateTime(receivedTime);
		logIn.setEndpointCode(endpointCode);
		logIn.setInvocationCode(actionName);
		logIn.setRequest();
		logIn.setRaw(receiverLogger.writeAsString(exchange.getMessage()));
		loggingClient.write(logIn);
		
		RouteLog logOut = new RouteLog();
		logOut.setTransactionId(trxId);
		logOut.setDateTime(new Date());
		logOut.setEndpointCode(endpointCode);
		logOut.setInvocationCode(actionName);
		logOut.setResponse();
		logOut.setRaw(receiverLogger.writeAsString(exchange.getMessage()));
		loggingClient.write(logOut);
	}
	
	/**
	 * Construct a wsdl definition based on its soap endpoint
	 * 
	 * @param endpointSoap
	 * @return Definition instance of soap endpoint from parameter
	 * @throws WSDLException
	 * @throws XmlSchemaSerializerException
	 */
	private Definition constructWSDL(EndpointSoap endpointSoap) throws WSDLException, XmlSchemaSerializerException {
		// Essential list of namespaces
		String endpointCode = endpointSoap.getEndpointCode();
		String wsdl = "http://schemas.xmlsoap.org/wsdl/";
		String xsd = "http://www.w3.org/2001/XMLSchema";
		String http = "http://schemas.xmlsoap.org/wsdl/http/";
		String soap = "http://schemas.xmlsoap.org/wsdl/soap/";
		String soapenc = "http://schemas.xmlsoap.org/soap/encoding/";
		String tm = "http://microsoft.com/wsdl/mime/textMatching/";
		String mime = "http://schemas.xmlsoap.org/wsdl/mime/";
		String tns = endpointSoap.getTargetNameSpace();
		String serviceName = endpointSoap.getServiceName();
		WSDLFactory factory = WSDLFactory.newInstance();
		
		// Create new wsdl definition
		Definition def = factory.newDefinition();
		def.setTargetNamespace(tns);
		def.addNamespace("", wsdl);
		def.addNamespace("tns", tns);
		def.addNamespace("xsd", xsd);
		def.addNamespace("http", http);
		def.addNamespace("soap", soap);
		def.addNamespace("soapenc", soapenc);
		def.addNamespace("tm", tm);
		def.addNamespace("mime", mime);
		
		Types types = def.createTypes();
		XmlSchema schema = (XmlSchema) packagers.remove(endpointCode+":schema");
		if(schema==null)
			throw new NullPointerException("SOAP ID: "+endpointCode+", XSD schema.XmlSchema is null");
		Element element = (Element) packagers.remove(endpointCode+":element");
		if(element==null)
			throw new NullPointerException("SOAP ID: "+endpointCode+", XSD w3c.dom.Element is null");
		
		javax.wsdl.extensions.schema.Schema xsdSchema = new SchemaImpl();
		element.removeAttribute("xmlns:xsd");
		element.removeAttribute("xmlns:tns");
		xsdSchema.setElement(element);
		xsdSchema.setElementType(new QName(xsd, "schema"));
		types.addExtensibilityElement(xsdSchema);
		def.setTypes(types);
		
		PortType portType = def.createPortType();
		portType.setQName(new QName(tns, serviceName));
		portType.setUndefined(false);
		
		SOAPBinding soapBind = new SOAPBindingImpl();
		soapBind.setElementType(new QName(soap, "binding"));
		soapBind.setTransportURI("http://schemas.xmlsoap.org/soap/http");
		soapBind.setStyle("document");
		
		Binding binding = def.createBinding();
		binding.setQName(new QName(tns, serviceName));
		binding.addExtensibilityElement(soapBind);
		binding.setUndefined(false);
		
		for(EndpointSoapAction action : endpointSoap.getActions()) {
			String actionName = action.getActionName().trim();
			if(actionName==null || actionName.isEmpty())
				throw new NullPointerException("SOAP action ID: "+action.getId()+", field 'actionName' is null");
			if(action.getInputElement()==null || action.getInputElement().trim().isEmpty())
				throw new NullPointerException("SOAP action ID: "+action.getId()+", field 'inputElement' is null");
			XmlSchemaElement eleInput = schema.getElementByName(action.getInputElement()); 
			if(eleInput==null)
				throw new NullPointerException("SOAP action ID: "+action.getId()+", cannot find element '"+action.getInputElement()+"' in schema");
			QName qnameInput = eleInput.getQName();
			qnameInput = new QName(qnameInput.getNamespaceURI(), qnameInput.getLocalPart(), "tns");
			Part partInput = def.createPart();
			partInput.setName("parameters");
			partInput.setElementName(qnameInput);
			
			if(action.getOutputElement()==null || action.getOutputElement().trim().isEmpty())
				throw new NullPointerException("SOAP action ID: "+action.getId()+", field 'outputElement' is null");
			XmlSchemaElement eleOutput = schema.getElementByName(action.getOutputElement()); 
			if(eleOutput==null)
				throw new NullPointerException("SOAP action ID: "+action.getId()+", cannot find element '"+action.getOutputElement()+"' in schema");
			QName qnameOutput = eleOutput.getQName();
			qnameOutput = new QName(qnameOutput.getNamespaceURI(), qnameOutput.getLocalPart(), "tns");
			Part partOutput = def.createPart();
			partOutput.setName("parameters");
			partOutput.setElementName(qnameOutput);
			
			Message messageInput = def.createMessage();
			messageInput.setQName(new QName(tns, qnameInput.getLocalPart()+"Message"));
			messageInput.addPart(partInput);
			messageInput.setUndefined(false);
			def.addMessage(messageInput);

			Message messageOutput = def.createMessage();
			messageOutput.setQName(new QName(tns, qnameOutput.getLocalPart()+"Message"));
			messageOutput.addPart(partOutput);
			messageOutput.setUndefined(false);
			def.addMessage(messageOutput);
			
			Input input = def.createInput();
			input.setMessage(messageInput);
			
			Output output = def.createOutput();
			output.setMessage(messageOutput);
			
			Operation operation = def.createOperation();
			operation.setName(actionName);
			operation.setInput(input);
			operation.setOutput(output);
			operation.setUndefined(false);
			operation.setStyle(OperationType.REQUEST_RESPONSE);
			portType.addOperation(operation);

			SOAPBody bodyInput = new SOAPBodyImpl();
			bodyInput.setElementType(new QName(soap, "body"));
			bodyInput.setUse("literal");

			SOAPBody bodyOutput = new SOAPBodyImpl();
			bodyOutput.setElementType(new QName(soap, "body"));
			bodyOutput.setUse("literal");
			
			BindingInput bindInput = new BindingInputImpl();
			bindInput.addExtensibilityElement(bodyInput);

			BindingOutput bindOutput = new BindingOutputImpl();
			bindOutput.addExtensibilityElement(bodyOutput);
			
			SOAPOperation soapOp = new SOAPOperationImpl();
			soapOp.setElementType(new QName(soap, "operation"));
			soapOp.setSoapActionURI(actionName);
			soapOp.setStyle("document");
			
			BindingOperation bindOp = def.createBindingOperation();
			bindOp.setName(actionName);
			bindOp.addExtensibilityElement(soapOp);
			bindOp.setBindingInput(bindInput);
			bindOp.setBindingOutput(bindOutput);
			bindOp.setOperation(operation);
			
			binding.addBindingOperation(bindOp);
		}
		def.addPortType(portType);
		binding.setPortType(portType);
		def.addBinding(binding);
		
		SOAPAddress soapAddress = new SOAPAddressImpl();
		soapAddress.setElementType(new QName(soap, "address"));
		soapAddress.setLocationURI(endpointSoap.getAddressLocation());
		
		Port port = def.createPort();
		port.setName(serviceName);
		port.setBinding(binding);
		port.addExtensibilityElement(soapAddress);
		
		Service service = def.createService();
		service.setQName(new QName(tns, serviceName+"Service"));
		service.addPort(port);

		def.addService(service);
		return def;
	}

	@Override
	public List<Endpoint> getAllEndpoint() {
		return endpointService.findByType(EndpointType.SOAP);
	}
}
