package com.bifrost.endpoint.soap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerConfigurationException;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.support.DefaultExchange;
import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaAll;
import org.apache.ws.commons.schema.XmlSchemaAllMember;
import org.apache.ws.commons.schema.XmlSchemaComplexType;
import org.apache.ws.commons.schema.XmlSchemaElement;
import org.apache.ws.commons.schema.XmlSchemaSequence;
import org.apache.ws.commons.schema.XmlSchemaSequenceMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;

import com.bifrost.endpoint.AbstractCodec;
import com.bifrost.endpoint.soap.model.EndpointSoap;
import com.bifrost.endpoint.soap.model.EndpointSoapAction;
import com.bifrost.endpoint.soap.service.EndpointSoapActionService;
import com.bifrost.endpoint.soap.service.EndpointSoapService;
import com.bifrost.exception.endpoint.CodecProcessException;
import com.bifrost.message.ExternalMessage;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class Codec extends AbstractCodec {

	private static final ObjectReader objectReader = new XmlMapper().reader().forType(Map.class);
	
	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private EndpointSoapService soapService;

	@Autowired
	private EndpointSoapActionService actionService;
	
	@SuppressWarnings("unchecked")
	public ExternalMessage decode(Object obj, Object packager) throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		ExternalMessage result = new ExternalMessage();
		if(obj instanceof Message) {
			Message message = (Message) obj;
			result.putHeaders(message.getHeaders());
			Map<String, Object> xml = objectReader.readValue((byte[]) message.getBody());
			for(Entry<String, Object> entry : xml.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				if(key.toLowerCase().startsWith("head") && value!=null)
					result.putHeaders((Map<String, Object>) value);
				else if(key.toLowerCase().startsWith("body") && value!=null)
					result.putBody((Map<? extends String, ? extends Object>) value);
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public void mapValue(String responseCode, String responseDesc, XmlSchemaElement element, SOAPElement soapElement, ExternalMessage externalMessage, Map<String, Object> map) throws SOAPException {
		String prefix = "bfr";
		if(map!=null && map.get(element.getName()) instanceof ArrayList) {
			List<Map<String, Object>> list = (List<Map<String, Object>>) map.get(element.getName());
			for(Map<String, Object> m : list)
				mapValue(prefix, responseCode, responseDesc, element, soapElement, externalMessage, Map.of(element.getName(), m));
		}else
			mapValue(prefix, responseCode, responseDesc, element, soapElement, externalMessage, map);
	}
	
	@SuppressWarnings("unchecked")
	public void mapValue(String prefix, String responseCode, String responseDesc, XmlSchemaElement element, SOAPElement soapElement, ExternalMessage externalMessage, Map<String, Object> map) throws SOAPException {
		Long minOccurs = element.getMinOccurs();
		Object valueObj = null;
		if(map!=null)
			valueObj = map.get(element.getName());
		if(valueObj==null && minOccurs==0 && !responseCode.equals(element.getName()) && !responseDesc.equals(element.getName()))
			return;
		
		if(element.getSchemaType() instanceof XmlSchemaComplexType) {
			soapElement = soapElement.addChildElement(element.getName(), prefix);
			XmlSchemaComplexType type = (XmlSchemaComplexType) element.getSchemaType();
			if(type.getParticle() instanceof XmlSchemaSequence) {
				XmlSchemaSequence sequence = (XmlSchemaSequence) type.getParticle();
		    	for(XmlSchemaSequenceMember member : sequence.getItems()) {
		    		XmlSchemaElement child = (XmlSchemaElement) member;
		    		mapValue(responseCode, responseDesc, child, soapElement, externalMessage, (Map<String, Object>) valueObj);
		    	}	
			}else if(type.getParticle() instanceof XmlSchemaAll) {
				XmlSchemaAll all = (XmlSchemaAll) type.getParticle();
		    	for(XmlSchemaAllMember member : all.getItems()) {
		    		XmlSchemaElement child = (XmlSchemaElement) member;
		    		mapValue(responseCode, responseDesc, child, soapElement, externalMessage, (Map<String, Object>) valueObj);
		    	}
			}
		}else {
			if(responseCode!=null && !responseCode.isBlank() && responseCode.equals(element.getName())) {
				soapElement = soapElement.addChildElement(element.getName(), prefix);
				soapElement.setValue(externalMessage.getMappedResponseCode());
			}else if(responseCode!=null && !responseCode.isBlank() && responseDesc.equals(element.getName())) {
				soapElement = soapElement.addChildElement(element.getName(), prefix);
				soapElement.setValue(externalMessage.getMappedResponseDesc());	
			}else if(valueObj!=null) {
				soapElement = soapElement.addChildElement(element.getName(), prefix);
				soapElement.setValue(String.valueOf(valueObj));
			}
		}
	}
	
	public Object encode(ExternalMessage externalMessage, Object packager) throws SOAPException, CodecProcessException, IOException, TransformerConfigurationException {
		EndpointSoapAction action = actionService.findByEndpointCodeAndActionName(externalMessage.getEndpointCode(), externalMessage.getInvocationCode());
		XmlSchema xmlSchema = (XmlSchema) packager;
		String tns = xmlSchema.getTargetNamespace();
    	XmlSchemaElement outputElement = xmlSchema.getElementByName(action.getOutputElement());
    	byte[] payload;
    	if(outputElement==null)
    		throw new CodecProcessException("Cannot find output element with name '"+action.getOutputElement()+"'");
    	else {
    		SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
    	    SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
    	    soapEnvelope.addNamespaceDeclaration("bfr", tns);
    	    SOAPBody soapBody = soapEnvelope.getBody();
    	    mapValue(action.getRcFieldName(), action.getRdFieldName(), outputElement, soapBody, externalMessage, externalMessage.getBody());
    		ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	    soapMessage.writeTo(baos);
    	    payload = baos.toByteArray();
    	    baos.close();
    	}
	    
		if(externalMessage.isRequest()) {
			EndpointSoap soap = soapService.findById(action.getSoap().getEndpointCode());
			String address = soap.getAddressLocation();
			if(soap.isSslEnabled()!=null && soap.isSslEnabled())
				if(address.startsWith("http://"))
					address.replace("http://", "https://");
				else
					address = "https://"+address;
			else if(!address.startsWith("http://"))
				address = "https://"+address;
			
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.add("SOAPAction", action.getActionName());
			for(Entry<String, Object> header: externalMessage.getHeaders().entrySet())
				httpHeaders.set(header.getKey(), String.valueOf(header.getValue()));

			RequestEntity<String> requestEntity;
			requestEntity = new RequestEntity<String>(new String(payload), httpHeaders, HttpMethod.POST, URI.create(address.toString()));
			return requestEntity;
		}else {
			Exchange exchange = new DefaultExchange(camelContext);
			Message result = exchange.getMessage();
			result.setHeaders(externalMessage.getHeaders());
			result.setBody(payload);
			return result;
		}
	}

	public Object encodeUnknownInvocation(Object packager) throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		Message result = exchange.getMessage();
		result.setHeader(Exchange.HTTP_RESPONSE_CODE, 501);
		return result;
	}
}
