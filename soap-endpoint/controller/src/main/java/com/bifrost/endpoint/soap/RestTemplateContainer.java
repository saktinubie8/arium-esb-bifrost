package com.bifrost.endpoint.soap;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class RestTemplateContainer extends ConcurrentHashMap<String, RestTemplate>{
	
	private static final long serialVersionUID = 1048115224782678022L;

}
