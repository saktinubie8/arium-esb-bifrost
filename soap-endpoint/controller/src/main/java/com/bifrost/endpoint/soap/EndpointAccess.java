package com.bifrost.endpoint.soap;

import org.springframework.stereotype.Component;

import com.bifrost.endpoint.access.GenericCamelRestEndpointAccess;


/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class EndpointAccess extends GenericCamelRestEndpointAccess {
	
}
