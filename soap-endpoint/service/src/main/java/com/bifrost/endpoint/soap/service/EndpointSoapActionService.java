package com.bifrost.endpoint.soap.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.soap.model.EndpointSoapAction;
import com.bifrost.endpoint.soap.repository.EndpointSoapActionRepository;
import com.bifrost.exception.system.InvalidDataException;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointSoapActionService extends GenericService<EndpointSoapActionRepository, EndpointSoapAction, Long>{

	private static final String KEY = "endpoint:soap:action";
	
	public EndpointSoapActionService() {
		super(KEY);
	}

	@SuppressWarnings("unchecked")
	public List<EndpointSoapAction> findByEndpointCode(String endpointCode) {
		return (List<EndpointSoapAction>) hazelcastInstance.getMap(KEY+":findByEndpointCode").get(endpointCode);
	}

	public EndpointSoapAction findByEndpointCodeAndActionName(String endpointCode, String actionName) {
		return (EndpointSoapAction) hazelcastInstance.getMap(KEY+":findByEndpointCodeAndActionName").get(new SimpleKey(endpointCode, actionName));
	}
	
	@CachePut(cacheNames = KEY+":findByEndpointCodeAndActionName", key = "new org.springframework.cache.interceptor.SimpleKey(#t.soap.endpointCode, #t.actionName)")
	public EndpointSoapAction save(EndpointSoapAction t) {
		EndpointSoapAction retVal = super.save(t);
		String endpointCode = retVal.getSoap().getEndpointCode();
		IMap<String, List<EndpointSoapAction>> findByEndpointCode = hazelcastInstance.getMap(KEY+":findByEndpointCode");
		findByEndpointCode.set(endpointCode, repository.findBySoap_EndpointCode(endpointCode));
		return retVal;
	}

	@Caching(evict = {
			@CacheEvict(
					cacheNames = KEY+":findByEndpointCode", 
					key = "#t.soap.endpointCode"
			),
			@CacheEvict(
					cacheNames = KEY+":findByEndpointCodeAndActionName", 
					key = "new org.springframework.cache.interceptor.SimpleKey(#t.soap.endpointCode, #t.actionName)"
			)
	})
	
	public void delete(EndpointSoapAction t) {
		super.delete(t);
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByEndpointCode", allEntries = true), 
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndActionName", allEntries = true)})
	public void evictAll() {
		super.evictAll();
	}

	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, List<EndpointSoapAction>> findByEndpointCode = hazelcastInstance.getMap(KEY+":findByEndpointCode");
		IMap<SimpleKey, EndpointSoapAction> findByEndpointCodeAndActionName = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndActionName");
		for(EndpointSoapAction t : findAll()) {
			if(t.getSoap()==null)
				throw new InvalidDataException("Endpoint SOAP '"+t.getSoap().getEndpointCode()+"' with action ID "+t.getId()+" doesn't exists in table endpoint_soap");
			else if(t.getSoap().getEndpoint()==null)
				throw new InvalidDataException("Endpoint SOAP '"+t.getSoap().getEndpointCode()+"' doesn't exists in table endpoint");
			if(findByEndpointCode.get(t.getSoap().getEndpointCode())==null)
				findByEndpointCode.set(t.getSoap().getEndpointCode(), repository.findBySoap_EndpointCode(t.getSoap().getEndpointCode()));
			SimpleKey key = new SimpleKey(t.getSoap().getEndpointCode(), t.getActionName());
			if(findByEndpointCodeAndActionName.get(key)==null)
				findByEndpointCodeAndActionName.set(key, t);
		}
	}

}
