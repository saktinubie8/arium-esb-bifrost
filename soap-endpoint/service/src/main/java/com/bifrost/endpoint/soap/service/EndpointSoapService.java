package com.bifrost.endpoint.soap.service;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.soap.model.EndpointSoap;
import com.bifrost.endpoint.soap.repository.EndpointSoapRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointSoapService extends GenericService<EndpointSoapRepository, EndpointSoap, String>{

	private static final String KEY = "endpoint:soap";

	public EndpointSoapService() {
		super(KEY);
	}
	
	public EndpointSoap findByAddressLocation(String addressLocation) {
		return (EndpointSoap) hazelcastInstance.getMap(KEY+":findByAddressLocation").get(addressLocation);
	}

	@Override
	@CachePut(cacheNames = KEY+":findByAddressLocation", key = "#t.addressLocation")
	public EndpointSoap save(EndpointSoap t) {
		return super.save(t);
	}

	@Override
	@CacheEvict(cacheNames = KEY+":findByAddressLocation", key = "#t.addressLocation")
	public void delete(EndpointSoap t) {
		super.delete(t);
	}

	@Override
	@CacheEvict(cacheNames = KEY+":findByAddressLocation", allEntries = true)
	public void evictAll() {
		super.evictAll();
	}
	
	@Override
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, EndpointSoap> findByAddressLocation = hazelcastInstance.getMap(KEY+":findByAddressLocation");
		for(EndpointSoap t : findAll())
			if(findByAddressLocation.get(t.getAddressLocation())==null)
				findByAddressLocation.set(t.getAddressLocation(), t);
	}
}