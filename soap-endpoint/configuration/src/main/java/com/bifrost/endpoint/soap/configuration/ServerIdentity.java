package com.bifrost.endpoint.soap.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bifrost.common.banner.ModuleInfo;
import com.bifrost.common.constant.ParamConstant;
import com.bifrost.system.configuration.AbstractSystemIdentity;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class ServerIdentity extends AbstractSystemIdentity {

	@Value("${"+ParamConstant.Bifrost.MODULE_ENDPOINT_KAFKA_PORT+":9999}")
	private int port;
	
	public int getDefaultPort() {
		return port;
	}

	public String getModuleCode() {
		return ModuleInfo.SOAP_ENDPOINT_CODE;
	}
	
}
