package com.bifrost.endpoint.soap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.soap.model.EndpointSoapAction;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointSoapActionRepository extends JpaRepository<EndpointSoapAction, Long>{
	
	public List<EndpointSoapAction> findBySoap_EndpointCode(String endpointCode);
	public EndpointSoapAction findBySoap_EndpointCodeAndActionName(String endpointCode, String actionName);

}
