package com.bifrost.endpoint.soap.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.endpoint.model.Endpoint;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint_soap_action", uniqueConstraints = {@UniqueConstraint(columnNames = { "endpoint_code", "action_name"}) })
public class EndpointSoapAction implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "endpoint_code", nullable = false)
	private EndpointSoap soap;

	@Column(name = "headers", nullable = true)
	private String headers;

	@Column(name = "action_name", nullable = false, length = 100)
	private String actionName;

	@Column(name = "input_element", nullable = false, length = 100)
	private String inputElement;

	@Column(name = "output_element", nullable = false, length = 100)
	private String outputElement;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "rc_field_name", nullable = true, length = 40)
	private String rcFieldName;

	@Column(name = "rc_in_headers", nullable = true)
	private Boolean rcInHeaders;

	@Column(name = "rd_field_name", nullable = true, length = 40)
	private String rdFieldName;

	@Column(name = "rd_in_headers", nullable = true)
	private Boolean rdInHeaders;

	@Column(name = "white_list", nullable = true)
	private String whiteList;

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EndpointSoap getSoap() {
		return soap;
	}

	public void setSoap(EndpointSoap soap) {
		this.soap = soap;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getInputElement() {
		return inputElement;
	}

	public void setInputElement(String inputElement) {
		this.inputElement = inputElement;
	}

	public String getOutputElement() {
		return outputElement;
	}

	public void setOutputElement(String outputElement) {
		this.outputElement = outputElement;
	}

	public String getRcFieldName() {
		return rcFieldName;
	}

	public void setRcFieldName(String rcFieldName) {
		this.rcFieldName = rcFieldName;
	}

	public Boolean getRcInHeaders() {
		return rcInHeaders;
	}

	public void setRcInHeaders(Boolean rcInHeaders) {
		this.rcInHeaders = rcInHeaders;
	}

	public String getRdFieldName() {
		return rdFieldName;
	}

	public void setRdFieldName(String rdFieldName) {
		this.rdFieldName = rdFieldName;
	}

	public Boolean getRdInHeaders() {
		return rdInHeaders;
	}

	public void setRdInHeaders(Boolean rdInHeaders) {
		this.rdInHeaders = rdInHeaders;
	}

	public String getWhiteList() {
		return whiteList;
	}

	public void setWhiteList(String whiteList) {
		this.whiteList = whiteList;
	}

	public Object getPk() {
		return id;
	}

	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(soap.getEndpointCode());
		if(headers!=null) {
			out.writeBoolean(true);
			out.writeUTF(headers);
		}else
			out.writeBoolean(false);
		out.writeUTF(actionName);
		out.writeUTF(inputElement);
		out.writeUTF(outputElement);
		if(description!=null) {
			out.writeBoolean(true);
			out.writeUTF(description);
		}else
			out.writeBoolean(false);
		if(rcFieldName!=null) {
			out.writeBoolean(true);
			out.writeUTF(rcFieldName);
		}else
			out.writeBoolean(false);
		if(rcInHeaders!=null) {
			out.writeBoolean(true);
			out.writeBoolean(rcInHeaders);
		}else
			out.writeBoolean(false);
		if(rdFieldName!=null) {
			out.writeBoolean(true);
			out.writeUTF(rdFieldName);
		}else
			out.writeBoolean(false);
		if(rdInHeaders!=null) {
			out.writeBoolean(true);
			out.writeBoolean(rdInHeaders);
		}else
			out.writeBoolean(false);
		if(whiteList!=null) {
			out.writeBoolean(true);
			out.writeUTF(whiteList);
		}else
			out.writeBoolean(false);
	}

	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		if(soap==null)
			soap = new EndpointSoap();
		soap.setEndpointCode(in.readUTF());
		if(soap.getEndpoint()==null) {
			Endpoint endpoint = new Endpoint();
			endpoint.setCode(soap.getEndpointCode());
			soap.setEndpoint(endpoint);
		}
		if(in.readBoolean())
			headers = in.readUTF();
		actionName = in.readUTF();
		inputElement = in.readUTF();
		outputElement = in.readUTF();
		if(in.readBoolean())
			description = in.readUTF();
		if(in.readBoolean())
			rcFieldName = in.readUTF();
		if(in.readBoolean())
			rcInHeaders = in.readBoolean();
		if(in.readBoolean())
			rdFieldName = in.readUTF();
		if(in.readBoolean())
			rdInHeaders = in.readBoolean();
		if(in.readBoolean())
			whiteList = in.readUTF();
	}
}
