package com.bifrost.endpoint.soap.model;

import java.io.IOException;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.common.util.HazelcastUtils;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.system.model.EtcFile;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint_soap", uniqueConstraints = @UniqueConstraint(columnNames = {"endpoint_code", "address_location"}))
public class EndpointSoap implements AbstractModel {

	@Id
	@Column(name = "endpoint_code", nullable = false)
	private String endpointCode;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "endpoint_code")
	private Endpoint endpoint;

	@Column(name = "as_server", nullable = false)
	private Boolean asServer;

	@Column(name = "service_name", nullable = true)
	private String serviceName;

	@Column(name = "target_name_space", nullable = false, columnDefinition = "varchar(40) default 'http://bifrost.com'")
	private String targetNameSpace;

	@Column(name = "address_location", nullable = false, unique = true)
	private String addressLocation;

	@Column(name = "ssl", nullable = false, columnDefinition = "boolean default false")
	private Boolean ssl;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "cert")
	private EtcFile cert;

	@Column(name = "description", nullable = true, length = 255)
	private String description;

	@Column(name = "white_list", nullable = true)
	private String whiteList;
	
	@Column(name = "max_con", nullable = false, columnDefinition = "integer default 50")
	private int maxCon;
	
	@Column(name = "keep_alive_millis", nullable = false, columnDefinition = "integer default 20000")
	private int keepAliveMillis;
	
	@Column(name = "close_idle_millis", nullable = false, columnDefinition = "integer default 30000")
	private int closeIdleMillis;
	
	@Column(name = "con_to_millis", nullable = false, columnDefinition = "integer default 30000")
	private int conTOMillis;
	
	@Column(name = "req_to_millis", nullable = false, columnDefinition = "integer default 30000")
	private int reqTOMillis;
	
	@Column(name = "soc_to_millis", nullable = false, columnDefinition = "integer default 60000")
	private int socTOMillis;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "soap")
	private Set<EndpointSoapAction> actions;

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public void setAsServer(Boolean asServer) {
		this.asServer = asServer;
	}

	public Boolean isServer() {
		return this.asServer;
	}

	public Boolean isClient() {
		return !this.asServer;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getAddressLocation() {
		return addressLocation;
	}

	public void setAddressLocation(String addressLocation) {
		this.addressLocation = addressLocation;
	}

	public Boolean isSslEnabled() {
		return ssl;
	}

	public void setSsl(Boolean ssl) {
		this.ssl = ssl;
	}

	public EtcFile getCert() {
		return cert;
	}

	public void setCert(EtcFile cert) {
		this.cert = cert;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<EndpointSoapAction> getActions() {
		return actions;
	}

	public void setActions(Set<EndpointSoapAction> actions) {
		this.actions = actions;
	}

	public String getTargetNameSpace() {
		return targetNameSpace;
	}

	public void setTargetNameSpace(String targetNameSpace) {
		this.targetNameSpace = targetNameSpace;
	}

	public String getWhiteList() {
		return whiteList;
	}

	public void setWhiteList(String whiteList) {
		this.whiteList = whiteList;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public Object getPk() {
		return endpointCode;
	}

	public int getMaxCon() {
		return maxCon;
	}

	public void setMaxCon(int maxCon) {
		this.maxCon = maxCon;
	}

	public int getKeepAliveMillis() {
		return keepAliveMillis;
	}

	public void setKeepAliveMillis(int keepAliveMillis) {
		this.keepAliveMillis = keepAliveMillis;
	}

	public int getCloseIdleMillis() {
		return closeIdleMillis;
	}

	public void setCloseIdleMillis(int closeIdleMillis) {
		this.closeIdleMillis = closeIdleMillis;
	}

	public int getConTOMillis() {
		return conTOMillis;
	}

	public void setConTOMillis(int conTOMillis) {
		this.conTOMillis = conTOMillis;
	}

	public int getReqTOMillis() {
		return reqTOMillis;
	}

	public void setReqTOMillis(int reqTOMillis) {
		this.reqTOMillis = reqTOMillis;
	}

	public int getSocTOMillis() {
		return socTOMillis;
	}

	public void setSocTOMillis(int socTOMillis) {
		this.socTOMillis = socTOMillis;
	}

	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeUTF(endpointCode);
		out.writeBoolean(asServer);
		HazelcastUtils.writeUTF(serviceName, out);
		out.writeUTF(targetNameSpace);
		out.writeUTF(addressLocation);
		out.writeBoolean(ssl);
		if(ssl)
			if(cert!=null) {
				out.writeBoolean(true);
				out.writeLong(cert.getId());
			}else
				out.writeBoolean(false);
		HazelcastUtils.writeUTF(description, out);
		HazelcastUtils.writeUTF(whiteList, out);
		out.writeInt(maxCon);
		out.writeInt(keepAliveMillis);
		out.writeInt(closeIdleMillis);
		out.writeInt(conTOMillis);
		out.writeInt(reqTOMillis);
		out.writeInt(socTOMillis);
	}

	public void readData(ObjectDataInput in) throws IOException {
		endpointCode = in.readUTF();
		asServer = in.readBoolean();
		serviceName = HazelcastUtils.readUTF(in);
		targetNameSpace = in.readUTF();
		addressLocation = in.readUTF();
		ssl = in.readBoolean();
		if(ssl && in.readBoolean()) {
			if(cert==null)
				cert = new EtcFile();
			cert.setId(in.readLong());
		}
		description = HazelcastUtils.readUTF(in);
		whiteList = HazelcastUtils.readUTF(in);
		maxCon = in.readInt();
		keepAliveMillis = in.readInt();
		closeIdleMillis = in.readInt();
		conTOMillis = in.readInt();
		reqTOMillis = in.readInt();
		socTOMillis = in.readInt();
	}
}
