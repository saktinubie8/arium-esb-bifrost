package com.bifrost.endpoint.soap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.soap.model.EndpointSoap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointSoapRepository extends JpaRepository<EndpointSoap, String>{

	public EndpointSoap findByAddressLocation(String addressLocation);
	
}
