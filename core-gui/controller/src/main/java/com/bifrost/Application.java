package com.bifrost;

import com.bifrost.common.banner.BannerInfo;
import com.bifrost.common.banner.ModuleInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableAuthorizationServer
@EnableCaching
@EnableScheduling
@EnableAsync
@EnableWebSecurity
@EnableResourceServer
public class Application extends BaseApplication {

    public static void main(String[] args) {
        app = new SpringApplication(Application.class);
        app.setBanner(new BannerInfo(ModuleInfo.CORE_GUI_DESC));
        context = app.run(args);
    }

}