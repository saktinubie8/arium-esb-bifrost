package com.bifrost.core.gui.controller.security;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class JwtHazelcastTokenConfiguration {

    @Value("${security.oauth2.resource.jwt.key-value}")
    private String jwtKey;

    String strScope = "scope";

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        final JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter() {
            @Override
            public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
                Map<String, Object> temp = new LinkedHashMap<>(accessToken.getAdditionalInformation());
                accessToken.getAdditionalInformation().clear();
                accessToken.getAdditionalInformation().put("createdDatetime", new Date().getTime());
                accessToken.getAdditionalInformation().put("device", temp.get("device"));
                accessToken.getAdditionalInformation().put("ipAddress", temp.get("ipAddress"));
                accessToken.getAdditionalInformation().put("uuid", temp.get("uuid"));
                accessToken.getAdditionalInformation().put("roleList", temp.get("roleList"));
                if (temp.get(strScope) != null)
                    accessToken.getAdditionalInformation().put(strScope, temp.get(strScope));
                OAuth2AccessToken newAccessToken = super.enhance(accessToken, authentication);
                newAccessToken.getAdditionalInformation().putAll(temp);
                return newAccessToken;
            }

            @Override
            public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
                OAuth2Authentication outh = super.extractAuthentication(map);
                outh.setDetails(map);
                return outh;
            }
        };
        jwtAccessTokenConverter.setSigningKey(jwtKey);
        return jwtAccessTokenConverter;
    }

    @Bean
    public TokenStore tokenStore(HazelcastInstance hazelcastInstance) {
        return new JwtHazelcastTokenStore(jwtAccessTokenConverter(), hazelcastInstance);
    }

}