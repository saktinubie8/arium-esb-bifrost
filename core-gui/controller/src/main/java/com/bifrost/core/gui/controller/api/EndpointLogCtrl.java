package com.bifrost.core.gui.controller.api;

import com.bifrost.core.gui.controller.BaseController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.logging.model.dto.EndpointLogDto;
import com.bifrost.core.gui.service.EndpointLogService;
import com.bifrost.exception.gui.SystemErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/endpoint-logs")
public class EndpointLogCtrl extends BaseController {

    @Autowired
    private EndpointLogService endpointLogservice;

    @PostMapping(value = "/view/get-list-endpoint-logs/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<EndpointLogDto>> getAll(@RequestBody(required = false) DTRequestDto body,
                                                                @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(endpointLogservice.getAllEndpointLogs(body), HttpStatus.OK);
    }

}