package com.bifrost.core.gui.controller.api;

import com.bifrost.core.gui.controller.BaseController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.EndpointStatusDto;
import com.bifrost.core.gui.dao.dto.ModuleStatusDto;
import com.bifrost.core.gui.service.EndpointStatusService;
import com.bifrost.core.gui.service.ModuleStatusService;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.logging.model.dto.RouteLogDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/monitoring")
public class MonitoringCtrl extends BaseController {

    @Autowired
    private EndpointStatusService endpointStatusService;

    @Autowired
    private ModuleStatusService moduleStatusService;

    @PostMapping(value = "/view/get-list-endpoint/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<EndpointStatusDto>> getAllEndpointStatus(@RequestBody(required = false) DTRequestDto body,
                                                                                 @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(endpointStatusService.getAllEndpointStatus(body), HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-list-module/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<ModuleStatusDto>> getAllModuleStatus(@RequestBody(required = false) DTRequestDto body,
                                                                             @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(moduleStatusService.getAllModuleStatus(body), HttpStatus.OK);
    }

}