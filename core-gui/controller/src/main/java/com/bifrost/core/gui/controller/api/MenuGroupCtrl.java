package com.bifrost.core.gui.controller.api;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bifrost.core.gui.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.configuration.utils.JsonUtils;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.DualListBoxContainerDto;
import com.bifrost.core.gui.dao.dto.MenuGroupDto;
import com.bifrost.core.gui.dao.dto.Select2RequestDto;
import com.bifrost.core.gui.dao.dto.Select2ResponseDto;
import com.bifrost.core.gui.dao.dto.TreeEditorContainerDto;
import com.bifrost.core.gui.dao.dto.ValidationRequestDto;
import com.bifrost.core.gui.dao.dto.ValidationResponseDto;
import com.bifrost.core.gui.dao.model.MenuEntity;
import com.bifrost.core.gui.dao.model.MenuGroupEntity;
import com.bifrost.core.gui.service.MenuGroupService;
import com.bifrost.core.gui.service.MenuService;
import com.bifrost.exception.gui.SystemErrorException;

@RestController
@RequestMapping("/api/menu-group")
public class MenuGroupCtrl extends BaseController {

    @Autowired
    private MenuGroupService menuGroupService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private JsonUtils jsonUtils;

    @PostMapping(value = "/view/get-list-datatable/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<MenuGroupDto>> getAllMenuGroup(
            @RequestBody(required = false) DTRequestDto body,
            @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(menuGroupService.getMenuGroups(body), HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-id/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<MenuGroupDto>> getMenuGroupWithId(@RequestBody MenuGroupDto body,
                                                                          @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(menuGroupService.getMenuGroupWithId(body), HttpStatus.OK);
    }

    @PostMapping(value = "/select2/get-list-menu-group/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Select2ResponseDto> getSelect2Role(
            @RequestBody(required = false) Select2RequestDto body,
            @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(menuGroupService.getSelect2MenuGroup(body), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/save/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuGroupDto> saveMenuGroup(@RequestBody MenuGroupDto body,
                                                      @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(menuGroupService.saveMenuGroup(body, principal), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/delete/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuGroupDto[]> deleteMenuGroup(@RequestBody MenuGroupDto[] body,
                                                          @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal)
            throws SystemErrorException {
        if (!menuGroupService.deleteMenuGroupBatch(body, principal))
            throw new SystemErrorException(ErrorCode.ERR_SYS0500);
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @PostMapping(value = "/view/render-menu-group-tree-as-editor/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> renderMenuGroupTreeAsEditor(@RequestBody(required = false) MenuGroupDto body,
                                                              @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        try {
            List<TreeEditorContainerDto> newTree = menuGroupService.renderMenuGroupTreeAsEditor(body);
            String treeViewJson = jsonUtils.objToJson(newTree);
            return new ResponseEntity<>("{\r\n" + "	\"tree1\":" + treeViewJson + "}", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("{}", ErrorCode.ERR_SYS0500.getStatus());
        }
    }

    @PostMapping(value = "/view/render-menu-group-tree-as-viewer/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> renderMenuGroupTreeAsViewer(@RequestBody(required = false) MenuGroupDto body,
                                                              @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        try {
            List<TreeEditorContainerDto> newTree = menuGroupService.renderMenuGroupTreeAsViewer(body);
            String treeViewJson = jsonUtils.objToJson(newTree);
            return new ResponseEntity<>("{\r\n" + "	\"tree1\":" + treeViewJson + "}", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("{}", ErrorCode.ERR_SYS0500.getStatus());
        }
    }

    @PostMapping(value = "/view/populate-dual-list-box/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> populateDualListBox(@RequestBody MenuGroupDto body,
                                                      @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        try {
            MenuGroupEntity menuGroup = menuGroupService.findActiveByGroupId(body.getGroupId());
            List<MenuEntity> allMenus = menuService.findAllActive();
            Map<String, MenuEntity> mapOfmenusSelected = new HashMap<>();
            Set<MenuEntity> menusSelected = menuGroup.getMenus();
            // put into map for efficent comparison
            for (Iterator iterator = menusSelected.iterator(); iterator.hasNext(); ) {
                MenuEntity menuEntity = (MenuEntity) iterator.next();
                mapOfmenusSelected.put(menuEntity.getMenuId().toString(), menuEntity);
            }
            List<DualListBoxContainerDto> dualListBox = new ArrayList<>();
            for (Iterator iterator = allMenus.iterator(); iterator.hasNext(); ) {
                MenuEntity menuEntity = (MenuEntity) iterator.next();
                DualListBoxContainerDto dualList = new DualListBoxContainerDto();
                dualList.setKey(menuEntity.getMenuId().toString());
                dualList.setValue(menuEntity.getTitle());
                if (mapOfmenusSelected.containsKey(menuEntity.getMenuId())) {
                    dualList.setSelected(true);
                }
                dualListBox.add(dualList);
            }
            String json = "{\r\n" + "	\"dualListBox1\":" + jsonUtils.objToJson(dualListBox) + "}";
            logger.warn(json);
            return new ResponseEntity<>(json, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("{}", ErrorCode.ERR_SYS0500.getStatus());
        }
    }

    @PostMapping(value = "/transaction/jajalSaveRelasi/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuGroupDto> jajalSaveRelasi(@RequestBody MenuGroupDto body,
                                                        @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(menuGroupService.jajalSaveMenuGroup(body, principal),
                HttpStatus.OK);
    }

    @PostMapping(value = "/validation/validate-menu-group-name/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateUsername(@RequestBody ValidationRequestDto dto,
                                                   @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return menuGroupService.validateMenuGroupNameUnique(dto);
    }

    @PostMapping(value = "/validate/validate-group-code/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValidationResponseDto> isDecimalMaksSix(@RequestBody(required = false) ValidationRequestDto validationRequestDto,
                                                                  @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(menuGroupService.isGroupCodeUnique(validationRequestDto), HttpStatus.OK);
    }

}