package com.bifrost.core.gui.controller.security;

import com.bifrost.core.gui.dao.dto.MainMenuTreeContainerDto;
import com.bifrost.core.gui.dao.model.RoleEntity;
import com.bifrost.core.gui.dao.model.UserEntity;
import com.bifrost.core.gui.dao.repository.UserRepo;
import com.bifrost.core.gui.service.MenuService;
import com.bifrost.core.gui.service.TokenSessionService;
import com.bifrost.system.model.SystemParam;
import com.bifrost.system.repository.SystemParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Component
public class SecurityTokenEnhancer implements TokenEnhancer {

    @Autowired
    private MenuService menuService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TokenSessionService tokenSessionService;

    @Autowired
    private SystemParamRepository sysParamRepo;

    @Transactional
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String, Object> additionalInfo = new HashMap<>();
        UserEntity user = (UserEntity) authentication.getPrincipal();
        List<MainMenuTreeContainerDto> menus = menuService.renderMainMenuTree(user.getUsername());
        String allowMultipleSession = "true";
        SystemParam systemParameterEntity = sysParamRepo.findByName("SESSION01");
        if (systemParameterEntity != null)
            allowMultipleSession = systemParameterEntity.getValue();
        if (allowMultipleSession.equals("false")) {
            tokenSessionService.revokeAccessTokenByUsername(user.getUsername());
        }
        additionalInfo.put("email", user.getEmail());
        additionalInfo.put("name", user.getName());
        additionalInfo.put("menus", menus);
        additionalInfo.put("uuid", user.getUserId());
        UserEntity userDb = userRepo.findByUsername(user.getUsername());
        Set<String> roleList = new HashSet();
        for (RoleEntity e : userDb.getRoles()) {
            roleList.add(e.getRoleId().toString());
        }
        additionalInfo.put("roleList", roleList);
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.
                        currentRequestAttributes()).
                        getRequest();
        String ipAddress = request.getHeader("X-Forwarded-For");
        if (ipAddress == null)
            ipAddress = request.getRemoteHost();
        String device = request.getHeader("UserEntity-Agent");
        additionalInfo.put("ipAddress", ipAddress);
        additionalInfo.put("device", device);
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }

}