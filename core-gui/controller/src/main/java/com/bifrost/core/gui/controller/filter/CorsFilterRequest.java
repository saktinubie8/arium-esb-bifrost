package com.bifrost.core.gui.controller.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Rudi.Sadria
 * @version $Id: CorsFilterRequest.java 1436 2018-01-30 02:32:06Z bagus.sugitayasa $
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilterRequest implements Filter {

    @Value("${spring.application.name}")
    String name = "backend";

    protected final Logger logger = LogManager.getLogger(this.getClass());

    public CorsFilterRequest() {
        logger.info("SimpleCORSFilter init");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Authorization, Content-Type, enctype, x-forwarded-for, responseType, Content-Disposition");
        response.setHeader("Access-Control-Expose-Headers", "x-requested-with, Authorization, Content-Type, enctype, x-forwarded-for, responseType, Content-Disposition");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Service-Name", name);
        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
        //do nothing
    }

    @Override
    public void destroy() {
        //do nothing
    }

}