package com.bifrost.core.gui.controller.api;

import java.security.Principal;
import java.util.List;

import com.bifrost.core.gui.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.system.dto.SystemParamDto;
import com.bifrost.core.gui.dao.dto.ValidationRequestDto;
import com.bifrost.core.gui.service.SystemParameterService;
import com.bifrost.exception.gui.SystemErrorException;

@RestController
@RequestMapping("/api/system-parameter")
public class SystemParameterCtrl extends BaseController {

    @Autowired
    SystemParameterService systemParamService;

    @PostMapping(value = "/transaction/insert/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SystemParamDto> addSystemParam(@RequestBody SystemParamDto body,
                                                         @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(systemParamService.addParameter(body, principal),
                HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-list-parameters/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<SystemParamDto>> getAll(@RequestBody(required = false) DTRequestDto body,
                                                                @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(systemParamService.getParameters(body),
                HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-parameters-by-id/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SystemParamDto> getById(@RequestBody(required = false) SystemParamDto body,
                                                  @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(systemParamService.getParameterById(body.getId()),
                HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-list-parameters-by-groupcode/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SystemParamDto>> getAllByGroupCode(
            @RequestBody(required = false) SystemParamDto body,
            @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(systemParamService.getAllByGroupCode(body.getCategory()),
                HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/delete/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SystemParamDto[]> deleteSystemParam(@RequestBody SystemParamDto[] body,
                                                              @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(systemParamService.deleteParameter(body),
                HttpStatus.OK);
    }

    @PostMapping(value = "/validate/validate-parameter-code/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateParameter(@RequestBody ValidationRequestDto dto,
                                                    @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return systemParamService.validateParameterCodeUnique(dto);
    }

}