package com.bifrost.core.gui.controller.api;

import com.bifrost.core.gui.controller.BaseController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.system.dto.TokenServerDto;
import com.bifrost.core.gui.service.TokenServerService;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.system.model.TokenEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/token-server")
public class TokenServerCtrl extends BaseController {

    @Autowired
    private TokenServerService tokenServerService;

    @PostMapping(value = "/view/get-list-token/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<TokenServerDto>> getAll(@RequestBody(required = false) DTRequestDto body,
                                                                @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(tokenServerService.getTokenServer(body), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/insert/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TokenEntity> addToken(@RequestBody TokenServerDto body,
                                                @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(tokenServerService.createToken(body), HttpStatus.OK);
    }

}