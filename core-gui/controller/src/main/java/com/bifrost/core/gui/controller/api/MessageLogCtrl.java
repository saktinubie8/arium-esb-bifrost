package com.bifrost.core.gui.controller.api;

import com.bifrost.core.gui.dao.dto.MessageLogCountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bifrost.core.gui.controller.BaseController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.logging.model.dto.MessageLogDto;
import com.bifrost.core.gui.service.MessageLogService;
import com.bifrost.exception.gui.SystemErrorException;

@RestController
@RequestMapping("/api/message-log")
public class MessageLogCtrl extends BaseController {

    @Autowired
    private MessageLogService messageLogService;

    @PostMapping(value = "/view/get-list-message-logs/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<MessageLogDto>> getAll(@RequestBody(required = false) DTRequestDto body,
                                                               @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(messageLogService.getMessageLogs(body), HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-message-logs/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageLogDto> getAll(@RequestBody(required = false) MessageLogDto body,
                                                @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(messageLogService.getMessageLogs(body), HttpStatus.OK);
    }

    @PostMapping(value = "/count/response/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageLogCountDto> countResponseCode(@RequestBody(required = false) DTRequestDto body,
                                                           @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(messageLogService.getCountMessageLogs(body), HttpStatus.OK);
    }

}