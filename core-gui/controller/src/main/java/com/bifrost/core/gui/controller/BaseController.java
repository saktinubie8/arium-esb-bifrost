package com.bifrost.core.gui.controller;

import com.bifrost.common.constant.GUIConstant;
import com.bifrost.exception.gui.InternalServerException;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.exception.gui.DataNotFoundException;
import com.bifrost.core.gui.dao.response.ApiErrorResponse;
import com.bifrost.core.gui.dao.response.BaseResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;

public class BaseController {

    protected final Logger logger = LogManager.getLogger(this.getClass());

    @Autowired
    protected ApiErrorResponse errorResponse;

    /**
     * Handles all Exceptions not addressed by more specific
     * <code>@ExceptionHandler</code> methods. Creates a response with the Exception
     * Attributes in the response body as JSON and a HTTP status code of 500,
     * internal server error.
     *
     * @param exception An Exception instance.
     * @param request   The HttpServletRequest in which the Exception was raised.
     * @return A ResponseEntity containing the Exception Attributes in the body and
     * a HTTP status code 500.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<BaseResponse> handleException(HttpServletRequest request, Exception exception) {
        logger.error("Handling Exception", exception);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(ErrorCode.ERR_SYS0500.name());
        baseResponse.setResponseDesc(GUIConstant.FAILURE);
        return new ResponseEntity<>(baseResponse,
                ErrorCode.ERR_SYS0500.getStatus());
    }

    /**
     * Handles JPA NoResultExceptions thrown from web service controller methods.
     * Creates a response with Exception Attributes as JSON and HTTP status code
     * 404, not found.
     *
     * @param request The HttpServletRequest in which the NoResultException was raised.
     * @return A ResponseEntity containing the Exception Attributes in the body and
     * HTTP status code 404.
     */
    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<BaseResponse> handleNoResultException(DataNotFoundException exception,
                                                                HttpServletRequest request) {
        logger.error(stackTrace(exception));
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(ErrorCode.ERR_SYS0001.name());
        baseResponse.setResponseDesc(GUIConstant.FAILURE);
        return new ResponseEntity<>(baseResponse,
                ErrorCode.ERR_SYS0001.getStatus());
    }

    @ExceptionHandler(InternalServerException.class)
    public ResponseEntity<BaseResponse> handleInternalException(HttpServletRequest request, InternalServerException exception) {
        logger.error(stackTrace(exception));
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(ErrorCode.ERR_SYS0500.name());
        baseResponse.setResponseDesc(GUIConstant.FAILURE);
        return new ResponseEntity<>(baseResponse,
                ErrorCode.ERR_SYS0500.getStatus());
    }

    @ExceptionHandler(SystemErrorException.class)
    public ResponseEntity<BaseResponse> handleSystemException(HttpServletRequest request, SystemErrorException exception) {
        logger.error(stackTrace(exception));
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(exception.getErrorCode().name());
        baseResponse.setResponseDesc(GUIConstant.FAILURE);
        return new ResponseEntity<>(baseResponse,
                exception.getErrorCode().getStatus());
    }

    private String stackTrace(Exception exception) {
        StringWriter errors = new StringWriter();
        exception.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

}