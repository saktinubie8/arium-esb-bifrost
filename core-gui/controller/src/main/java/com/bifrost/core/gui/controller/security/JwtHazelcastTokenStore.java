package com.bifrost.core.gui.controller.security;

import com.hazelcast.core.HazelcastInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class JwtHazelcastTokenStore extends JwtTokenStore {

    public static final String KEY_REFRESHTOKEN_LIST = "security.refreshTokenList";
    public static final String KEY_ACCESSTOKEN_LIST = "security.accessTokenList";
    private static final String LOGGER_FAIL_ACCESS_HAZELCAST = "Fail accessing token via Hazelcast";
    protected final Logger logger = LogManager.getLogger(this.getClass());
    private HazelcastInstance instance;

    public JwtHazelcastTokenStore(JwtAccessTokenConverter jwtTokenEnhancer, HazelcastInstance instance) {
        super(jwtTokenEnhancer);
        this.instance = instance;
    }

    public JwtHazelcastTokenStore(JwtAccessTokenConverter jwtTokenEnhancer) {
        super(jwtTokenEnhancer);
    }

    @Override
    public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
        logger.debug("readAuthentication from Hazelcast:" + token.toString());
        if (instance != null && instance.getLifecycleService().isRunning() && !instance.getMap(KEY_ACCESSTOKEN_LIST).containsKey(token.getValue()))
            throw new InvalidTokenException("Access Token Not found");
        return super.readAuthentication(token);
    }

    @Override
    public OAuth2Authentication readAuthentication(String token) {
        logger.debug("readAuthentication:" + token);
        return super.readAuthentication(token);
    }

    @Override
    public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
        logger.debug("storeAccessToken:" + token.toString() + " , " + authentication.toString());
        super.storeAccessToken(token, authentication);
        try {
            instance.getMap(KEY_ACCESSTOKEN_LIST).putAsync(token.getValue(), authentication.getName(), token.getExpiresIn(), TimeUnit.SECONDS);
            if (authentication.getOAuth2Request().getRefreshTokenRequest() != null && "refresh_token".equals(authentication.getOAuth2Request().getRefreshTokenRequest().getGrantType())) {
                DefaultExpiringOAuth2RefreshToken expired = (DefaultExpiringOAuth2RefreshToken) token.getRefreshToken();
                Date expDate = expired.getExpiration();
                Date currentDate = new Date();
                int expiredInSeconds = (int) ((expDate.getTime() / 1000) - (currentDate.getTime() / 1000));
                instance.getMap(KEY_REFRESHTOKEN_LIST).putAsync(expired.getValue(), authentication.getName(), expiredInSeconds, TimeUnit.SECONDS);
            }
        } catch (Exception e) {
            logger.warn(LOGGER_FAIL_ACCESS_HAZELCAST, e);
        }
    }

    @Override
    public OAuth2AccessToken readAccessToken(String tokenValue) {
        logger.debug("readAccessToken:" + tokenValue);
        return super.readAccessToken(tokenValue);
    }

    @Override
    public void removeAccessToken(OAuth2AccessToken token) {
        logger.debug("removeAccessToken:" + token.toString());
        super.removeAccessToken(token);
        try {
            instance.getMap(KEY_ACCESSTOKEN_LIST).remove(token.getValue());
        } catch (Exception e) {
            logger.warn(LOGGER_FAIL_ACCESS_HAZELCAST, e);
        }
    }

    @Override
    public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
        logger.debug("storeRefreshToken:" + refreshToken.toString() + " , " + authentication.toString());
        super.storeRefreshToken(refreshToken, authentication);
        try {
            if (refreshToken instanceof DefaultExpiringOAuth2RefreshToken) {
                DefaultExpiringOAuth2RefreshToken expired = (DefaultExpiringOAuth2RefreshToken) refreshToken;
                Date expDate = expired.getExpiration();
                Date currentDate = new Date();
                int expiredInSeconds = (int) ((expDate.getTime() / 1000) - (currentDate.getTime() / 1000));
                instance.getMap(KEY_REFRESHTOKEN_LIST).putAsync(refreshToken.getValue(), authentication.getName(), expiredInSeconds, TimeUnit.SECONDS);
            }
        } catch (Exception e) {
            logger.warn(LOGGER_FAIL_ACCESS_HAZELCAST, e);
        }
    }

    @Override
    public OAuth2RefreshToken readRefreshToken(String tokenValue) {
        logger.debug("readRefreshToken:" + tokenValue);
        return super.readRefreshToken(tokenValue);
    }

    @Override
    public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
        logger.debug("readAuthenticationForRefreshToken from hazelcast:" + token.toString());
        if (instance != null && instance.getLifecycleService().isRunning() && !instance.getMap(KEY_REFRESHTOKEN_LIST).containsKey(token.getValue()))
            throw new InvalidTokenException("Refresh Token Not found");
        return super.readAuthenticationForRefreshToken(token);
    }

    @Override
    public void removeRefreshToken(OAuth2RefreshToken token) {
        logger.debug("removeRefreshToken:" + token.toString());
        super.removeRefreshToken(token);
        try {
            instance.getMap(KEY_REFRESHTOKEN_LIST).remove(token.getValue());
        } catch (Exception e) {
            logger.warn(LOGGER_FAIL_ACCESS_HAZELCAST, e);
        }
    }

    @Override
    public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
        logger.debug("removeAccessTokenUsingRefreshToken:" + refreshToken.toString());
        super.removeAccessTokenUsingRefreshToken(refreshToken);
    }

    @Override
    public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
        logger.debug("getAccessToken:" + authentication.toString());
        return super.getAccessToken(authentication);
    }

}