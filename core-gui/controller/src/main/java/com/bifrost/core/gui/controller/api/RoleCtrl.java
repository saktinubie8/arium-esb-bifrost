package com.bifrost.core.gui.controller.api;

import java.security.Principal;

import com.bifrost.core.gui.controller.BaseController;
import com.bifrost.core.gui.configuration.utils.AuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.RoleDto;
import com.bifrost.core.gui.dao.dto.Select2RequestDto;
import com.bifrost.core.gui.dao.dto.Select2ResponseDto;
import com.bifrost.core.gui.dao.dto.ValidationRequestDto;
import com.bifrost.core.gui.dao.dto.ValidationResponseDto;
import com.bifrost.core.gui.service.RoleService;
import com.bifrost.exception.gui.SystemErrorException;

@RestController
@RequestMapping("/api/role")
public class RoleCtrl extends BaseController {

    @Autowired
    private RoleService roleService;

    @PostMapping(value = "/view/get-all-roles/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<RoleDto>> getAllRoles(@RequestBody(required = false) DTRequestDto body,
                                                              @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(roleService.getRoles(body), HttpStatus.OK);
    }

    @PostMapping(value = "/select2/get-list-roles/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Select2ResponseDto> getSelect2Role(@RequestBody(required = false) Select2RequestDto body,
                                                             @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(roleService.getSelect2Role(body), HttpStatus.OK);
    }

    @AuditLog(title = "Role Management", description = "Insert/Update Data Role", module = "Master Role")
    @PostMapping(value = "/transaction/insert/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleDto> saveRole(@RequestBody RoleDto body,
                                            @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(roleService.saveRole(body), HttpStatus.OK);
    }

    @AuditLog(title = "Role Management", description = "Delete Data Role", module = "Master Role")
    @PostMapping(value = "/transaction/delete/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleDto[]> deleteRole(@RequestBody RoleDto[] body,
                                                @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        roleService.deleteRole(body, principal);
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @PostMapping(value = "/validate/is-role-duplicate/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValidationResponseDto> isRoleDuplicate(@RequestBody(required = false) ValidationRequestDto body,
                                                                 @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return roleService.isRoleDuplicate(body);
    }

    @PostMapping(value = "/view/get-role-by-role-id/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleDto> getRoleByRoleId(@RequestBody(required = false) RoleDto body,
                                                   @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(roleService.getRoleByRoleId(body), HttpStatus.OK);
    }

    @PostMapping(value = "/validate/validate-role-code/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateParameterCode(@RequestBody ValidationRequestDto dto,
                                                        @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return roleService.validateRoleCodeUnique(dto);
    }

    @PostMapping(value = "/validate/validate-role-name/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateParameterName(@RequestBody ValidationRequestDto dto,
                                                        @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return roleService.validateRoleNameUnique(dto);
    }

}