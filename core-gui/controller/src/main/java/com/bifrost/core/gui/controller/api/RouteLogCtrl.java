package com.bifrost.core.gui.controller.api;

import com.bifrost.core.gui.controller.BaseController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.service.RouteLogService;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.logging.model.dto.RouteLogDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/route-logs")
public class RouteLogCtrl extends BaseController {

    @Autowired
    private RouteLogService routeLogservice;

    @PostMapping(value = "/view/get-list-route-logs/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<RouteLogDto>> getAll(@RequestBody(required = false) DTRequestDto body,
                                                             @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(routeLogservice.getAllRouteLogs(body), HttpStatus.OK);
    }

}