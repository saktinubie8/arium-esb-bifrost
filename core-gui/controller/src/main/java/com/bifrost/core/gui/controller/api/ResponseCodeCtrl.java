package com.bifrost.core.gui.controller.api;

import com.bifrost.core.gui.controller.BaseController;
import com.bifrost.core.gui.dao.dto.*;
import com.bifrost.core.gui.service.ResponseCodeUIService;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.orchestrator.service.mapper.ResponseCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/rc")
public class ResponseCodeCtrl extends BaseController {

    @Autowired
    private ResponseCodeUIService responseCodeUIService;


    @PostMapping(value = "/view/get-list-response-code/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<ResponseCodeDto>> getAllResponseCode(@RequestBody(required = false) DTRequestDto body,
                                                                               @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(responseCodeUIService.getAllResponseCode(body), HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-user/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseCodeDto> getRC(@RequestBody(required = false) ResponseCodeDto body,
                                          @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(responseCodeUIService.findResponseCode(body), HttpStatus.OK);
    }

    @PostMapping(value = "/description/update/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseCodeDto> setUser(@RequestBody ResponseCodeDto body,
                                           @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(responseCodeUIService.updateResponseCode(body), HttpStatus.OK);
    }

}