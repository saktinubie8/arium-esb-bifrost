package com.bifrost.core.gui.controller.api;

import java.security.Principal;
import java.util.Base64;
import java.util.List;

import com.bifrost.core.gui.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.DualListBoxContainerDto;
import com.bifrost.core.gui.dao.dto.UserDto;
import com.bifrost.core.gui.dao.dto.ValidationRequestDto;
import com.bifrost.core.gui.dao.dto.ValidationResponseDto;
import com.bifrost.core.gui.dao.model.UserEntity;
import com.bifrost.core.gui.service.UserService;
import com.bifrost.exception.gui.SystemErrorException;

@RestController
@RequestMapping("/api/user")
public class UserCtrl extends BaseController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/view/get-list-users/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<UserDto>> getAll(@RequestBody(required = false) DTRequestDto body,
                                                         @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(userService.getUsers(body), HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-user/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getAll(@RequestBody(required = false) UserDto body,
                                          @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(userService.getUser(body), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/insert/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> addUser(@RequestBody UserDto body,
                                           @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(userService.addUser(body, principal), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/update/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> setUser(@RequestBody UserDto body,
                                           @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(userService.setUser(body, principal), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/update-profile/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> setUserProfile(@RequestBody UserDto body,
                                                  @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(userService.setUserProfile(body, principal), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/update-password/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> changePassword(@RequestBody UserDto body, Principal principal) {
        String clearPass = new String(Base64.getDecoder().decode(body.getPassword()));
        ValidationResponseDto valCheck = this.userService.validatePasswordRule(clearPass, principal);
        if (!valCheck.getResponValidations().isEmpty())
            return new ResponseEntity(valCheck, HttpStatus.FORBIDDEN);
        else
            return new ResponseEntity<>(userService.changeUserPassword(body, principal), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/delete/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto[]> deleteUser(@RequestBody UserDto[] body,
                                                @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(userService.deleteUser(body, principal), HttpStatus.OK);
    }

    @PostMapping(value = "/validate-username/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateUsername(@RequestBody ValidationRequestDto dto,
                                                   @RequestHeader(name = "Accept-Language", required = false) String locale, OAuth2Authentication auth) {
        return userService.validateUsernameUnique(dto);
    }

    @PostMapping(value = "/validate/validate-pass/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValidationResponseDto> validateOldPassword(@RequestBody UserDto dto,
                                                                     @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return userService.validateUserOldPassword(dto);
    }

    @PostMapping(value = "/validate/validate-password-rule/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValidationResponseDto> validatePasswordRule(@RequestBody ValidationRequestDto dto, Principal principal) {
        ValidationResponseDto resp = this.userService.validatePasswordRule(dto.getValueToValidate(), principal);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/reset-password-users/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValidationResponseDto> setUserPassword(@RequestBody UserDto body,
                                                                 @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return new ResponseEntity<>(this.userService.doResetPasswordUser(body), HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-role-dual-list-box/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DualListBoxContainerDto>> getRoleSchemeDualListBox(
            @RequestBody(required = false) UserDto body,
            @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(userService.getRoleDualListBox(
                body.getUserId(), UserEntity.class.getName()), HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-menugroup-dual-list-box/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DualListBoxContainerDto>> getMenuGroupSchemeDualListBox(
            @RequestBody(required = false) UserDto body,
            @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(userService.getMenuGroupDualListBox(
                body.getUserId(), UserEntity.class.getName()), HttpStatus.OK);
    }

}