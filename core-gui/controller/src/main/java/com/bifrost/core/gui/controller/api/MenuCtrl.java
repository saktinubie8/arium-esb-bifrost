package com.bifrost.core.gui.controller.api;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import com.bifrost.core.gui.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.configuration.utils.JsonUtils;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.MainMenuTreeContainerDto;
import com.bifrost.core.gui.dao.dto.MenuDto;
import com.bifrost.core.gui.dao.dto.Select2RequestDto;
import com.bifrost.core.gui.dao.dto.Select2ResponseDto;
import com.bifrost.core.gui.dao.dto.TreeEditorContainerDto;
import com.bifrost.core.gui.dao.dto.ValidationRequestDto;
import com.bifrost.core.gui.dao.model.MenuEntity;
import com.bifrost.core.gui.service.MenuService;
import com.bifrost.exception.gui.SystemErrorException;

@RestController
@RequestMapping("/api/menu")
public class MenuCtrl extends BaseController {

    @Autowired
    private MenuService menuService;

    @Autowired
    private JsonUtils jsonUtils;

    @PostMapping(value = "/view/get-menu-by-menu-id/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuDto> getMenuByMenuId(@RequestBody MenuDto body,
                                                   @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        return new ResponseEntity<>(menuService.getMenuByIdActivev2(body.getMenuId()),
                HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-all-menu-as-model/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuDto> getAllMenuAsModel(@RequestBody(required = false) MenuDto body,
                                                     @RequestHeader(name = "Accept-Language", required = false) String locale) {
        List<MenuEntity> menuList = menuService.findAllActive();
        List<MenuDto> response = new ArrayList<>();
        for (MenuEntity menu : menuList) {
            response.add(menu.parseDto(false));
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/view/get-list-menus/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTResponseDto<MenuDto>> getListMenus(@RequestBody(required = false) DTRequestDto body,
                                                               @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(menuService.getMenus(body), HttpStatus.OK);
    }

    @PostMapping(value = "/select2/get-list-menus/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Select2ResponseDto> getSelect2ListMenus(@RequestBody(required = false) Select2RequestDto body,
                                                                  @RequestHeader(name = "Accept-Language", required = false) String locale) throws SystemErrorException {
        return new ResponseEntity<>(menuService.getSelect2Menu(body), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/save/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuDto> saveRole(@RequestBody MenuDto body,
                                            @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) throws SystemErrorException {
        return new ResponseEntity<>(menuService.saveMenu(body, principal), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/delete/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuDto> deleteMenu(@RequestBody MenuDto body,
                                              @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal)
            throws SystemErrorException {
        if (!menuService.deleteMenu(body, principal))
            throw new SystemErrorException(ErrorCode.ERR_SYS0500);
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/delete-batch/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MenuDto[]> deleteBatchMenu(@RequestBody MenuDto[] body,
                                                     @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal)
            throws SystemErrorException {
        if (!menuService.deleteBatchMenu(body, principal))
            throw new SystemErrorException(ErrorCode.ERR_SYS0500);
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/insert/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> insertMenu(@RequestBody MenuEntity menu) {
        ResponseEntity<String> response = null;
        try {
            menuService.createMenu(menu);
            response = new ResponseEntity<>(jsonUtils.objToJson(menu), HttpStatus.CREATED);
        } catch (Exception e) {
            response = new ResponseEntity<>(jsonUtils.objToJson(menu), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @PostMapping(value = "/view/get-all-menu-id/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllMenuId(HttpServletResponse response) {
        try {
            StringBuilder sb = new StringBuilder("{");
            List<MenuEntity> menus = menuService.getMenus();
            for (Iterator iterator = menus.iterator(); iterator.hasNext(); ) {
                MenuEntity menuEntity = (MenuEntity) iterator.next();
                sb.append("\"" + menuEntity.getMenuId() + "\"");
                if (iterator.hasNext()) {
                    sb.append(",");
                }
            }
            sb.append("}");
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    @PostMapping(value = "/view/render-parent-id/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> renderParentId(@RequestBody MenuEntity menu) {
        String excludeMenuId = menu.getMenuId().toString();
        try {
            StringBuilder sb = new StringBuilder("[");
            List<MenuEntity> menus = menuService.findAllActive();
            for (Iterator iterator = menus.iterator(); iterator.hasNext(); ) {
                MenuEntity menuEntity = (MenuEntity) iterator.next();
                if (!menuEntity.getMenuId().equals(UUID.fromString(excludeMenuId))) {
                    sb.append("{" + "\"" + "key" + "\"" + ":" + "\"" + menuEntity.getMenuId() + "\"" + "," + "\""
                            + "value" + "\"" + ":" + "\"" + menuEntity.getTitle() + "\"");
                    if (iterator.hasNext()) {
                        sb.append("},");
                    } else {
                        sb.append("}");
                    }
                }
            }
            sb.append("]");
            String result = sb.toString();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/view/render-main-menu-tree/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> renderMainMenuTree(
            @RequestHeader(name = "Accept-Language", required = false) String locale, Principal principal) {
        try {
            List<MainMenuTreeContainerDto> newTree2 = menuService.renderMainMenuTree(principal.getName());
            return new ResponseEntity<>(jsonUtils.objToJson(newTree2), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/view/render-menu-tree-as-editor/v.1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> renderMenuTreeAsEditor() {
        try {
            logger.debug("Try Render Menu Tree");
            List<TreeEditorContainerDto> newTree = menuService.renderMenuTreeAsEditor();
            String treeViewJson = jsonUtils.objToJson(newTree);
            return new ResponseEntity<>("{\r\n" + "	\"tree1\":" + treeViewJson + "}", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/validation/validate-menu-title/v.1", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateUsername(@RequestBody ValidationRequestDto dto,
                                                   @RequestHeader(name = "Accept-Language", required = false) String locale) {
        return menuService.validateTitleNameUnique(dto);
    }

}