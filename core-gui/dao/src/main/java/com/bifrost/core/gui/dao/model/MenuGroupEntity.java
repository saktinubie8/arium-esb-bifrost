package com.bifrost.core.gui.dao.model;

import com.bifrost.core.gui.dao.dto.MenuDto;
import com.bifrost.core.gui.dao.dto.MenuGroupDto;
import com.bifrost.core.gui.dao.dto.TreeEditorContainerDto;
import com.bifrost.core.gui.dao.dto.UserDto;
import com.bifrost.common.model.ApprovalAuditTrailEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "M_MENU_GROUP")
public class MenuGroupEntity extends ApprovalAuditTrailEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /***************************** - Field - ****************************/
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "GROUP_ID", nullable = false, unique = true)
    @Type(type = "pg-uuid")
    private UUID groupId;

    @Column(name = "GROUP_NAME", unique = true)
    private String groupName;

    @Column(name = "DESCRIPTION", length = 200)
    private String description;

    @Column(name = "GROUP_CODE", length = 20, unique = true)
    private String groupCode;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "R_USER_MENU_GROUP", joinColumns = {
            @JoinColumn(name = "groupId")}, inverseJoinColumns = @JoinColumn(name = "userId"))
    @JsonIgnore
    private Set<UserEntity> user = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "R_MENU_GROUP_MENU", joinColumns = {
            @JoinColumn(name = "groupId")}, inverseJoinColumns = @JoinColumn(name = "menuId"))
    @JsonIgnore
    private Set<MenuEntity> menus = new HashSet<>();

    /***************************** - Constructor - ****************************/
    public MenuGroupEntity() {
    }

    public MenuGroupEntity(UUID id) {
        this.groupId = id;
    }

    public MenuGroupEntity(MenuGroupEntity menuGroup) {
        this.groupId = menuGroup.groupId;
        this.groupName = menuGroup.groupName;
        this.groupCode = menuGroup.groupCode;
        this.description = menuGroup.description;
        this.version = menuGroup.version;
        this.isActive = menuGroup.isActive;
        this.createdBy = menuGroup.createdBy;
        this.createdDate = menuGroup.createdDate;
        this.modifiedBy = menuGroup.modifiedBy;
        this.modifiedDate = menuGroup.modifiedDate;
        this.lockedStatus = menuGroup.lockedStatus;
        this.lockedBy = menuGroup.lockedBy;
        this.lastApprovedBy = menuGroup.lastApprovedBy;
        this.lastApprovedDate = menuGroup.lastApprovedDate;
    }

    public MenuGroupEntity(MenuGroupDto menuGroupDto) {
        this.groupId = UUID.fromString(menuGroupDto.getGroupId());
        this.groupName = menuGroupDto.getGroupName();
        this.groupCode = menuGroupDto.getGroupCode();
        this.description = menuGroupDto.getDescription();
        this.isActive = menuGroupDto.getActive();
        this.version = menuGroupDto.getVersion();
        this.createdBy = menuGroupDto.getCreatedBy();
        this.createdDate = menuGroupDto.getCreatedDateParse();
        this.modifiedBy = menuGroupDto.getModifiedBy();
        this.modifiedDate = menuGroupDto.getModifiedDateParse();
        this.lockedBy = menuGroupDto.getLockedBy();
        this.lockedStatus = menuGroupDto.getLockedStatus();
        this.lastApprovedBy = menuGroupDto.getLastApprovedBy();
        this.lastApprovedDate = menuGroupDto.getLastApprovedDate();
        for (MenuDto menu : menuGroupDto.getMenus()) {
            this.menus.add(new MenuEntity(menu));
        }
        if (menuGroupDto.getUser() != null) {
            for (UserDto userDto : menuGroupDto.getUser()) {
                this.user.add(new UserEntity(userDto));
            }
        } else {
            this.user = null;
        }
    }

    public MenuGroupDto parseDto() {
        MenuGroupDto menuGroupDto = new MenuGroupDto();
        menuGroupDto.setGroupId(this.groupId.toString());
        menuGroupDto.setGroupName(this.groupName);
        menuGroupDto.setGroupCode(this.groupCode);
        menuGroupDto.setDescription(this.description);
        menuGroupDto.setIsActive(this.isActive);
        menuGroupDto.setVersion(this.version);
        menuGroupDto.setCreatedBy(this.createdBy);
        menuGroupDto.setCreatedDate(this.createdDate);
        menuGroupDto.setModifiedBy(this.modifiedBy);
        menuGroupDto.setModifiedDate(this.modifiedDate);
        menuGroupDto.setLockedBy(this.lockedBy);
        menuGroupDto.setLockedStatus(this.lockedStatus);
        menuGroupDto.setLastApprovedBy(this.lastApprovedBy);
        menuGroupDto.setLastApprovedDate(this.lastApprovedDate);
        Set<MenuDto> menuDtos = new HashSet<>();
        for (Iterator iterator = this.menus.iterator(); iterator.hasNext(); ) {
            MenuEntity menuEntity = (MenuEntity) iterator.next();
            if (menuEntity.getActive()) {
                menuDtos.add(menuEntity.parseDto(false));
            }
        }
        menuGroupDto.setMenus(menuDtos);
        Set<UserDto> userDtos = new HashSet<>();
        for (Iterator iterator = this.user.iterator(); iterator.hasNext(); ) {
            UserEntity userEntity = (UserEntity) iterator.next();
            if (userEntity.getActive()) {
                userDtos.add(userEntity.parseDtoAsMenuGroupChild());
            }
        }
        menuGroupDto.setUser(userDtos);
        return menuGroupDto;
    }

    public TreeEditorContainerDto parseTreeEditorContainer() {
        TreeEditorContainerDto treeEditorContainer = new TreeEditorContainerDto();
        treeEditorContainer.setId(this.groupId.toString());
        treeEditorContainer.setContent(this.groupName);
        treeEditorContainer.setLevel(1);
        treeEditorContainer.setOrder(1);
        treeEditorContainer.setDataMenuGroupDto(this.parseDto());
        List<TreeEditorContainerDto> children = new ArrayList<>();
        List<MenuEntity> menuToInclude = new ArrayList<>();
        menuToInclude.addAll(this.menus);
        for (Iterator iterator2 = this.menus.iterator(); iterator2.hasNext(); ) {
            MenuEntity menuEntity = (MenuEntity) iterator2.next();
            if (menuEntity.getActive()) {
                TreeEditorContainerDto childDto = menuEntity.parseTreeEditorContainer(false, false, menuToInclude);
                childDto.setEditable(false);
                childDto.setDeletable(false);
                children.add(childDto);
            }
        }
        if (!children.isEmpty()) {
            treeEditorContainer.setChildren(children);
        }
        return treeEditorContainer;
    }

    /***************************** - Other Method - ****************************/

    public enum System {
        ADMIN("ADMIN"), CS("CS"), CUSTOMER("CUSTOMER"), SYSTEM("SYSTEM");

        private String authority;

        System(String authority) {
            this.authority = authority;
        }

        public String getAuthority() {
            return this.authority;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MenuGroupEntity other = (MenuGroupEntity) obj;
        if (groupId == null) {
            if (other.groupId != null)
                return false;
        } else if (!groupId.equals(other.groupId))
            return false;
        if (groupName == null) {
            if (other.groupName != null)
                return false;
        } else if (!groupName.equals(other.groupName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "MenuGroupEntity [groupId=" + groupId + ", groupName=" + groupName + "]";
    }

    //isa here
    public MenuGroupDto parseDtoAsMenusChild() {
        MenuGroupDto menuGroupDto = new MenuGroupDto();
        menuGroupDto.setGroupId(this.groupId.toString());
        menuGroupDto.setGroupName(this.groupName);
        menuGroupDto.setGroupCode(this.groupCode);
        menuGroupDto.setDescription(this.description);
        menuGroupDto.setIsActive(this.isActive);
        menuGroupDto.setVersion(this.version);
        menuGroupDto.setCreatedBy(this.createdBy);
        menuGroupDto.setCreatedDate(this.createdDate);
        menuGroupDto.setModifiedBy(this.modifiedBy);
        menuGroupDto.setModifiedDate(this.modifiedDate);
        menuGroupDto.setLockedBy(this.lockedBy);
        menuGroupDto.setLockedStatus(this.lockedStatus);
        menuGroupDto.setLastApprovedBy(this.lastApprovedBy);
        menuGroupDto.setLastApprovedDate(this.lastApprovedDate);
        return menuGroupDto;
    }

    public UUID getGroupId() {
        return groupId;
    }

    public void setGroupId(UUID groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public Set<UserEntity> getUser() {
        return user;
    }

    public void setUser(Set<UserEntity> user) {
        this.user = user;
    }

    public Set<MenuEntity> getMenus() {
        return menus;
    }

    public void setMenus(Set<MenuEntity> menus) {
        this.menus = menus;
    }

}