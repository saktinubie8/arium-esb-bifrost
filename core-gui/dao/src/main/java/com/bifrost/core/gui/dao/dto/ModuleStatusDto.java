package com.bifrost.core.gui.dao.dto;

public class ModuleStatusDto extends GenericDto {

    private String moduleCode;
    private String moduleHost;

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getModuleHost() {
        return moduleHost;
    }

    public void setModuleHost(String moduleHost) {
        this.moduleHost = moduleHost;
    }
}
