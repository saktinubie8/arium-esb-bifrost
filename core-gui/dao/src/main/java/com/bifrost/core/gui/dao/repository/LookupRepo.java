package com.bifrost.core.gui.dao.repository;

import com.bifrost.core.gui.dao.model.LookupEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface LookupRepo extends JpaRepository<LookupEntity, String>, JpaSpecificationExecutor<LookupEntity> {

    List<LookupEntity> findByGroupCode(String groupCode);

    Page<LookupEntity> findAll(Pageable pageable);

    @Query("SELECT a FROM LookupEntity a WHERE a.groupCode = :groupCode AND a.isActive = 1")
    List<LookupEntity> getActiveLookupByGroupCode(@Param("groupCode") String groupCode);

    @Modifying
    @Transactional
    @Query("UPDATE LookupEntity a SET a.isActive=FALSE WHERE a.groupCode = :groupCode AND a.lookupKey IN (:listOfLookupKey)")
    Integer softDeleteLookupDetailByKey(@Param("listOfLookupKey") List<String> listOfLookupKey, @Param("groupCode") String groupCode);

    @Modifying
    @Transactional
    @Query("UPDATE LookupEntity a SET a.isActive=FALSE WHERE a.groupCode IN (:listOfGroupCode)")
    void softDeleteLookupDetailByGroupCode(@Param("listOfGroupCode") List<String> listOfGroupCode);

    long countByIsActive(Boolean b);

}