package com.bifrost.core.gui.dao.repository;

import com.bifrost.core.gui.dao.model.OauthClientDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OauthClientDetailsRepo extends JpaRepository<OauthClientDetailsEntity, String>, JpaSpecificationExecutor<OauthClientDetailsEntity> {

}