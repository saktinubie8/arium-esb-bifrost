package com.bifrost.core.gui.dao.repository;

import com.bifrost.core.gui.dao.model.MenuGroupEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface MenuGroupRepo extends JpaRepository<MenuGroupEntity, UUID> {

    MenuGroupEntity findByGroupId(UUID roleId);

    MenuGroupEntity findByGroupName(String groupName);

    MenuGroupEntity findByGroupCode(String groupCode);

    Page<MenuGroupEntity> findAll(Pageable pageable);

    Page<MenuGroupEntity> findAll(Specification<MenuGroupEntity> spec, Pageable pageable);

    int countByIsActive(boolean isActive);

    /**
     * findAllActive
     *
     * @return List<MenuGroupEntity>
     * @author alpinthor
     */
    @Query("SELECT MGE FROM MenuGroupEntity MGE WHERE MGE.isActive = true ORDER BY MGE.groupName")
    List<MenuGroupEntity> findAllActive();

    List<MenuGroupEntity> findMenuGroupByGroupId(UUID groupId);

    /**
     * findAllActive
     *
     * @return List<MenuGroupEntity>
     * @author alpinthor
     */
    @Query("SELECT MGE FROM MenuGroupEntity MGE WHERE MGE.groupId = :groupId")
    MenuGroupEntity findActiveByGroupId(@Param("groupId") UUID groupId);

}