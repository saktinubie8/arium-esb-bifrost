package com.bifrost.core.gui.dao.dto;

import java.util.HashSet;
import java.util.Set;

public class MenuGroupDto extends GenericDto {

    /**
     *
     */
    private static final long serialVersionUID = -4575011369962126275L;
    private String groupId;
    private String groupName;
    private String groupCode;
    private String description;
    private Integer version;
    private transient Set<MenuDto> menus = new HashSet<>();
    private transient Set<UserDto> user = new HashSet<>();
    private String key;
    private String value;
    private Boolean selected;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Set<MenuDto> getMenus() {
        return menus;
    }

    public void setMenus(Set<MenuDto> menus) {
        this.menus = menus;
    }

    public Set<UserDto> getUser() {
        return user;
    }

    public void setUser(Set<UserDto> user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

}