package com.bifrost.core.gui.dao.specification;

import com.bifrost.common.constant.GUIConstant;
import com.bifrost.core.gui.dao.dto.Select2DataDto;
import com.bifrost.core.gui.dao.model.MenuEntity;
import com.bifrost.core.gui.dao.util.QueryOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;
import java.util.UUID;

public class MenuSpecification {

    private static final Logger logger = LogManager.getLogger(MenuSpecification.class);

    private MenuSpecification() {
        throw new IllegalStateException("Utility class");
    }

    private static final String MENUID_NOT_IN = "menuIdNotIn";
    private static final String MENUID = "menuId";

    public static Specification<MenuEntity> getMenusSpec(Map<String, FilterSpec> filter) {
        return new Specification<MenuEntity>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<MenuEntity> root, CriteriaQuery<?> criteria, CriteriaBuilder builder) {
                Predicate predicate = builder.disjunction();
                if (filter == null || filter.isEmpty())
                    predicate = builder.conjunction();
                else {
                    for (Map.Entry<String, FilterSpec> field : filter.entrySet()) {
                        String key = field.getKey();
                        FilterSpec filterSpec = (field.getValue() == null) ? null : field.getValue();
                        if (filterSpec != null && filterSpec.getValues() != null) {
                            QueryOperator operator = (field.getValue().getOperator() == null) ? null
                                    : field.getValue().getOperator();
                            String value = (field.getValue().getValues() == null) ? null
                                    : field.getValue().getValues()[0].toString();
                            if (operator != null) {
                                switch (operator) {
                                    case EQUAL:
                                        if (value == null)
                                            predicate.getExpressions().add(builder.isNull(root.get(key)));
                                        else
                                            predicate.getExpressions().add(builder.equal(root.get(key), value));
                                        break;
                                    case LIKE_BOTH_SIDE:
                                        switch (key) {
                                            case MENUID:
                                            case "createdBy":
                                                predicate.getExpressions().add(builder.like(root.<String>get(key),
                                                        String.format(operator.getOperator(), value)));
                                                break;
                                            case "version":
                                                try {
                                                    predicate.getExpressions().add(builder.like(root.get(key).as(String.class),
                                                            String.format(operator.getOperator(), value)));
                                                } catch (Exception e) {
                                                    logger.error(e);
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                predicate = builder.and(predicate, builder.equal(root.get(GUIConstant.IS_ACTIVE), true));
                return predicate;
            }
        };
    }

    public static Specification<MenuEntity> getSelect2MenuSpec(Select2DataDto search) {
        return new Specification<MenuEntity>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<MenuEntity> root, CriteriaQuery<?> criteria, CriteriaBuilder builder) {
                Predicate predicate = builder.conjunction();
                if (search != null) {
                    predicate = builder.conjunction();
                    if (search.getId() != null && !search.getId().isEmpty()) {
                        predicate.getExpressions().add(builder.equal(root.get(MENUID), search.getId()));
                    }
                    if (search.getText() != null && !search.getText().isEmpty()) {
                        predicate.getExpressions().add(builder.like(root.<String>get("title"),
                                String.format(QueryOperator.LIKE_BOTH_SIDE.getOperator(), search.getText())));
                    }
                    if (!search.getQuery().isEmpty()) {
                        for (String key : search.getQuery().keySet()) {
                            if (key.equals(MENUID_NOT_IN)) {
                                // for callback parent shouldn't add self as parent
                                if (root.<String>get(MENUID) != null && search.getQuery().get(key) != null) {
                                    // validasi null
                                    predicate.getExpressions().add(
                                            builder.notEqual(root.get(MENUID), UUID.fromString(search.getQuery().get(key).toString())));
                                }
                            } else {
                                predicate.getExpressions().add(builder.like(root.<String>get(key), String.format(
                                        QueryOperator.LIKE_BOTH_SIDE.getOperator(), search.getQuery().get(key))));
                            }
                        }
                    }
                }
                predicate = builder.and(predicate, builder.equal(root.get(GUIConstant.IS_ACTIVE), true));
                return predicate;
            }
        };
    }

}