package com.bifrost.core.gui.dao.model;

import com.bifrost.core.gui.dao.dto.LookupGroupDto;
import com.bifrost.common.model.BaseAuditTrailEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "M_LOOKUP_GROUP")
public class LookupGroupEntity extends BaseAuditTrailEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /***************************** - Field - ****************************/

    @Id
    @Column(name = "GROUP_CODE", nullable = false, unique = true)
    private String groupCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lookupGroup", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<LookupEntity> lookupList;

    /***************************** - Constructor - ****************************/
    public LookupGroupEntity() {
        super();
    }

    public LookupGroupEntity(String groupCode) {
        this.groupCode = groupCode;
    }

    public LookupGroupEntity(LookupGroupEntity lookupGroupEntity) {
        this.groupCode = lookupGroupEntity.groupCode;
        this.description = lookupGroupEntity.description;
        this.isActive = lookupGroupEntity.isActive;
        this.createdBy = lookupGroupEntity.createdBy;
        this.createdDate = lookupGroupEntity.createdDate;
        this.modifiedBy = lookupGroupEntity.modifiedBy;
        this.modifiedDate = lookupGroupEntity.modifiedDate;
    }

    public LookupGroupEntity(LookupGroupDto lookupGroupDto) {
        this.groupCode = lookupGroupDto.getGroupCode();
        this.description = lookupGroupDto.getDescription();
        this.isActive = lookupGroupDto.getActive();
        this.createdBy = lookupGroupDto.getCreatedBy();
        this.createdDate = lookupGroupDto.getCreatedDateParse();
        this.modifiedBy = lookupGroupDto.getModifiedBy();
        this.modifiedDate = lookupGroupDto.getModifiedDateParse();
    }

    /***************************** - Other Method - ****************************/

    public LookupGroupDto parseDto() {
        LookupGroupDto lookupGroupDto = new LookupGroupDto();
        lookupGroupDto.setGroupCode(this.groupCode);
        lookupGroupDto.setDescription(this.description);
        lookupGroupDto.setIsActive(this.isActive);
        lookupGroupDto.setCreatedBy(this.createdBy);
        lookupGroupDto.setCreatedDate(this.createdDate);
        lookupGroupDto.setModifiedBy(this.modifiedBy);
        lookupGroupDto.setModifiedDate(this.modifiedDate);
        return lookupGroupDto;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LookupGroupEntity other = (LookupGroupEntity) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (groupCode == null) {
            if (other.groupCode != null)
                return false;
        } else if (!groupCode.equals(other.groupCode))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((groupCode == null) ? 0 : groupCode.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "LookupGroupEntity [groupCode=" + groupCode + ", description=" + description + "]";
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LookupEntity> getLookupList() {
        return lookupList;
    }

    public void setLookupList(List<LookupEntity> lookupList) {
        this.lookupList = lookupList;
    }

}