package com.bifrost.core.gui.dao.specification;

import com.bifrost.core.gui.dao.util.QueryOperator;
import com.bifrost.system.model.TokenEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;

public class TokenSpecification {

    private TokenSpecification(){throw new IllegalStateException("Token Class");}

    public static Specification<TokenEntity> getTokenSpec(Map<String, FilterSpec> filter) {
        return new Specification<TokenEntity>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<TokenEntity> root, CriteriaQuery<?> criteria,
                                         CriteriaBuilder builder) {
                Predicate predicate = builder.disjunction();
                if (filter == null || filter.isEmpty())
                    predicate = builder.conjunction();
                else {
                    for (Map.Entry<String, FilterSpec> field : filter.entrySet()) {
                        String key = field.getKey();
                        FilterSpec filterSpec = (field.getValue() == null) ? null : field.getValue();
                        if (filterSpec != null && filterSpec.getValues() != null) {
                            QueryOperator operator = (field.getValue().getOperator() == null) ? null
                                    : field.getValue().getOperator();
                            String value = (field.getValue().getValues() == null) ? null
                                    : field.getValue().getValues()[0].toString();
                            if (operator != null) {
                                switch (operator) {
                                    case EQUAL:
                                        if (value == null)
                                            predicate.getExpressions().add(builder.isNull(root.get(key)));
                                        else
                                            predicate.getExpressions().add(builder.equal(root.get(key), value));
                                        break;
                                    case LIKE_BOTH_SIDE:
                                        switch (key) {
                                            case "id":
                                            case "username":
                                            case "token":
                                            case "secretKey":
                                            case "ipPublic":
                                            case "merchantName":
                                            default:
                                                break;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                return predicate;
            }
        };
    }

}
