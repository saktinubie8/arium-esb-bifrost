package com.bifrost.core.gui.dao.model;

import com.bifrost.core.gui.dao.dto.LookupDto;
import com.bifrost.common.model.BaseAuditTrailEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "M_LOOKUP")
public class LookupEntity extends BaseAuditTrailEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /***************************** - Field - ****************************/

    @Column(name = "GROUP_CODE")
    private String groupCode;

    @Id
    @Column(name = "LOOKUP_KEY", nullable = false, unique = true)
    private String lookupKey;

    @Column(name = "KEY_ONLY")
    private String keyOnly;

    @Column(name = "LABEL")
    private String label;

    @JoinColumn(name = "GROUP_CODE", referencedColumnName = "GROUP_CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonBackReference
    private LookupGroupEntity lookupGroup;

    /***************************** - Transient Field - ****************************/

    /***************************** - Constructor - ****************************/
    public LookupEntity() {
        super();
    }

    public LookupEntity(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public LookupEntity(LookupEntity lookupEntity) {
        this.lookupKey = lookupEntity.lookupKey;
        this.groupCode = lookupEntity.groupCode;
        this.label = lookupEntity.label;
        this.isActive = lookupEntity.isActive;
        this.createdBy = lookupEntity.createdBy;
        this.createdDate = lookupEntity.createdDate;
        this.modifiedBy = lookupEntity.modifiedBy;
        this.modifiedDate = lookupEntity.modifiedDate;
    }

    /***************************** - Other Method - ****************************/

    public LookupDto parseDto() {
        LookupDto lookupDto = new LookupDto();
        lookupDto.setGroupCode(this.groupCode);
        lookupDto.setLookupKey(this.lookupKey);
        lookupDto.setKeyOnly(keyOnly);
        lookupDto.setLabel(this.label);
        lookupDto.setActive(this.isActive);
        lookupDto.setCreatedBy(this.createdBy);
        lookupDto.setCreatedDate(this.createdDate);
        lookupDto.setModifiedBy(this.modifiedBy);
        lookupDto.setModifiedDate(this.modifiedDate);
        return lookupDto;
    }

    @Override
    public String toString() {
        return "LookupEntity [groupCode=" + groupCode + ", lookupKey=" + lookupKey + ", keyOnly=" + keyOnly + ", label="
                + label + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((groupCode == null) ? 0 : groupCode.hashCode());
        result = prime * result + ((keyOnly == null) ? 0 : keyOnly.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((lookupKey == null) ? 0 : lookupKey.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LookupEntity other = (LookupEntity) obj;
        if (groupCode == null) {
            if (other.groupCode != null)
                return false;
        } else if (!groupCode.equals(other.groupCode))
            return false;
        if (keyOnly == null) {
            if (other.keyOnly != null)
                return false;
        } else if (!keyOnly.equals(other.keyOnly))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (lookupKey == null) {
            if (other.lookupKey != null)
                return false;
        } else if (!lookupKey.equals(other.lookupKey))
            return false;
        return true;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getKeyOnly() {
        return keyOnly;
    }

    public void setKeyOnly(String keyOnly) {
        this.keyOnly = keyOnly;
    }

}