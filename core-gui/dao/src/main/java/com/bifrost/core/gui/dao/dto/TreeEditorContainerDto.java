package com.bifrost.core.gui.dao.dto;

import java.util.List;

public class TreeEditorContainerDto {

    private String id;

    private String parentId;

    private String parentContent;

    private TreeEditorContainerDto parent;

    private String content;

    private Boolean expanded;

    private Integer level;

    private Integer order;

    private List<TreeEditorContainerDto> children;

    private Boolean deletable = true;

    private Boolean editable = true;

    private Boolean activeForSmartRender = false;

    /* for input model */
    private MenuDto dataMenuDto;

    private MenuGroupDto dataMenuGroupDto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentContent() {
        return parentContent;
    }

    public void setParentContent(String parentContent) {
        this.parentContent = parentContent;
    }

    public TreeEditorContainerDto getParent() {
        return parent;
    }

    public void setParent(TreeEditorContainerDto parent) {
        this.parent = parent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<TreeEditorContainerDto> getChildren() {
        return children;
    }

    public void setChildren(List<TreeEditorContainerDto> children) {
        this.children = children;
    }

    public Boolean getDeletable() {
        return deletable;
    }

    public void setDeletable(Boolean deletable) {
        this.deletable = deletable;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Boolean getActiveForSmartRender() {
        return activeForSmartRender;
    }

    public void setActiveForSmartRender(Boolean activeForSmartRender) {
        this.activeForSmartRender = activeForSmartRender;
    }

    public MenuDto getDataMenuDto() {
        return dataMenuDto;
    }

    public void setDataMenuDto(MenuDto dataMenuDto) {
        this.dataMenuDto = dataMenuDto;
    }

    public MenuGroupDto getDataMenuGroupDto() {
        return dataMenuGroupDto;
    }

    public void setDataMenuGroupDto(MenuGroupDto dataMenuGroupDto) {
        this.dataMenuGroupDto = dataMenuGroupDto;
    }

}