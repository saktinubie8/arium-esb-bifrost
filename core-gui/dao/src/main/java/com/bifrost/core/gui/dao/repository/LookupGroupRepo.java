package com.bifrost.core.gui.dao.repository;

import com.bifrost.core.gui.dao.model.LookupGroupEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import javax.transaction.Transactional;
import java.util.List;

public interface LookupGroupRepo extends JpaRepository<LookupGroupEntity, String>, JpaSpecificationExecutor<LookupGroupEntity> {

    Page<LookupGroupEntity> findAll(Pageable pageable);

    @Query("SELECT a FROM LookupGroupEntity a WHERE a.groupCode = :valueToValidate AND a.groupCode <> :currentUpdatedValue")
    List<LookupGroupEntity> findByGroupCodeExcludingCurrentGroupCode(@Param("valueToValidate") String valueToValidate, @Param("currentUpdatedValue") String currentUpdatedValue);

    @Modifying
    @Transactional
    @Query("UPDATE LookupGroupEntity a SET a.isActive=FALSE WHERE a.groupCode IN (:listOfGroupCode)")
    void softDeleteLookupGroupByGroupCode(@Param("listOfGroupCode") List<String> listOfGroupCode);

    @Query("SELECT a FROM LookupGroupEntity a WHERE a.groupCode = :valueToValidate AND a.isActive=1")
    LookupGroupEntity findByGroupCodeActiveOnly(@Param("valueToValidate") String valueToValidate);

    int countByIsActive(boolean isActive);

}