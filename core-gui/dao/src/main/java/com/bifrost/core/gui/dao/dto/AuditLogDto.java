package com.bifrost.core.gui.dao.dto;

import java.util.Date;

public class AuditLogDto {

    private static final long serialVersionUID = 1L;
    private String txAuditLogId;
    private String txTableName;
    private String txDataId;
    private String txColumnName;
    private String beforeData;
    private String afterData;
    private Date txDate;
    private String userId;
    private Integer version;

    public String getTxAuditLogId() {
        return txAuditLogId;
    }

    public void setTxAuditLogId(String txAuditLogId) {
        this.txAuditLogId = txAuditLogId;
    }

    public String getTxTableName() {
        return txTableName;
    }

    public void setTxTableName(String txTableName) {
        this.txTableName = txTableName;
    }

    public String getTxDataId() {
        return txDataId;
    }

    public void setTxDataId(String txDataId) {
        this.txDataId = txDataId;
    }

    public String getTxColumnName() {
        return txColumnName;
    }

    public void setTxColumnName(String txColumnName) {
        this.txColumnName = txColumnName;
    }

    public String getBeforeData() {
        return beforeData;
    }

    public void setBeforeData(String beforeData) {
        this.beforeData = beforeData;
    }

    public String getAfterData() {
        return afterData;
    }

    public void setAfterData(String afterData) {
        this.afterData = afterData;
    }

    public Date getTxDate() {
        return txDate;
    }

    public void setTxDate(Date txDate) {
        this.txDate = txDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}