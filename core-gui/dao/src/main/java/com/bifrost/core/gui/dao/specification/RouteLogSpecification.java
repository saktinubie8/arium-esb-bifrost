package com.bifrost.core.gui.dao.specification;

import com.bifrost.core.gui.dao.util.QueryOperator;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.logging.model.RouteLogId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;

public class RouteLogSpecification {

    private static final Logger logger = LogManager.getLogger(RouteLogSpecification.class);

    private RouteLogSpecification() {
        throw new IllegalStateException("Utility class");
    }

    public static Specification<RouteLog> getRouteLogsSpec(Map<String, FilterSpec> filter) {
        return new Specification<RouteLog>() {

            private static final long serialVersionUID = 1L;

            RouteLogId routeLogId;

            @Override
            public Predicate toPredicate(Root<RouteLog> root, CriteriaQuery<?> criteria,
                                         CriteriaBuilder builder) {
                Predicate predicate = builder.and();
                if (filter == null || filter.isEmpty())
                    predicate = builder.conjunction();
                else {
                    for (Map.Entry<String, FilterSpec> field : filter.entrySet()) {
                        String key = field.getKey();
                        logger.debug("Key : "+key);
                        FilterSpec filterSpec = (field.getValue() == null) ? null : field.getValue();
                        if (filterSpec != null && filterSpec.getValues() != null) {
                            QueryOperator operator = (field.getValue().getOperator() == null) ? null
                                    : field.getValue().getOperator();
                            String value = (field.getValue().getValues() == null) ? null
                                    : field.getValue().getValues()[0].toString();
                            logger.debug("Value : "+value);
                            logger.debug("Operator : "+operator.getOperator());
                            if (operator != null) {
                                switch (operator) {
                                    case EQUAL:
                                        if (value == null) {
                                            predicate.getExpressions().add(builder.isNull(root.get(key)));
                                        }
                                        else {
                                            predicate.getExpressions().add(builder.equal(root.get(key), value));
                                        }
                                        break;
                                    case AND:
                                        switch (key) {
                                            case "transactionId":
                                                key = "routeLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("transactionId"),value));
                                                break;
                                            case "dateTime":
                                                key = "routeLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("dateTime"),value));
                                                break;
                                            case "endpointCode":
                                                key = "routeLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("endpointCode"),value));
                                                break;
                                            case "invocationCode":
                                                key = "routeLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("invocationCode"),value));
                                                break;
                                            case "request":
                                                key = "routeLogId";
                                                if(value.equals("true")){
                                                    predicate.getExpressions().add(builder.isTrue(root.get(key).get("isRequest")));
                                                }else{
                                                    predicate.getExpressions().add(builder.isFalse(root.get(key).get("isRequest")));
                                                }
                                                break;
                                            case "raw":
                                            case "formattedRaw":
                                            default:
                                                break;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                return predicate;
            }
        };
    }

}