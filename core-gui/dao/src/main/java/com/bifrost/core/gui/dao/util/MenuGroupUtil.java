package com.bifrost.core.gui.dao.util;

import com.bifrost.core.gui.dao.dto.MenuDto;
import com.bifrost.core.gui.dao.dto.MenuGroupDto;
import com.bifrost.core.gui.dao.dto.TreeEditorContainerDto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MenuGroupUtil {

    private MenuGroupUtil() {
    }

    /**
     * @return List<TreeEditorContainerDto>
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     */
    public static List<TreeEditorContainerDto> buildCustomMenuGroupTreeEditorContainerForJson(
            List<TreeEditorContainerDto> listTreeEditorContainer, MenuGroupDto menuGroupDto) {
        List<TreeEditorContainerDto> rootNodes = new ArrayList<>();
        for (TreeEditorContainerDto item : listTreeEditorContainer) {
            item.setEditable(true);
            item.setActiveForSmartRender(true);
            item.setDeletable(false);
            if (menuGroupDto.getGroupId() != null) {
                item.setExpanded(true);
                Set<MenuDto> menus = item.getDataMenuGroupDto().getMenus();
                if (menus != null) {
                    List<TreeEditorContainerDto> newChildList = new ArrayList<>();
                    for (Iterator iterator = menus.iterator(); iterator.hasNext(); ) {
                        MenuDto menuDto = (MenuDto) iterator.next();
                        newChildList.add(menuDto.parseTreeEditorContainer());
                    }
                    item.setChildren(TreeEditorUtil.buildTreeEditorContainerForJson(newChildList));
                }
            } else {
                item.setExpanded(false);
            }
            rootNodes.add(item);
        }
        return rootNodes;
    }

    /**
     * @return List<TreeEditorContainerDto>
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     */
    public static List<TreeEditorContainerDto> backupBuildCustomMenuGroupTreeViewerContainerForJson(
            TreeEditorContainerDto item) {
        List<TreeEditorContainerDto> rootNodes = new ArrayList<>();
        item.setEditable(true);
        item.setActiveForSmartRender(true);
        item.setDeletable(false);
        Set<MenuDto> menus = item.getDataMenuGroupDto().getMenus();
        if (menus != null) {
            item.setExpanded(true);
            List<TreeEditorContainerDto> newChildList = new ArrayList<>();
            for (Iterator iterator = menus.iterator(); iterator.hasNext(); ) {
                MenuDto menuDto = (MenuDto) iterator.next();
                newChildList.add(menuDto.parseTreeEditorContainer());
            }
            item.setChildren(TreeEditorUtil.buildTreeEditorContainerForJson(newChildList));
        } else {
            item.setExpanded(false);
        }
        rootNodes.add(item);
        return rootNodes;
    }

    /**
     * @return List<TreeEditorContainerDto>
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     */
    public static List<TreeEditorContainerDto> buildCustomMenuGroupTreeViewerContainerForJson(
            TreeEditorContainerDto item) {
        List<TreeEditorContainerDto> rootNodes = new ArrayList<>();
        item.setEditable(true);
        item.setActiveForSmartRender(true);
        item.setDeletable(false);
        List<TreeEditorContainerDto> menus = item.getChildren();
        if (menus != null) {
            item.setExpanded(true);
            item.setChildren(TreeEditorUtil.buildTreeEditorContainerForJson(menus));
        } else {
            item.setExpanded(false);
        }
        rootNodes.add(item);
        return rootNodes;
    }

    /**
     * @return List<TreeEditorContainer>
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     */
    public static List<TreeEditorContainerDto> backupBuildCustomMenuGroupTreeEditorContainerForJson(
            List<TreeEditorContainerDto> listTreeEditorContainer) {
        List<TreeEditorContainerDto> rootNodes = new ArrayList<>();
        for (TreeEditorContainerDto item : listTreeEditorContainer) {
            item.setDeletable(false);
            item.setEditable(false);
            item.setExpanded(false);
            Set<MenuDto> menus = item.getDataMenuGroupDto().getMenus();
            List<TreeEditorContainerDto> newChildList = new ArrayList<>();
            for (Iterator iterator = menus.iterator(); iterator.hasNext(); ) {
                MenuDto menuDto = (MenuDto) iterator.next();
                newChildList.add(menuDto.parseTreeEditorContainer());
            }
            item.setChildren(newChildList);
            rootNodes.add(item);
        }
        return rootNodes;
    }

}