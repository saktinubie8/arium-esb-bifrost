package com.bifrost.core.gui.dao.specification;

import com.bifrost.logging.model.EndpointLog;
import org.springframework.data.jpa.domain.Specification;
import com.bifrost.core.gui.dao.util.QueryOperator;

import java.util.Map;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EndpointLogSpecification {

    private static final Logger logger = LogManager.getLogger(EndpointLogSpecification.class);

    private EndpointLogSpecification() {
        throw new IllegalStateException("Utility class");
    }

    public static Specification<EndpointLog> getEndpointLogsSpec(Map<String, FilterSpec> filter) {
        return new Specification<EndpointLog>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<EndpointLog> root, CriteriaQuery<?> criteria,
                                         CriteriaBuilder builder) {
                Predicate predicate = builder.and();
                if (filter == null || filter.isEmpty())
                    predicate = builder.conjunction();
                else {
                    for (Map.Entry<String, FilterSpec> field : filter.entrySet()) {
                        String key = field.getKey();
                        FilterSpec filterSpec = (field.getValue() == null) ? null : field.getValue();
                        if (filterSpec != null && filterSpec.getValues() != null) {
                            QueryOperator operator = (field.getValue().getOperator() == null) ? null
                                    : field.getValue().getOperator();
                            String value = (field.getValue().getValues() == null) ? null
                                    : field.getValue().getValues()[0].toString();
                            if (operator != null) {
                                switch (operator) {
                                    case EQUAL:
                                        if (value == null)
                                            predicate.getExpressions().add(builder.isNull(root.get(key)));
                                        else
                                            predicate.getExpressions().add(builder.equal(root.get(key), value));
                                        break;
                                    case LIKE_BOTH_SIDE:
                                        switch (key) {
                                            case "dateTime":
                                                key = "endpointLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("dateTime"),value));
                                                break;
                                            case "endpointCode":
                                                key = "endpointLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("endpointCode"),value));
                                                break;
                                            case "node":
                                                key = "endpointLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("node"),value));
                                                break;
                                            case "status":
                                                key = "endpointLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("status"),value));
                                                break;
                                            case "description":
                                                key = "endpointLogId";
                                                predicate.getExpressions().add(builder.like(root.get(key).get("description"),value));
                                                break;
                                            default:
                                                break;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                return predicate;
            }
        };
    }

}