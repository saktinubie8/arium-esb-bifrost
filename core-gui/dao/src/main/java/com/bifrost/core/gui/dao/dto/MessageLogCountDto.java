package com.bifrost.core.gui.dao.dto;

public class MessageLogCountDto {

    private String countSuccess;
    private String countFail;
    private String countDeviation;
    private String totalTransaction;

    public String getCountSuccess() {
        return countSuccess;
    }

    public void setCountSuccess(String countSuccess) {
        this.countSuccess = countSuccess;
    }

    public String getCountFail() {
        return countFail;
    }

    public void setCountFail(String countFail) {
        this.countFail = countFail;
    }

    public String getCountDeviation() {
        return countDeviation;
    }

    public void setCountDeviation(String countDeviation) {
        this.countDeviation = countDeviation;
    }

    public String getTotalTransaction() {
        return totalTransaction;
    }

    public void setTotalTransaction(String totalTransaction) {
        this.totalTransaction = totalTransaction;
    }
}
