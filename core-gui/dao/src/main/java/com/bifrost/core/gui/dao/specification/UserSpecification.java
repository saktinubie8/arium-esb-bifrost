package com.bifrost.core.gui.dao.specification;

import com.bifrost.common.constant.GUIConstant;
import com.bifrost.core.gui.dao.dto.Select2DataDto;
import com.bifrost.core.gui.dao.model.UserEntity;
import com.bifrost.core.gui.dao.util.QueryOperator;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;

public class UserSpecification {

    private UserSpecification() {
        throw new IllegalStateException("Utility class");
    }

    public static Specification<UserEntity> getUsersSpec(Map<String, FilterSpec> filter) {
        return new Specification<UserEntity>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> criteria, CriteriaBuilder builder) {
                Predicate predicate = builder.disjunction();
                if (filter == null || filter.isEmpty())
                    predicate = builder.conjunction();
                else {
                    for (Map.Entry<String, FilterSpec> field : filter.entrySet()) {
                        String key = field.getKey();
                        FilterSpec filterSpec = (field.getValue() == null) ? null : field.getValue();
                        if (filterSpec != null && filterSpec.getValues() != null) {
                            QueryOperator operator = (field.getValue().getOperator() == null) ? null
                                    : field.getValue().getOperator();
                            String value = (field.getValue().getValues() == null) ? null
                                    : field.getValue().getValues()[0].toString();
                            if (operator != null) {
                                switch (operator) {
                                    case EQUAL:
                                        if (value == null)
                                            predicate.getExpressions().add(builder.isNull(root.get(key)));
                                        else
                                            predicate.getExpressions().add(builder.equal(root.get(key), value));
                                        break;
                                    case LIKE_BOTH_SIDE:
                                        switch (key) {
                                            case "name":
                                            case "username":
                                            case "email":
                                            case "address":
                                                predicate.getExpressions().add(builder.like(root.<String>get(key),
                                                        String.format(operator.getOperator(), value)));
                                                break;
                                            case "branch.branchName":
                                                predicate.getExpressions()
                                                        .add(builder.like(root.get("branch").get("branchName"),
                                                                String.format(operator.getOperator(), value)));
                                                break;
                                            default:
                                                break;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                predicate = builder.and(predicate, builder.equal(root.get(GUIConstant.IS_ACTIVE), true));
                return predicate;
            }
        };
    }

    /**
     * @param search
     * @return
     */
    public static Specification<UserEntity> getSelect2UserSpec(Select2DataDto search) {
        return new Specification<UserEntity>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> criteria, CriteriaBuilder builder) {
                Predicate predicate = builder.disjunction();
                if (search == null) {
                    predicate = builder.conjunction();
                } else {
                    if (search.getId() != null && !search.getId().isEmpty())
                        predicate.getExpressions().add(builder.equal(root.get("userId"), search.getId()));
                    if (search.getText() != null)
                        if (!search.getText().isEmpty())
                            predicate.getExpressions().add(builder.like(root.<String>get("name"),
                                    String.format(QueryOperator.LIKE_BOTH_SIDE.getOperator(), search.getText())));
                        else
                            predicate = builder.conjunction();
                    else
                        predicate = builder.conjunction();
                }
                predicate = builder.and(predicate, builder.equal(root.get(GUIConstant.IS_ACTIVE), true));
                return predicate;
            }
        };
    }

}