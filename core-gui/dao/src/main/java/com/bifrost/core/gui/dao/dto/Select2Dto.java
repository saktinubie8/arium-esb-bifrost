package com.bifrost.core.gui.dao.dto;

import java.util.List;

public class Select2Dto {

    private String id;
    private String text;
    private boolean selected;
    private boolean disabled;
    private List<Select2Dto> children;

    public Select2Dto(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public List<Select2Dto> getChildren() {
        return children;
    }

    public void setChildren(List<Select2Dto> children) {
        this.children = children;
    }

}