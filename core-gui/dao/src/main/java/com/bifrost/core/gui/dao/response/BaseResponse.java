package com.bifrost.core.gui.dao.response;

public class BaseResponse {

    private String responseCode = "00";//default 00
    private String responseDesc = "SUCCESS";//default SUCCESS

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

}