package com.bifrost.core.gui.dao.dto;

public class TreeUtilContainerDto implements Comparable<TreeUtilContainerDto> {

    private String id;

    private String parentId;

    private TreeUtilContainerDto parent;

    private String label;

    private String parentLabel;

    /*
     * level 1 for top level, > 1 is leaf
     */
    private Integer level;

    private Integer order;

    private String key;

    public TreeUtilContainerDto() {

    }

    public TreeUtilContainerDto(String id, String parentId, TreeUtilContainerDto parent, String label, String parentLabel,
                                Integer level, Integer order) {
        this.id = id;

        this.parentId = parentId;

        this.parent = parent;

        this.label = label;

        this.parentLabel = parentLabel;

        this.level = level;

        this.order = order;

        this.key = parent.key + order;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getParentLabel() {
        return parentLabel;
    }

    public void setParentLabel(String parentLabel) {
        this.parentLabel = parentLabel;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public TreeUtilContainerDto getParent() {
        return parent;
    }

    public void setParent(TreeUtilContainerDto parent) {
        this.parent = parent;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + level;
        result = prime * result + order;
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((parentLabel == null) ? 0 : parentLabel.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TreeUtilContainerDto other = (TreeUtilContainerDto) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (level != other.level)
            return false;
        if (order != other.order)
            return false;
        if (parentId == null) {
            if (other.parentId != null)
                return false;
        } else if (!parentId.equals(other.parentId))
            return false;
        if (parentLabel == null) {
            if (other.parentLabel != null)
                return false;
        } else if (!parentLabel.equals(other.parentLabel))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TreeUtilContainer [label=" + label + "]";
    }

    @Override
    public int compareTo(TreeUtilContainerDto o) {
        return this.key.compareTo(o.key);
    }

    public TreeEditorContainerDto parseTreeEditorContainer() {
        TreeEditorContainerDto treeEditorContainer = new TreeEditorContainerDto();
        treeEditorContainer.setId(this.id);
        if (this.parent != null) {
            treeEditorContainer.setParentId(this.parentId);
        }
        treeEditorContainer.setContent(this.label);
        treeEditorContainer.setLevel(this.level);
        treeEditorContainer.setOrder(this.order);
        return treeEditorContainer;
    }

    public MainMenuTreeContainerDto parseMainMenuTreeContainer() {
        MainMenuTreeContainerDto mainMenuTreeContainer = new MainMenuTreeContainerDto();
        mainMenuTreeContainer.setId(this.id);
        if (this.parent != null) {
            mainMenuTreeContainer.setParentId(this.parentId);
        }
        mainMenuTreeContainer.setLabel(this.label);
        mainMenuTreeContainer.setIcon(this.label);
        mainMenuTreeContainer.setUrl(this.label);
        mainMenuTreeContainer.setLevel(this.level);
        mainMenuTreeContainer.setOrder(this.order);
        return mainMenuTreeContainer;
    }

}