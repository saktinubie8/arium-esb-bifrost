package com.bifrost.core.gui.dao.response;

import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.configuration.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

@Component
public class ApiErrorResponse {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private JsonUtils jsonUtils;

    public String errorResponse(ErrorCode errorCode, Locale locale) {
        if (locale == null)
            locale = Locale.forLanguageTag("id-ID");
        Map<String, Object> response = new TreeMap<>();
        response.put("errorCode", errorCode);
        response.put("errorMessage", messageSource.getMessage(errorCode.name(), null, locale));
        return jsonUtils.objToJson(response);
    }

    public String getErrorMessage(ErrorCode errorCode, Locale locale) {
        return messageSource.getMessage(errorCode.name(), null, locale);
    }

}