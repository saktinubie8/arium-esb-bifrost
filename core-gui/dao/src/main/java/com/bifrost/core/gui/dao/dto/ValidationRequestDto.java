package com.bifrost.core.gui.dao.dto;

public class ValidationRequestDto {

    private String valueToValidate;

    public ValidationRequestDto(String valueToValidate) {
        this.valueToValidate = valueToValidate;
    }

    public ValidationRequestDto() {
    }

    public String getValueToValidate() {
        return valueToValidate;
    }

    public void setValueToValidate(String valueToValidate) {
        this.valueToValidate = valueToValidate;
    }

    public ValidationRequestDto parseDto() {
        ValidationRequestDto validationRequestDto = new ValidationRequestDto();
        validationRequestDto.setValueToValidate(this.valueToValidate);
        return validationRequestDto;
    }

}