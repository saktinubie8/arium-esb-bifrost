package com.bifrost.core.gui.dao.dto;

import java.util.List;
import java.util.Set;

public class MenuDto extends GenericDto {

    private static final long serialVersionUID = -957155994282275658L;
    private String menuId;
    private String parentId;
    private String parentContent;
    private Short isLeaf;
    private String title;
    private String pageUrl;
    private int level;
    private int order;
    private String module;
    private Integer version;
    private String icon;
    private String description;
    private Set<MenuGroupDto> menuGroupDto;
    private MenuDto menuParent;
    private List<MenuDto> childMenu;

    public TreeEditorContainerDto parseTreeEditorContainer() {
        TreeEditorContainerDto treeEditorContainer = new TreeEditorContainerDto();
        treeEditorContainer.setId(this.menuId);
        treeEditorContainer.setParentId(this.parentId);
        treeEditorContainer.setContent(this.title);
        treeEditorContainer.setLevel(this.level);
        treeEditorContainer.setOrder(this.order);
        treeEditorContainer.setDataMenuDto(null);
        return treeEditorContainer;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentContent() {
        return parentContent;
    }

    public void setParentContent(String parentContent) {
        this.parentContent = parentContent;
    }

    public Short getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Short isLeaf) {
        this.isLeaf = isLeaf;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MenuGroupDto> getMenuGroupDto() {
        return menuGroupDto;
    }

    public void setMenuGroupDto(Set<MenuGroupDto> menuGroupDto) {
        this.menuGroupDto = menuGroupDto;
    }

    public MenuDto getMenuParent() {
        return menuParent;
    }

    public void setMenuParent(MenuDto menuParent) {
        this.menuParent = menuParent;
    }

    public List<MenuDto> getChildMenu() {
        return childMenu;
    }

    public void setChildMenu(List<MenuDto> childMenu) {
        this.childMenu = childMenu;
    }

}