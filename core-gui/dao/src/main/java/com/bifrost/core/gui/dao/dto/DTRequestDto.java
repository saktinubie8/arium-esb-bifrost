package com.bifrost.core.gui.dao.dto;

import java.util.List;

public class DTRequestDto {

    private long draw;
    private List<DTColumn> columns;
    private List<DTOrder> order;
    private long start;
    private short length;
    private DTSearch search;
    private String fieldId;

    public long getDraw() {
        return draw;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public List<DTColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<DTColumn> columns) {
        this.columns = columns;
    }

    public List<DTOrder> getOrder() {
        return order;
    }

    public void setOrder(List<DTOrder> order) {
        this.order = order;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public short getLength() {
        return length;
    }

    public void setLength(short length) {
        this.length = length;
    }

    public DTSearch getSearch() {
        return search;
    }

    public void setSearch(DTSearch search) {
        this.search = search;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

}