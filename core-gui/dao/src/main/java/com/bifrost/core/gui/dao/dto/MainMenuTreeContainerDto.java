package com.bifrost.core.gui.dao.dto;

import java.util.List;

public class MainMenuTreeContainerDto {

    private String id;

    private String parentId;

    private MainMenuTreeContainerDto parent;

    private String label;

    private String icon;

    private String description;

    private String url;

    private Integer level;

    private Integer order;

    private List<MainMenuTreeContainerDto> subMenu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public MainMenuTreeContainerDto getParent() {
        return parent;
    }

    public void setParent(MainMenuTreeContainerDto parent) {
        this.parent = parent;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<MainMenuTreeContainerDto> getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(List<MainMenuTreeContainerDto> subMenu) {
        this.subMenu = subMenu;
    }

}