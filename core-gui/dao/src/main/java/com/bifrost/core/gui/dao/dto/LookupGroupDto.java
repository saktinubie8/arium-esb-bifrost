package com.bifrost.core.gui.dao.dto;

import java.util.List;

public class LookupGroupDto extends GenericDto {

    private String groupCode;
    private String description;
    private int version;
    private transient List<LookupDto> lookupList;
    private transient List<String> deletedLookupKeyList;

    public LookupGroupDto() {
        super();
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<LookupDto> getLookupList() {
        return lookupList;
    }

    public void setLookupList(List<LookupDto> lookupList) {
        this.lookupList = lookupList;
    }

    public List<String> getDeletedLookupKeyList() {
        return deletedLookupKeyList;
    }

    public void setDeletedLookupKeyList(List<String> deletedLookupKeyList) {
        this.deletedLookupKeyList = deletedLookupKeyList;
    }

}