package com.bifrost.core.gui.dao.specification;

import com.bifrost.common.constant.GUIConstant;
import com.bifrost.core.gui.dao.util.QueryOperator;
import com.bifrost.system.model.SystemParam;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;

public class SystemParamSpecification {

    private SystemParamSpecification() {
        throw new IllegalStateException("Utility class");
    }

    public static Specification<SystemParam> getSysParamSpec(Map<String, FilterSpec> filter) {
        return new Specification<SystemParam>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<SystemParam> root, CriteriaQuery<?> criteria,
                                         CriteriaBuilder builder) {
                Predicate predicate = builder.disjunction();
                if (filter == null || filter.isEmpty())
                    predicate = builder.conjunction();
                else {
                    for (Map.Entry<String, FilterSpec> field : filter.entrySet()) {
                        String key = field.getKey();
                        FilterSpec filterSpec = (field.getValue() == null) ? null : field.getValue();
                        if (filterSpec != null && filterSpec.getValues() != null) {
                            QueryOperator operator = (field.getValue().getOperator() == null) ? null
                                    : field.getValue().getOperator();
                            String value = (field.getValue().getValues() == null) ? null
                                    : field.getValue().getValues()[0].toString();
                            if (operator != null) {
                                switch (operator) {
                                    case EQUAL:
                                        if (value == null)
                                            predicate.getExpressions().add(builder.isNull(root.get(key)));
                                        else
                                            predicate.getExpressions().add(builder.equal(root.get(key), value));
                                        break;
                                    case LIKE_BOTH_SIDE:
                                        switch (key) {
                                            case "category":
                                            case "name":
                                            case "value":
                                            case "application":
                                            case "profile":
                                            case "label":
                                                predicate.getExpressions().add(builder.like(root.<String>get(key),
                                                        String.format(operator.getOperator(), value)));
                                                break;
                                            default:
                                                break;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                return predicate;
            }
        };
    }

}