package com.bifrost.core.gui.dao.dto;

import java.util.HashMap;
import java.util.Map;

public class Select2DataDto {

    private String id = "";
    private String text = "";
    private Map<String, Object> query = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<String, Object> getQuery() {
        return query;
    }

    public void setQuery(Map<String, Object> query) {
        this.query = query;
    }

}