package com.bifrost.core.gui.dao.model;

import com.bifrost.core.gui.dao.dto.MenuGroupDto;
import com.bifrost.core.gui.dao.dto.RoleDto;
import com.bifrost.core.gui.dao.dto.UserDto;
import com.bifrost.common.model.ApprovalAuditTrailEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

/**
 * Created by daniel on 3/31/15.
 */
@Entity
@Table(name = "M_USERS")
public class UserEntity extends ApprovalAuditTrailEntity implements UserDetails {
    /**
     *
     */
    private static final long serialVersionUID = -2442773369159964802L;
    /***************************** - Field - ****************************/
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "USER_ID", nullable = false, unique = true)
    @Type(type = "pg-uuid")
    private UUID userId;

    @Column(name = "USER_NAME", nullable = false, unique = true)
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "ACCOUNT_ENABLED", nullable = false)
    private boolean enabled = false;

    @Column(name = "ACCOUNT_NON_EXPIRED", nullable = false)
    private boolean accountNonExpired = true;

    @Column(name = "ACCOUNT_NON_LOCKED", nullable = false)
    private boolean accountNonLocked = true;

    @Column(name = "CREDENTIAL_NON_EXPIRED", nullable = false)
    private boolean credentialsNonExpired = true;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "R_USER_ROLE", joinColumns = {
            @JoinColumn(name = "userId")}, inverseJoinColumns = @JoinColumn(name = "roleId"))
    private Set<RoleEntity> roles = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "R_USER_MENU_GROUP", joinColumns = {
            @JoinColumn(name = "userId")}, inverseJoinColumns = @JoinColumn(name = "groupId"))
    private Set<MenuGroupEntity> menuGroups = new HashSet<>();

    @Column(name = "FULL_NAME", nullable = false)
    private String name;

    @Column(name = "EMAIL", unique = true)
    private String email;

    @Column(name = "ADDRESS", nullable = false)
    private String address;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "MOBILE_NUMBER")
    private String mobileNumber;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "RAW", columnDefinition = "jsonb")
    @Type(type = "JsonUserType")
    private transient Map<String, Object> raw;

    @Column(name = "IMAGE_URL")
    private String imageUrl;

    @Column(name = "EXPIRED_DATE")
    private Date expiredDate;

    @Column(name = "PASSWORD_DATE")
    private Date passwordDate;

    /***************************** - Getter Setter - ****************************/

    public String getName() {
        if (name == null)
            return "";
        return name;
    }

    public String getEmail() {
        if (email == null)
            return "";
        return email;
    }

    public String getPhoneNumber() {
        if (phoneNumber == null)
            return "";
        return phoneNumber;
    }

    public String getMobileNumber() {
        if (mobileNumber == null)
            return "";
        return mobileNumber;
    }

    /*****************************
     * - Transient Field -
     ****************************/
    @Transient
    private String passwordConfirm;
    @Transient
    private String captcha;
    @Transient
    private String enabledStr;
    @Transient
    private String accountNonLockedStr;
    @Transient
    private Properties prop;

    /***************************** - Constructor - ****************************/
    public UserEntity() {
    }

    public UserEntity(String username) {
        this.username = username;
    }

    public UserEntity(UUID userId, String username) {
        this.username = username;
        this.userId = userId;
    }

    public UserEntity(UserEntity user) {
        setUsername(user.getUsername());
        setPassword(user.getPassword());
        setEnabled(user.isEnabled());
        setAccountNonExpired(user.isAccountNonExpired());
        setAccountNonLocked(user.isAccountNonLocked());
        setCredentialsNonExpired(user.isCredentialsNonExpired());
        for (RoleEntity role : user.getRoles()) {
            this.roles.add(role);
        }
        for(MenuGroupEntity menuGroup : user.getMenuGroups()){
            this.menuGroups.add(menuGroup);
        }
        setName(user.getName());
        setEmail(user.getEmail());
        setAddress(user.getAddress());
        setPhoneNumber(user.getPhoneNumber());
        setMobileNumber(user.getMobileNumber());
        setDescription(user.getDescription());
        setCreatedBy(user.getCreatedBy());
        setCreatedDate(user.getCreatedDate());
        setModifiedBy(user.getModifiedBy());
        setModifiedDate(user.getModifiedDate());
        setActive(user.getActive());
        setImageUrl(user.getImageUrl());
        setExpiredDate(user.getExpiredDate());
        setPasswordDate(user.getPasswordDate());
    }

    public UserEntity(UserDto user) {
        setUsername(user.getUsername());
        setPassword(user.getPassword());
        setEnabled(user.isEnabled());
        setAccountNonExpired(user.isAccountNonExpired());
        setAccountNonLocked(user.isAccountNonLocked());
        setCredentialsNonExpired(user.isCredentialsNonExpired());
        for (RoleDto role : user.getRoles()) {
            this.roles.add(new RoleEntity(role));
        }
        for (MenuGroupDto menuGroupDto : user.getMenuGroups()){
            this.menuGroups.add(new MenuGroupEntity(menuGroupDto));
        }
        setName(user.getName());
        setEmail(user.getEmail());
        setAddress(user.getAddress());
        setPhoneNumber(user.getPhoneNumber());
        setMobileNumber(user.getMobileNumber());
        setDescription(user.getDescription());
        setCreatedBy(user.getCreatedBy());
        setCreatedDate(user.getCreatedDateParse());
        setModifiedBy(user.getModifiedBy());
        setModifiedDate(user.getModifiedDateParse());
        setActive(user.getActive());
        setImageUrl(user.getImageUrl());
        setExpiredDate(user.getExpiredDate());
        setPasswordDate(user.getPasswordDate());
    }

    public UserDto parseDto() {
        UserDto userDto = new UserDto();
        userDto.setUserId(this.userId.toString());
        userDto.setUsername(this.username);
        userDto.setEnabled(this.enabled);
        userDto.setAccountNonExpired(this.accountNonExpired);
        userDto.setAccountNonLocked(this.accountNonLocked);
        userDto.setCredentialsNonExpired(this.credentialsNonExpired);
        List<RoleDto> roleDtos = new ArrayList<>();
        for (RoleEntity role : this.roles) {
            userDto.addRole(role.parseDto());
            roleDtos.add(role.parseDto());
        }
        for (MenuGroupEntity menuGroup : this.menuGroups) {
            userDto.addMenuGroup(menuGroup.parseDto());
        }
        userDto.setName(this.name);
        userDto.setEmail(this.email);
        userDto.setAddress(this.address);
        userDto.setPhoneNumber(this.phoneNumber);
        userDto.setMobileNumber(this.mobileNumber);
        userDto.setDescription(this.description);
        userDto.setCreatedBy(this.createdBy);
        userDto.setCreatedDate(this.createdDate);
        userDto.setModifiedBy(this.modifiedBy);
        userDto.setModifiedDate(this.modifiedDate);
        userDto.setRaw(this.raw);
        userDto.setIsActive(this.isActive);
        userDto.setStrIsActive();
        userDto.setImageUrl(this.imageUrl);
        userDto.setExpiredDate(this.expiredDate);
        userDto.setPasswordDate(this.passwordDate);
        return userDto;
    }

    /*****************************
     * - Other Method -
     ****************************/
    @Override
    public Set<GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new LinkedHashSet<>();
        authorities.addAll(roles);
        return authorities;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public void addRole(RoleEntity role) {
        if (roles == null) {
            roles = new HashSet<>();
        }
        roles.add(role);
    }

    public void setMenuGroups(Set<MenuGroupEntity> menuGroups) {
        this.menuGroups = menuGroups;
    }

    public void addMenuGroup(MenuGroupEntity menuGroup) {
        if (this.menuGroups == null) {
            this.menuGroups = new HashSet<>();
        }
        this.menuGroups.add(menuGroup);
    }

    public void setEnabled() {
        this.enabled = enabledStr.equalsIgnoreCase("1");
    }

    public String getEnabledStr() {
        return enabledStr;
    }

    public void setEnabledStr(String enabledStr) {
        this.enabledStr = enabledStr;
    }

    public void setEnabledStr() {
        if (enabled) {
            enabledStr = "1";
        } else {
            enabledStr = "0";
        }
    }

    public void setAccountNonLocked() {
        this.accountNonLocked = accountNonLockedStr.equalsIgnoreCase("1");
    }

    public String getAccountNonLockedStr() {
        return accountNonLockedStr;
    }

    public void setAccountNonLockedStr(String accountNonLockedStr) {
        this.accountNonLockedStr = accountNonLockedStr;
    }

    public void setAccountNonLockedStr() {
        if (accountNonLocked) {
            accountNonLockedStr = "1";
        } else {
            accountNonLockedStr = "0";
        }
    }

    public Properties getProp() {
        return prop;
    }

    public void setProp(Properties prop) {
        this.prop = prop;
    }

    public String getCustomProp(String key) {
        if (prop == null)
            prop = new Properties();
        return (String) prop.get(key);
    }

    public void setCustomProp(String key, String value) {
        if (prop == null)
            prop = new Properties();
        prop.put(key, value);
        getRaw();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserEntity other = (UserEntity) obj;
        if (accountNonExpired != other.accountNonExpired)
            return false;
        if (accountNonLocked != other.accountNonLocked)
            return false;
        if (accountNonLockedStr == null) {
            if (other.accountNonLockedStr != null)
                return false;
        } else if (!accountNonLockedStr.equals(other.accountNonLockedStr)) {
            return false;
        }
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address)) {
            return false;
        }
        if (captcha == null) {
            if (other.captcha != null)
                return false;
        } else if (!captcha.equals(other.captcha)) {
            return false;
        }
        if (credentialsNonExpired != other.credentialsNonExpired)
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (enabled != other.enabled)
            return false;
        if (enabledStr == null) {
            if (other.enabledStr != null)
                return false;
        } else if (!enabledStr.equals(other.enabledStr)) {
            return false;
        }
        if (mobileNumber == null) {
            if (other.mobileNumber != null)
                return false;
        } else if (!mobileNumber.equals(other.mobileNumber)) {
            return false;
        }
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (passwordConfirm == null) {
            if (other.passwordConfirm != null)
                return false;
        } else if (!passwordConfirm.equals(other.passwordConfirm)) {
            return false;
        }
        if (phoneNumber == null) {
            if (other.phoneNumber != null)
                return false;
        } else if (!phoneNumber.equals(other.phoneNumber)) {
            return false;
        }
        if (prop == null) {
            if (other.prop != null)
                return false;
        } else if (!prop.equals(other.prop)) {
            return false;
        }
        if (raw == null) {
            if (other.raw != null)
                return false;
        } else if (!raw.equals(other.raw)) {
            return false;
        }
        if (roles == null) {
            if (other.roles != null)
                return false;
        } else if (!roles.equals(other.roles)) {
            return false;
        }
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId)) {
            return false;
        }
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (accountNonExpired ? 1231 : 1237);
        result = prime * result + (accountNonLocked ? 1231 : 1237);
        result = prime * result + ((accountNonLockedStr == null) ? 0 : accountNonLockedStr.hashCode());
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((captcha == null) ? 0 : captcha.hashCode());
        result = prime * result + (credentialsNonExpired ? 1231 : 1237);
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + (enabled ? 1231 : 1237);
        result = prime * result + ((enabledStr == null) ? 0 : enabledStr.hashCode());
        result = prime * result + ((mobileNumber == null) ? 0 : mobileNumber.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((passwordConfirm == null) ? 0 : passwordConfirm.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + ((prop == null) ? 0 : prop.hashCode());
        result = prime * result + ((raw == null) ? 0 : raw.hashCode());
        result = prime * result + ((roles == null) ? 0 : roles.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    //isa here parse as menu group child
    public UserDto parseDtoAsMenuGroupChild() {
        UserDto userDto = new UserDto();
        userDto.setUserId(this.userId.toString());
        userDto.setUsername(this.username);
        return userDto;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

    public Set<MenuGroupEntity> getMenuGroups() {
        return menuGroups;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Object> getRaw() {
        return raw;
    }

    public void setRaw(Map<String, Object> raw) {
        this.raw = raw;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getPasswordDate() {
        return passwordDate;
    }

    public void setPasswordDate(Date passwordDate) {
        this.passwordDate = passwordDate;
    }

    @Override
    public String toString() {
        return "UserEntity [userId=" + userId + ", username=" + username + ", password=" + password + ", enabled="
                + enabled + ", accountNonExpired=" + accountNonExpired + ", accountNonLocked=" + accountNonLocked
                + ", credentialsNonExpired=" + credentialsNonExpired + ", roles=" + roles
                + ", name=" + name + ", email=" + email + ", address=" + address + ", phoneNumber=" + phoneNumber
                + ", mobileNumber=" + mobileNumber + ", description=" + description + ", raw=" + raw
                + ", imageUrl=" + imageUrl + ", expiredDate=" + expiredDate + ", passwordDate=" + passwordDate
                + ", passwordConfirm=" + passwordConfirm + ", captcha=" + captcha + ", enabledStr=" + enabledStr
                + ", accountNonLockedStr=" + accountNonLockedStr + ", prop=" + prop + "]";
    }

}