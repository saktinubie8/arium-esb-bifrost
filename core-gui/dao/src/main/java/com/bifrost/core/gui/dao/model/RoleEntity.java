package com.bifrost.core.gui.dao.model;

import com.bifrost.core.gui.dao.dto.RoleDto;
import com.bifrost.common.model.ApprovalAuditTrailEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.*;

/**
 * Created by daniel on 3/31/15.
 */
@Entity
@Table(name = "M_ROLES")
public class RoleEntity extends ApprovalAuditTrailEntity implements GrantedAuthority {
    private static final long serialVersionUID = 1L;
    /***************************** - Field - ****************************/
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "pg-uuid")
    @Column(name = "ROLE_ID", nullable = false, unique = true)
    private UUID roleId;

    @Column(name = "ROLE_NAME", unique = true, nullable = false)
    private String authority;

    @Column(name = "ROLE_CODE", unique = true, nullable = false)
    private String roleCode;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "R_USER_ROLE", joinColumns = {
            @JoinColumn(name = "roleId")}, inverseJoinColumns = @JoinColumn(name = "userId"))
    private Set<UserEntity> user = new HashSet<>();

    public RoleEntity() {
    }

    public RoleEntity(UUID id) {
        this.roleId = id;
    }

    public RoleEntity(RoleEntity role) {
        this.roleId = role.roleId;
        this.authority = role.authority;
        this.version = role.version;
        this.createdBy = role.createdBy;
        this.createdDate = role.createdDate;
        this.modifiedBy = role.modifiedBy;
        this.modifiedDate = role.modifiedDate;
        this.description = role.description;
        this.roleCode = role.roleCode;
        this.isActive = role.isActive;
    }

    public RoleEntity(RoleDto roleDto) {
        if (roleDto.getRoleId() != null) {
            this.roleId = UUID.fromString(roleDto.getRoleId());
        }
        this.authority = roleDto.getAuthority();
        this.version = roleDto.getVersion();
        this.createdBy = roleDto.getCreatedBy();
        this.createdDate = roleDto.getCreatedDateParse();
        this.modifiedBy = roleDto.getModifiedBy();
        this.modifiedDate = roleDto.getModifiedDateParse();
        this.description = roleDto.getDescription();
        this.roleCode = roleDto.getRoleCode();
        this.isActive = roleDto.getActive();
    }

    public RoleDto parseDto() {
        RoleDto roleDto = new RoleDto();
        roleDto.setRoleId(this.roleId.toString());
        roleDto.setAuthority(this.authority);
        roleDto.setVersion(this.version);
        roleDto.setCreatedBy(this.createdBy);
        roleDto.setCreatedDate(this.getCreatedDate());
        roleDto.setModifiedBy(this.modifiedBy);
        roleDto.setModifiedDate(this.modifiedDate);
        roleDto.setRoleCode(this.roleCode);
        roleDto.setDescription(this.description);
        roleDto.setIsActive(this.isActive);
        return roleDto;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }

    /***************************** - Other Method - ****************************/

    public enum System {
        ADMIN("ADMIN"), CS("CS"), CUSTOMER("CUSTOMER"), SYSTEM("SYSTEM");

        private String authority;

        System(String authority) {
            this.authority = authority;
        }

        public String getAuthority() {
            return this.authority;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        RoleEntity other = (RoleEntity) obj;
        if (authority == null) {
            if (other.authority != null)
                return false;
        } else if (!authority.equals(other.authority)) {
            return false;
        }
        if (roleId == null) {
            if (other.roleId != null)
                return false;
        } else if (!roleId.equals(other.roleId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((authority == null) ? 0 : authority.hashCode());
        result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
        return result;
    }

    public UUID getRoleId() {
        return roleId;
    }

    public void setRoleId(UUID roleId) {
        this.roleId = roleId;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<UserEntity> getUser() {
        return user;
    }

    public void setUser(Set<UserEntity> user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "RoleEntity [roleId=" + roleId + ", authority=" + authority + "]";
    }

}