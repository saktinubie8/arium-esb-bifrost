package com.bifrost.core.gui.dao.repository;

import com.bifrost.core.gui.dao.model.MenuEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface MenuRepo extends JpaRepository<MenuEntity, UUID>, JpaSpecificationExecutor<MenuEntity> {

    /**
     * Get list menu by role id on function
     *
     * @return list of Menu object
     */
    List<MenuEntity> findByMenuIdIn(UUID[] listMenuId);

    /**
     * Get all menu
     *
     * @return list of Menu object
     */
    List<MenuEntity> findByMenuParentMenuIdNotNull();

    /**
     * findAllActive
     *
     * @return List<MenuEntity>
     * @author alpinthor
     */
    @Query("SELECT ME FROM MenuEntity ME WHERE ME.isActive = true ORDER BY ME.title")
    List<MenuEntity> findAllActive();

    /**
     * findAllActiveMenusByUsername
     *
     * @param username
     * @return List<MenuEntity>
     * @author alpinthor
     */
    @Query("SELECT ME FROM MenuEntity ME INNER JOIN ME.menuGroups MEMG INNER JOIN MEMG.user MEMGU"
            + " WHERE MEMGU.username = :username AND ME.isActive = true ORDER BY ME.title")
    List<MenuEntity> findAllActiveMenusByUsername(@Param("username") String username);

    /**
     * Get MenuEntity by title
     *
     * @param title
     * @return single MenuEntity object
     */
    MenuEntity findByTitle(String title);

    MenuEntity findByMenuId(UUID menuId);

    @Query("SELECT ME FROM MenuEntity ME WHERE ME.menuId = :menuId AND ME.isActive = true")
    MenuEntity getMenuByMenuIdActive(@Param("menuId") UUID menuId);

    Page<MenuEntity> findAll(Pageable pageable);

    int countByIsActive(boolean active);

}