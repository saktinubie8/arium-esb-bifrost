package com.bifrost.core.gui.dao.dto;

import com.bifrost.core.gui.dao.response.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class ValidationResponseDto {

    List<BaseResponse> responValidations = new ArrayList<>();

    public void addError(String respStatusCode) {
        if (responValidations == null)
            responValidations = new ArrayList<>();
        BaseResponse baseResp = new BaseResponse();
        baseResp.setResponseCode(respStatusCode);
        responValidations.add(baseResp);
    }

    public void addError(String respStatusCode, String message) {
        if (responValidations == null)
            responValidations = new ArrayList<>();
        BaseResponse baseResp = new BaseResponse();
        baseResp.setResponseCode(respStatusCode);
        baseResp.setResponseDesc(message);
        responValidations.add(baseResp);
    }

    public List<BaseResponse> getResponValidations() {
        return responValidations;
    }

    public void setResponValidations(List<BaseResponse> responValidations) {
        this.responValidations = responValidations;
    }

}