package com.bifrost.core.gui.dao.util;

import com.bifrost.core.gui.dao.dto.MainMenuTreeContainerDto;
import com.bifrost.core.gui.dao.model.MenuEntity;

import java.util.*;

public class MenuUtil {

    public MenuEntity convertJsonToMenu() {
        return null;
    }

    /**
     * @return List<MainMenuTreeContainer>
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     */
    public static List<MainMenuTreeContainerDto> buildMainMenuTreeContainerForJson(
            List<MainMenuTreeContainerDto> listMainMenuTreeContainer) {
        Map<String, List<MainMenuTreeContainerDto>> mapOfChildren = new HashMap<>();
        List<MainMenuTreeContainerDto> rootNodes = new ArrayList<>();
        Comparator<MainMenuTreeContainerDto> compByOrder = new Comparator<MainMenuTreeContainerDto>() {
            @Override
            public int compare(MainMenuTreeContainerDto o1, MainMenuTreeContainerDto o2) {
                if (o1.getOrder() > o2.getOrder()) {
                    return 1;
                } else if (o1.getOrder() < o2.getOrder()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
        for (MainMenuTreeContainerDto item : listMainMenuTreeContainer) {
            // hook into parent
            if (item.getParentId() == null) {
                // Menu nya ga dimasukin
                List<MainMenuTreeContainerDto> childrenListFromMap = mapOfChildren.get(item.getId());
                item.setSubMenu(childrenListFromMap);
                rootNodes.add(item);
            } else {
                // sibling object
                List<MainMenuTreeContainerDto> siblingListFromMap = mapOfChildren.get(item.getParentId());
                // child object
                List<MainMenuTreeContainerDto> childrenListFromMap = mapOfChildren.get(item.getId());
                if (siblingListFromMap == null) {
                    if (childrenListFromMap != null) {
                        // set to children
                        item.setSubMenu(childrenListFromMap);
                    }
                    // new child
                    List<MainMenuTreeContainerDto> newChildList = new ArrayList<>();
                    newChildList.add(item);
                    mapOfChildren.put(item.getParentId(), newChildList);
                } else {
                    if (childrenListFromMap != null) {
                        // set to children
                        item.setSubMenu(childrenListFromMap);
                    }
                    // add child
                    siblingListFromMap.add(item);
                    // sort by order
                    Collections.sort(siblingListFromMap, compByOrder);
                    // replace / update map
                    mapOfChildren.put(item.getParentId(), siblingListFromMap);
                }
            }
        }
        Collections.sort(rootNodes, compByOrder);
        return rootNodes;
    }

}