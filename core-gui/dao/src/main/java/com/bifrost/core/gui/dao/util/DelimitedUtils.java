package com.bifrost.core.gui.dao.util;

import java.util.*;

public class DelimitedUtils {

    private DelimitedUtils() {
    }

    public static String toDelimitedString(Map<String, Object> source) {
        StringBuilder retVal = new StringBuilder("");
        for (Map.Entry<String, Object> entry : source.entrySet()) {
            String val = "";
            if (entry.getValue() != null)
                val = entry.getValue().toString();
            retVal.append(entry.getKey()).append("=").append(val).append(",");
        }
        //remove last ,
        retVal.deleteCharAt(retVal.length());
        return retVal.toString();
    }

    public static String toDelimitedString(Collection source) {
        StringBuilder retVal = new StringBuilder("");
        for (Object key : source) {
            String val = key.toString();
            retVal.append(val).append(",");
        }
        retVal.deleteCharAt(retVal.length());
        return retVal.toString();
    }

    public static String toDelimitedString(Set source) {
        StringBuilder retVal = new StringBuilder("");
        if (source != null)
            for (Object key : source) {
                String val = key.toString();
                retVal.append(val).append(",");
            }
        //remove last ,
        retVal.deleteCharAt(retVal.length());
        return retVal.toString();
    }

    public static Map<String, Object> toMap(String source) {
        Map<String, Object> retVal = new LinkedHashMap<>();
        if (source != null)
            for (String record : source.split("\\,")) {
                if (record.indexOf('=') >= 0) {
                    String[] data = record.split("\\=");
                    retVal.put(data[0], data[1]);
                }
            }
        return retVal;
    }

    public static Set<String> toSet(String source) {
        Set<String> retVal = new LinkedHashSet<>();
        if (source != null)
            for (String record : source.split("\\,")) {
                retVal.add(record);
            }
        return retVal;
    }

    public static Collection<String> toCollection(String source) {
        List<String> retVal = new ArrayList<>();
        for (String record : source.split("\\,")) {
            retVal.add(record);
        }
        return retVal;
    }

}