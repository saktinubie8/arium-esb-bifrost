package com.bifrost.core.gui.dao.repository;

import com.bifrost.core.gui.dao.model.RoleEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface RoleRepo extends JpaRepository<RoleEntity, UUID>, JpaSpecificationExecutor<RoleEntity> {

    int countByIsActive(boolean active);

    @Query("SELECT COUNT(a) FROM RoleEntity a WHERE a.roleCode = :roleCode  AND a.isActive = true")
    int validateCode(@Param("roleCode") String roleCode);

    @Query("SELECT COUNT(a) FROM RoleEntity a WHERE a.authority = :authority  AND a.isActive = true")
    int validateName(@Param("authority") String roleCode);

    RoleEntity findByRoleId(UUID roleId);

    List<RoleEntity> findByAuthority(String authority);

    Page<RoleEntity> findAll(Pageable pageable);

    Page<RoleEntity> findAll(Specification<RoleEntity> spec, Pageable pageable);

    @SuppressWarnings("unchecked")
    RoleEntity saveAndFlush(RoleEntity role);

    @SuppressWarnings("unchecked")
    RoleEntity save(RoleEntity role);

    void delete(RoleEntity role);

    @Query("SELECT a FROM RoleEntity a WHERE a.authority = :valueToValidate AND a.isActive=true")
    RoleEntity findByRoleNameActiveOnly(@Param("valueToValidate") String valueToValidate);

    List<RoleEntity> findRoleByRoleId(UUID roleId);

    @Query("SELECT a FROM RoleEntity a WHERE a.isActive=true")
    List<RoleEntity> findAllIsActive();

}