package com.bifrost.core.gui.dao.model;

import com.bifrost.core.gui.dao.dto.*;
import com.bifrost.common.model.ApprovalAuditTrailEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by abet on 12/5/15.
 */
@Entity
@Table(name = "M_MENUS")
public class MenuEntity extends ApprovalAuditTrailEntity implements Serializable {

    private static final long serialVersionUID = -8786805085128140615L;

    public MenuEntity() {
        super();
    }

    public MenuEntity(MenuDto menuDto) {
        this.menuId = UUID.fromString(menuDto.getMenuId());
        this.parentId = (menuDto.getParentId() == null) ? null : UUID.fromString(menuDto.getParentId());
//		this.isLeaf = menuDto.getIsLeaf();
        this.title = menuDto.getTitle();
        this.pageUrl = menuDto.getPageUrl();
        this.level = menuDto.getLevel();
        this.order = menuDto.getOrder();
        this.module = menuDto.getModule();
        this.version = menuDto.getVersion();
        this.createdBy = menuDto.getCreatedBy();
        this.createdDate = menuDto.getCreatedDateParse();
        this.modifiedBy = menuDto.getModifiedBy();
        this.modifiedDate = menuDto.getModifiedDateParse();
        this.icon = menuDto.getIcon();
        this.description = menuDto.getDescription();
    }

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "ID", nullable = false, unique = true)
    @Type(type = "pg-uuid")
    private UUID menuId;

    @Column(name = "PARENT_ID", nullable = true)
    @Type(type = "pg-uuid")
    private UUID parentId;

    @ManyToOne(targetEntity = MenuEntity.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "PARENT_ID", nullable = true, insertable = false, updatable = false)
    private MenuEntity menuParent;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "PAGE_URL", nullable = true)
    private String pageUrl;

    @Column(name = "LEVEL", nullable = false)
    private int level;

    @Column(name = "ORDERING", nullable = true)
    private int order;

    @Column(name = "MODULE", nullable = false)
    private String module;

    @Column(name = "ICON")
    private String icon;

    @Column(name = "DESCRIPTION", length = 200)
    private String description;

    @OneToMany(mappedBy = "menuParent", fetch = FetchType.EAGER)
    @OrderBy("order")
    private Set<MenuEntity> childMenu;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "R_MENU_GROUP_MENU", joinColumns = {
            @JoinColumn(name = "menuId")}, inverseJoinColumns = @JoinColumn(name = "groupId"))
    @JsonIgnore
    private Set<MenuGroupEntity> menuGroups = new HashSet<>();

    public MenuDto parseDto(Boolean insertParent) {
        MenuDto menuDto = new MenuDto();
        menuDto.setMenuId(this.menuId.toString());
        if (this.parentId != null)
            menuDto.setParentId(this.parentId.toString());
        if (this.menuParent != null && insertParent)
            menuDto.setMenuParent(this.menuParent.parseDto(true));
//		menuDto.setIsLeaf(this.isLeaf);
        menuDto.setTitle(this.title);
        menuDto.setPageUrl(this.pageUrl);
        menuDto.setLevel(this.level);
        menuDto.setOrder(this.order);
        menuDto.setModule(this.module);
        menuDto.setVersion(this.version);
        menuDto.setCreatedBy(this.createdBy);
        menuDto.setCreatedDate(this.createdDate);
        menuDto.setModifiedBy(this.modifiedBy);
        menuDto.setModifiedDate(this.modifiedDate);
        menuDto.setIcon(this.icon);
        menuDto.setDescription(this.description);
        menuDto.setIsActive(this.isActive);
        Set<MenuGroupDto> menuGroupDtos = new HashSet<>();
        for (MenuGroupEntity menuGroupEntity : menuGroups) {
            if (menuGroupEntity.getActive()) {
                MenuGroupDto menuGroupDto = menuGroupEntity.parseDtoAsMenusChild();
                menuGroupDtos.add(menuGroupDto);
            }
        }
        menuDto.setMenuGroupDto(menuGroupDtos);
        return menuDto;
    }

    public TreeUtilContainerDto parseTreeUtilContainer() {
        TreeUtilContainerDto treeUtilContainer = new TreeUtilContainerDto();
        treeUtilContainer.setId(this.menuId.toString());
        treeUtilContainer.setParentId(this.parentId.toString());
        if (this.menuParent != null)
            treeUtilContainer.setParent(this.menuParent.parseTreeUtilContainer());
        treeUtilContainer.setLabel(this.title);
        if (this.menuParent != null)
            treeUtilContainer.setParentLabel(this.menuParent.title);
        treeUtilContainer.setLevel(this.level);
        treeUtilContainer.setOrder(this.order);
        return treeUtilContainer;
    }

    public TreeEditorContainerDto parseTreeEditorContainer(Boolean isEditable, Boolean isDeletable,
                                                           List<MenuEntity> menuToInclude) {
        TreeEditorContainerDto treeEditorContainer = new TreeEditorContainerDto();
        treeEditorContainer.setId(this.menuId.toString());
        if (this.menuParent != null) {
            treeEditorContainer.setParentId(this.parentId.toString());
            treeEditorContainer.setParentContent(this.menuParent.title);
        }
        treeEditorContainer.setContent(this.title);
        treeEditorContainer.setLevel(this.level);
        treeEditorContainer.setOrder(this.order);
        treeEditorContainer.setDataMenuDto(this.parseDto(false));
        treeEditorContainer.setEditable(isEditable);
        treeEditorContainer.setDeletable(isDeletable);
        List<TreeEditorContainerDto> children = new ArrayList<>();
        for (Iterator iterator = this.childMenu.iterator(); iterator.hasNext(); ) {
            MenuEntity menuEntity = (MenuEntity) iterator.next();
            if (menuEntity.getActive() && menuToInclude.contains(menuEntity)) {
                children.add(menuEntity.parseTreeEditorContainer(isEditable, isDeletable, menuToInclude));
            }
        }
        if (!children.isEmpty()) {
            treeEditorContainer.setChildren(children);
        }
        return treeEditorContainer;
    }

    public MainMenuTreeContainerDto parseMainMenuTreeContainer() {
        MainMenuTreeContainerDto mainMenuTreeContainer = new MainMenuTreeContainerDto();
        mainMenuTreeContainer.setId(this.menuId.toString());
        if (this.menuParent != null) {
            mainMenuTreeContainer.setParentId(this.parentId.toString());
        }
        mainMenuTreeContainer.setLabel(this.title);
        mainMenuTreeContainer.setIcon(null);
        mainMenuTreeContainer.setUrl(this.pageUrl);
        mainMenuTreeContainer.setLevel(this.level);
        mainMenuTreeContainer.setOrder(this.order);
        mainMenuTreeContainer.setIcon(this.icon);
        mainMenuTreeContainer.setDescription(this.description);
        return mainMenuTreeContainer;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MenuEntity other = (MenuEntity) obj;
        if (icon == null) {
            if (other.icon != null)
                return false;
        } else if (!icon.equals(other.icon))
            return false;
        if (level != other.level)
            return false;
        if (menuId == null) {
            if (other.menuId != null)
                return false;
        } else if (!menuId.equals(other.menuId))
            return false;
        if (module == null) {
            if (other.module != null)
                return false;
        } else if (!module.equals(other.module))
            return false;
        if (order != other.order)
            return false;
        if (pageUrl == null) {
            if (other.pageUrl != null)
                return false;
        } else if (!pageUrl.equals(other.pageUrl))
            return false;
        if (parentId == null) {
            if (other.parentId != null)
                return false;
        } else if (!parentId.equals(other.parentId))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((icon == null) ? 0 : icon.hashCode());
//		result = prime * result + ((isLeaf == null) ? 0 : isLeaf.hashCode());
        result = prime * result + level;
        result = prime * result + ((menuId == null) ? 0 : menuId.hashCode());
        result = prime * result + ((module == null) ? 0 : module.hashCode());
        result = prime * result + order;
        result = prime * result + ((pageUrl == null) ? 0 : pageUrl.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    public UUID getMenuId() {
        return menuId;
    }

    public void setMenuId(UUID menuId) {
        this.menuId = menuId;
    }

    public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID parentId) {
        this.parentId = parentId;
    }

    public MenuEntity getMenuParent() {
        return menuParent;
    }

    public void setMenuParent(MenuEntity menuParent) {
        this.menuParent = menuParent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MenuEntity> getChildMenu() {
        return childMenu;
    }

    public void setChildMenu(Set<MenuEntity> childMenu) {
        this.childMenu = childMenu;
    }

    public Set<MenuGroupEntity> getMenuGroups() {
        return menuGroups;
    }

    public void setMenuGroups(Set<MenuGroupEntity> menuGroups) {
        this.menuGroups = menuGroups;
    }

    @Override
    public String toString() {
        return "MenuEntity [menuId=" + menuId + ", parentId=" + parentId + ", isLeaf=" + ", title=" + title
                + ", pageUrl=" + pageUrl + ", level=" + level + ", order=" + order + ", module=" + module + ", icon="
                + icon + "]";
    }

}