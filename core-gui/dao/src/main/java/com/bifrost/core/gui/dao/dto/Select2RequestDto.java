package com.bifrost.core.gui.dao.dto;

public class Select2RequestDto {

    private Select2DataDto search;
    private Integer page = 0;
    private Integer pageLimit = 10;

    public Select2DataDto getSearch() {
        return search;
    }

    public void setSearch(Select2DataDto search) {
        this.search = search;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageLimit() {
        return pageLimit;
    }

    public void setPageLimit(Integer pageLimit) {
        this.pageLimit = pageLimit;
    }

}