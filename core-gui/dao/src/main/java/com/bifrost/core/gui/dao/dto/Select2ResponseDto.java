package com.bifrost.core.gui.dao.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Select2ResponseDto {

    @JsonProperty("count_filtered")
    private long recordsFiltered;
    @JsonProperty("total_count")
    private long recordsTotal;
    private List<Select2Dto> results = new ArrayList<>();
    private Select2PaginationDto pagination;

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public List<Select2Dto> getResults() {
        return results;
    }

    public void setResults(List<Select2Dto> results) {
        this.results = results;
    }

    public Select2PaginationDto getPagination() {
        return pagination;
    }

    public void setPagination(Select2PaginationDto pagination) {
        this.pagination = pagination;
    }

}