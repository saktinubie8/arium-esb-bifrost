package com.bifrost.core.gui.dao.util;

import com.bifrost.core.gui.dao.dto.TreeEditorContainerDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class TreeEditorUtil {

    private TreeEditorUtil() {
    }

    /**
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     * @param List<TreeEditorContainerDto>,
     * isViewerOnly
     * @return List<TreeEditorContainerDto>
     */
    protected static final Logger logger = LogManager.getLogger();

    public static List<TreeEditorContainerDto> buildTreeEditorContainerForJsonBACKUP(
            List<TreeEditorContainerDto> listTreeEditorContainer, Boolean isViewerOnly) {
        Map<String, List<TreeEditorContainerDto>> mapOfChildren = new HashMap<>();
        List<TreeEditorContainerDto> rootNodes = new ArrayList<>();
        Comparator<TreeEditorContainerDto> compByOrder = new Comparator<TreeEditorContainerDto>() {
            @Override
            public int compare(TreeEditorContainerDto o1, TreeEditorContainerDto o2) {
                if (o1.getOrder() > o2.getOrder()) {
                    return 1;
                } else if (o1.getOrder() < o2.getOrder()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
        // sort to level desc
        Comparator<TreeEditorContainerDto> compTreeEditorContainerByLevelDesc = new Comparator<TreeEditorContainerDto>() {
            @Override
            public int compare(TreeEditorContainerDto o1, TreeEditorContainerDto o2) {
                if (o1.getOrder() > o2.getOrder()) {
                    return 1;
                } else if (o1.getOrder() < o2.getOrder()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
        Collections.sort(listTreeEditorContainer, compTreeEditorContainerByLevelDesc);
        for (TreeEditorContainerDto item : listTreeEditorContainer) {
            // hook into parent
            if (item.getParentId() == null) {
                // root
                List<TreeEditorContainerDto> childrenListFromMap = mapOfChildren.get(item.getId());
                item.setChildren(childrenListFromMap);
                if (isViewerOnly) {
                    item.setEditable(false);
                    item.setDeletable(false);
                }
                rootNodes.add(item);
            } else {
                // sibling object
                List<TreeEditorContainerDto> siblingListFromMap = mapOfChildren.get(item.getParentId());
                // child object
                List<TreeEditorContainerDto> childrenListFromMap = mapOfChildren.get(item.getId());
                if (siblingListFromMap == null) {
                    if (isViewerOnly) {
                        item.setEditable(false);
                        item.setDeletable(false);
                    }
                    if (childrenListFromMap != null) {
                        // set to children
                        item.setChildren(childrenListFromMap);
                    }
                    // new child
                    List<TreeEditorContainerDto> newChildList = new ArrayList<>();
                    newChildList.add(item);
                    mapOfChildren.put(item.getParentId(), newChildList);
                } else {
                    if (isViewerOnly) {
                        item.setEditable(false);
                        item.setDeletable(false);
                    }
                    if (childrenListFromMap != null) {
                        // set to children
                        item.setChildren(childrenListFromMap);
                    }
                    // add child
                    siblingListFromMap.add(item);
                    // sort by order
                    Collections.sort(siblingListFromMap, compByOrder);
                    // replace / update map
                    mapOfChildren.put(item.getParentId(), siblingListFromMap);
                }
            }
        }
        Collections.sort(rootNodes, compByOrder);
        return rootNodes;
    }

    /**
     * @return List<TreeEditorContainerDto>
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     */
    public static List<TreeEditorContainerDto> buildTreeEditorContainerForJsonNYOBAJAJAL(
            List<TreeEditorContainerDto> listTreeEditorContainer) {
        Map<String, List<TreeEditorContainerDto>> mapOfListOfChildren = new HashMap<>();
        Map<String, List<TreeEditorContainerDto>> mapOfListOfChildren2 = new HashMap<>();
        Map<String, TreeEditorContainerDto> mapOfRoots = new HashMap<>();
        List<TreeEditorContainerDto> rootNodes = new ArrayList<>();
        Comparator<TreeEditorContainerDto> compByOrder = new Comparator<TreeEditorContainerDto>() {
            @Override
            public int compare(TreeEditorContainerDto o1, TreeEditorContainerDto o2) {
                if (o1.getOrder() > o2.getOrder()) {
                    return 1;
                } else if (o1.getOrder() < o2.getOrder()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
        for (TreeEditorContainerDto item : listTreeEditorContainer) {
            if (item.getParentId() == null) {
                mapOfRoots.put(item.getId(), item);
            } else {
                List<TreeEditorContainerDto> listOfChildren = mapOfListOfChildren.get(item.getParentId());
                if (listOfChildren == null) {
                    listOfChildren = new ArrayList<>();
                    listOfChildren.add(item);
                    mapOfListOfChildren.put(item.getParentId(), listOfChildren);
                } else {
                    listOfChildren.add(item);
                    Collections.sort(listOfChildren, compByOrder);
                    mapOfListOfChildren.put(item.getParentId(), listOfChildren);
                }
            }
        }
        String parentId = null;
        List<TreeEditorContainerDto> listOfChildrenFromMap = null;
        int failSafe = 0;
        do {
            if (mapOfListOfChildren2.size() > 0) {
                mapOfListOfChildren = mapOfListOfChildren2;
            }
            for (Iterator iterator = mapOfListOfChildren.entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry<String, List<TreeEditorContainerDto>> entry = (Map.Entry<String, List<TreeEditorContainerDto>>) iterator
                        .next();
                parentId = entry.getKey();
                List<TreeEditorContainerDto> listOfChildren = entry.getValue();
                for (Iterator iterator2 = listOfChildren.iterator(); iterator2.hasNext(); ) {
                    TreeEditorContainerDto treeEditorContainerDto = (TreeEditorContainerDto) iterator2.next();
                    listOfChildrenFromMap = mapOfListOfChildren.get(treeEditorContainerDto.getParentId());
                    listOfChildrenFromMap.add(treeEditorContainerDto);
                    Collections.sort(listOfChildrenFromMap, compByOrder);
                    mapOfListOfChildren2.put(treeEditorContainerDto.getParentId(), listOfChildrenFromMap);
                }
            }
            TreeEditorContainerDto root = mapOfRoots.get(parentId);
            root.setChildren(listOfChildrenFromMap);
            mapOfRoots.put(root.getId(), root);
            mapOfListOfChildren.remove(root.getId());
            failSafe++;
            logger.info("failSafe = " + failSafe);
        } while (mapOfListOfChildren.size() > 0 || failSafe > 100);
        // put into root nodes
        for (Map.Entry<String, TreeEditorContainerDto> entry : mapOfRoots.entrySet()) {
            rootNodes.add(entry.getValue());
        }
        Collections.sort(rootNodes, compByOrder);
        return rootNodes;
    }

    /**
     * @return List<TreeEditorContainerDto>
     * @author alpinthor
     * <p>
     * parameter must be descending order by level
     */
    public static List<TreeEditorContainerDto> buildTreeEditorContainerForJson(
            List<TreeEditorContainerDto> listTreeEditorContainer) {
        List<TreeEditorContainerDto> rootNodes = new ArrayList<>();
        Comparator<TreeEditorContainerDto> compByOrder = new Comparator<TreeEditorContainerDto>() {
            @Override
            public int compare(TreeEditorContainerDto o1, TreeEditorContainerDto o2) {
                if (o1.getOrder() > o2.getOrder()) {
                    return 1;
                } else if (o1.getOrder() < o2.getOrder()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
        for (TreeEditorContainerDto item : listTreeEditorContainer) {
            if (item.getParentId() == null) {
                rootNodes.add(item);
            }
        }
        Collections.sort(rootNodes, compByOrder);
        return rootNodes;
    }

}