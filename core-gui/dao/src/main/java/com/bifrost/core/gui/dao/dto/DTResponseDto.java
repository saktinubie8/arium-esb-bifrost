package com.bifrost.core.gui.dao.dto;

import com.bifrost.core.gui.dao.response.BaseResponse;

import java.util.List;

public class DTResponseDto<T> extends BaseResponse {

    private long draw;
    private long recordsFiltered;
    private long recordsTotal;
    List<T> data;
    private String error;

    public long getDraw() {
        return draw;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}