package com.bifrost.core.gui.dao.util;

import java.util.Arrays;

public class FilterSpec {
	
	private Object[] values;
	private QueryOperator operator;

	public Object[] getValues() {
		return values;
	}

	public void setValues(Object[] values) {
		this.values = values;
	}

	public QueryOperator getOperator() {
		return operator;
	}

	public void setOperator(QueryOperator operator) {
		this.operator = operator;
	}

	@Override
	public String toString() {
		return "FilterSpec [values=" + Arrays.toString(values) + ", operator=" + operator + "]";
	}

}
