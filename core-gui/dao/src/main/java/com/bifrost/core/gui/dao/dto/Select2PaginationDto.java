package com.bifrost.core.gui.dao.dto;

public class Select2PaginationDto {

    private boolean more;

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public Select2PaginationDto(boolean more) {
        this.more = more;
    }

}