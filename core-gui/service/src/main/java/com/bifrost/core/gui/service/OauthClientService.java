package com.bifrost.core.gui.service;

import com.bifrost.core.gui.dao.model.OauthClientDetailsEntity;
import com.bifrost.core.gui.dao.model.RoleEntity;
import com.bifrost.core.gui.dao.repository.OauthClientDetailsRepo;
import com.bifrost.core.gui.dao.repository.RoleRepo;
import com.bifrost.core.gui.dao.util.DelimitedUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OauthClientService extends BaseService implements ClientDetailsService, ClientRegistrationService {

    @Autowired
    private OauthClientDetailsRepo oauthClientDetailsRepo;

    @Autowired
    private RoleRepo roleRepo;

    String strNotExist = " not exists";
    String strClient = "Client ";

    @Override
    public void addClientDetails(ClientDetails clientDetails) {
        OauthClientDetailsEntity oauthClientDetailsEntity = this.oauthClientDetailsRepo.findById(clientDetails.getClientId()).orElse(null);
        if (oauthClientDetailsEntity != null)
            throw new ClientAlreadyExistsException(strClient + clientDetails.getClientId() + " already exists");
        else {
            oauthClientDetailsEntity = new OauthClientDetailsEntity();
            oauthClientDetailsEntity.setAccessTokenValidity(clientDetails.getAccessTokenValiditySeconds());
            oauthClientDetailsEntity.setAdditionalInformation(DelimitedUtils.toDelimitedString(clientDetails.getAdditionalInformation()));
            oauthClientDetailsEntity.setAuthorities(DelimitedUtils.toDelimitedString(clientDetails.getAuthorities()));
            oauthClientDetailsEntity.setAuthorizedGrantTypes(DelimitedUtils.toDelimitedString(clientDetails.getAuthorizedGrantTypes()));
            oauthClientDetailsEntity.setAutoapprove("true");
            oauthClientDetailsEntity.setClientId(clientDetails.getClientId());
            oauthClientDetailsEntity.setClientSecret(clientDetails.getClientSecret());
            oauthClientDetailsEntity.setRefreshTokenValidity(clientDetails.getRefreshTokenValiditySeconds());
            oauthClientDetailsEntity.setResourceIds(DelimitedUtils.toDelimitedString(clientDetails.getResourceIds()));
            oauthClientDetailsEntity.setScope(DelimitedUtils.toDelimitedString(clientDetails.getScope()));
            oauthClientDetailsEntity.setWebServerRedirectUri(DelimitedUtils.toDelimitedString(clientDetails.getRegisteredRedirectUri()));
            this.oauthClientDetailsRepo.save(oauthClientDetailsEntity);
        }
    }

    @Override
    public void updateClientDetails(ClientDetails clientDetails) {
        OauthClientDetailsEntity oauthClientDetailsEntity = this.oauthClientDetailsRepo.findById(clientDetails.getClientId()).orElse(null);
        if (oauthClientDetailsEntity == null)
            throw new NoSuchClientException(strClient + clientDetails.getClientId() + strNotExist);
        else {
            oauthClientDetailsEntity.setAccessTokenValidity(clientDetails.getAccessTokenValiditySeconds());
            oauthClientDetailsEntity.setAdditionalInformation(DelimitedUtils.toDelimitedString(clientDetails.getAdditionalInformation()));
            oauthClientDetailsEntity.setAuthorities(DelimitedUtils.toDelimitedString(clientDetails.getAuthorities()));
            oauthClientDetailsEntity.setAuthorizedGrantTypes(DelimitedUtils.toDelimitedString(clientDetails.getAuthorizedGrantTypes()));
            oauthClientDetailsEntity.setAutoapprove("true");
            oauthClientDetailsEntity.setClientId(clientDetails.getClientId());
            oauthClientDetailsEntity.setClientSecret(clientDetails.getClientSecret());
            oauthClientDetailsEntity.setRefreshTokenValidity(clientDetails.getRefreshTokenValiditySeconds());
            oauthClientDetailsEntity.setResourceIds(DelimitedUtils.toDelimitedString(clientDetails.getResourceIds()));
            oauthClientDetailsEntity.setScope(DelimitedUtils.toDelimitedString(clientDetails.getScope()));
            oauthClientDetailsEntity.setWebServerRedirectUri(DelimitedUtils.toDelimitedString(clientDetails.getRegisteredRedirectUri()));
            this.oauthClientDetailsRepo.save(oauthClientDetailsEntity);
        }
    }

    @Override
    public void updateClientSecret(String clientId, String secret) {
        OauthClientDetailsEntity oauthClientDetailsEntity = this.oauthClientDetailsRepo.findById(clientId).orElse(null);
        if (oauthClientDetailsEntity == null)
            throw new NoSuchClientException(strClient + clientId + strNotExist);
        else {
            oauthClientDetailsEntity.setClientSecret(secret);
            this.oauthClientDetailsRepo.save(oauthClientDetailsEntity);
        }
    }

    @Override
    public void removeClientDetails(String clientId) {
        OauthClientDetailsEntity oauthClientDetailsEntity = this.oauthClientDetailsRepo.findById(clientId).orElse(null);
        if (oauthClientDetailsEntity == null)
            throw new NoSuchClientException(strClient + clientId + strNotExist);
        else {
            this.oauthClientDetailsRepo.deleteById(clientId);
        }
    }

    @Override
    public List<ClientDetails> listClientDetails() {
        List<OauthClientDetailsEntity> allOauthClientDetails = this.oauthClientDetailsRepo.findAll();
        List<ClientDetails> retVal = new ArrayList<>();
        for (OauthClientDetailsEntity oauthClientDetailsEntity : allOauthClientDetails) {
            BaseClientDetails clientDetails = new BaseClientDetails();
            clientDetails.setAccessTokenValiditySeconds(oauthClientDetailsEntity.getAccessTokenValidity());
            clientDetails.setAdditionalInformation(DelimitedUtils.toMap(oauthClientDetailsEntity.getAdditionalInformation()));
            List<String> roleNames = (List<String>) DelimitedUtils.toCollection(oauthClientDetailsEntity.getAuthorities());
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            for (String roleName : roleNames) {
                RoleEntity roleEntity = this.roleRepo.findByRoleNameActiveOnly(roleName);
                grantedAuthorities.add(roleEntity);
            }
            clientDetails.setAuthorities(grantedAuthorities);
            clientDetails.setAuthorizedGrantTypes(DelimitedUtils.toCollection(oauthClientDetailsEntity.getAuthorizedGrantTypes()));
            clientDetails.setAutoApproveScopes(DelimitedUtils.toCollection(oauthClientDetailsEntity.getAutoapprove()));
            clientDetails.setClientId(oauthClientDetailsEntity.getClientId());
            clientDetails.setClientSecret(oauthClientDetailsEntity.getClientSecret());
            clientDetails.setRefreshTokenValiditySeconds(oauthClientDetailsEntity.getRefreshTokenValidity());
            clientDetails.setResourceIds(DelimitedUtils.toCollection(oauthClientDetailsEntity.getResourceIds()));
            clientDetails.setRegisteredRedirectUri(DelimitedUtils.toSet(oauthClientDetailsEntity.getWebServerRedirectUri()));
            clientDetails.setScope(DelimitedUtils.toCollection(oauthClientDetailsEntity.getScope()));
        }
        return retVal;
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId) {
        OauthClientDetailsEntity oauthClientDetailsEntity = this.oauthClientDetailsRepo.findById(clientId).orElse(null);
        if (oauthClientDetailsEntity == null)
            throw new ClientRegistrationException(strClient + clientId + strNotExist);
        else {
            BaseClientDetails clientDetails = new BaseClientDetails();
            clientDetails.setAccessTokenValiditySeconds(oauthClientDetailsEntity.getAccessTokenValidity());
            clientDetails.setAdditionalInformation(DelimitedUtils.toMap(oauthClientDetailsEntity.getAdditionalInformation()));
            List<String> roleNames = (List<String>) DelimitedUtils.toCollection(oauthClientDetailsEntity.getAuthorities());
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            for (String roleName : roleNames) {
                RoleEntity roleEntity = this.roleRepo.findByRoleNameActiveOnly(roleName);
                grantedAuthorities.add(roleEntity);
            }
            clientDetails.setAuthorities(grantedAuthorities);
            clientDetails.setAuthorizedGrantTypes(DelimitedUtils.toCollection(oauthClientDetailsEntity.getAuthorizedGrantTypes()));
            clientDetails.setAutoApproveScopes(DelimitedUtils.toCollection(oauthClientDetailsEntity.getAutoapprove()));
            clientDetails.setClientId(oauthClientDetailsEntity.getClientId());
            clientDetails.setClientSecret(oauthClientDetailsEntity.getClientSecret());
            clientDetails.setRefreshTokenValiditySeconds(oauthClientDetailsEntity.getRefreshTokenValidity());
            clientDetails.setResourceIds(DelimitedUtils.toCollection(oauthClientDetailsEntity.getResourceIds()));
            clientDetails.setRegisteredRedirectUri(DelimitedUtils.toSet(oauthClientDetailsEntity.getWebServerRedirectUri()));
            clientDetails.setScope(DelimitedUtils.toCollection(oauthClientDetailsEntity.getScope()));
            return clientDetails;
        }
    }

}