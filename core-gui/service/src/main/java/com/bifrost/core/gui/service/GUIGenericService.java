package com.bifrost.core.gui.service;

import com.bifrost.common.constant.GUIConstant;
import com.bifrost.core.gui.dao.dto.*;
import com.bifrost.core.gui.dao.model.LookupEntity;
import com.bifrost.core.gui.dao.model.LookupGroupEntity;
import com.bifrost.core.gui.dao.repository.LookupGroupRepo;
import com.bifrost.core.gui.dao.repository.LookupRepo;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.core.gui.dao.util.QueryOperator;
import com.bifrost.system.model.SystemParam;
import com.bifrost.system.repository.SystemParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.data.domain.Sort.by;

@Service
public class GUIGenericService extends BaseService {

    @Autowired
    private SystemParamRepository systemParameterRepo;
    @Autowired
    private LookupRepo lookupRepo;
    @Autowired
    private LookupGroupRepo lookupGroupRepo;

    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    protected Pageable getPageable(SearchCriteriaDto dto) {
        Sort sort = null;
        if (dto.getDirection() != null) {
            if (dto.getDirection().equalsIgnoreCase("ASC")) {
                sort = by(Sort.Direction.ASC, dto.getOrderBy());
            } else {
                sort = by(Sort.Direction.DESC, dto.getOrderBy());
            }
            return PageRequest.of(dto.getPage(), dto.getLimit(), sort);
        }
        return PageRequest.of(dto.getPage(), dto.getLimit());
    }

    protected Pageable getPageable(Select2RequestDto select2RequestDto, String orderBy) {
        return getPageable(select2RequestDto.getPage(), select2RequestDto.getPageLimit(), orderBy);
    }

    protected Pageable getPageable(int page, int limit, String orderBy) {
        Integer pageIndex = 0;
        Integer pageLimit = 10;
        if (page > 0) {
            pageIndex = page - 1;
        }
        if (limit > 10) {
            pageLimit = limit;
        }
        if (orderBy != null) {
            Sort sort = by(Sort.Direction.ASC, orderBy);
            return PageRequest.of(pageIndex, pageLimit, sort);
        }
        return PageRequest.of(pageIndex, pageLimit);
    }

    protected Pageable getPageable(DTRequestDto dtRequestDto) {
        if (dtRequestDto == null) {
            return PageRequest.of(0, 10);
        }
        int limit = dtRequestDto.getLength() > 10 ? (int) dtRequestDto.getLength() : 10;
        int pageIndex = dtRequestDto.getStart() / limit > 0 ? (int) dtRequestDto.getStart() / limit : 0;
        if (dtRequestDto.getOrder() == null || dtRequestDto.getColumns() == null
                || dtRequestDto.getColumns().isEmpty()) {
            return PageRequest.of(pageIndex, limit);
        }
        Sort sort = null;
        if (!dtRequestDto.getOrder().isEmpty()) {
            for (DTOrder dtOrder : dtRequestDto.getOrder()) {
                if (dtOrder.getColumn() > 0 || dtOrder.getColumn() <= dtRequestDto.getColumns().size()) {
                    DTColumn dtColumn = dtRequestDto.getColumns().get(dtOrder.getColumn());
                    if (dtColumn.isOrderable()) {
                        Sort.Direction direction = dtOrder.getDir().equalsIgnoreCase("ASC") ? Sort.Direction.ASC
                                : Sort.Direction.DESC;
                        if (sort == null) {
                            sort = by(direction, dtColumn.getData());
                            continue;
                        }
                        sort.and(by(direction, dtColumn.getData()));
                    }
                }
            }
        } else {
            sort = by(Sort.Direction.DESC, "modifiedDate");
        }
        return PageRequest.of(pageIndex, limit, sort);

    }

    protected Pageable getPageableWithoutSort(DTRequestDto dtRequestDto) {
        if (dtRequestDto == null) {
            return PageRequest.of(0, 10);
        }
        int limit = dtRequestDto.getLength() > 10 ? (int) dtRequestDto.getLength() : 10;
        int pageIndex = dtRequestDto.getStart() / limit > 0 ? (int) dtRequestDto.getStart() / limit : 0;
        if (dtRequestDto.getOrder() == null || dtRequestDto.getColumns() == null
                || dtRequestDto.getColumns().isEmpty()) {
            return PageRequest.of(pageIndex, limit);
        }
        Sort sort = null;
        if (!dtRequestDto.getOrder().isEmpty()) {
            for (DTOrder dtOrder : dtRequestDto.getOrder()) {
                if (dtOrder.getColumn() > 0 || dtOrder.getColumn() <= dtRequestDto.getColumns().size()) {
                    DTColumn dtColumn = dtRequestDto.getColumns().get(dtOrder.getColumn());
                    if (dtColumn.isOrderable()) {
                        Sort.Direction direction = dtOrder.getDir().equalsIgnoreCase("ASC") ? Sort.Direction.ASC
                                : Sort.Direction.DESC;
                        if (sort == null) {
                            sort = by(direction, dtColumn.getData());
                            continue;
                        }
                        sort.and(by(direction, dtColumn.getData()));
                    }
                }
            }
            return PageRequest.of(pageIndex, limit, sort);
        } else {
            return PageRequest.of(pageIndex, limit);
        }

    }

    protected Pageable getPageableForMessageLog(DTRequestDto dtRequestDto) {
        if (dtRequestDto == null) {
            return PageRequest.of(0, 10);
        }
        int limit = dtRequestDto.getLength() > 10 ? (int) dtRequestDto.getLength() : 10;
        int pageIndex = dtRequestDto.getStart() / limit > 0 ? (int) dtRequestDto.getStart() / limit : 0;
        if (dtRequestDto.getOrder() == null || dtRequestDto.getColumns() == null
                || dtRequestDto.getColumns().isEmpty()) {
            return PageRequest.of(pageIndex, limit);
        }
        Sort sort = null;
        if (!dtRequestDto.getOrder().isEmpty()) {
            for (DTOrder dtOrder : dtRequestDto.getOrder()) {
                if (dtOrder.getColumn() > 0 || dtOrder.getColumn() <= dtRequestDto.getColumns().size()) {
                    DTColumn dtColumn = dtRequestDto.getColumns().get(dtOrder.getColumn());
                    if(dtOrder.getColumn() == 5){
                        dtColumn.setData("received");
                    }
                    if (dtColumn.isOrderable()) {
                        Sort.Direction direction = dtOrder.getDir().equalsIgnoreCase("ASC") ? Sort.Direction.ASC
                                : Sort.Direction.DESC;
                        if (sort == null) {
                            sort = by(direction, dtColumn.getData());
                            continue;
                        }
                        sort.and(by(direction, dtColumn.getData()));
                    }
                }
            }
            return PageRequest.of(pageIndex, limit, sort);
        } else {
            return PageRequest.of(pageIndex, limit);
        }

    }


    protected Pageable getPageable(Map<String, Object> filter) {
        int limit = (int) filter.get("limit");
        int page = (int) filter.get("page");
        String orderBy = filter.get("orderBy").toString();
        String direction = filter.get("direction").toString();
        Sort sort = null;
        if (direction.equalsIgnoreCase("ASC")) {
            sort = by(Sort.Direction.ASC, orderBy);
        } else {
            sort = by(Sort.Direction.DESC, orderBy);
        }
        return PageRequest.of(page, limit, sort);
    }

    protected Map<String, FilterSpec> getFilter(DTRequestDto dtRequestDto) {
        if (dtRequestDto == null) {
            return null;
        }
        Map<String, FilterSpec> filterMap = new HashMap<>();
        for (DTColumn dtColumn : dtRequestDto.getColumns()) {
            if (dtColumn.getData().isEmpty() || dtColumn.getData().equals("no")) {
                continue;
            }
            FilterSpec filterSpec = new FilterSpec();
            filterSpec.setOperator(QueryOperator.LIKE_BOTH_SIDE);
            if (!dtColumn.getSearch().getValue().isEmpty()) {
                filterSpec.setValues(new String[]{dtColumn.getSearch().getValue()});
                filterMap.put(dtColumn.getData(), filterSpec);
            } else if (!dtRequestDto.getSearch().getValue().isEmpty()) {
                filterSpec.setValues(new String[]{dtRequestDto.getSearch().getValue()});
                filterMap.put(dtColumn.getData(), filterSpec);
            }
        }
        return filterMap;
    }

    protected Map<String, FilterSpec> getFilterAnd(DTRequestDto dtRequestDto) {
        if (dtRequestDto == null) {
            return null;
        }
        Map<String, FilterSpec> filterMap = new HashMap<>();
        for (DTColumn dtColumn : dtRequestDto.getColumns()) {
            if (dtColumn.getData().isEmpty() || dtColumn.getData().equals("no")) {
                continue;
            }
            FilterSpec filterSpec = new FilterSpec();
            filterSpec.setOperator(QueryOperator.AND);
            if (!dtColumn.getSearch().getValue().isEmpty()) {
                filterSpec.setValues(new String[]{dtColumn.getSearch().getValue()});
                filterMap.put(dtColumn.getData(), filterSpec);
            } else if (!dtRequestDto.getSearch().getValue().isEmpty()) {
                filterSpec.setValues(new String[]{dtRequestDto.getSearch().getValue()});
                filterMap.put(dtColumn.getData(), filterSpec);
            }
        }
        return filterMap;
    }

    /**
     * Convert String ke Date untuk digunakan di Specification.
     *
     * @param dateStr
     * @param formatDate
     * @return Date[]
     * @throws ParseException
     */
    public static Date[] getStartDateAndEndDate(String dateStr, String formatDate) throws ParseException {
        Date startDate = new SimpleDateFormat(formatDate).parse(dateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.SECOND, 86399);
        return new Date[]{startDate, calendar.getTime()};
    }

    /************** Workflow *****************/
    protected Map<String, DocumentStatusDto> getDocumentStatusMap() {
//		ResponseEntity<List<DocumentStatusDto>> response = docStatusFeign.getAllDocumentStatus(new DocumentStatusDto(),
//				null);
//		List<DocumentStatusDto> listDocStatus = response.getBody();
//		logger.info("listDocStatus : {}", listDocStatus);
        Map<String, DocumentStatusDto> mapDocStatus = new HashMap<>();
//		for (DocumentStatusDto documentStatusDto : listDocStatus) {
//			mapDocStatus.put(documentStatusDto.getDocStatus().toString(), documentStatusDto);
//		}
        return mapDocStatus;
    }

    /**
     * BigDecimal validate fraction must 6.
     *
     * @param bigDecimalInput
     * @return boolean
     */
    protected boolean doValidateBigDecimalFraction(BigDecimal bigDecimalInput) {
        logger.trace("mulai validasi fraction = 6 dari bigdecimal: " + bigDecimalInput);
        Boolean bigDecimalFractionBoolean = false;
        Integer bigDecimalScale = bigDecimalInput.scale();
        if (bigDecimalScale <= 6) {
            bigDecimalFractionBoolean = true;
        }
        logger.trace(
                "selesai validasi bigdecimal: " + bigDecimalInput + " , dengan hasil: " + bigDecimalFractionBoolean);
        return bigDecimalFractionBoolean;
    }

    /**
     * Validasi apakah string adalah integer.
     *
     * @param s
     * @return
     */
    protected boolean isStringInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * Parse date from front end.
     *
     * @param dateIn
     * @param formatFront
     * @param formatBack
     * @return date
     * @throws ParseException
     */
    protected Date dateFrontToBack(Date dateIn, String formatFront, String formatBack) throws ParseException {
        SystemParam systemParameterEntity = systemParameterRepo.findByName(formatBack);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatFront);
        String formattedDate1 = simpleDateFormat1.format(dateIn);
        Date date1 = simpleDateFormat1.parse(formattedDate1);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(systemParameterEntity.getValue());
        String formattedDate2 = simpleDateFormat2.format(date1);
        return simpleDateFormat2.parse(formattedDate2);
    }

    /**
     * Lookup by group code.
     *
     * @param groupCode
     * @return mapOfLookup
     */
    public Map<String, String> lookupByGroupCode(String groupCode) {
        Map<String, String> mapOfLookup = new HashMap<>();
        List<LookupEntity> lookups = lookupRepo.findByGroupCode(groupCode);
        for (LookupEntity lookupEntity : lookups) {
            mapOfLookup.put(lookupEntity.getKeyOnly(), lookupEntity.getLabel());
        }
        return mapOfLookup;
    }

    /**
     * LookupGroup by List group code.
     *
     * @param listGroupCode
     * @return lookupGroupMap
     */
    protected Map<String, String> lookUpGroupByGroupCode(String[] listGroupCode) {
        Map<String, String> lookupGroupMap = new HashMap<>();
        for (String groupCode : listGroupCode) {
            LookupGroupEntity lookupGroupEntity = lookupGroupRepo.findByGroupCodeActiveOnly(groupCode);
            if (lookupGroupEntity != null) {
                for (LookupEntity lookupEntity : lookupGroupEntity.getLookupList()) {
                    lookupGroupMap.put(lookupEntity.getLookupKey(), lookupEntity.getLabel());
                }
            }
        }
        return lookupGroupMap;
    }

    /**
     * Method for helping date validation process.
     *
     * @param expired
     * @param effective
     * @return boolean
     */
    protected boolean isDateValid(Date expired, Date effective) {
        return expired.compareTo(effective) > 0;
    }

    protected Date stringToDate(String dateIn, String formatDb, String formatResult) throws ParseException {
        SimpleDateFormat formater = new SimpleDateFormat(formatDb);
        Date date = formater.parse(dateIn);
        if (formatResult != null) {
            formater = new SimpleDateFormat(formatResult);
            String tmp = formater.format(date);
            date = formater.parse(tmp);
        }
        return date;
    }

    /**
     * Convert <code>Date</code> type entity to <code>String</code> based on <b><code>GUIConstant.FORMAT_DATE</code></b> format.
     *
     * @param builder - CriteriaBuilder from each toPredicate function.
     * @param root    - Root<?> from each toPredicate function.
     * @param key     - Column to be searched.
     * @return Expression<String>
     */
    public static Expression<String> convertDateToStr(CriteriaBuilder builder, Root<?> root, String key) {
        return builder.function("TO_CHAR", String.class, root.get(key), builder.literal(GUIConstant.FORMAT_DATE));
    }

    protected Pageable getPageableWithDateTimeOrder(DTRequestDto dtRequestDto) {
        if (dtRequestDto == null) {
            return PageRequest.of(0, 10);
        }
        int limit = dtRequestDto.getLength() > 10 ? (int) dtRequestDto.getLength() : 10;
        int pageIndex = dtRequestDto.getStart() / limit > 0 ? (int) dtRequestDto.getStart() / limit : 0;
        if (dtRequestDto.getOrder() == null || dtRequestDto.getColumns() == null
                || dtRequestDto.getColumns().isEmpty()) {
            return PageRequest.of(pageIndex, limit);
        }
        Sort sort = null;
        if (!dtRequestDto.getOrder().isEmpty()) {
            for (DTOrder dtOrder : dtRequestDto.getOrder()) {
                if (dtOrder.getColumn() > 0 || dtOrder.getColumn() <= dtRequestDto.getColumns().size()) {
                    DTColumn dtColumn = dtRequestDto.getColumns().get(dtOrder.getColumn());
                    if (dtColumn.isOrderable()) {
                        Sort.Direction direction = dtOrder.getDir().equalsIgnoreCase("ASC") ? Sort.Direction.ASC
                                : Sort.Direction.DESC;
                        if (sort == null) {
                            sort = by(direction, dtColumn.getData());
                            continue;
                        }
                        sort.and(by(direction, dtColumn.getData()));
                    }
                }
            }
        } else {
            sort = by(Sort.Direction.DESC, "dateTime");
        }
        return PageRequest.of(pageIndex, limit, sort);

    }

    protected Pageable getPageableWithTransactionIdOrder(DTRequestDto dtRequestDto) {
        if (dtRequestDto == null) {
            return PageRequest.of(0, 10);
        }
        int limit = dtRequestDto.getLength() > 10 ? (int) dtRequestDto.getLength() : 10;
        int pageIndex = dtRequestDto.getStart() / limit > 0 ? (int) dtRequestDto.getStart() / limit : 0;
        if (dtRequestDto.getOrder() == null || dtRequestDto.getColumns() == null
                || dtRequestDto.getColumns().isEmpty()) {
            return PageRequest.of(pageIndex, limit);
        }
        Sort sort = null;
        if (!dtRequestDto.getOrder().isEmpty()) {
            for (DTOrder dtOrder : dtRequestDto.getOrder()) {
                if (dtOrder.getColumn() > 0 || dtOrder.getColumn() <= dtRequestDto.getColumns().size()) {
                    DTColumn dtColumn = dtRequestDto.getColumns().get(dtOrder.getColumn());
                    if (dtColumn.isOrderable()) {
                        Sort.Direction direction = dtOrder.getDir().equalsIgnoreCase("ASC") ? Sort.Direction.ASC
                                : Sort.Direction.DESC;
                        if (sort == null) {
                            sort = by(direction, dtColumn.getData());
                            continue;
                        }
                        sort.and(by(direction, dtColumn.getData()));
                    }
                }
            }
        } else {
            sort = by(Sort.Direction.DESC, "transactionId");
        }
        return PageRequest.of(pageIndex, limit, sort);

    }

}