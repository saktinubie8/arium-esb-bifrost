package com.bifrost.core.gui.service;

import com.bifrost.common.constant.CacheName;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.ModuleStatusDto;
import com.bifrost.exception.gui.SystemErrorException;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service("moduleStatusService")
public class ModuleStatusService extends GUIGenericService {

    private IMap<String, String> statusMap;

    @Autowired
    private HazelcastInstance instance;

    @PostConstruct
    public void init() {
        statusMap = instance.getMap(CacheName.SYSTEM_MATRIX);
    }

    public DTResponseDto<ModuleStatusDto> getAllModuleStatus(DTRequestDto body) throws SystemErrorException {
        try {
            int length = 0;
            int startIndex = 0;
            DTResponseDto<ModuleStatusDto> dataTableResult = new DTResponseDto<>();
            int no = 0;
            if (body == null) {
                dataTableResult.setDraw(1);
            } else {
                dataTableResult.setDraw(body.getDraw());
                no = (int) body.getStart();
                length = (int) body.getLength();
                startIndex = (int) body.getStart();
            }
            List<ModuleStatusDto> moduleStatusDtoList = new ArrayList<>();
            for (IMap.Entry<String, String> entry : statusMap.entrySet()) {
                ModuleStatusDto moduleStatusDto = new ModuleStatusDto();
                moduleStatusDto.setModuleHost(entry.getKey());
                moduleStatusDto.setModuleCode(entry.getValue());
                moduleStatusDto.setNumber(++no);
                moduleStatusDtoList.add(moduleStatusDto);
            }
            List<ModuleStatusDto> moduleStatusDtoListAfterPaging = new ArrayList<>();
            Pageable pageable = getPageable(body);
            Page<ModuleStatusDto> moduleStatusPage = new PageImpl<>(moduleStatusDtoList, pageable,
                    pageable.getOffset() + pageable.getPageSize());
            for (ModuleStatusDto moduleStatusDto : moduleStatusPage.getContent()) {
                ModuleStatusDto moduleStatusDtoAfterPaging = new ModuleStatusDto();
                moduleStatusDtoAfterPaging.setModuleHost(moduleStatusDto.getModuleHost());
                moduleStatusDtoAfterPaging.setModuleCode(moduleStatusDto.getModuleCode());
                moduleStatusDtoAfterPaging.setNumber(moduleStatusDto.getNumber());
                moduleStatusDtoListAfterPaging.add(moduleStatusDtoAfterPaging);
            }
            dataTableResult.setRecordsFiltered(moduleStatusPage.getContent().size());
            if (moduleStatusDtoListAfterPaging.size() < length + startIndex) {
                dataTableResult.setData(moduleStatusDtoListAfterPaging.subList(startIndex, moduleStatusDtoListAfterPaging.size()));
            } else {
                dataTableResult.setData(moduleStatusDtoListAfterPaging.subList(startIndex, length + startIndex));
            }
            dataTableResult.setRecordsTotal(moduleStatusDtoList.size());
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

}