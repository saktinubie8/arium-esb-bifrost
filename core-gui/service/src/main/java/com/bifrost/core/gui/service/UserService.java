package com.bifrost.core.gui.service;

import com.bifrost.common.util.StringUtils;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.*;
import com.bifrost.core.gui.dao.model.MenuGroupEntity;
import com.bifrost.core.gui.dao.model.RoleEntity;
import com.bifrost.core.gui.dao.model.UserEntity;
import com.bifrost.core.gui.dao.repository.UserRepo;
import com.bifrost.core.gui.dao.specification.UserSpecification;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.system.repository.SystemParamRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Pattern;

@Service("userService")
public class UserService extends GUIGenericService implements UserDetailsService {

    Logger log = LogManager.getLogger(UserService.class);

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuGroupService menuGroupService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SystemParamRepository systemParameterRepo;

 /*   @Autowired
    private EmailSenderFeign emailSenderFeign;*/

    @Autowired
    private TokenSessionService tokenSessionService;

    String sec03 = "SEC03";
    String strPassHistory = "passwordHistory";
    String strFindUser = "Find User ";
    String strFromDatabase = " from database";

    @Cacheable(value = "UserService.findByUserId")
    private UserEntity getUserById(String userId) {
        return userRepo.findByUserId(UUID.fromString(userId));
    }

    @Override
    public UserDetails loadUserByUsername(final String username) {
        logger.debug(strFindUser + username + strFromDatabase);
        UserEntity user = userRepo.findByUsername(username.toLowerCase());
        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found.");
        }
        return user;
    }

    public UserDetails findUserByUsername(final String username) {
        logger.debug(strFindUser + username + strFromDatabase);
        return userRepo.findByUsername(username.toLowerCase());
    }

    @Transactional
    public ValidationResponseDto doResetPassword(UserDto user) {
        ValidationResponseDto resp = new ValidationResponseDto();
        // EmailSenderRequestDto email = new EmailSenderRequestDto();
        String headerRequest = "Basic d2ViLWNvcmU6c2lnbWExMjM=";
        String newPassword = "telkomsigma";
        UserEntity result = userRepo.findByUsername(user.getUsername());
        if (result != null && result.getEmail().equals(user.getEmail())) {
            result.setPassword(newPassword);
            result.setPassword(passwordEncoder.encode(result.getPassword()));
            result.setModifiedBy(user.getUsername());
            result.setModifiedDate(new Timestamp(System.currentTimeMillis()));
            userRepo.save(result);
         /*   email.setReceiver(result.getEmail());
            email.setSubject("Reset Password");
            email.setBodyMessage("New password for you is " + newPassword);
            this.emailSenderFeign.sendPublicEmail(email, headerRequest);*/
            return resp;
        } else {
            resp.addError("notFounded", "User ID or Email is not registered");
            return resp;
        }
    }

    public UserEntity loadUserByUserId(final String userId) {
        return userRepo.findByUserId(UUID.fromString(userId));
    }

    public UserEntity findByUsername(String username) {
        logger.debug(strFindUser + username + strFromDatabase);
        return userRepo.findByUsername(username.toLowerCase());
    }

    @SuppressWarnings("unused")
    public void lockUser(UserEntity user, final Long lockDuration) {
        UserEntity fromDb = userRepo.findByUserId(user.getUserId());
        user.setAccountNonLocked(false);
        fromDb.setAccountNonLocked(false);
        userRepo.save(fromDb);
    }

    public void unlockUser(UserEntity user) {
        UserEntity fromDb = userRepo.findByUserId(user.getUserId());
        fromDb.setAccountNonLocked(true);
        userRepo.save(fromDb);
    }

    @SuppressWarnings("unused")
    public UserEntity createUser(UserEntity user) {
        user.setCreatedDate(new Date());
        if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("")) {
            /*user.setPassword(new String(Base64.getDecoder().decode(user.getPassword())));
            logger.debug("Pasword Decode : "+user.getPassword());*/
            user.setPassword(passwordEncoder().encode(user.getPassword()));
            logger.debug("Pasword Encode : "+user.getPassword());
        }
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        user.setActive(true);
        // do search user by username / email address
        user = userRepo.save(user);
        return user;
    }

    public UserEntity updateUser(UserEntity result) {
        UserEntity fromDb = userRepo.findByUserId(result.getUserId());
        if (result.getPassword() != null && !result.getPassword().equalsIgnoreCase("") && !fromDb.getPassword().equals(result.getPassword())) {
            fromDb.setPassword(passwordEncoder.encode(result.getPassword()));
        }
        fromDb.setEmail(result.getEmail());
        fromDb.setName(result.getName());
        fromDb.setAddress(result.getAddress());
        fromDb.setPhoneNumber(result.getPhoneNumber());
        fromDb.setMobileNumber(result.getMobileNumber());
        fromDb.setDescription(result.getDescription());
        fromDb.setRoles(result.getRoles());
        fromDb.setMenuGroups(result.getMenuGroups());
        fromDb.setImageUrl(result.getImageUrl());
        fromDb.setExpiredDate(result.getExpiredDate());
        fromDb.setPasswordDate(result.getPasswordDate());
        fromDb.setRaw(result.getRaw());
        userRepo.save(fromDb);
        return fromDb;
    }

    public UserEntity updateProfile(UserEntity user) {
        UserEntity result = userRepo.findByUsername(user.getUsername());
        if (result != null) {
            if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("") && !result.getPassword().equals(user.getPassword())) {
                result.setPassword(passwordEncoder.encode(user.getPassword()));
            }
            result.setName(user.getName());
            result.setAddress(user.getAddress());
            result.setPhoneNumber(user.getPhoneNumber());
            result.setMobileNumber(user.getMobileNumber());
            result.setModifiedBy(result.getUserId().toString());
            result.setModifiedDate(new Timestamp(System.currentTimeMillis()));
            userRepo.save(result);
        }
        return result;
    }

    public List<UserEntity> getAllLockedUser() {
        return userRepo.findByAccountNonLocked(false);
    }

    public List<UserEntity> getAllUser() {
        return userRepo.findAll();
    }

    public Integer failedAttempt(UserEntity user) {
        return 0;
    }

    public UserEntity updateUserFromUpload(UserEntity user) {
        if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("")) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        user.setModifiedDate(new Timestamp(System.currentTimeMillis()));
        userRepo.save(user);
        return user;
    }

    public void resetPassword(UserEntity user) {
        UserEntity result = userRepo.findByUsername(user.getUsername());
        if (result != null) {
            result.setPassword(passwordEncoder.encode("000000"));
            result.setModifiedBy(user.getUserId().toString());
            result.setModifiedDate(new Timestamp(System.currentTimeMillis()));
            userRepo.save(result);
        }
    }

    public void enableUser(UserEntity user) {
        UserEntity fromDb = userRepo.findByUserId(user.getUserId());
        fromDb.setEnabled(true);
        userRepo.save(fromDb);
    }

    public void disableUser(UserEntity user) {
        UserEntity fromDb = userRepo.findByUserId(user.getUserId());
        fromDb.setEnabled(false);
        userRepo.save(fromDb);
    }

    public void deleteUser(UserEntity user) {
        try {
            userRepo.delete(user);
        } catch (Exception e) {
            logger.error("ERROR DELETING USER WITH USERNAME : " + user.getUsername() + ", CAUSE : " + e.getMessage());
        }
    }

    public void softDeleteUser(List<UserEntity> userEntities) throws SystemErrorException {
        if (userEntities != null) {
            userRepo.saveAll(userEntities);
        } else {
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public DTResponseDto<UserDto> getUsers(DTRequestDto body) throws SystemErrorException {
        try {
            Map<String, FilterSpec> filter = getFilter(body);
            Boolean stateButton = true;
            DTResponseDto<UserDto> dataTableResult = new DTResponseDto<>();
            Page<UserEntity> userEntities = userRepo.findAll(UserSpecification.getUsersSpec(filter),
                    getPageable(body));
            List<UserDto> users = new ArrayList<>();
            for (UserEntity _user : userEntities.getContent()) {
                logger.debug("User : " + _user.getUserId());
                logger.debug("User : " + _user.getMenuGroups());
                UserDto userDto = _user.parseDto();
                List<String> paramButtons = new ArrayList<>();
                paramButtons.add(stateButton.toString());
                userDto.setButton(paramButtons);
                users.add(userDto);
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(userEntities.getTotalElements());
            dataTableResult.setRecordsTotal(userRepo.countByIsActive(true));
            dataTableResult.setData(users);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    @Transactional
    public UserDto addUser(UserDto body, Principal principal) {
        if (body != null && body.getUserId() != null) {
            return setUser(body, principal);
        }
        UserEntity user = new UserEntity();
        UserEntity checkUserName = new UserEntity();

        checkUserName = findByUsername(body.getUsername());
        if (checkUserName != null && !checkUserName.getActive()) {
            user = checkUserName;
        }
        user.setUsername(body.getUsername());
        user.setName(body.getName());
        user.setEmail(body.getEmail());
        user.setPassword(body.getPassword());
        logger.debug("password : "+user.getPassword());
        user.setAddress(body.getAddress());
        user.setPhoneNumber(body.getPhoneNumber());
        user.setMobileNumber(body.getMobileNumber());
        user.setDescription(body.getDescription());
        user.setImageUrl(body.getImageUrl());
        user.setExpiredDate(body.getExpiredDate());
        user.setPasswordDate(body.getPasswordDate());
        Set<RoleEntity> roles = new HashSet<>();
        for (RoleDto roleDto : body.getRoles()) {
            RoleEntity role = new RoleEntity();
            role.setRoleId(UUID.fromString(roleDto.getRoleId()));
            roles.add(role);
        }
        user.setRoles(roles);
        Set<MenuGroupEntity> menuGroups = new HashSet<>();
        for (MenuGroupDto _menuGroupDto : body.getMenuGroups()) {
            MenuGroupEntity menuGroup = new MenuGroupEntity();
            menuGroup.setGroupId(UUID.fromString(_menuGroupDto.getGroupId()));
            menuGroups.add(menuGroup);
        }
        user.setMenuGroups(menuGroups);
        user.setCreatedBy(principal.getName());
        user = this.createUser(user);

        return user.parseDto();
    }

    public UserDto setUser(UserDto body, Principal principal) {
        UserEntity user = new UserEntity();
        if (body != null && body.getUserId() != null) {
            UserEntity checkUserId = loadUserByUserId(body.getUserId());
            if (checkUserId != null) {
                user.setUserId(UUID.fromString(body.getUserId()));
                user.setUsername(body.getUsername());
                user.setName(body.getName());
                user.setEmail(body.getEmail());
                if (!body.getPassword().equalsIgnoreCase("null")) {
                    user.setPassword(new String(Base64.getDecoder().decode(body.getPassword())));
                } else {
                    user.setPassword(checkUserId.getPassword());
                }
                user.setAddress(body.getAddress());
                user.setPhoneNumber(body.getPhoneNumber());
                user.setMobileNumber(body.getMobileNumber());
                user.setDescription(body.getDescription());
                user.setImageUrl(body.getImageUrl());
                user.setExpiredDate(body.getExpiredDate());
                user.setPasswordDate(body.getPasswordDate());
                Set<RoleEntity> roles = new HashSet<>();
                for (RoleDto roleDto : body.getRoles()) {
                    RoleEntity role = new RoleEntity();
                    role.setRoleId(UUID.fromString(roleDto.getRoleId()));
                    roles.add(role);
                }
                user.setRoles(roles);
                /***************/
                Set<MenuGroupEntity> menuGroups = new HashSet<>();
                for (MenuGroupDto _menuGroupDto : body.getMenuGroups()) {
                    MenuGroupEntity menuGroup = new MenuGroupEntity();
                    menuGroup.setGroupId(UUID.fromString(_menuGroupDto.getGroupId()));
                    menuGroups.add(menuGroup);
                }
                user.setMenuGroups(menuGroups);
                user.setModifiedBy(principal.getName());
                user.setModifiedDate(new Date());
                user = this.updateUser(user);
            }
        }
        return user.parseDto();
    }

    public UserDto[] deleteUser(UserDto[] body, Principal principal) throws SystemErrorException {
        List<UserEntity> userEntities = new ArrayList<>();
        for (UserDto _userDto : body) {
            UserEntity userIsExists = userRepo.findByUserId(UUID.fromString(_userDto.getUserId()));
            if (userIsExists != null) {
                userIsExists.setModifiedDate(new Date());
                userIsExists.setModifiedBy(principal.getName());
                userIsExists.setEnabled(false);
                userIsExists.setActive(false);
                userEntities.add(userIsExists);
            }
        }
        softDeleteUser(userEntities);
        return body;
    }

    public ResponseEntity<String> validateUsernameUnique(ValidationRequestDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        UserEntity userEntity = null;
        userEntity = userRepo.findByUsername(dto.getValueToValidate());
        if (userEntity != null && userEntity.getActive()) {
            resp.addError(ErrorCode.ERR_SCR0001.name());
        }
        return new ResponseEntity<>(jsonUtils.objToJson(resp), HttpStatus.OK);
    }

    public UserDto getUser(UserDto body) throws SystemErrorException {
        try {
            UserEntity userEntity = null;
            if (body.getUserId() != null) {
                userEntity = userRepo.findByUserId(UUID.fromString(body.getUserId()));
            } else if (body.getUsername() != null) {
                userEntity = userRepo.findByUsername(body.getUsername());
            }
            if (userEntity != null) {
                return userEntity.parseDto();
            } else {
                throw new SystemErrorException(ErrorCode.ERR_SYS0001);
            }
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public UserDto setUserProfile(UserDto body, Principal principal) {
        UserEntity user = new UserEntity();
        if (body != null && body.getUsername() != null) {
            UserEntity checkUserName = findByUsername(body.getUsername());
            if (checkUserName != null) {
                user = checkUserName;
                user.setUsername(body.getUsername());
                user.setName(body.getName());
                user.setEmail(body.getEmail());
                user.setAddress(body.getAddress());
                user.setPhoneNumber(body.getPhoneNumber());
                user.setMobileNumber(body.getMobileNumber());
                user.setModifiedBy(principal.getName());
                user.setModifiedDate(new Date());
                user = this.updateProfile(user);
            }
        }
        return user.parseDto();
    }

    @Transactional
    public UserDto changeUserPassword(UserDto body, Principal principal) {
        UserEntity user = new UserEntity();
        int maxHistory = -1;
      /*  if (this.systemParameterRepo.findByName(sec03) != null)
            maxHistory = Integer.parseInt(this.systemParameterRepo.findByCategory(sec03).getParameterValue());*/
        if (body != null && body.getUsername() != null) {
            UserEntity checkUsername = findByUsername(body.getUsername());
            if (checkUsername != null) {
                user = checkUsername;
                String oldPassword = checkUsername.getPassword();
                String newPassword = passwordEncoder
                        .encode(new String(Base64.getDecoder().decode(body.getPassword())));
                user.setPassword(newPassword);
                user.setModifiedBy(principal.getName());
                user.setModifiedDate(new Date());
                if (checkUsername.getRaw() == null)
                    checkUsername.setRaw(new HashMap());
                if (checkUsername.getRaw().get(strPassHistory) == null)
                    checkUsername.getRaw().put(strPassHistory, new ArrayList<String>());
                List<String> passwordHistoryList = (List<String>) checkUsername.getRaw().get(strPassHistory);
                if (!passwordHistoryList.isEmpty() && maxHistory > 0) {
                    while (passwordHistoryList.size() >= maxHistory - 1) {
                        passwordHistoryList.remove(0);
                    }
                }
                passwordHistoryList.add(oldPassword);
                checkUsername.getRaw().put(strPassHistory, passwordHistoryList);
                user = this.updateUser(user);
                ArrayList<String> revoke = new ArrayList<>();
                revoke.add(user.getUsername());
                tokenSessionService.revokeTokens(revoke);
            }
        }
        return user.parseDto();
    }

    public ResponseEntity<ValidationResponseDto> validateUserOldPassword(UserDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        logger.debug("Validating Lookup Group by group code from database");
        UserEntity userEntity = userRepo.findByUsername(dto.getUsername());
        dto.setPassword(new String(Base64.getDecoder().decode(dto.getPassword())));
        if (!passwordEncoder.matches(dto.getPassword(), userEntity.getPassword())) {
            resp.addError("unmatch");
        }
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    public ValidationResponseDto validatePasswordRule(String pass, Principal principal) {
        ValidationResponseDto resp = new ValidationResponseDto();
        UserEntity user = this.userRepo.findByUsername(principal.getName());
        if (user == null)
            resp.addError("invalidUser", "User not found");
        else if (pass == null || pass.equals("")) {
            resp.addError("emptyPassword", "Password must not empty");
        } else {
            int maxHistory = -1;
            int maxLength = -1;
            int minLength = -1;
            int minAlphabets = 0;
            int minLowerCase = 0;
            int minUpperCase = 0;
            int minSpecialChar = 0;
            int minNumeric = 0;
           /* try {
                if (this.systemParameterRepo.findByParameterCode("SEC01") != null)
                    maxLength = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode("SEC01").getParameterValue());
                if (this.systemParameterRepo.findByParameterCode("SEC02") != null)
                    minLength = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode("SEC02").getParameterValue());
                if (this.systemParameterRepo.findByParameterCode(sec03) != null)
                    maxHistory = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode(sec03).getParameterValue());
                if (this.systemParameterRepo.findByParameterCode("SEC04") != null)
                    minSpecialChar = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode("SEC04").getParameterValue());
                if (this.systemParameterRepo.findByParameterCode("SEC05") != null)
                    minAlphabets = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode("SEC05").getParameterValue());
                if (this.systemParameterRepo.findByParameterCode("SEC06") != null)
                    minLowerCase = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode("SEC06").getParameterValue());
                if (this.systemParameterRepo.findByParameterCode("SEC07") != null)
                    minUpperCase = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode("SEC07").getParameterValue());
                if (this.systemParameterRepo.findByParameterCode("SEC08") != null)
                    minNumeric = Integer
                            .parseInt(this.systemParameterRepo.findByParameterCode("SEC08").getParameterValue());
            } catch (Exception e) {
                logger.warn(e);
            }*/
            Pattern patern3 = Pattern.compile("(?=.*[@#$%^&+=])", Pattern.CASE_INSENSITIVE);
            int countAlphabet = 0;
            int countLowerCase = 0;
            int countUpperCase = 0;
            int countSpecialCharacter = 0;
            int countNumeric = 0;
            for (int i = 0; i < pass.length(); i++) {
                String temp = Character.toString(pass.charAt(i));
                if (StringUtils.isAlpha(temp))
                    countAlphabet++;
                if (StringUtils.isNumeric(temp))
                    countNumeric++;
                if (StringUtils.isAllLowerCase(temp))
                    countLowerCase++;
                if (StringUtils.isAllUpperCase(temp))
                    countUpperCase++;
                if (patern3.matcher(temp).find())
                    countSpecialCharacter++;
            }
            logger.trace("Param Config: [ maxHistory:" + maxHistory + " ] ");
            logger.trace("Param Config: [ maxLength:" + maxLength + " ] ");
            logger.trace("Param Config: [ minLength:" + minLength + " ] ");
            logger.trace("Param Config: [ minAlphabets:" + minAlphabets + " ] ");
            logger.trace("Param Config: [ minLowerCase:" + minLowerCase + " ] ");
            logger.trace("Param Config: [ minUpperCase:" + minUpperCase + " ] ");
            logger.trace("Param Config: [ minSpecialChar:" + minSpecialChar + " ] ");
            logger.trace("Param Config: [ minNumeric:" + minNumeric + " ] ");
            logger.trace("Check Rule: [ length:" + pass.length() + " ] ");
            logger.trace("Check Rule: [ countAlphabet:" + countAlphabet + " ] ");
            logger.trace("Check Rule: [ countNumeric:" + countNumeric + " ] ");
            logger.trace("Check Rule: [ countLowerCase:" + countLowerCase + " ] ");
            logger.trace("Check Rule: [ countUpperCase:" + countUpperCase + " ] ");
            logger.trace("Check Rule: [ countSpecialCharacter:" + countSpecialCharacter + " ] ");
            if (user.getRaw() != null && user.getRaw().get(strPassHistory) != null) {
                List<String> passwordHistoryList = (List<String>) user.getRaw().get(strPassHistory);
                if (passwordHistoryList != null) {
                    for (String passHistory : passwordHistoryList) {
                        if (passwordEncoder.matches(pass, passHistory)) {
                            resp.addError("matchPreviousPassword",
                                    "New password cannot be the same as previous password");
                            break;
                        }
                    }
                }
            }
            if (minLength > 0 && pass.length() < minLength) {
                resp.addError("minLength", "Minimum password length is " + minLength);
            }
            if (maxLength > 0 && pass.length() > maxLength) {
                resp.addError("maxLength", "Maximum password length is " + maxLength);
            }
            if (minAlphabets > 0 && countAlphabet < minAlphabets) {
                resp.addError("minAlphabets", "Minimum Alphabets character(s) is " + minAlphabets);
            }
            if (minLowerCase > 0 && countLowerCase < minLowerCase) {
                resp.addError("minLowerCase", "Minimum lower-case character(s) is " + minLowerCase);
            }
            if (minUpperCase > 0 && countUpperCase < minUpperCase) {
                resp.addError("minUpperCase", "Minimum upper-case character(s) is " + minUpperCase);
            }
            if (minSpecialChar > 0 && countSpecialCharacter < minSpecialChar) {
                resp.addError("minSpecialChar", "Minimum special character(s) is " + minSpecialChar);
            }
            if (minNumeric > 0 && countNumeric < minNumeric) {
                resp.addError("minNumeric", "Minimum numeric character(s) is " + minNumeric);
            }
        }
        logger.trace("ValidationDto: [ " + resp.toString() + " ]");
        return resp;
    }

    @Transactional
    public ValidationResponseDto doResetPasswordUser(UserDto user) {
        ValidationResponseDto resp = new ValidationResponseDto();
        /*  EmailSenderRequestDto email = new EmailSenderRequestDto();*/
        String headerRequest = "Basic d2ViLWNvcmU6c2lnbWExMjM=";
        String newPassword = randomAlphaNumeric(10);
        UserEntity result = userRepo.findByUserId(UUID.fromString(user.getUserId()));
        if (result != null && result.getEmail() != null) {
            result.setPassword(newPassword);
            result.setPassword(passwordEncoder.encode(result.getPassword()));
            result.setModifiedBy(user.getUsername());
            result.setModifiedDate(new Timestamp(System.currentTimeMillis()));
            userRepo.save(result);
          /*  email.setReceiver(result.getEmail());
            email.setSubject("Reset Password");
            email.setBodyMessage("New password for you is " + newPassword);

            this.emailSenderFeign.sendPublicEmail(email, headerRequest);
*/
            return resp;
        } else {
            resp.addError("notFounded", "Username or Email is not registerd");
            return resp;
        }
    }

    private static String randomAlphaNumeric(int count) {
        String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (new Random().nextInt() * alphaNumericString.length());
            builder.append(alphaNumericString.charAt(character));
        }
        return builder.toString();
    }

    public List<DualListBoxContainerDto> getRoleDualListBox(String id, String type) throws SystemErrorException {
        try {
            Map<String, DualListBoxContainerDto> resultMap = new HashMap<>();
            List<DualListBoxContainerDto> response = roleService.getRoleDualListBox(UUID.fromString(id));
            for (Iterator iterator = response.iterator(); iterator.hasNext(); ) {
                DualListBoxContainerDto dualListBoxContainerDto = (DualListBoxContainerDto) iterator.next();
                resultMap.put(dualListBoxContainerDto.getKey(), dualListBoxContainerDto);
            }
            // set selected
            if (id != null && !id.equalsIgnoreCase("")) {
                if (type.equalsIgnoreCase(UserEntity.class.getName())) {
                    UserEntity userEntity = getUserById(id);
                    for (Iterator iterator = userEntity.getRoles().iterator(); iterator.hasNext(); ) {
                        RoleEntity roleEntity = (RoleEntity) iterator.next();
                        DualListBoxContainerDto setSelected = resultMap.get(roleEntity.getRoleId().toString());
                        if (setSelected != null) {
                            setSelected.setSelected(true);
                            resultMap.put(roleEntity.getRoleId().toString(), setSelected);
                        }
                    }
                }
            } else {
                // do nothing
            }
            return (List<DualListBoxContainerDto>) resultMap.values();
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public List<DualListBoxContainerDto> getMenuGroupDualListBox(String id, String type) throws SystemErrorException {
        try {
            Map<String, DualListBoxContainerDto> resultMap = new HashMap<>();
            List<DualListBoxContainerDto> response = menuGroupService.getMenuGroupDualListBox(UUID.fromString(id));
            for (Iterator iterator = response.iterator(); iterator.hasNext(); ) {
                DualListBoxContainerDto dualListBoxContainerDto = (DualListBoxContainerDto) iterator.next();
                resultMap.put(dualListBoxContainerDto.getKey(), dualListBoxContainerDto);
            }
            // set selected
            if (id != null && !id.equalsIgnoreCase("") && type.equalsIgnoreCase(UserEntity.class.getName())) {
                UserEntity userEntity = getUserById(id);
                for (Iterator iterator = userEntity.getMenuGroups().iterator(); iterator.hasNext(); ) {
                    MenuGroupEntity menuGroupEntity = (MenuGroupEntity) iterator.next();
                    DualListBoxContainerDto setSelected = resultMap.get(menuGroupEntity.getGroupId().toString());
                    if (setSelected != null) {
                        setSelected.setSelected(true);
                        resultMap.put(menuGroupEntity.getGroupId().toString(), setSelected);
                    }
                }
            }
            return (List<DualListBoxContainerDto>) resultMap.values();
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

}