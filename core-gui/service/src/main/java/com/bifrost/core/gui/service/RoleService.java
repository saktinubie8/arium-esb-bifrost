package com.bifrost.core.gui.service;

import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.configuration.utils.JsonUtils;
import com.bifrost.core.gui.dao.dto.*;
import com.bifrost.core.gui.dao.model.RoleEntity;
import com.bifrost.core.gui.dao.model.UserEntity;
import com.bifrost.core.gui.dao.repository.RoleRepo;
import com.bifrost.core.gui.dao.specification.RoleSpecification;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.security.Principal;
import java.util.*;

@Service
public class RoleService extends GUIGenericService {

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private JsonUtils jsonUtil;

    private String username;

    String strLine = "================================================ ";

    @Cacheable(value = "RoleService.findByRoleId")
    private RoleEntity getRoleById(String roleId) {
        return roleRepo.findByRoleId(UUID.fromString(roleId));
    }

    @Cacheable(value = "RoleService.findByRoleId")
    public RoleEntity loadUserByRoleId(final String roleId) {
        return roleRepo.findByRoleId(UUID.fromString(roleId));
    }

    @Cacheable(value = "RoleService.findByAuthority")
    public List<RoleEntity> getRoleByRoleName(final String authority) {
        return roleRepo.findByAuthority(authority);
    }

    @Transactional
    @Caching(evict = {
            @CacheEvict(value = "RoleService.findByRoleId", key = "#role.roleId", condition = "#role != null", beforeInvocation = false),
            @CacheEvict(value = "RoleService.findByAuthority", key = "#role.authority", condition = "#role != null", beforeInvocation = false)})
    public RoleEntity updateRole(RoleEntity role) {
        RoleEntity roleExists = new RoleEntity();
        logger.warn(strLine);
        logger.warn(strLine);
        logger.warn(role.getAuthority());
        logger.warn(strLine);
        logger.warn(strLine);
        logger.warn(strLine);
        if (role.getRoleId() != null) {
            roleExists = getRoleById(role.getRoleId().toString());
            roleExists.setModifiedBy(this.username);
        } else {
            roleExists.setCreatedBy(this.username);
            roleExists.setCreatedDate(new Date());
        }
        roleExists.setAuthority(role.getAuthority());
        roleExists.setModifiedDate(new Date());
        roleExists.setDescription(role.getDescription());
        roleExists.setRoleCode(role.getRoleCode());
        roleExists = roleRepo.save(roleExists);
        return roleExists;
    }

    @Transactional
    public void deleteRole(RoleEntity role) throws SystemErrorException {
        RoleEntity roleIsExists = roleRepo.findByRoleId(role.getRoleId());
        if (roleIsExists == null)
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        roleRepo.delete(roleIsExists);
    }

    private void validateDelete(RoleEntity roleIsExists) throws SystemErrorException {
        logger.warn("masuk validate delete");
        if (roleIsExists != null && !roleIsExists.getUser().isEmpty()) {
            logger.warn("masuk exit");
            for (UserEntity _user : roleIsExists.getUser()) {
                logger.warn("masuk exist loop");
                if (_user != null && _user.getActive()) {
                    logger.warn("user is active throw error");
                    throw new SystemErrorException(ErrorCode.ERR_SYS0005);
                }
            }
            logger.warn("after sukses lewat validate ");
        }
        logger.warn("null");
    }

    @Transactional
    public void softDeleteRole(RoleEntity role) throws SystemErrorException {
        RoleEntity roleIsExists = roleRepo.findByRoleId(role.getRoleId());
        logger.warn("after search by id");
        validateDelete(roleIsExists);
        if (roleIsExists == null)
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        logger.warn("suskses no active");
        roleIsExists.setActive(false);
        roleIsExists.setModifiedBy(username);
        roleIsExists.setModifiedDate(new Date());
        roleRepo.save(roleIsExists);
    }

    private void validate(RoleDto body) throws SystemErrorException {
        if (body == null || body.getAuthority() == null || body.getAuthority().length() <= 0
                || body.getRoleCode() == null || body.getRoleCode().length() <= 0) {
            throw new SystemErrorException(ErrorCode.ERR_SYS0005);
        }
    }

    @Transactional
    public RoleDto saveRole(RoleDto body) throws SystemErrorException {
        validate(body);
        RoleEntity role = new RoleEntity(body);
        role = updateRole(role);
        return role.parseDto();
    }

    @Transactional
    public boolean deleteRole(RoleDto[] body, Principal principal) throws SystemErrorException {
        if (body != null) {
            for (int i = 0; i < body.length; i++) {
                RoleEntity role = new RoleEntity(body[i]);
                this.username = principal.getName();
                this.softDeleteRole(role);
            }
            return true;
        }
        return false;
    }

    public DTResponseDto<RoleDto> getRoles(DTRequestDto body) throws SystemErrorException {
        try {
            Page<RoleEntity> roleEntities = null;
            Map<String, FilterSpec> filter = getFilter(body);
            DTResponseDto<RoleDto> dataTableResult = new DTResponseDto<>();
            roleEntities = roleRepo.findAll(RoleSpecification.getRolesSpec(filter), getPageable(body));
            List<RoleDto> roles = new ArrayList<>();
            int no = 0;
            if (body == null) {
                dataTableResult.setDraw(1);
            } else {
                dataTableResult.setDraw(body.getDraw());
                no = (int) body.getStart();
            }
            for (RoleEntity _role : roleEntities.getContent()) {
                RoleDto roleDto = _role.parseDto();
                roleDto.setNumber(++no);
                roles.add(roleDto);
            }
            dataTableResult.setRecordsFiltered(roleEntities.getTotalElements());
            dataTableResult.setRecordsTotal(roleRepo.countByIsActive(true));
            dataTableResult.setData(roles);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public Select2ResponseDto getSelect2Role(Select2RequestDto body) throws SystemErrorException {
        try {
            Page<RoleEntity> roleEntities = roleRepo.findAll(RoleSpecification.getSelect2RoleSpec(body.getSearch()),
                    getPageable(body, "authority"));
            Select2ResponseDto response = new Select2ResponseDto();
            roleEntities.getContent().forEach(role ->
                    response.getResults().add(new Select2Dto(role.getRoleId().toString(), role.getAuthority()))
            );
            if (body.getPage() < 0)
                response.setPagination(new Select2PaginationDto(10 < roleEntities.getTotalElements()));
            else
                response.setPagination(
                        new Select2PaginationDto((body.getPage() * 10) < roleEntities.getTotalElements()));
            response.setRecordsFiltered(roleEntities.getTotalElements());
            response.setRecordsTotal(roleRepo.countByIsActive(true));
            return response;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public ResponseEntity<ValidationResponseDto> isRoleDuplicate(ValidationRequestDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        RoleEntity roleEntity = roleRepo.findByRoleNameActiveOnly(dto.getValueToValidate());
        if (roleEntity != null) {
            resp.addError("DUPL");
        }
        logger.warn(new ResponseEntity<ValidationResponseDto>(resp, HttpStatus.OK));
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    /**
     * @param body
     * @return
     * @throws SystemErrorException
     */
    public RoleDto getRoleByRoleId(RoleDto body) throws SystemErrorException {
        try {
            RoleEntity roleEntity = null;
            if (body.getRoleId() != null) {
                roleEntity = roleRepo.findByRoleId(UUID.fromString(body.getRoleId()));
            }
            if (roleEntity != null) {
                return roleEntity.parseDto();
            } else {
                throw new SystemErrorException(ErrorCode.ERR_SYS0001);
            }
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public List<DualListBoxContainerDto> getRoleDualListBox(UUID ids) throws SystemErrorException {
        try {
            List<RoleEntity> listOfRoles;
            if (ids != null) {
                listOfRoles = roleRepo.findRoleByRoleId(ids);
            } else {
                listOfRoles = roleRepo.findAllIsActive();
            }
            listOfRoles.sort(Comparator.comparing(RoleEntity::getRoleId));
            List<DualListBoxContainerDto> dualListboxList = new ArrayList<>();
            for (@SuppressWarnings("rawtypes")
                 Iterator iterator = listOfRoles.iterator(); iterator.hasNext(); ) {
                RoleEntity roleEntity = (RoleEntity) iterator.next();
                if (roleEntity.getActive()) {
                    DualListBoxContainerDto dualListBoxContainerDto = new DualListBoxContainerDto();
                    dualListBoxContainerDto.setKey(roleEntity.getRoleId().toString());
                    dualListBoxContainerDto.setValue(roleEntity.getRoleId() + " - " + roleEntity.getAuthority());
                    dualListboxList.add(dualListBoxContainerDto);
                }
            }
            return dualListboxList;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        } catch (Exception e) {
            logger.error(e);
            throw new SystemErrorException(ErrorCode.ERR_SYS0500);
        }
    }

    public ResponseEntity<String> validateRoleCodeUnique(ValidationRequestDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        if (roleRepo.validateCode(dto.getValueToValidate()) > 0) {
            resp.addError(ErrorCode.ERR_SCR0002.name());
        }
        return new ResponseEntity<>(jsonUtil.objToJson(resp), HttpStatus.OK);
    }

    public ResponseEntity<String> validateRoleNameUnique(ValidationRequestDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        if (roleRepo.validateName(dto.getValueToValidate()) > 0) {
            resp.addError(ErrorCode.ERR_SCR0002.name());
        }
        return new ResponseEntity<>(jsonUtil.objToJson(resp), HttpStatus.OK);
    }

}