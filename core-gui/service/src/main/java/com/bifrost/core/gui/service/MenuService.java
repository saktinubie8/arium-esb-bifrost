package com.bifrost.core.gui.service;

import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.configuration.utils.JsonUtils;
import com.bifrost.core.gui.dao.dto.*;
import com.bifrost.core.gui.dao.model.MenuEntity;
import com.bifrost.core.gui.dao.model.MenuGroupEntity;
import com.bifrost.core.gui.dao.repository.MenuRepo;
import com.bifrost.core.gui.dao.specification.MenuSpecification;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.core.gui.dao.util.MenuUtil;
import com.bifrost.core.gui.dao.util.TreeEditorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.security.Principal;
import java.util.*;

@Service("menuService")
public class MenuService extends GUIGenericService {

    @Autowired
    private MenuRepo menuRepo;

    @Autowired
    private JsonUtils jsonUtil;

    private String username;

    @Cacheable(value = "menuService.findByMenuId")
    public MenuEntity getMenuById(String menuId) {
        return menuRepo.findByMenuId(UUID.fromString(menuId));
    }

    public MenuDto getMenuByIdActive(String menuId) {
        MenuEntity menu = menuRepo.getMenuByMenuIdActive(UUID.fromString(menuId));
        return menu.parseDto(false);
    }

    public List<MenuEntity> getMenus() {
        return menuRepo.findAll();
    }

    public List<MenuEntity> findAllActive() {
        return menuRepo.findAllActive();
    }

    public List<MenuEntity> findAllActiveMenusByUsername(String username) {
        return menuRepo.findAllActiveMenusByUsername(username);
    }

    @Transactional
    public MenuEntity createMenu(MenuEntity menu) {
        menu = menuRepo.save(menu);
        return menu;
    }

    @Transactional
    public MenuEntity updateMenu(MenuEntity menu) {
        MenuEntity menuFromDb = menuRepo.findByMenuId(menu.getMenuId());
        menuFromDb.setVersion(menu.getVersion());
        menuFromDb.setModifiedBy(menu.getModifiedBy());
        menuRepo.save(menuFromDb);
        return menu;
    }

    /**
     * Delete menu
     *
     * @throws SystemErrorException
     */
    @Transactional
    public boolean deleteMenu(MenuDto body, Principal principal) throws SystemErrorException {
        if (body != null) {
            MenuEntity menu = new MenuEntity(body);
            this.username = principal.getName();
            this.softDeleteMenu(menu);
            return true;
        }
        return false;
    }

    /**
     * Delete menu
     *
     * @return boolean
     * @throws SystemErrorException
     */
    @Transactional
    public boolean deleteBatchMenu(MenuDto[] body, Principal principal) throws SystemErrorException {
        if (body != null) {
            for (int i = 0; i < body.length; i++) {
                MenuEntity menu = new MenuEntity(body[i]);
                this.username = principal.getName();
                this.softDeleteMenu(menu);
            }
            return true;
        }
        return false;
    }

    /**
     * Soft Delete menu
     *
     * @throws SystemErrorException
     */
    @Transactional
    public void softDeleteMenu(MenuEntity menu) throws SystemErrorException {
        MenuEntity menuExists = menuRepo.findByMenuId(menu.getMenuId());
        if (menuExists == null) {
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
        if (!menuExists.getMenuGroups().isEmpty()) {
            for (MenuGroupEntity menuGroupEntity : menuExists.getMenuGroups()) {
                if (menuGroupEntity.getActive()) {
                    throw new SystemErrorException(ErrorCode.ERR_SYS0007);
                }
            }
        }
        menuExists.setActive(false);
        menuExists.setModifiedBy(username);
        menuExists.setModifiedDate(new Date());
        menuRepo.save(menuExists);
    }

    /**
     * Save and update menu
     *
     * @return MenuEntity
     * @author alpinthor
     */
    @Transactional
    public MenuEntity saveAndFlushMenu(MenuEntity menu) {
        menu = menuRepo.saveAndFlush(menu);
        return menu;
    }

    /**
     * Save and update menu
     *
     * @return MenuEntity
     * @throws SystemErrorException
     * @throws Exception
     * @author alpinthor
     */
    @Transactional
    public MenuDto saveMenu(MenuDto body, Principal principal) throws SystemErrorException {
        this.username = principal.getName();
        MenuEntity menuEntity = new MenuEntity();
        if (body.getMenuId() != null) {
            menuEntity = new MenuEntity(body);
        } else {
            menuEntity.setParentId((body.getParentId() == null) ? null : UUID.fromString(body.getParentId()));
            menuEntity.setTitle(body.getTitle());
            menuEntity.setPageUrl(body.getPageUrl());
            menuEntity.setLevel(body.getLevel());
            menuEntity.setOrder(body.getOrder());
            menuEntity.setModule(body.getModule());
            menuEntity.setIcon(body.getIcon());
            menuEntity.setDescription(body.getDescription());
        }
        saveToDB(menuEntity);
        return menuEntity.parseDto(false);
    }

    @Transactional
    @Caching(evict = {
            @CacheEvict(value = "MenuService.findByMenuId", key = "#menu.menuId", condition = "#menu != null", beforeInvocation = false)})
    public MenuEntity saveToDB(MenuEntity menu) throws SystemErrorException {
        if (menu.getMenuId() != null) {
            MenuEntity menuExists = getMenuById(menu.getMenuId().toString());
            if (menuExists == null)
                throw new SystemErrorException(ErrorCode.ERR_SYS0001);
            menu.setModifiedBy(this.username);
            menu.setModifiedDate(new Date());
            menu.setVersion(menuExists.getVersion() + 1);
            menu.setMenuGroups(menuExists.getMenuGroups());
        } else {
            menu.setCreatedBy(this.username);
            menu.setCreatedDate(new Date());
            menu.setVersion(0);
        }
        menu = menuRepo.save(menu);
        return menu;
    }

    /**
     * Render main menu tree for main page
     *
     * @param username
     * @return MainMenuTreeContainer
     * @author alpinthor
     */
    public List<MainMenuTreeContainerDto> renderMainMenuTree(String username) {
        try {
            List<MenuEntity> menusFromDb = menuRepo.findAllActiveMenusByUsername(username);
            List<MainMenuTreeContainerDto> mainMenuTreeContainer = new ArrayList<>();
            /*
             * convert to container
             */
            for (MenuEntity menu : menusFromDb) {
                mainMenuTreeContainer.add(menu.parseMainMenuTreeContainer());
            }
            /*
             * sort container by level
             */
            Comparator<MainMenuTreeContainerDto> compMainMenuTreeByLevelDesc = new Comparator<MainMenuTreeContainerDto>() {
                @Override
                public int compare(MainMenuTreeContainerDto o1, MainMenuTreeContainerDto o2) {
                    if (o1.getOrder() > o2.getOrder()) {
                        return 1;
                    } else if (o1.getOrder() < o2.getOrder()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            };
            Collections.sort(mainMenuTreeContainer, compMainMenuTreeByLevelDesc);
            return MenuUtil.buildMainMenuTreeContainerForJson(mainMenuTreeContainer);
        } catch (Exception e) {
            logger.error(e);
            return Collections.emptyList();
        }
    }

    /**
     * Render menu tree as editor
     *
     * @return MainMenuTreeContainer
     * @author alpinthor
     */
    public List<TreeEditorContainerDto> renderMenuTreeAsEditor() {
        try {
            logger.debug("Find Menu Active");
            List<MenuEntity> menusFromDb = menuRepo.findAllActive();
            List<TreeEditorContainerDto> treeEditorContainer = new ArrayList<>();
            /*
             * convert to container
             */
            for (MenuEntity menu : menusFromDb) {
                logger.debug("Get Menu : "+menu.getTitle());
                treeEditorContainer.add(menu.parseTreeEditorContainer(true, true, menusFromDb));
            }
            return TreeEditorUtil.buildTreeEditorContainerForJson(treeEditorContainer);
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    /**
     * Get menus for datatables
     *
     * @return DTResponseDto
     * @throws SystemErrorException
     */
    public DTResponseDto<MenuDto> getMenus(DTRequestDto body) throws SystemErrorException {
        try {
            Page<MenuEntity> menuEntities = null;
            Map<String, FilterSpec> filter = getFilter(body);
            DTResponseDto<MenuDto> dataTableResult = new DTResponseDto<>();
            menuEntities = menuRepo.findAll(MenuSpecification.getMenusSpec(filter), getPageable(body));
            List<MenuDto> menus = new ArrayList<>();
            for (MenuEntity _menu : menuEntities.getContent()) {
                MenuEntity menuParent = menuRepo.findByMenuId(_menu.getParentId());
                if (menuParent != null) {
                    MenuDto menuDto = _menu.parseDto(false);
                    menuDto.setParentContent(menuParent.getTitle());
                    menus.add(menuDto);
                } else {
                    menus.add(_menu.parseDto(false));
                }
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(menuEntities.getTotalElements());
            dataTableResult.setRecordsTotal(menuRepo.countByIsActive(true));
            dataTableResult.setData(menus);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public MenuDto getMenuByIdActivev2(String menuId) {
        MenuEntity menu = menuRepo.getMenuByMenuIdActive(UUID.fromString(menuId));
        MenuDto menuDto = menu.parseDto(false);
        MenuEntity menup = menuRepo.getMenuByMenuIdActive(menu.getParentId());
        if (menup != null) {
            menuDto.setParentContent(menup.getTitle());
        }
        return menuDto;
    }

    /**
     * Get menus for select2
     *
     * @return Select2RequestDto
     * @throws SystemErrorException
     */
    public Select2ResponseDto getSelect2Menu(Select2RequestDto body) throws SystemErrorException {
        try {
            Page<MenuEntity> menuEntities = menuRepo.findAll(MenuSpecification.getSelect2MenuSpec(body.getSearch()),
                    getPageable(body, "title"));
            Select2ResponseDto response = new Select2ResponseDto();
            menuEntities.getContent().forEach(menu ->
                    response.getResults()
                            .add(new Select2Dto(menu.getMenuId().toString(), menu.getTitle() + ", Module = " + menu.getModule() + ", Level = " + menu.getLevel()))
            );
            if (body.getPage() < 0)
                response.setPagination(new Select2PaginationDto(10 < menuEntities.getTotalElements()));
            else
                response.setPagination(
                        new Select2PaginationDto((body.getPage() * 10) < menuEntities.getTotalElements()));
            response.setRecordsFiltered(menuEntities.getTotalElements());
            response.setRecordsTotal(menuRepo.countByIsActive(true));
            return response;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public ResponseEntity<String> validateTitleNameUnique(ValidationRequestDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        MenuEntity menu = null;
        menu = menuRepo.findByTitle(dto.getValueToValidate());
        if (menu != null && menu.getActive()) {
            resp.addError(ErrorCode.ERR_SCR0001.name());
        }
        return new ResponseEntity<>(jsonUtil.objToJson(resp), HttpStatus.OK);
    }

}