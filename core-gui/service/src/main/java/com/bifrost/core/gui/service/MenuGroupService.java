package com.bifrost.core.gui.service;

import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.configuration.utils.JsonUtils;
import com.bifrost.core.gui.dao.dto.*;
import com.bifrost.core.gui.dao.model.MenuEntity;
import com.bifrost.core.gui.dao.model.MenuGroupEntity;
import com.bifrost.core.gui.dao.model.UserEntity;
import com.bifrost.core.gui.dao.repository.MenuGroupRepo;
import com.bifrost.core.gui.dao.repository.MenuRepo;
import com.bifrost.core.gui.dao.repository.UserRepo;
import com.bifrost.core.gui.dao.specification.MenuGroupSpecification;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.core.gui.dao.util.MenuGroupUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.security.Principal;
import java.util.*;

@Service
public class MenuGroupService extends GUIGenericService {

    @Autowired
    private MenuGroupRepo menuGroupRepo;

    @Autowired
    private MenuRepo menuRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private JsonUtils jsonUtil;

    private String username;

    String strGroupCodeDuplicate = "GROUP CODE DUPLICATE";

    public List<MenuGroupEntity> getMenuGroups() {
        return menuGroupRepo.findAll();
    }

    @Transactional
    public MenuGroupEntity createMenuGroup(MenuGroupEntity menuGroup) {
        menuGroup = menuGroupRepo.save(menuGroup);
        return menuGroup;
    }

    public MenuGroupEntity loadUserByMenuGroupId(final String groupId) {
        return menuGroupRepo.findByGroupId(UUID.fromString(groupId));
    }

    @Transactional
    public MenuGroupEntity updateMenuGroup(MenuGroupEntity menuGroup) {
        menuGroup = menuGroupRepo.saveAndFlush(menuGroup);
        return menuGroup;
    }

    public MenuGroupEntity loadUserByGroupName(final String groupName) {
        return menuGroupRepo.findByGroupName(groupName);
    }

    @Cacheable(value = "MenuGroupService.findByGroupId")
    public MenuGroupEntity getMenuGroupById(String groupId) {
        return menuGroupRepo.findByGroupId(UUID.fromString(groupId));
    }

    public MenuGroupEntity findActiveByGroupId(String groupId) {
        return menuGroupRepo.findActiveByGroupId(UUID.fromString(groupId));
    }

    /**
     * Save Menu Group
     *
     * @return MenuGroupDto
     * @throws SystemErrorException
     * @author alpinthor
     */
    @Transactional
    public MenuGroupDto saveMenuGroup(MenuGroupDto body, Principal principal) throws SystemErrorException {
        this.username = principal.getName();
        MenuGroupEntity role = new MenuGroupEntity();
        Set<MenuEntity> menus = new HashSet<>();
        Set<UserEntity> users = new HashSet<>();
        MenuGroupDto menuGroupDto = new MenuGroupDto();

        if (body.getGroupId() != null) {
            role.setGroupId(UUID.fromString(body.getGroupId()));
            MenuGroupEntity menuGroupEntity = menuGroupRepo.findByGroupId(UUID.fromString(body.getGroupId()));
            if (menuGroupEntity.getGroupCode() != null) {
                if (!menuGroupEntity.getGroupCode().equals(body.getGroupCode())) {
                    MenuGroupEntity menuGroupEntityB = menuGroupRepo.findByGroupCode(body.getGroupCode());
                    if (menuGroupEntityB != null) {
                        menuGroupDto.setGroupCode(strGroupCodeDuplicate);
                        return menuGroupDto;
                    }
                }
            } else {
                MenuGroupEntity menuGroupEntityB = menuGroupRepo.findByGroupCode(body.getGroupCode());
                if (menuGroupEntityB != null) {
                    menuGroupDto.setGroupCode(strGroupCodeDuplicate);
                    return menuGroupDto;
                }
            }

        } else {
            MenuGroupEntity menuGroupEntity = menuGroupRepo.findByGroupCode(body.getGroupCode());
            if (menuGroupEntity != null) {
                menuGroupDto.setGroupCode(strGroupCodeDuplicate);
                return menuGroupDto;
            }
        }
        role.setGroupName(body.getGroupName());
        role.setGroupCode(body.getGroupCode());
        role.setDescription(body.getDescription());
        for (MenuDto menu : body.getMenus()) {
            menus.add(menuRepo.findByMenuId(UUID.fromString(menu.getMenuId())));
        }
        role.setMenus(menus);
        if (body.getUser() != null) {
            for (UserDto user : body.getUser()) {
                users.add(userRepo.findByUserId(UUID.fromString(user.getUserId())));
            }
            role.setUser(users);
        } else {
            role.setUser(null);
        }
        saveToDB(role);
        return role.parseDto();
    }

    /**
     * Save Menu Group
     *
     * @return MenuGroupDto
     * @throws SystemErrorException
     * @author alpinthor
     */
    @Transactional
    public MenuGroupDto jajalSaveMenuGroup(MenuGroupDto body, Principal principal) throws SystemErrorException {
        this.username = principal.getName();
        MenuGroupEntity role = new MenuGroupEntity(body);
        Set<MenuEntity> menus = new HashSet<>();
        MenuEntity menu = new MenuEntity();
        menu.setMenuId(UUID.fromString("90EB16B9-D184-4271-AA13-9C4387B61D8A"));
        menus.add(menu);
        role.setMenus(menus);
        logger.warn(jsonUtil.objToJson(role));
        saveToDB(role);
        return role.parseDto();
    }

    /**
     * Menu Group Save To DB
     *
     * @return MenuGroupEntity
     * @throws SystemErrorException
     * @author alpinthor
     */
    @Transactional
    @Caching(evict = {
            @CacheEvict(value = "MenuGroupService.findByGroupId", key = "#menuGroup.groupId", condition = "#MenuGroup != null", beforeInvocation = false)})
    public MenuGroupEntity saveToDB(MenuGroupEntity menuGroup) throws SystemErrorException {
        if (menuGroup.getGroupId() != null) {
            MenuGroupEntity menuGroupExists = menuGroupRepo.findByGroupId(menuGroup.getGroupId());
            if (menuGroupExists == null)
                throw new SystemErrorException(ErrorCode.ERR_SYS0001);
            menuGroupExists.setGroupName(menuGroup.getGroupName());
            menuGroupExists.setGroupCode(menuGroup.getGroupCode());
            menuGroupExists.setDescription(menuGroup.getDescription());
            menuGroupExists.setVersion(menuGroupExists.getVersion() + 1);
            menuGroupExists.setMenus(menuGroup.getMenus());
        } else {
            menuGroup = menuGroupRepo.save(menuGroup);
        }
        return menuGroup;
    }

    /**
     * Delete Menu Group
     *
     * @return boolean
     * @throws SystemErrorException
     * @author alpinthor
     */
    @Transactional
    public boolean deleteMenuGroup(MenuGroupDto body, Principal principal) throws SystemErrorException {
        if (body != null) {
            MenuGroupEntity role = new MenuGroupEntity(body);
            this.username = principal.getName();
            this.softDeleteMenuGroup(role);
            return true;
        }
        return false;
    }

    /**
     * Delete Menu Group Batch
     *
     * @return boolean
     * @throws SystemErrorException
     * @author alpinthor
     */
    @Transactional
    public boolean deleteMenuGroupBatch(MenuGroupDto[] body, Principal principal) throws SystemErrorException {
        if (body != null) {
            for (int i = 0; i < body.length; i++) {
                MenuGroupEntity role = new MenuGroupEntity();
                role.setGroupId(UUID.fromString(body[i].getGroupId()));
                this.username = principal.getName();
                logger.warn("\n\n string  : " + role.getGroupId());
                this.softDeleteMenuGroup(role);
            }
            return true;
        }
        return false;
    }

    /**
     * Soft Delete Menu Group / Update is active false
     *
     * @return void
     * @throws SystemErrorException
     * @author alpinthor
     */
    @Transactional
    public void softDeleteMenuGroup(MenuGroupEntity role) throws SystemErrorException {
        MenuGroupEntity menuGroupExist = menuGroupRepo.findByGroupId(role.getGroupId());
        if (menuGroupExist == null) {
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
        if (!menuGroupExist.getUser().isEmpty()) {
            for (UserEntity userEntity : menuGroupExist.getUser()) {
                if (userEntity.getActive()) {
                    throw new SystemErrorException(ErrorCode.ERR_SYS0007);
                }
            }
        }
        menuGroupExist.setActive(false);
        menuGroupExist.setModifiedBy(username);
        menuGroupExist.setModifiedDate(new Date());
        menuGroupRepo.save(menuGroupExist);
    }

    /**
     * Get Menu Groups For Data Table
     *
     * @return DTResponseDto<MenuGroupDto>
     * @throws SystemErrorException
     * @author alpinthor
     */
    public DTResponseDto<MenuGroupDto> getMenuGroups(DTRequestDto body) throws SystemErrorException {
        try {
            Page<MenuGroupEntity> menuGroupEntities = null;
            Map<String, FilterSpec> filter = getFilter(body);
            DTResponseDto<MenuGroupDto> dataTableResult = new DTResponseDto<>();
            menuGroupEntities = menuGroupRepo.findAll(MenuGroupSpecification.getMenuGroupSpec(filter),
                    getPageable(body));
            List<MenuGroupDto> menuGroups = new ArrayList<>();
            for (MenuGroupEntity _menuGroup : menuGroupEntities.getContent()) {
                menuGroups.add(_menuGroup.parseDto());
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(menuGroupEntities.getTotalElements());
            dataTableResult.setRecordsTotal(menuGroupRepo.countByIsActive(true));
            dataTableResult.setData(menuGroups);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public Select2ResponseDto getSelect2MenuGroup(Select2RequestDto body) throws SystemErrorException {
        try {
            Page<MenuGroupEntity> menuGroupEntities = menuGroupRepo.findAll(MenuGroupSpecification.getSelect2MenuGroupSpec(body.getSearch()), getPageable(body, "groupName"));
            Select2ResponseDto response = new Select2ResponseDto();
            menuGroupEntities.getContent().forEach(menuGroup ->
                    response.getResults().add(new Select2Dto(menuGroup.getGroupId().toString(), menuGroup.getGroupName()))
            );
            if (body.getPage() < 0)
                response.setPagination(new Select2PaginationDto(10 < menuGroupEntities.getTotalElements()));
            else
                response.setPagination(new Select2PaginationDto((body.getPage() * 10) < menuGroupEntities.getTotalElements()));
            response.setRecordsFiltered(menuGroupEntities.getTotalElements());
            response.setRecordsTotal(menuGroupRepo.countByIsActive(true));
            return response;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    /**
     * Render menu tree as editor
     *
     * @return TreeEditorContainerDto
     * @author alpinthor
     */
    public List<TreeEditorContainerDto> renderMenuGroupTreeAsEditor(MenuGroupDto menuGroupDto) {
        try {
            List<MenuGroupEntity> menuGroupsFromDb = menuGroupRepo.findAllActive();
            List<TreeEditorContainerDto> treeEditorContainer = new ArrayList<>();
            /*
             * convert to container
             */
            for (MenuGroupEntity menu : menuGroupsFromDb) {
                treeEditorContainer.add(menu.parseTreeEditorContainer());
            }
            return MenuGroupUtil
                    .buildCustomMenuGroupTreeEditorContainerForJson(treeEditorContainer, menuGroupDto);
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    /**
     * Render menu tree as viewer
     *
     * @return TreeEditorContainerDto
     * @author alpinthor
     */
    public List<TreeEditorContainerDto> renderMenuGroupTreeAsViewer(MenuGroupDto menuGroupDto) {
        try {
            MenuGroupEntity menuGroupFromDb = menuGroupRepo.findActiveByGroupId(UUID.fromString(menuGroupDto.getGroupId()));
            TreeEditorContainerDto treeEditorContainer = menuGroupFromDb.parseTreeEditorContainer();
            return MenuGroupUtil
                    .buildCustomMenuGroupTreeViewerContainerForJson(treeEditorContainer);
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public ResponseEntity<String> validateMenuGroupNameUnique(ValidationRequestDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        MenuGroupEntity menuGroup = null;
        menuGroup = menuGroupRepo.findByGroupName(dto.getValueToValidate());
        if (menuGroup != null && menuGroup.getActive()) {
            resp.addError(ErrorCode.ERR_SCR0001.name());
        }
        return new ResponseEntity<>(jsonUtil.objToJson(resp), HttpStatus.OK);
    }

    public DTResponseDto<MenuGroupDto> getMenuGroupWithId(MenuGroupDto menuGroupDto) throws SystemErrorException {
        try {
            DTResponseDto<MenuGroupDto> dataTableResult = new DTResponseDto<>();
            MenuGroupEntity menuGroupEntity = menuGroupRepo.findActiveByGroupId(UUID.fromString(menuGroupDto.getGroupId()));
            List<MenuGroupDto> menuGroups = new ArrayList<>();
            menuGroups.add(menuGroupEntity.parseDto());
            dataTableResult.setData(menuGroups);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public ValidationResponseDto isGroupCodeUnique(ValidationRequestDto validationRequestDto) {
        logger.trace("mulai method isGroupCodeUnique");
        ValidationResponseDto validationResponseDto = new ValidationResponseDto();
        MenuGroupEntity menuGroupEntity = menuGroupRepo.findByGroupCode(validationRequestDto.getValueToValidate());
        if (menuGroupEntity != null) {
            validationResponseDto.addError("DUPL");
        }
        logger.trace("selesai method isGroupCodeUnique");
        return validationResponseDto;
    }


    public List<DualListBoxContainerDto> getMenuGroupDualListBox(UUID ids) throws SystemErrorException {
        try {
            List<MenuGroupEntity> listOfMenuGroups;
            if (ids != null) {
                listOfMenuGroups = menuGroupRepo.findMenuGroupByGroupId(ids);
            } else {
                listOfMenuGroups = menuGroupRepo.findAllActive();
            }
            listOfMenuGroups.sort(Comparator.comparing(MenuGroupEntity::getGroupId));
            List<DualListBoxContainerDto> dualListboxList = new ArrayList<>();
            for (@SuppressWarnings("rawtypes")
                 Iterator iterator = listOfMenuGroups.iterator(); iterator.hasNext(); ) {
                MenuGroupEntity menuGroupEntity = (MenuGroupEntity) iterator.next();
                if (menuGroupEntity.getActive()) {
                    DualListBoxContainerDto dualListBoxContainerDto = new DualListBoxContainerDto();
                    dualListBoxContainerDto.setKey(menuGroupEntity.getGroupId().toString());
                    dualListBoxContainerDto.setValue(menuGroupEntity.getGroupId() + " - " + menuGroupEntity.getGroupName());
                    dualListboxList.add(dualListBoxContainerDto);
                }
            }
            return dualListboxList;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        } catch (Exception e) {
            logger.error(e);
            throw new SystemErrorException(ErrorCode.ERR_SYS0500);
        }
    }

}