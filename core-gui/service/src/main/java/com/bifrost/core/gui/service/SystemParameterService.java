package com.bifrost.core.gui.service;

import java.security.Principal;
import java.util.*;
import javax.persistence.NoResultException;

import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.core.gui.dao.specification.SystemParamSpecification;
import com.bifrost.system.model.SystemParam;
import com.bifrost.system.repository.SystemParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.system.dto.SystemParamDto;
import com.bifrost.core.gui.dao.dto.ValidationRequestDto;
import com.bifrost.core.gui.dao.dto.ValidationResponseDto;
import com.bifrost.exception.gui.SystemErrorException;

@Service("systemParameterService")
public class SystemParameterService extends GUIGenericService {

    @Autowired
    private SystemParamRepository sysParamRepo;

    public SystemParamDto addParameter(SystemParamDto body, Principal principal) {
        SystemParam sysParam = sysParamRepo.findByName(body.getName());
        if (sysParam == null) {
            sysParam = new SystemParam();
        }
        sysParam.setName(body.getName());
        sysParam.setCategory(body.getCategory());
        sysParam.setValue(body.getValue());
        sysParam.setDescription(body.getDescription());
        sysParam.setApplication(body.getApplication());
        sysParam.setLabel(body.getLabel());
        sysParam.setProfile(body.getProfile());
        sysParamRepo.save(sysParam);
        return body;
    }

    public DTResponseDto<SystemParamDto> getParameters(DTRequestDto body) throws SystemErrorException {
        try {
            Map<String, FilterSpec> filter = getFilter(body);
            DTResponseDto<SystemParamDto> dataTableResult = new DTResponseDto<>();
            Page<SystemParam> sysParamEntities = sysParamRepo.findAll(SystemParamSpecification.getSysParamSpec(filter), getPageableWithoutSort(body));
            List<SystemParamDto> sysParams = new ArrayList<>();
            int nomor = 0;
            if (body != null) {
                nomor = (Math.toIntExact(body.getStart())) + 1;
            }
            for (SystemParam _sysParam : sysParamEntities.getContent()) {
                SystemParamDto systemParameterDto = _sysParam.parseDto();
                systemParameterDto.setNumber(nomor);
                sysParams.add(systemParameterDto);
                nomor = nomor + 1;
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(sysParamEntities.getTotalElements());
            dataTableResult.setRecordsTotal(sysParamRepo.count());
            dataTableResult.setData(sysParams);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public SystemParamDto[] deleteParameter(SystemParamDto[] body) throws SystemErrorException {
        List<SystemParam> sysParamEntities = new ArrayList<>();
        for (SystemParamDto _sysParamDto : body) {
            SystemParam sysParamIsExists = sysParamRepo.findByName(_sysParamDto.getName());
            sysParamRepo.delete(sysParamIsExists);
        }
        return body;
    }

    public ResponseEntity<String> validateParameterCodeUnique(ValidationRequestDto dto) {
        ValidationResponseDto resp = new ValidationResponseDto();
        SystemParam systemParameterEntity = sysParamRepo.findByName(dto.getValueToValidate());
        if (systemParameterEntity != null) {
            resp.addError(ErrorCode.ERR_SCR0002.name());
        }
        return new ResponseEntity<>(
                jsonUtils.objToJson(resp),
                HttpStatus.OK);
    }

    public List<SystemParamDto> getAllByGroupCode(String groupCode) {
        List<SystemParamDto> systemParameters = new ArrayList<>();
        List<SystemParam> systemParameter = sysParamRepo.findByCategory(groupCode);
        for (SystemParam _sysParam : systemParameter) {
            systemParameters.add(_sysParam.parseDto());
        }
        return systemParameters;
    }

    public SystemParamDto getParameterById(String systemParameterId) {
        Optional<SystemParam> opti = this.sysParamRepo.findById(Long.parseLong(systemParameterId));
        if (opti.isPresent()) {
            SystemParamDto dto = opti.get().parseDto();
            return dto;
        }
        return null;
    }

}