package com.bifrost.core.gui.service;

import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.core.gui.dao.specification.RouteLogSpecification;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.logging.model.dto.RouteLogDto;
import com.bifrost.logging.repository.RouteLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("routeLogService")
public class RouteLogService extends GUIGenericService {

    @Autowired
    RouteLogRepository routeRepo;

    public DTResponseDto<RouteLogDto> getAllRouteLogs(DTRequestDto body) throws SystemErrorException {
        try {
            Map<String, FilterSpec> filter = getFilterAnd(body);
            Boolean stateButton = true;
            DTResponseDto<RouteLogDto> dataTableResult = new DTResponseDto<>();
            Page<RouteLog> endpointEntities = routeRepo.findAll(RouteLogSpecification.getRouteLogsSpec(filter),
                    getPageableWithoutSort(body));
            List<RouteLogDto> users = new ArrayList<>();
            for (RouteLog _data : endpointEntities.getContent()) {
                RouteLogDto endpointDto = _data.parseDto();
                List<String> paramButtons = new ArrayList<>();
                paramButtons.add(stateButton.toString());
                endpointDto.setButton(paramButtons);
                users.add(endpointDto);
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(endpointEntities.getTotalElements());
            dataTableResult.setRecordsTotal(routeRepo.count());
            dataTableResult.setData(users);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

}