package com.bifrost.core.gui.service;

import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.TokenDto;
import com.bifrost.core.gui.dao.dto.UserDto;
import com.bifrost.core.gui.dao.repository.UserRepo;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TokenSessionService extends GUIGenericService {

    private static final String SECURITY_ACCESS_TOKEN_LIST = "security.accessTokenList";
    private static final String SECURITY_REFRESH_TOKEN_LIST = "security.refreshTokenList";

    Logger log = LogManager.getLogger(TokenSessionService.class);

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private HazelcastInstance instance;

    @Autowired
    private UserRepo userRepo;

    public List<String> getAccessTokenByUsername(String username) {
        List<String> tokenList = new ArrayList<>();
        IMap<String, String> sessionMap = instance.getMap(SECURITY_ACCESS_TOKEN_LIST);
        for (Map.Entry<String, String> entry : sessionMap.entrySet()) {
            String token = entry.getKey();
            String tokenUsername = entry.getValue();
            if (tokenUsername.equals(username))
                tokenList.add(token);
        }
        return tokenList;
    }

    public void revokeAccessToken(String token) {
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
        tokenStore.removeAccessToken(accessToken);
    }

    public void revokeRefreshToken(String token) {
        OAuth2RefreshToken refreshToken = tokenStore.readRefreshToken(token);
        tokenStore.removeRefreshToken(refreshToken);
    }

    public void revokeAccessTokenByUsername(String username) {
        IMap<String, String> sessionMap = instance.getMap(SECURITY_ACCESS_TOKEN_LIST);
        for (Map.Entry<String, String> entry : sessionMap.entrySet()) {
            String token = entry.getKey();
            String tokenUsername = entry.getValue();
            if (tokenUsername.equals(username)) {
                revokeAccessToken(token);
            }
        }
    }

    public void logout(String username, TokenDto body) {
        if (instance.getMap(SECURITY_ACCESS_TOKEN_LIST).get(body.getAccessToken()) != null) {
            String usernameFromCache = instance.getMap(SECURITY_ACCESS_TOKEN_LIST).get(body.getAccessToken())
                    .toString();
            if (usernameFromCache.equals(username)) {
                revokeAccessToken(body.getAccessToken());
            }
        }
        if (instance.getMap(SECURITY_REFRESH_TOKEN_LIST).get(body.getAccessToken()) != null) {
            String usernameFromCache = instance.getMap(SECURITY_REFRESH_TOKEN_LIST).get(body.getAccessToken())
                    .toString();
            if (usernameFromCache.equals(username)) {
                revokeRefreshToken(body.getAccessToken());
            }
        }
    }

    public DTResponseDto<UserDto> getAllAccessToken(DTRequestDto body, Principal principal) {
        DTResponseDto<UserDto> dataTableResult = new DTResponseDto<>();
        List<UserDto> tokens = new ArrayList<>();
        IMap<String, String> sessionMap = instance.getMap(SECURITY_ACCESS_TOKEN_LIST);
        for (Map.Entry<String, String> entry : sessionMap.entrySet()) {
            String token = entry.getKey();
            String tokenUsername = entry.getValue();
            String tokend = token.substring(0, 255);
            byte[] tokenb = Base64.getMimeDecoder().decode(tokend);
            tokend = new String(tokenb, StandardCharsets.UTF_8);
            String s1 = tokend;
            s1 = s1.replace("\"", "");
            s1 = s1.replace("{", "");
            s1 = s1.replace("[", "");
            s1 = s1.replace("]", "");
            s1 = s1.replace("}", ",");
            String[] pairs = s1.split(",");
            String ip = pairs[5].substring(10);
            try {
                if (!tokenUsername.equalsIgnoreCase(principal.getName())) {
                    UserDto userDto = userRepo.findByUsername(tokenUsername).parseDto();
                    userDto.setIpAddress(ip);
                    userDto.setAddress(userDto.getAddress() != null ? userDto.getAddress() : "");
                    tokens.add(userDto);
                }
            } catch (Exception ex) {
                revokeAccessToken(token);
            }
        }
        Map<String, FilterSpec> filter = getFilter(body);
        tokens = filter == null ? tokens : getFiltered(tokens, filter);
        Pageable tokenPage = getPageable(body);
        tokens = getSorted(tokens, tokenPage);
        Integer start = tokenPage.getPageNumber() > 0 ? tokenPage.getPageNumber() * tokenPage.getPageSize() : 0;
        Integer end = start + tokenPage.getPageSize() > tokens.size() ? tokens.size()
                : start + tokenPage.getPageSize();
        Page<UserDto> tokenPages = new PageImpl<>(tokens.subList(start, end), tokenPage, tokens.size());
        if (body == null)
            dataTableResult.setDraw(1);
        else
            dataTableResult.setDraw(body.getDraw());
        dataTableResult.setRecordsFiltered(tokenPages.getTotalElements());
        dataTableResult.setRecordsTotal(tokens.size());
        dataTableResult.setData(tokenPages.getContent());
        return dataTableResult;
    }

    public ResponseEntity<UserDto> getCurrentAccessTokens(Principal principal) {
        UserDto tokens = new UserDto();
        IMap<String, String> sessionMap = instance.getMap(SECURITY_ACCESS_TOKEN_LIST);
        for (Map.Entry<String, String> entry : sessionMap.entrySet()) {
            String token = entry.getKey();
            String tokenUsername = entry.getValue();
            String tokend = token.substring(0, 255);
            byte[] tokenb = Base64.getMimeDecoder().decode(tokend);
            tokend = new String(tokenb, StandardCharsets.UTF_8);
            String s1 = tokend;
            s1 = s1.replace("\"", "");
            s1 = s1.replace("{", "");
            s1 = s1.replace("[", "");
            s1 = s1.replace("]", "");
            s1 = s1.replace("}", ",");
            String[] pairs = s1.split(",");
            String ip = pairs[8].substring(10);
            if (tokenUsername.equalsIgnoreCase(principal.getName())) {
                UserDto userDto = userRepo.findByUsername(tokenUsername).parseDto();
                userDto.setIpAddress(ip);
                tokens = userDto;
                return new ResponseEntity<>(tokens, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(tokens, HttpStatus.NOT_FOUND);
    }

    public void revokeTokens(List<String> usernames) {
        IMap<String, String> accessSessionMap = instance.getMap(SECURITY_ACCESS_TOKEN_LIST);
        IMap<String, String> refreshSessionMap = instance.getMap(SECURITY_REFRESH_TOKEN_LIST);
        for (Map.Entry<String, String> access : accessSessionMap.entrySet()) {
            String token = access.getKey();
            String tokenUsername = access.getValue();
            for (String username : usernames) {
                if (tokenUsername.equals(username)) {
                    revokeAccessToken(token);
                }
            }
        }
        for (Map.Entry<String, String> refresh : refreshSessionMap.entrySet()) {
            String token = refresh.getKey();
            String tokenUsername = refresh.getValue();
            for (String username : usernames) {
                if (tokenUsername.equals(username)) {
                    revokeRefreshToken(token);
                }
            }
        }
    }

    private List<UserDto> getSorted(List<UserDto> tokenss, Pageable tokenPage) {
        Sort sort = tokenPage.getSort();
        for (Sort.Order order : sort) {
            Comparator<UserDto> comparator = null;
            switch (order.getProperty()) {
                case "name":
                    comparator = (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName());
                    break;
                case "username":
                    comparator = (o1, o2) -> o1.getUsername().compareToIgnoreCase(o2.getUsername());
                    break;
                case "email":
                    comparator = (o1, o2) -> o1.getEmail().compareToIgnoreCase(o2.getEmail());
                    break;
                case "address":
                    comparator = (o1, o2) -> (o1.getAddress() != null ? o1.getAddress() : "").compareToIgnoreCase(o2.getAddress() != null ? o2.getAddress() : "");
                    break;
                case "roles[, ].authority":
                    comparator = (o1, o2) -> o1.getRoles().toString().compareToIgnoreCase(o2.getRoles().toString());
                    break;
                case "ipAddress":
                    comparator = (o1, o2) -> o1.getIpAddress().compareToIgnoreCase(o2.getIpAddress());
                    break;
                default:
                    comparator = (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName());
                    break;
            }
            if (order.isAscending()) {
                tokenss.sort(comparator);
            } else {
                tokenss.sort(comparator.reversed());
            }
        }
        return tokenss;
    }

    private List<UserDto> getFiltered(List<UserDto> tokenss, Map<String, FilterSpec> filter) {
        List<UserDto> listName = new ArrayList<>();
        List<UserDto> listUsername = new ArrayList<>();
        List<UserDto> listEmail = new ArrayList<>();
        List<UserDto> listAddress = new ArrayList<>();
        List<UserDto> listBranch = new ArrayList<>();
        List<UserDto> listIp = new ArrayList<>();
        boolean isFiltered = !filter.isEmpty();
        for (Map.Entry<String, FilterSpec> _field : filter.entrySet()) {
            String key = _field.getKey();
            String value = _field.getValue().getValues()[0].toString();

            switch (key) {
                case "name":
                    listName = tokenss.stream().filter((UserDto u) -> u.getName().contains(value)).collect(Collectors.toList());
                    break;
                case "username":
                    listUsername = tokenss.stream().filter((UserDto u) -> u.getUsername().contains(value)).collect(Collectors.toList());
                    break;
                case "email":
                    listEmail = tokenss.stream().filter((UserDto u) -> u.getEmail().contains(value)).collect(Collectors.toList());
                    break;
                case "address":
                    listAddress = tokenss.stream().filter((UserDto u) -> u.getAddress().contains(value)).collect(Collectors.toList());
                    break;
                case "ipAddress":
                    listIp = tokenss.stream().filter((UserDto u) -> u.getIpAddress().contains(value)).collect(Collectors.toList());
                    break;
                default:
                    break;
            }
        }
        tokenss = isFiltered ? Stream.of(listName, listUsername, listEmail, listAddress, listBranch, listIp).flatMap(Collection::stream).distinct().collect(Collectors.toList()) : tokenss;
        return tokenss;
    }

    public Boolean checkRefreshToken(String refreshToken) {
        IMap<String, String> sessionMap = instance.getMap(SECURITY_REFRESH_TOKEN_LIST);
        return sessionMap.get(refreshToken) != null;
    }

}