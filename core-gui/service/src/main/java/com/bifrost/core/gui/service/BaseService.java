package com.bifrost.core.gui.service;

import com.bifrost.core.gui.configuration.utils.JsonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseService {

    @Autowired
    protected JsonUtils jsonUtils;

    protected final Log logger = LogFactory.getLog(this.getClass());

}