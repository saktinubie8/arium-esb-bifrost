package com.bifrost.core.gui.service;

import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.configuration.ResponseCodeGroupConfig;
import com.bifrost.core.gui.dao.dto.DTOrder;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.MessageLogCountDto;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.core.gui.dao.specification.MessageLogSpecification;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.logging.model.MessageLog;
import com.bifrost.logging.model.dto.MessageLogDto;
import com.bifrost.logging.repository.MessageLogRepository;
import com.bifrost.logging.repository.RouteLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("messageLogService")
public class MessageLogService extends GUIGenericService {

    @Autowired
    private MessageLogRepository messageLogRepo;

    @Autowired
    private RouteLogRepository routeLogRepository;

    @Autowired
    private ResponseCodeGroupConfig responseCodeGroupConfig;

    public DTResponseDto<MessageLogDto> getMessageLogs(DTRequestDto body) throws SystemErrorException {
        try {
            Map<String, FilterSpec> filter = getFilter(body);
            Boolean stateButton = true;
            DTResponseDto<MessageLogDto> dataTableResult = new DTResponseDto<>();
            Page<MessageLog> messageLogEntities = messageLogRepo.findAll(MessageLogSpecification.getMessageLogSpec(filter),
                    getPageableForMessageLog(body));
            List<MessageLogDto> messageLogs = new ArrayList<>();
            for (MessageLog _msgLog : messageLogEntities.getContent()) {
                MessageLogDto messageLogDto = _msgLog.parseDto();
                List<String> paramButtons = new ArrayList<>();
                paramButtons.add(stateButton.toString());
                messageLogDto.setButton(paramButtons);
                messageLogs.add(messageLogDto);
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(messageLogEntities.getTotalElements());
            dataTableResult.setRecordsTotal(messageLogRepo.count());
            dataTableResult.setData(messageLogs);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }


    public MessageLogDto getMessageLogs(MessageLogDto body) throws SystemErrorException {
        try {
            MessageLog messageLog = null;
            if (body.getTransactionId() != null) {
                messageLog = messageLogRepo.findByTransactionId(body.getTransactionId());
            }
	            /*  blm confirm mau by apa aja , default masih by Id
	            else if (body.getUsername() != null) {
	                userEntity = userRepo.findByUsername(body.getUsername());
	            }
	            */
            if (messageLog != null) {
                return messageLog.parseDto();
            } else {
                throw new SystemErrorException(ErrorCode.ERR_SYS0001);
            }
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public MessageLogCountDto getCountMessageLogs(DTRequestDto body) throws SystemErrorException {
        MessageLogCountDto messageLogCountDto = new MessageLogCountDto();
        try {
            List<String> rcList = Arrays.asList(responseCodeGroupConfig.getResponseCodeSuccessGroup()
                    .split("\\s*,\\s*"));
            int messageLogSuccess = messageLogRepo.countByResponseCodeIn(rcList);
            int messageLogFail = messageLogRepo.countByResponseCodeNotIn(rcList);
            int messageLogDeviation = routeLogRepository.countDeviation();
            int totalTransaction = routeLogRepository.countDistinctRouteLogId_TransactionId();
            messageLogCountDto.setCountSuccess(String.valueOf(messageLogSuccess));
            messageLogCountDto.setCountFail(String.valueOf(messageLogFail));
            messageLogCountDto.setCountDeviation(String.valueOf(messageLogDeviation));
            messageLogCountDto.setTotalTransaction(String.valueOf(totalTransaction));
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
        return messageLogCountDto;
    }

}