package com.bifrost.core.gui.service;

import com.bifrost.common.constant.CacheName;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.EndpointStatusDto;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.logging.model.EndpointStatus;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service("endpointStatusService")
public class EndpointStatusService extends GUIGenericService {

    private IMap<String, String> statusMap;

    @Autowired
    private HazelcastInstance instance;

    @PostConstruct
    public void init() {
        statusMap = instance.getMap(CacheName.ENDPOINT_STATUS);
    }

    public DTResponseDto<EndpointStatusDto> getAllEndpointStatus(DTRequestDto body) throws SystemErrorException {
        try {
            int length = 0;
            int startIndex = 0;
            DTResponseDto<EndpointStatusDto> dataTableResult = new DTResponseDto<>();
            int no = 0;
            if (body == null) {
                dataTableResult.setDraw(1);
            } else {
                dataTableResult.setDraw(body.getDraw());
                no = (int) body.getStart();
                length = (int) body.getLength();
                startIndex = (int) body.getStart();
            }
            List<EndpointStatusDto> endpointStatusDtoList = new ArrayList<>();
            for (IMap.Entry<String, String> entry : statusMap.entrySet()) {
                EndpointStatusDto endpointStatusDto = new EndpointStatusDto();
                String[] key = EndpointStatus.parseKey(entry.getKey());
                endpointStatusDto.setEndpointCode(key[0]);
                endpointStatusDto.setNode(key[1]);
                endpointStatusDto.setStatus(entry.getValue());
                endpointStatusDto.setNumber(++no);
                endpointStatusDtoList.add(endpointStatusDto);
            }
            List<EndpointStatusDto> endpointStatusDtoListAfterPaging = new ArrayList<>();
            Pageable pageable = getPageable(body);
            Page<EndpointStatusDto> endpointStatusPage = new PageImpl<>(endpointStatusDtoList, pageable,
                    pageable.getOffset() + pageable.getPageSize());
            for (EndpointStatusDto endpointStatusDto : endpointStatusPage.getContent()) {
                EndpointStatusDto endpointStatusDtoAfterPaging = new EndpointStatusDto();
                endpointStatusDtoAfterPaging.setEndpointCode(endpointStatusDto.getEndpointCode());
                endpointStatusDtoAfterPaging.setNode(endpointStatusDto.getNode());
                endpointStatusDtoAfterPaging.setStatus(endpointStatusDto.getStatus());
                endpointStatusDtoAfterPaging.setNumber(endpointStatusDto.getNumber());
                endpointStatusDtoListAfterPaging.add(endpointStatusDtoAfterPaging);
            }
            dataTableResult.setRecordsFiltered(endpointStatusPage.getContent().size());
            if (endpointStatusDtoListAfterPaging.size() < length + startIndex) {
                dataTableResult.setData(endpointStatusDtoListAfterPaging.subList(startIndex, endpointStatusDtoListAfterPaging.size()));
            } else {
                dataTableResult.setData(endpointStatusDtoListAfterPaging.subList(startIndex, length + startIndex));
            }
            dataTableResult.setRecordsTotal(endpointStatusDtoList.size());
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

}