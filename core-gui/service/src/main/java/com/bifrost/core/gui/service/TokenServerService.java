package com.bifrost.core.gui.service;

import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.model.UserEntity;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.core.gui.dao.specification.TokenSpecification;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.system.dto.TokenServerDto;
import com.bifrost.system.model.TokenEntity;
import com.bifrost.system.repository.TokenEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("tokenServerService")
public class TokenServerService extends GUIGenericService {

    @Autowired
    private TokenEntityRepository tokenEntityRepository;

    public DTResponseDto<TokenServerDto> getTokenServer(DTRequestDto body) throws SystemErrorException {
        try {
            Map<String, FilterSpec> filter = getFilter(body);
            DTResponseDto<TokenServerDto> dataTableResult = new DTResponseDto<>();
            Page<TokenEntity> tokenEntities = tokenEntityRepository.findAll(TokenSpecification.getTokenSpec(filter), getPageableWithoutSort(body));
            List<TokenServerDto> tokenDtoList = new ArrayList<>();
            int nomor = 0;
            if (body != null) {
                nomor = (Math.toIntExact(body.getStart())) + 1;
            }
            for (TokenEntity _tokenEntity : tokenEntities.getContent()) {
                TokenServerDto tokenServerDto = _tokenEntity.parseDto();
                tokenServerDto.setNumber(nomor);
                tokenDtoList.add(tokenServerDto);
                nomor = nomor + 1;
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(tokenEntities.getTotalElements());
            dataTableResult.setRecordsTotal(tokenEntityRepository.count());
            dataTableResult.setData(tokenDtoList);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public TokenEntity createToken(TokenServerDto token) {
        TokenEntity tokenEntity = new TokenEntity();
        tokenEntity.setId(Integer.parseInt(token.getId()));
        tokenEntity.setUsername(token.getUsername());
        tokenEntity.setToken(token.getToken());
        tokenEntity.setSecretKey(token.getSecretKey());
        tokenEntity.setIpPublic(token.getIpPublic());
        tokenEntity.setMerchantName(token.getMerchantName());
        return tokenEntityRepository.save(tokenEntity);
    }

}