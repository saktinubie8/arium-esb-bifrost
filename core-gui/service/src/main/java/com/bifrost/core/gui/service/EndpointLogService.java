package com.bifrost.core.gui.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;

import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.repository.EndpointLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.logging.model.dto.EndpointLogDto;
import com.bifrost.core.gui.dao.specification.EndpointLogSpecification;
import com.bifrost.core.gui.dao.specification.FilterSpec;
import com.bifrost.exception.gui.SystemErrorException;
import org.springframework.stereotype.Service;

@Service("endpointLogService")
public class EndpointLogService extends GUIGenericService {

    @Autowired
    EndpointLogRepository msgRepo;

    public DTResponseDto<EndpointLogDto> getAllEndpointLogs(DTRequestDto body) throws SystemErrorException {
        try {
            Map<String, FilterSpec> filter = getFilter(body);
            Boolean stateButton = true;
            DTResponseDto<EndpointLogDto> dataTableResult = new DTResponseDto<>();
            Page<EndpointLog> endpointEntities = msgRepo.findAll(EndpointLogSpecification.getEndpointLogsSpec(filter),
                    getPageableWithoutSort(body));
            List<EndpointLogDto> users = new ArrayList<>();
            for (EndpointLog _data : endpointEntities.getContent()) {
                EndpointLogDto endpointDto = _data.parseDto();
                List<String> paramButtons = new ArrayList<>();
                paramButtons.add(stateButton.toString());
                endpointDto.setButton(paramButtons);
                users.add(endpointDto);
            }
            if (body == null)
                dataTableResult.setDraw(1);
            else
                dataTableResult.setDraw(body.getDraw());
            dataTableResult.setRecordsFiltered(endpointEntities.getTotalElements());
            dataTableResult.setRecordsTotal(msgRepo.count());
            dataTableResult.setData(users);
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

}