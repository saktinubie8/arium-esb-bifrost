package com.bifrost.core.gui.service;

import com.bifrost.common.util.ErrorCode;
import com.bifrost.core.gui.dao.dto.DTRequestDto;
import com.bifrost.core.gui.dao.dto.DTResponseDto;
import com.bifrost.core.gui.dao.dto.ResponseCodeDto;
import com.bifrost.exception.gui.SystemErrorException;
import com.bifrost.orchestrator.model.mapper.ResponseCode;
import com.bifrost.orchestrator.model.mapper.ResponseDesc;
import com.bifrost.orchestrator.service.mapper.ResponseCodeService;
import com.bifrost.orchestrator.service.mapper.ResponseDescService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service("responseCodeUIService")
public class ResponseCodeUIService extends GUIGenericService {

    @Autowired
    private ResponseCodeService responseCodeService;

    @Autowired
    private ResponseDescService responseDescService;

    public DTResponseDto<ResponseCodeDto> getAllResponseCode(DTRequestDto body) throws SystemErrorException {
        try {
            DTResponseDto<ResponseCodeDto> dataTableResult = new DTResponseDto<>();
            int no = 0;
            int length = 0;
            int startIndex = 0;
            if (body == null) {
                dataTableResult.setDraw(1);
            } else {
                dataTableResult.setDraw(body.getDraw());
                no = (int) body.getStart();
                length = (int) body.getLength();
                startIndex = (int) body.getStart();
            }
            Pageable pageable = getPageable(body);
            Page<ResponseCode> responseCodePage = new PageImpl<>(responseCodeService.findAll(),
                    pageable, pageable.getOffset() + pageable.getPageSize());
            List<ResponseCodeDto> responseCodeDtoList = new ArrayList<>();
            for (ResponseCode rc : responseCodePage.getContent()) {
                ResponseCodeDto responseCodeDto = new ResponseCodeDto();
                responseCodeDto.setEndpointCode(rc.getEndpointCode());
                responseCodeDto.setResponseCode(rc.getCode());
                ResponseDesc responseDesc = responseDescService.findByEndpointCodeNullAndResponseCode(responseCodeDto.getResponseCode());
                if (responseDesc != null)
                    responseCodeDto.setResponseDesc(responseDesc.getDescription());
                responseCodeDtoList.add(responseCodeDto);
            }
            dataTableResult.setRecordsFiltered(responseCodePage.getContent().size());
            if (responseCodeDtoList.size() < length + startIndex) {
                dataTableResult.setData(responseCodeDtoList.subList(startIndex, responseCodeDtoList.size()));
            } else {
                dataTableResult.setData(responseCodeDtoList.subList(startIndex, length + startIndex));
            }
            dataTableResult.setRecordsTotal(responseCodeDtoList.size());
            return dataTableResult;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

    public ResponseCodeDto updateResponseCode(ResponseCodeDto body) throws SystemErrorException {
        ResponseCode rc;
        ResponseCodeDto responseCodeDto = new ResponseCodeDto();
        if (body != null && !"".equals(body.getResponseCode()) && !"".equals(body.getEndpointCode())) {
            rc = responseCodeService.findByEndpointCodeAndResponseCode(body.getEndpointCode(), body.getResponseCode());
            if (rc != null) {
                ResponseDesc rdesc = responseDescService.findByEndpointCodeNullAndResponseCode(rc.getCode());
                rdesc.setDescription(body.getResponseDesc());
                responseCodeDto.setResponseDesc(rdesc.getDescription());
                responseCodeDto.setEndpointCode(rc.getEndpointCode());
                responseCodeDto.setResponseCode(rc.getCode());
            }
        }
        return responseCodeDto;
    }

    public ResponseCodeDto findResponseCode(ResponseCodeDto body) throws SystemErrorException {
        try {
            ResponseCode rc;
            ResponseCodeDto responseCodeDto = new ResponseCodeDto();
            if (body != null && !"".equals(body.getResponseCode()) && !"".equals(body.getEndpointCode())) {
                rc = responseCodeService.findByEndpointCodeAndResponseCode(body.getEndpointCode(), body.getResponseCode());
                if (rc != null) {
                    ResponseDesc rdesc = responseDescService.findByEndpointCodeNullAndResponseCode(rc.getCode());
                    responseCodeDto.setResponseDesc(rdesc.getDescription());
                    responseCodeDto.setEndpointCode(rc.getEndpointCode());
                    responseCodeDto.setResponseCode(rc.getCode());
                }
            }
            return responseCodeDto;
        } catch (NoResultException nre) {
            logger.error(nre);
            throw new SystemErrorException(ErrorCode.ERR_SYS0001);
        }
    }

}