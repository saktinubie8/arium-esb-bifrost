package com.bifrost.core.gui.configuration.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target(METHOD)
public @interface AuditLog {

    String title() default "";

    String description() default "";

    String module() default "";

}