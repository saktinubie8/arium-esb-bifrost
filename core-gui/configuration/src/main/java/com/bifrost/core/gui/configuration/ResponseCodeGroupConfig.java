package com.bifrost.core.gui.configuration;

import com.bifrost.common.constant.ParamConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ResponseCodeGroupConfig {

    @Value("${"+ParamConstant.ResponseCode.RESPONSE_CODE_SUCCESS_GROUP+"}")
    private String responseCodeSuccessGroup;

    public String getResponseCodeSuccessGroup() {
        return responseCodeSuccessGroup;
    }

    public void setResponseCodeSuccessGroup(String responseCodeSuccessGroup) {
        this.responseCodeSuccessGroup = responseCodeSuccessGroup;
    }
}
