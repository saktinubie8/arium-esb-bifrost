package com.bifrost.core.gui.configuration;

import com.bifrost.common.banner.ModuleInfo;
import com.bifrost.common.constant.ParamConstant;
import com.bifrost.system.configuration.AbstractSystemIdentity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author rosaekapratama@gmail.com
 */
@Component
public class ServerIdentity extends AbstractSystemIdentity {

    @Value("${" + ParamConstant.Bifrost.MODULE_CORE_GUI_PORT + ":9993}")
    private int port;

    public int getDefaultPort() {
        return port;
    }

    public String getModuleCode() {
        return ModuleInfo.CORE_GUI_CODE;
    }

}