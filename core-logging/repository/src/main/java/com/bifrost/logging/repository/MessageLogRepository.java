package com.bifrost.logging.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.bifrost.logging.model.MessageLog;
import org.springframework.data.jpa.repository.Query;

/**
 * @author rosaekapratama@gmail.com
 */
public interface MessageLogRepository extends JpaRepository<MessageLog, String>, JpaSpecificationExecutor<MessageLog> {

    List<MessageLog> findByOriginEndpoint(String endpointCode);

    List<MessageLog> findByReceivedBetween(Date startDate, Date endDate);

    List<MessageLog> findByReceivedLessThan(Date date);

    MessageLog findByTransactionId(String transactionId);

    int countByResponseCodeIn(List<String> rc);

    int countByResponseCodeNotIn(List<String> rc);

}