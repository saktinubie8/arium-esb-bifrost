package com.bifrost.logging.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.bifrost.logging.model.RouteLog;
import com.bifrost.logging.model.RouteLogId;
import org.springframework.data.jpa.repository.Query;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface RouteLogRepository extends JpaRepository<RouteLog, RouteLogId>, JpaSpecificationExecutor<RouteLog> {

	List<RouteLog> findByRouteLogId_DateTimeLessThan(Date date);
	List<RouteLog> findByRouteLogId_DateTimeBetween(Date startDate, Date endDate);

	@Query("SELECT COUNT(distinct a.routeLogId.transactionId) FROM RouteLog a WHERE a.routeLogId.transactionId NOT IN " +
			"(SELECT c.transactionId FROM MessageLog c) ")
	int countDeviation();

	@Query("SELECT COUNT(distinct a.routeLogId.transactionId) FROM RouteLog a")
	int countDistinctRouteLogId_TransactionId();

}