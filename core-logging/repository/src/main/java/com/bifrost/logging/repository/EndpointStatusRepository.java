package com.bifrost.logging.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.logging.model.EndpointStatus;
import com.bifrost.logging.model.EndpointStatusId;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointStatusRepository extends JpaRepository<EndpointStatus, EndpointStatusId> {

	public EndpointStatus findByEndpointStatusId_endpointCodeAndEndpointStatusId_node(String endpointCode, String node);
	
}
