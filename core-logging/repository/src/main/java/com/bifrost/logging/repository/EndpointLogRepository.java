package com.bifrost.logging.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.model.EndpointLogId;

/**
 * @author rosaekapratama@gmail.com
 */
public interface EndpointLogRepository extends JpaRepository<EndpointLog, EndpointLogId>, JpaSpecificationExecutor<EndpointLog> {

    List<EndpointLog> findByEndpointLogId_EndpointCode(String endpointCode);
    List<EndpointLog> findByEndpointLogId_DateTimeLessThan(Date date);
    List<EndpointLog> findByEndpointLogId_DateTimeBetween(Date startDate, Date endDate);

}