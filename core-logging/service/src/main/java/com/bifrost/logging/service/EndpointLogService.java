package com.bifrost.logging.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.repository.EndpointLogRepository;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointLogService {

	@Autowired
	private EndpointLogRepository endpointLogRepository;
	
	public List<EndpointLog> findByEndpointCode(String endpointCode) {
		return endpointLogRepository.findByEndpointLogId_EndpointCode(endpointCode);
	}

	public List<EndpointLog> findByDateTimeLessThan(Date date) {
		return endpointLogRepository.findByEndpointLogId_DateTimeLessThan(date);
	}

	public List<EndpointLog> findByDateTimeBetween(Date startDate, Date endDate) {
		return endpointLogRepository.findByEndpointLogId_DateTimeBetween(startDate, endDate);
	}

	public EndpointLog save(EndpointLog t) {
		return endpointLogRepository.save(t);
	}

	public void delete(EndpointLog t) {
		endpointLogRepository.delete(t);
	}	
}
