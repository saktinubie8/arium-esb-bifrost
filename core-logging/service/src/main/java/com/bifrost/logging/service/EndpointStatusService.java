package com.bifrost.logging.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.CacheName;
import com.bifrost.logging.model.EndpointStatus;
import com.bifrost.logging.repository.EndpointStatusRepository;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;


/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointStatusService {

	private IMap<String, String> statusMap;
	
	@Autowired
	private HazelcastInstance instance;
	
	@Autowired
	private EndpointStatusRepository r;
	
	@Autowired
	private EndpointStatusListener listener;
	
	@PostConstruct
	public void init() {
		statusMap = instance.getMap(CacheName.ENDPOINT_STATUS);	
		statusMap.addEntryListener(listener, true);
	}
	
	@Scheduled(fixedDelay = 3600000)
	public void clean() {
		for(EndpointStatus t : r.findAll()) {
			String endpointCode = t.getEndpointStatusId().getEndpointCode();
			String node = t.getEndpointStatusId().getNode();
			if(statusMap.containsKey(EndpointStatus.constructKey(endpointCode, node)))
				continue;
			else
				r.delete(t);
		}
	}
}
