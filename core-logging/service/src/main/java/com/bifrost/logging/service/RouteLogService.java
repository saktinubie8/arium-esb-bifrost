package com.bifrost.logging.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bifrost.logging.model.RouteLog;
import com.bifrost.logging.repository.RouteLogRepository;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class RouteLogService {
	
	@Autowired
	private RouteLogRepository routeLogRepository;

	public List<RouteLog> findByDateTimeLessThan(Date date) {
		return routeLogRepository.findByRouteLogId_DateTimeLessThan(date);
	}
	
	public List<RouteLog> findByDateTimeBetween(Date startDate, Date endDate) {
		return routeLogRepository.findByRouteLogId_DateTimeBetween(startDate, endDate);
	}

	public RouteLog save(RouteLog t) {
		return routeLogRepository.save(t);
	}

	public void delete(RouteLog t) {
		routeLogRepository.delete(t);
	}
}
