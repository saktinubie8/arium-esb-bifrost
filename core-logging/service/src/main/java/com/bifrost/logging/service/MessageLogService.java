package com.bifrost.logging.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bifrost.logging.model.MessageLog;
import com.bifrost.logging.repository.MessageLogRepository;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class MessageLogService {
	
	@Autowired
	private MessageLogRepository messageLogRepository;

	public List<MessageLog> findByEndpointCode(String endpointCode) {
		return messageLogRepository.findByOriginEndpoint(endpointCode);
	}

	public List<MessageLog> findByDateTimeLessThan(Date date) {
		return messageLogRepository.findByReceivedLessThan(date);
	}

	public List<MessageLog> findByDateTimeBetween(Date startDate, Date endDate) {
		return messageLogRepository.findByReceivedBetween(startDate, endDate);
	}

	public MessageLog save(MessageLog t) {
		return messageLogRepository.save(t);
	}

	public void delete(MessageLog t) {
		messageLogRepository.delete(t);
	}
}
