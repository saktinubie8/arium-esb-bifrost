package com.bifrost.logging.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bifrost.logging.model.EndpointStatus;
import com.bifrost.logging.repository.EndpointStatusRepository;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryEvictedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class EndpointStatusListener implements 
	EntryAddedListener<String, String>, 
	EntryUpdatedListener<String, String>,
	EntryRemovedListener<String, String>,
	EntryEvictedListener<String, String> {
	
	@Autowired
	private EndpointStatusRepository statusRepository;
	
	@Override
	public void entryAdded(EntryEvent<String, String> event) {
		String[] key = EndpointStatus.parseKey(event.getKey());
		String status = event.getValue();
		statusRepository.save(new EndpointStatus(key[0], key[1], status));
	}

	@Override
	public void entryUpdated(EntryEvent<String, String> event) {
		String[] key = EndpointStatus.parseKey(event.getKey());
		String status = event.getValue();
		statusRepository.save(new EndpointStatus(key[0], key[1], status));
	}

	@Override
	public void entryRemoved(EntryEvent<String, String> event) {
		String[] key = EndpointStatus.parseKey(event.getKey());
		EndpointStatus t = statusRepository.findByEndpointStatusId_endpointCodeAndEndpointStatusId_node(key[0], key[1]);
		if(t!=null)
			statusRepository.delete(t);
	}

	@Override
	public void entryEvicted(EntryEvent<String, String> event) {
		String[] key = EndpointStatus.parseKey(event.getKey());
		EndpointStatus t = statusRepository.findByEndpointStatusId_endpointCodeAndEndpointStatusId_node(key[0], key[1]);
		if(t!=null)
			statusRepository.delete(t);
	}
}
