package com.bifrost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bifrost.common.banner.BannerInfo;
import com.bifrost.common.banner.ModuleInfo;
import com.bifrost.logging.controller.LogConsumer;

@SpringBootApplication
public class Application extends BaseApplication {

	public static void main(String[] args) {
		app = new SpringApplication(Application.class);
		app.setBanner(new BannerInfo(ModuleInfo.CORE_LOGGING_DESC));
		context = app.run(args);
		LogConsumer consumer = context.getBean(LogConsumer.class);
		consumer.start();
	}
}