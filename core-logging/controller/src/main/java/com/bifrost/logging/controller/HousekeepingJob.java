package com.bifrost.logging.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.common.util.DateUtils;
import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.model.MessageLog;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.logging.service.EndpointLogService;
import com.bifrost.logging.service.MessageLogService;
import com.bifrost.logging.service.RouteLogService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class HousekeepingJob implements Runnable {
	
	@Autowired
	@Qualifier("logEntityManagerFactory")
	private EntityManager entityManager;

	@Autowired
	@Qualifier("logTransactionManager")
	private PlatformTransactionManager platformTransactionManager;
	
    @Autowired
    private ObjectMapper objectMapper;
	
	@Autowired
	private RouteLogService routeLogService;
	
	@Autowired
	private MessageLogService messageLogService;
	
	@Autowired
	private EndpointLogService endpointLogService;
	
    @Autowired
    private RestHighLevelClient client; 
    
    @Value("${"+ParamConstant.Housekeeping.DAYS_TO_KEEP+":365}")
    private int daysToKeep;
    
    TransactionTemplate transactionTemplate;
    private static final Log LOGGER = LogFactory.getLog(HousekeepingJob.class);
	private static final String DELETE_ENDPOINT_LOG_LESS_THAN = "DELETE FROM EndpointLog e WHERE e.endpointLogId.dateTime < :dateTime";
	private static final String DELETE_MESSAGE_LOG_LESS_THAN = "DELETE FROM MessageLog e WHERE e.received < :dateTime";
	private static final String DELETE_ROUTE_LOG_LESS_THAN = "DELETE FROM RouteLog e WHERE e.routeLogId.dateTime < :dateTime";
	
	@PostConstruct
	public void init() {
		transactionTemplate = new TransactionTemplate(platformTransactionManager);
	}
	
	@Override
	public void run() {
		try {
			if(!client.ping(RequestOptions.DEFAULT)) {
				LOGGER.error("[Housekeeping] Can't start process, cause by: CLUSTER PING FAILED");
				return;
			}
		} catch (IOException e) {
			LOGGER.error("[Housekeeping] Can't start process, cause by: "+e.getMessage());
			e.printStackTrace();
			return;
		}
		LOGGER.trace("[Housekeeping] Starting process ===================================");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		List<EndpointLog> endpointLogs = endpointLogService.findByDateTimeLessThan(cal.getTime());
		List<MessageLog> messageLogs = messageLogService.findByDateTimeLessThan(cal.getTime());
		List<RouteLog> routeLogs = routeLogService.findByDateTimeLessThan(cal.getTime());
		cal.add(Calendar.DATE, -1);
		String index = DateUtils.formatInDate(cal.getTime());
		cal.add(Calendar.DATE, +1);
		try {
			if(endpointLogs.size()<1)
				LOGGER.trace("[Housekeeping] Endpoint log has 0 row, skipping process");
			else if(bulkInsert(index+"endpointlog", endpointLogs))
				transactionTemplate.execute(new TransactionCallbackWithoutResult() {
					
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						Query query = entityManager.createQuery(DELETE_ENDPOINT_LOG_LESS_THAN);
						int deletedCount = query
								.setParameter("dateTime", cal.getTime())
								.executeUpdate();
						LOGGER.trace("[Housekeeping] Success removing "+deletedCount+" endpoint log source row");
					}
				});
		} catch (IOException e) {
			LOGGER.error("[Housekeeping] Error when trying to process endpoint log row, cause by: "+e.getMessage());
			e.printStackTrace();
		}
		try {
			if(messageLogs.size()<1)
				LOGGER.trace("[Housekeeping] Message log has 0 row, skipping process");
			else if(bulkInsert(index+"messagelog", messageLogs))
				transactionTemplate.execute(new TransactionCallbackWithoutResult() {
					
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						Query query = entityManager.createQuery(DELETE_MESSAGE_LOG_LESS_THAN);
						int deletedCount = query
								.setParameter("dateTime", cal.getTime())
								.executeUpdate();
						LOGGER.trace("[Housekeeping] Success removing "+deletedCount+" message log source row");
					}
				});
		} catch (IOException e) {
			LOGGER.error("[Housekeeping] Error when trying to process message log row, cause by: "+e.getMessage());
			e.printStackTrace();
		}
		try {
			if(routeLogs.size()<1)
				LOGGER.trace("[Housekeeping] Route log has 0 row, skipping process");
			else if(bulkInsert(index+"routelog", routeLogs))
				transactionTemplate.execute(new TransactionCallbackWithoutResult() {
					
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						Query query = entityManager.createQuery(DELETE_ROUTE_LOG_LESS_THAN);
						int deletedCount = query
								.setParameter("dateTime", cal.getTime())
								.executeUpdate();
						LOGGER.trace("[Housekeeping] Success removing "+deletedCount+" route log source row");
					}
				});
		} catch (IOException e) {
			LOGGER.error("[Housekeeping] Error when trying to process route log row, cause by: "+e.getMessage());
			e.printStackTrace();
		}
		
		cal.set(Calendar.DATE, -daysToKeep-1);
		cleanHousekeep(cal);
		LOGGER.info("[Housekeeping] Process finished =====================================");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Boolean bulkInsert(String index, List list) throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
        list.forEach(log -> {
            IndexRequest indexRequest = new IndexRequest(index).source(objectMapper.convertValue(log, Map.class));
            bulkRequest.add(indexRequest);
        });
        BulkResponse response = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        if(response.hasFailures()) {
    	    LOGGER.error("[Housekeeping] Failed to put "+list.size()+" row in index '"+index+"', cause by: "+response.buildFailureMessage());
    	    return false;
        }else {
    	    LOGGER.info("[Housekeeping] "+list.size()+" row has been put in index '"+index+"' with process time "+response.getTook().getMillis()+" ms");
    	    return true;
        }
    }
	
	private void cleanHousekeep(Calendar threshold) {
		List<String> indices = null;
		RestClient restClient = client.getLowLevelClient();
		Request request = new Request("GET", "/_cat/indices?v&format=json");
		Response response = null;
		try {
		    response = restClient.performRequest(request);
		} catch (IOException e) {
		    LOGGER.error("[Housekeeping] Error when trying to get indices, cause by: "+e.getMessage());
		    e.printStackTrace();
		}

		// parse the JSON response
		List<Map<String, String>> list = null;
		if (response != null) {
		    String rawBody;
			try {
				rawBody = EntityUtils.toString(response.getEntity());
			    TypeReference<List<Map<String, String>>> typeRef = new TypeReference<List<Map<String, String>>>() {};
			    list = objectMapper.readValue(rawBody, typeRef);
			} catch (ParseException | IOException e) {
			    LOGGER.error("[Housekeeping] Error when trying to parse indices, cause by: "+e.getMessage());
			    e.printStackTrace();
			}
		}

		// get the index names
		if (list != null) {
		    indices = list.stream()
		        .map(x -> x.get("index"))
		        .collect(Collectors.toList());
		}
		
		List<String> toBeDeleted = null;
		if(indices!=null)
			for(String index : indices) {
				if(index.endsWith("endpointlog") || index.endsWith("messagelog") || index.endsWith("routelog")) {
					Calendar indexDate = Calendar.getInstance();
					try {
						indexDate.setTime(DateUtils.parseDateString(index.substring(0, 8)));
					} catch (java.text.ParseException e) {
					    LOGGER.error("[Housekeeping] Skip parsing index "+index+", cause by: "+e.getMessage());
					    e.printStackTrace();
					    continue;
					}
					if(indexDate.compareTo(threshold)<0) {
						if(toBeDeleted==null)
							toBeDeleted = new ArrayList<String>();
						toBeDeleted.add(index);
					}
				}
			}
		
		if(toBeDeleted!=null) {
			DeleteIndexRequest deleteRequest = new DeleteIndexRequest(toBeDeleted.toArray(new String[0]));
			try {
				client.indices().delete(deleteRequest, RequestOptions.DEFAULT);
			} catch (IOException e) {
			    LOGGER.error("[Housekeeping] Error when trying to clean indices, cause by: "+e.getMessage());
			    e.printStackTrace();
			}
		}
	}
}
