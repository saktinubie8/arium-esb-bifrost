package com.bifrost.logging.controller;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.ParamConstant;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class HousekeepingScheduler {
	
	@Autowired
	private HousekeepingJob job;
	
	@Autowired
	private ThreadPoolTaskScheduler scheduler;
	
	@Value("${"+ParamConstant.Housekeeping.SCHEDULE+":0 0 1 * * ?}")
	private String schedule;
	
	private static final Log LOGGER = LogFactory.getLog(HousekeepingScheduler.class);
	
	@PostConstruct
	public void start() {
		LOGGER.info("[Housekeeping] Scheduled at '"+schedule+"'");
		scheduler.schedule(job, new CronTrigger(schedule));
	}
	
}
