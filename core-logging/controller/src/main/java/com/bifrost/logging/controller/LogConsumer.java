package com.bifrost.logging.controller;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.logging.model.EndpointLog;
import com.bifrost.logging.model.MessageLog;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.logging.service.EndpointLogService;
import com.bifrost.logging.service.MessageLogService;
import com.bifrost.logging.service.RouteLogService;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class LogConsumer {

	private static final Log LOGGER = LogFactory.getLog(LogConsumer.class);
	private ConcurrentMessageListenerContainer<String, EndpointLog> endpointContainer;
	private ConcurrentMessageListenerContainer<String, MessageLog> messageContainer;
	private ConcurrentMessageListenerContainer<String, RouteLog> routeContainer;
	
	@Value("${" + ParamConstant.Kafka.ENDPOINT_LOG_KAFKA_CONCURRENCY + ":10}")
	private int endpointLogConcurrency;
	
	@Value("${" + ParamConstant.Kafka.ROUTE_LOG_KAFKA_CONCURRENCY + ":20}")
	private int routeLogConcurrency;
	
	@Value("${" + ParamConstant.Kafka.MESSAGE_LOG_KAFKA_CONCURRENCY + ":10}")
	private int messageLogConcurrency;

	@Autowired
	private ConsumerFactory<String, Object> consumerFactory;
	
	@Autowired
	private EndpointLogService endpointLogService;
	
	@Autowired
	private MessageLogService messageLogService;
	
	@Autowired
	private RouteLogService routeLogService;
	
	@PostConstruct
	public void init() {
		ContainerProperties endpointProp = new ContainerProperties("logging.endpoint.in");
		endpointProp.setMissingTopicsFatal(false);
		endpointProp.setMessageListener(new MessageListener<String, EndpointLog>() {

			@Override
			public void onMessage(ConsumerRecord<String, EndpointLog> data) {
				LOGGER.trace("[Logger] Saving "+data.value());
				endpointLogService.save(data.value());
			}
		});
		endpointContainer = new ConcurrentMessageListenerContainer<String, EndpointLog>(consumerFactory, endpointProp);
		endpointContainer.setConcurrency(endpointLogConcurrency);
		
		ContainerProperties messageProp = new ContainerProperties("logging.message.in");
		messageProp.setMissingTopicsFatal(false);
		messageProp.setMessageListener(new MessageListener<String, MessageLog>() {

			@Override
			public void onMessage(ConsumerRecord<String, MessageLog> data) {
				LOGGER.trace("[Logger] Saving "+data.value());
				messageLogService.save(data.value());
			}
		});
		messageContainer = new ConcurrentMessageListenerContainer<String, MessageLog>(consumerFactory, messageProp);
		messageContainer.setConcurrency(messageLogConcurrency);

		ContainerProperties routeProp = new ContainerProperties("logging.route.in");
		routeProp.setMissingTopicsFatal(false);
		routeProp.setMessageListener(new MessageListener<String, RouteLog>() {

			@Override
			public void onMessage(ConsumerRecord<String, RouteLog> data) {
				LOGGER.trace("[Logger] Saving "+data.value());
				routeLogService.save(data.value());
			}
		});
		routeContainer = new ConcurrentMessageListenerContainer<String, RouteLog>(consumerFactory, routeProp);
		routeContainer.setConcurrency(routeLogConcurrency);
	}
	
	public void start() {
		endpointContainer.start();
		LOGGER.debug("[Logging] Endpoint consumer is STARTED");
		messageContainer.start();
		LOGGER.debug("[Logging] Message consumer is STARTED");
		routeContainer.start();
		LOGGER.debug("[Logging] Route consumer is STARTED");
	}
}
