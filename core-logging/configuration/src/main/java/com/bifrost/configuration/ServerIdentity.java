package com.bifrost.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bifrost.common.banner.ModuleInfo;
import com.bifrost.common.constant.ParamConstant;
import com.bifrost.system.configuration.AbstractSystemIdentity;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class ServerIdentity extends AbstractSystemIdentity {

	@Value("${"+ParamConstant.Bifrost.MODULE_CORE_LOGGING_PORT+":9992}")
	private int port;
	
	@Override
	public int getDefaultPort() {
		return port;
	}

	@Override
	public String getModuleCode() {
		return ModuleInfo.CORE_LOGGING_CODE;
	}
	
}

