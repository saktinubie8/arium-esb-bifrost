INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'NULL', '2018-01-31 16:56:15.007', true, NULL, 'ariani', '2018-09-10 09:13:18.330', 2, NULL, NULL, NULL, NULL, NULL, 'fa fa-lg fa-fw fa-table', 0, 'security', 5, '#', NULL, 'Master');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('42d3ca6f-b67b-4088-b8ad-16624c36834f', 'NULL', '2018-04-10 23:01:27.140', true, NULL, 'admin', '2018-04-10 23:01:28.427', 1, NULL, NULL, NULL, NULL, NULL, 'fa fa-lg fa-fw fa-group', 1, 'security', 5, '/telkomsigma/master/user', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'User');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('c99648a7-5008-4df1-98a6-0b161455c74a', 'NULL', '2018-02-02 17:26:27.237', true, NULL, 'admin', '2018-02-02 17:26:27.683', 1, NULL, NULL, NULL, NULL, NULL, 'fa fa-lg fa-fw fa-sitemap', 1, 'security', 2, '/telkomsigma/master/menu-group', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Menu Group Maintenance');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('d0d68291-5fa9-4ffa-a4e9-72fa41e58e55', 'admin', '2019-07-15 16:36:58.087', true, true, 'admin', '2019-07-15 16:36:58.087', 0, NULL, NULL, NULL, NULL, 'Menu List', 'fa fa-lg fa-fw fa-list', 1, 'security', 3, '/telkomsigma/master/menu', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Menu');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('da028ca7-553b-4fec-a9a9-e63dc09d28c9', 'NULL', '2018-02-15 15:53:39.783', true, true, 'admin', '2019-08-07 15:44:11.345', 2, NULL, NULL, NULL, NULL, NULL, 'fa fa-lg fa-fw fa-male', 1, 'Security', 6, '/telkomsigma/master/role', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Role');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('8a1b41d1-d6c3-4cd4-86c8-8afe9f84e5d5', 'admin', '2019-10-15 10:49:39.657', true, true, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Message Log Table', 'fa fa-lg fa-fw fa-pencil', 1, 'logging', 8, '/telkomsigma/master/messagelog', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Message Log');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('215171a4-de75-4af6-9e3e-364c2ec407bf', 'admin', '2019-10-15 14:49:25.365', true, true, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Endpoint Logging', 'fa fa-lg fa-fw fa-dot-circle-o', 1, 'logging', 7, '/telkomsigma/master/endpointlog', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Endpoint Log');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('4bd1ca2f-d3b4-45aa-90de-f31971c4a08e', 'admin', '2019-10-15 14:50:26.412', true, true, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Route Log', 'fa fa-lg fa-fw fa-gear', 1, 'logging', 9, '/telkomsigma/master/routelog', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Route Log');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('1cbed925-1b06-4d23-8e13-63e4884c6ea5', 'admin', '2019-10-15 17:23:42.999', true, true, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'System Parameters', 'fa fa-lg fa-fw fa-gear', 1, 'System Configuration', 4, '/telkomsigma/master/setup-general-parameter', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'System Parameter');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('0ffb7196-32ae-449c-bebf-9785bbbfda0f', 'admin', '2019-10-17 15:27:14.736', true, true, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Response Code', 'fa fa-lg fa-fw fa-code', 1, 'Response Code', 10, '/telkomsigma/master/response-code', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Response Code');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('1c64c5c5-069b-4c6a-aeef-67ce5e6a3b59', 'admin', '2019-10-21 17:48:59.593', true, true, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'endpoint status', 'fa fa-lg fa-fw fa-bell', 2, 'monitoring', 1, '/telkomsigma/master/endpoint-status', 'cc9bf30a-41bd-463a-9373-92cc62b51126', 'Endpoint Status');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('c592d432-c973-4a4d-b3bb-b22a28b32d0c', 'admin', '2019-10-21 17:51:00.899', true, true, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'module status', 'fa fa-lg fa-fw fa-bell-o', 2, 'monitoring', 2, '/telkomsigma/master/module-status', 'cc9bf30a-41bd-463a-9373-92cc62b51126', 'Module Status');
INSERT INTO public.m_menus
(id, created_by, created_date, is_active, is_used, modified_by, modified_date, "version", last_approved_by, last_approved_date, locked_by, locked_status, description, icon, "level", "module", "ordering", page_url, parent_id, title)
VALUES('cc9bf30a-41bd-463a-9373-92cc62b51126', 'admin', '2019-10-21 17:47:34.604', true, true, 'admin', '2019-10-21 18:46:46.368', 2, NULL, NULL, NULL, NULL, 'Monitoring', 'fa fa-lg fa-fw fa-desktop', 1, 'monitoring', 1, '/telkomsigma/master/endpoint-status', 'ddb7c0f3-8446-40f7-9085-2a6d49b68495', 'Monitoring');
