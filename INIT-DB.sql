--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: endpoint; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE endpoint (
    code character varying(10) NOT NULL,
    description character varying(255) NOT NULL,
    type character varying(10) NOT NULL
);


ALTER TABLE endpoint OWNER TO bfr_user;

--
-- Name: endpoint_kafka; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE endpoint_kafka (
    id bigint NOT NULL,
    concurrent integer,
    consume_request boolean NOT NULL,
    consumer_bootstrap character varying(255) NOT NULL,
    consumer_config character varying(255),
    consumer_container_properties character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    hmac_enabled boolean NOT NULL,
    hmac_key character varying(255),
    invocation_code character varying(40),
    producer_bootstrap character varying(255) NOT NULL,
    producer_config character varying(255),
    producer_headers character varying(255) NOT NULL,
    endpoint_code character varying(10) NOT NULL
);


ALTER TABLE endpoint_kafka OWNER TO bfr_user;

--
-- Name: endpoint_kafka_id_seq; Type: SEQUENCE; Schema: public; Owner: bfr_user
--

CREATE SEQUENCE endpoint_kafka_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE endpoint_kafka_id_seq OWNER TO bfr_user;

--
-- Name: endpoint_kafka_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bfr_user
--

ALTER SEQUENCE endpoint_kafka_id_seq OWNED BY endpoint_kafka.id;


--
-- Name: endpoint_status; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE endpoint_status (
    endpoint_code character varying(255) NOT NULL,
    node integer NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE endpoint_status OWNER TO bfr_user;

--
-- Name: endpoint_type; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE endpoint_type (
    type character varying(10) NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE endpoint_type OWNER TO bfr_user;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: bfr_user
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO bfr_user;

--
-- Name: invocation_timeout; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE invocation_timeout (
    endpoint_code character varying(20) NOT NULL,
    invocation_code character varying(40) NOT NULL,
    timeout_milis bigint,
    id bigint NOT NULL
);


ALTER TABLE invocation_timeout OWNER TO bfr_user;

--
-- Name: system_matrix; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE system_matrix (
    id bigint NOT NULL,
    host character varying(15) NOT NULL,
    node integer NOT NULL,
    module_code character varying(10) NOT NULL
);


ALTER TABLE system_matrix OWNER TO bfr_user;

--
-- Name: system_matrix_id_seq; Type: SEQUENCE; Schema: public; Owner: bfr_user
--

CREATE SEQUENCE system_matrix_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_matrix_id_seq OWNER TO bfr_user;

--
-- Name: system_matrix_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bfr_user
--

ALTER SEQUENCE system_matrix_id_seq OWNED BY system_matrix.id;


--
-- Name: system_module; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE system_module (
    code character varying(10) NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE system_module OWNER TO bfr_user;

--
-- Name: system_param; Type: TABLE; Schema: public; Owner: bfr_user; Tablespace: 
--

CREATE TABLE system_param (
    id integer NOT NULL,
    category character varying(20) NOT NULL,
    name character varying(100) NOT NULL,
    value character varying(255),
    description character varying(255),
    application character varying(20),
    profile character varying(20),
    label character varying(20)
);


ALTER TABLE system_param OWNER TO bfr_user;

--
-- Name: system_param_id_seq; Type: SEQUENCE; Schema: public; Owner: bfr_user
--

CREATE SEQUENCE system_param_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_param_id_seq OWNER TO bfr_user;

--
-- Name: system_param_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bfr_user
--

ALTER SEQUENCE system_param_id_seq OWNED BY system_param.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bfr_user
--

ALTER TABLE ONLY endpoint_kafka ALTER COLUMN id SET DEFAULT nextval('endpoint_kafka_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bfr_user
--

ALTER TABLE ONLY system_matrix ALTER COLUMN id SET DEFAULT nextval('system_matrix_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bfr_user
--

ALTER TABLE ONLY system_param ALTER COLUMN id SET DEFAULT nextval('system_param_id_seq'::regclass);


--
-- Data for Name: endpoint; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY endpoint (code, description, type) FROM stdin;
\.


--
-- Data for Name: endpoint_kafka; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY endpoint_kafka (id, concurrent, consume_request, consumer_bootstrap, consumer_config, consumer_container_properties, description, hmac_enabled, hmac_key, invocation_code, producer_bootstrap, producer_config, producer_headers, endpoint_code) FROM stdin;
\.


--
-- Name: endpoint_kafka_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bfr_user
--

SELECT pg_catalog.setval('endpoint_kafka_id_seq', 1, false);


--
-- Data for Name: endpoint_status; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY endpoint_status (endpoint_code, node, status) FROM stdin;
\.


--
-- Data for Name: endpoint_type; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY endpoint_type (type, description) FROM stdin;
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: bfr_user
--

SELECT pg_catalog.setval('hibernate_sequence', 1, false);


--
-- Data for Name: invocation_timeout; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY invocation_timeout (endpoint_code, invocation_code, timeout_milis, id) FROM stdin;
\.


--
-- Data for Name: system_matrix; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY system_matrix (id, host, node, module_code) FROM stdin;
1	localhost	1	KFK_ENP
\.


--
-- Name: system_matrix_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bfr_user
--

SELECT pg_catalog.setval('system_matrix_id_seq', 1, true);


--
-- Data for Name: system_module; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY system_module (code, description) FROM stdin;
COR_LGC	Core Logic
ISO_ENP	Generic ISO8583 Endpoint Adapter
KFK_ENP	Generic Kafka Endpoint Adapter
OBJ_TRN	Object Translator
RST_ENP	Generic REST Endpoint Adapter
SVC_LOG	Generic SOAP Endpoint Adapter
SOP_ENP	Core Logic
\.


--
-- Data for Name: system_param; Type: TABLE DATA; Schema: public; Owner: bfr_user
--

COPY system_param (id, category, name, value, description, application, profile, label) FROM stdin;
1	System	bifrost.module.port.min	9990	Minimal open port range for all modules	bifrost	default	master
2	System	bifrost.module.port.max	9999	Maximal open port range for all modules	bifrost	default	master
3	System	bifrost.config.server.host	localhost	Bifrost config server host adress	bifrost	default	master
4	System	bifrost.config.server.port	8888	Bifrost config server port adress	bifrost	default	master
5	Database	database.cfg.host	localhost	System configuration database host location	bifrost	default	master
6	Database	database.cfg.port	5432	System configuration database port location	bifrost	default	master
7	Database	database.cfg.name	bifrost-config	System configuration database name	bifrost	default	master
8	Database	database.cfg.user	bfr_user	System configuration database username	bifrost	default	master
9	Database	database.cfg.pass	bfr_pass	System configuration database password	bifrost	default	master
10	Database	database.log.host	localhost	System logging database host location	bifrost	default	master
11	Database	database.log.port	5432	System logging database port location	bifrost	default	master
12	Database	database.log.name	bifrost-log	System logging database name	bifrost	default	master
13	Database	database.log.user	bfr_user	System logging database username	bifrost	default	master
14	Database	database.log.pass	bfr_pass	System logging database password	bifrost	default	master
15	Database	database.gui.host	localhost	System user interface database host location	bifrost	default	master
16	Database	database.gui.port	5432	System user interface database port location	bifrost	default	master
17	Database	database.gui.name	bifrost-gui	System user interface database name	bifrost	default	master
18	Database	database.gui.user	bfr_user	System user interface database username	bifrost	default	master
19	Database	database.gui.pass	bfr_pass	System user interface database password	bifrost	default	master
20	Thread	thread.pool.size.core	100	Initial system thread pool open	bifrost	default	master
21	Thread	thread.pool.size.max	5000	Maximal system thread pool open	bifrost	default	master
22	Thread	thread.pool.keep-alive-seconds	300	Thread will off after idle in seconds	bifrost	default	master
23	Thread	thread.pool.queue-capacity	1000000	Quota for waiting threads which will be processed	bifrost	default	master
24	Thread	thread.pool.wait-task-on-shutdown	true	Complete all threads  before shutting down	bifrost	default	master
25	Hazelcast	hazelcast.server.instance.name	bifrost	Hazelcast server name	bifrost	default	master
26	Hazelcast	hazelcast.server.group.name	bifrost	Hazelcast server group name	bifrost	default	master
27	Hazelcast	hazelcast.server.management-center.enabled	false	Enable/disable hazelcast management GUI	bifrost	default	master
28	Hazelcast	hazelcast.server.management-center.url	http://localhost:9191/hazelcast-mancenter	URL for hazelacast management GUI	bifrost	default	master
29	Hazelcast	hazelcast.server.join.mode	multicast	Node searching, tcpip/multicast(for cluster purpose)	bifrost	default	master
30	Hazelcast	hazelcast.server.multicast.group	224.2.2.3	Multicast group IP(required if join mode is 'multicast')	bifrost	default	master
31	Hazelcast	hazelcast.server.multicast.port	54327	Multicast group port(required if join mode is 'multicast')	bifrost	default	master
32	Hazelcast	hazelcast.server.tcpip.members	\N	List host of hazelcast server node IP, delimited by comma(required if join mode is 'tcpip')	bifrost	default	master
33	Hazelcast	hazelcast.server.network.port	5701	Hazelcast open port	bifrost	default	master
34	Hazelcast	hazelcast.server.network.port.auto-increment	true	If current port is used, hazelcast will try other port automatically	bifrost	default	master
35	Hazelcast	hazelcast.server.network.port.count	100	Maximal count for automatic port searching	bifrost	default	master
36	Hazelcast	hazelcast.client.server.members	localhost:5701	Hazelcast server list [host:port], delimited by comma	bifrost	default	master
37	Hazelcast	hazelcast.client.smart.routing	true	Cache fetching mechanism	bifrost	default	master
38	Kafka	kafka.server.address-list	localhost:9092	List of kafka server, delimited by comma	bifrost	default	master
39	Kafka	kafka.consumer.auto-offset-reset	earliest	What to do when there is no initial offset or current offset on the server	bifrost	default	master
40	Kafka	kafka.topic.group-id	ragnarok	Kafka topic's group ID	bifrost	default	master
41	Kafka	kafka.topic.partitions	1	Kafka topic's log partitions	bifrost	default	master
42	Kafka	kafka.topic.replication	2	Topic partitions replication factor across multiple nodes for failover(2 or 3)	bifrost	default	master
99	Kafka	spring.jpa.hibernate.ddl-auto	update	ddl auto	bifrost	default	master
97	Kafka	org.springframework.cache	TRACE	show sql	bifrost	default	master
\.


--
-- Name: system_param_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bfr_user
--

SELECT pg_catalog.setval('system_param_id_seq', 5, true);


--
-- Name: endpoint_kafka_pkey; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY endpoint_kafka
    ADD CONSTRAINT endpoint_kafka_pkey PRIMARY KEY (id);


--
-- Name: endpoint_pkey; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY endpoint
    ADD CONSTRAINT endpoint_pkey PRIMARY KEY (code);


--
-- Name: endpoint_status_pkey; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY endpoint_status
    ADD CONSTRAINT endpoint_status_pkey PRIMARY KEY (endpoint_code, node);


--
-- Name: endpoint_type_pkey; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY endpoint_type
    ADD CONSTRAINT endpoint_type_pkey PRIMARY KEY (type);


--
-- Name: invocation_timeout_pkey; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY invocation_timeout
    ADD CONSTRAINT invocation_timeout_pkey PRIMARY KEY (endpoint_code, invocation_code);


--
-- Name: system_matrix_pkey; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY system_matrix
    ADD CONSTRAINT system_matrix_pkey PRIMARY KEY (id);


--
-- Name: system_module_pkey; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY system_module
    ADD CONSTRAINT system_module_pkey PRIMARY KEY (code);


--
-- Name: system_param_pk; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY system_param
    ADD CONSTRAINT system_param_pk PRIMARY KEY (id);


--
-- Name: uk1vqcseuni92935oaovt18f60c; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY system_matrix
    ADD CONSTRAINT uk1vqcseuni92935oaovt18f60c UNIQUE (node, module_code);


--
-- Name: uk7s3r1xe9dv9t3wl206m4dlsh1; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY invocation_timeout
    ADD CONSTRAINT uk7s3r1xe9dv9t3wl206m4dlsh1 UNIQUE (endpoint_code, invocation_code);


--
-- Name: uk_agc729eadffku0i5vff7vwr24; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY endpoint_status
    ADD CONSTRAINT uk_agc729eadffku0i5vff7vwr24 UNIQUE (endpoint_code);


--
-- Name: ukmx5eg2aglcrhpfbj4k2c43n9x; Type: CONSTRAINT; Schema: public; Owner: bfr_user; Tablespace: 
--

ALTER TABLE ONLY system_matrix
    ADD CONSTRAINT ukmx5eg2aglcrhpfbj4k2c43n9x UNIQUE (host, module_code);


--
-- Name: fk75ilim1c5a48pspuvgxrw2x9u; Type: FK CONSTRAINT; Schema: public; Owner: bfr_user
--

ALTER TABLE ONLY endpoint_kafka
    ADD CONSTRAINT fk75ilim1c5a48pspuvgxrw2x9u FOREIGN KEY (endpoint_code) REFERENCES endpoint(code);


--
-- Name: fkejr1rbca2eurrojfegg0ea3i5; Type: FK CONSTRAINT; Schema: public; Owner: bfr_user
--

ALTER TABLE ONLY endpoint_status
    ADD CONSTRAINT fkejr1rbca2eurrojfegg0ea3i5 FOREIGN KEY (endpoint_code) REFERENCES endpoint(code);


--
-- Name: fkemcjk6d696hioa6y4s9j3a6n0; Type: FK CONSTRAINT; Schema: public; Owner: bfr_user
--

ALTER TABLE ONLY system_matrix
    ADD CONSTRAINT fkemcjk6d696hioa6y4s9j3a6n0 FOREIGN KEY (module_code) REFERENCES system_module(code);


--
-- Name: fkr03g61ah6nvmf0skuxapspc9; Type: FK CONSTRAINT; Schema: public; Owner: bfr_user
--

ALTER TABLE ONLY endpoint
    ADD CONSTRAINT fkr03g61ah6nvmf0skuxapspc9 FOREIGN KEY (type) REFERENCES endpoint_type(type);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

