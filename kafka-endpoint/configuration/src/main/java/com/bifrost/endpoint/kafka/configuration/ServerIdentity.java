package com.bifrost.endpoint.kafka.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.system.configuration.AbstractSystemIdentity;
import com.bifrost.system.model.SystemModule;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class ServerIdentity implements AbstractSystemIdentity {

	@Value("${"+ParamConstant.Bifrost.MODULE_ENDPOINT_KAFKA_PORT+":9993}")
	private String port;
	
	public String getDefaultPort() {
		return port;
	}

	public String getModuleCode() {
		return SystemModule.MODULE_KAFKA_ENDPOINT;
	}
	
}
