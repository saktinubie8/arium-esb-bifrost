package com.bifrost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bifrost.common.banner.BannerInfo;
import com.bifrost.common.banner.ModuleInfo;

@SpringBootApplication
public class Application extends BaseApplication {

	public static void main(String[] args) {
		app = new SpringApplication(Application.class);
		app.setBanner(new BannerInfo(ModuleInfo.ENDPOINT_KAFKA, ModuleInfo.VERSION));
		context = app.run(args);
	}
}
