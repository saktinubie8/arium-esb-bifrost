package com.bifrost.endpoint.kafka;

import org.springframework.expression.ExpressionParser;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.messaging.support.MessageHeaderAccessor;

import com.bifrost.common.util.SpELUtils;
import com.bifrost.endpoint.dispatcher.AsynchronousDispatcher;
import com.bifrost.endpoint.kafka.model.EndpointKafka;
import com.bifrost.endpoint.kafka.service.EndpointKafkaService;
import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */

public class Dispatcher implements AsynchronousDispatcher {

	private ExpressionParser expressionParser;
	private KafkaTemplate<String, Object> template;
	private EndpointKafkaService endpointKafkaService;

	public Dispatcher setEndpointKafkaService(EndpointKafkaService endpointKafkaService) {
		this.endpointKafkaService = endpointKafkaService;
		return this;
	}

	public Dispatcher setExpressionParser(ExpressionParser expressionParser) {
		this.expressionParser = expressionParser;
		return this;
	}

	public Dispatcher setTemplate(KafkaTemplate<String, Object> template) {
		this.template = template;
		return this;
	}

	public void send(Object obj, ExternalMessage externalMessage) {
		/*
		 * SpEL example for producer_args
		 * setHeader(KafkaHeaders.TOPIC, 'topic's name');
		 * setHeader(KafkaHeaders.MESSAGE_KEY, 'key');
		 * setHeader(KafkaHeaders.PARTITION_ID, 0);
		 * deilimited by semicolon
		 */
		MessageHeaderAccessor accessor = new MessageHeaderAccessor();
		EndpointKafka kafka = endpointKafkaService.findByInvocationCode(externalMessage.getInvocationCode());
		for (String exp : kafka.getProducerHeaders().split(";"))
			expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(exp, true)).getValue(accessor);
		Message<Object> message = MessageBuilder
				.withPayload(obj)
				.setHeaders(accessor)
				.build();
		template.send(message);
	}

}
