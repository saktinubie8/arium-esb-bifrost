package com.bifrost.endpoint.kafka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.support.TopicPartitionInitialOffset;
import org.springframework.stereotype.Component;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.common.pojo.GenericResponse;
import com.bifrost.common.serialization.ObjectDeserializer;
import com.bifrost.common.serialization.ObjectSerializer;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.endpoint.AbstractController;
import com.bifrost.endpoint.EndpointProcess;
import com.bifrost.endpoint.kafka.model.EndpointKafka;
import com.bifrost.endpoint.kafka.service.EndpointKafkaService;
import com.bifrost.endpoint.message.EndpointServiceResponse;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.model.EndpointType;
import com.bifrost.endpoint.service.EndpointService;
import com.bifrost.endpoint.service.EndpointStatusManager;
import com.bifrost.exception.endpoint.InvalidContainerPropertiesClassException;
import com.bifrost.system.service.SystemMatrixService;
import com.hazelcast.core.HazelcastInstance;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class Controller implements AbstractController {

	public String node;
	private static final Log LOGGER = LogFactory.getLog(Controller.class);
	private static Map<String, Map<Long, ConcurrentMessageListenerContainer<String, Object>>> endpoints = new HashMap<String, Map<Long, ConcurrentMessageListenerContainer<String,Object>>>();
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private PackageLoader packageLoader;
	
	@Autowired
	private EndpointService endpointService;
	
	@Autowired
	private ExpressionParser expressionParser;
	
	@Autowired
	protected HazelcastInstance hazelcastInstance;
	
	@Autowired
	private EndpointKafkaService endpointKafkaService;
	
	@Autowired
	protected SystemMatrixService systemMatrixService;
	
	@Autowired
	protected EndpointStatusManager endpointStatusManager;

	@Value("${" + ParamConstant.Kafka.TOPIC_GROUP_ID + "}")
	private String groupId;

	@Value("${" + ParamConstant.Kafka.TOPIC_PARTITIONS + "}")
	private String topicPartitions;
	
	@Value("${" + ParamConstant.Kafka.TOPIC_REPLICATION + "}")
	private String topicReplication;

	@Value("${" + ParamConstant.Kafka.CONSUMER_PARTITION_ASSIGNMENT_STRATEGY + ":org.apache.kafka.clients.consumer.RoundRobinAssignor}")
	private String partAssignStrategy;
	
	@PostConstruct
	public void init() {
		node = systemMatrixService.getThisSystemNode()+"";
		reloadAll();
	}
	
	@PreDestroy
	public void destroy() {
		stopAll();
	}

	@Override
	public EndpointServiceResponse start(String endpointCode) {
		if(endpoints.get(endpointCode)==null)
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), "No endpoint records");
		
		// Start all kafka service
		for(Map.Entry<Long, ConcurrentMessageListenerContainer<String, Object>> entry : endpoints.get(endpointCode).entrySet()) {
			ConcurrentMessageListenerContainer<String, Object> kafka = entry.getValue();
			if(!kafka.isRunning())
				kafka.start();
		}
		endpointStatusManager.setOnline(endpointCode, node, "Endpoint started successfully");
		return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());
	}

	@Override
	public EndpointServiceResponse stop(String endpointCode) {
		if(endpoints.get(endpointCode)==null)
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), "No endpoint records");
		
		// Stop all kafka service
		for(Map.Entry<Long, ConcurrentMessageListenerContainer<String, Object>> entry : endpoints.get(endpointCode).entrySet()) {
			ConcurrentMessageListenerContainer<String, Object> kafka = entry.getValue();
			if(kafka.isRunning())
				kafka.stop();
		}
		endpointStatusManager.setOffline(endpointCode, node, "Endpoint stopped successfully");
		return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());
	}

	@Override
	public EndpointServiceResponse restart(String endpointCode) {
		EndpointServiceResponse response = stop(endpointCode);
		if(response.getCode().equals(GenericResponse.SUCCESS.getCode()))
			response = start(endpointCode);
		else
			return response;
		return response;
	}
	
	@Override
	public EndpointServiceResponse reload(String endpointCode) {
		if(endpoints.get(endpointCode)!=null) {
			EndpointServiceResponse response = stop(endpointCode);
			if(!response.getCode().equals(GenericResponse.SUCCESS.getCode()))
				return response;
		}
		try {
			loadPackager(endpointCode);
			loadEndpoint(endpointCode);
		} catch (Exception e) {
			endpointStatusManager.setError(endpointCode, node, "Error when trying to load configuration, cause by: "+e.getMessage());
			e.printStackTrace();
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.appendDesc(", cause by: "+e.getMessage()).getDesc());
		}
		return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());
	}

	@Override
	public List<EndpointServiceResponse> startAll() {
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Map.Entry<String, Map<Long, ConcurrentMessageListenerContainer<String, Object>>> endpoint : endpoints.entrySet())
			listResponse.add(start(endpoint.getKey()));
		return listResponse;
	}

	@Override
	public List<EndpointServiceResponse> stopAll() {
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Map.Entry<String, Map<Long, ConcurrentMessageListenerContainer<String, Object>>> endpoint : endpoints.entrySet())
			listResponse.add(stop(endpoint.getKey()));
		return listResponse;
	}

	@Override
	public List<EndpointServiceResponse> restartAll() {
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Map.Entry<String, Map<Long, ConcurrentMessageListenerContainer<String, Object>>> endpoint : endpoints.entrySet())
			listResponse.add(restart(endpoint.getKey()));
		return listResponse;
	}

	@Override
	public List<EndpointServiceResponse> reloadAll() {
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : endpointService.findByType(EndpointType.KAFKA))
			listResponse.add(reload(endpoint.getCode()));
		return listResponse;
	}
	
	public void loadPackager(String endpointCode) throws Exception {
		packageLoader.load(EndpointType.KAFKA, endpointCode, node);
	}
	
	private void loadEndpoint(String endpointCode) throws InvalidContainerPropertiesClassException {
		Endpoint endpoint = endpointService.findById(endpointCode);
		EndpointProcess process = new EndpointProcess(endpoint, node, context);

		Map<Long, ConcurrentMessageListenerContainer<String, Object>> kafkaMap = new HashMap<Long, ConcurrentMessageListenerContainer<String,Object>>();
		for(EndpointKafka kafka : endpointKafkaService.findByEndpointCode(endpointCode))
			kafkaMap.put(kafka.getId(), loadKafka(kafka, process));
			
		// Set endpoint status to online
		endpointStatusManager.setOnline(endpointCode, node, "Endpoint loaded and started successfully");
		kafkaMap.put(-1L, process.getListener());
		endpoints.put(endpointCode, kafkaMap);

		LOGGER.trace("["+endpointCode+"] ====================================================================================================================================================================================");
		LOGGER.info("["+endpointCode+"] Endpoint IS REGISTERED successfully");
		LOGGER.trace("["+endpointCode+"] ====================================================================================================================================================================================");
	}

	private ConcurrentMessageListenerContainer<String, Object> loadKafka(EndpointKafka kafka, EndpointProcess process) throws InvalidContainerPropertiesClassException {		
		String consumerServer = kafka.getConsumerBootstrap();
		String consumerConfig = kafka.getConsumerConfig();
		String consumerConProps = kafka.getConsumerContainerProperties();
		String producerServer = kafka.getProducerBootstrap();
		String producerConfig = kafka.getProducerConfig();
		String endpointCode = kafka.getEndpoint().getCode();
		String invocationCode = kafka.getInvocationCode().toLowerCase();
		String assocKeyField = kafka.getAssocKeyField();
		Boolean isReceiver = kafka.getConsumeRequest();
		
		// Load configuration for kafka producer server lookup
		Map<String, Object> producerProps = new HashMap<String, Object>();
		producerProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, producerServer);
		producerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		producerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ObjectSerializer.class);
		
		// Add producer config using SpEL
		if(producerConfig!=null && !producerConfig.trim().isEmpty())
			for(String exp : producerConfig.split(";")) {
				exp = exp.trim();
				if(exp.isEmpty())
					continue;
				exp = SpELUtils.addPackagerOnSpEL(exp, true);
				LOGGER.debug("["+endpointCode+"][ProducerConfig] Applying expression: "+exp);
				expressionParser.parseExpression(exp).getValue(producerProps);
			}
						
		ProducerFactory<String, Object> producerFactory = new DefaultKafkaProducerFactory<String, Object>(producerProps);
		final KafkaTemplate<String, Object> endpointTemplate = new KafkaTemplate<String, Object>(producerFactory);
		Dispatcher dispatcher = new Dispatcher()
				.setEndpointKafkaService(endpointKafkaService)
				.setExpressionParser(expressionParser)
				.setTemplate(endpointTemplate);
		process.setAsyncDispatcher(dispatcher);
		process.setAssocKeyField(assocKeyField);
		LOGGER.info("["+endpointCode+"] Producer ID: "+kafka.getId()+", broker: "+producerServer+", IS REGISTERED");
		
		/*
		 * Load configuration for kafka consumer server lookup
		 * BOOTSTRAP_SERVERS_CONFIG value format is [IP]:[port],
		 * between address delimited by comma
		 * Example for BOOTSTRAP_SERVERS_CONFIG: localhost:9092,172.0.0.1:9092
		 */
		Map<String, Object> consumerProps = new HashMap<String, Object>();
		consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, consumerServer);
		consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ObjectDeserializer.class);
		consumerProps.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, partAssignStrategy);
		
		// Add consumer config using SpEL
		if(consumerConfig!=null && !consumerConfig.trim().isEmpty())
			for(String exp : consumerConfig.split(";")) {
				exp = exp.trim();
				if(exp.isEmpty())
					continue;
				exp = SpELUtils.addPackagerOnSpEL(exp, true);
				LOGGER.debug("["+endpointCode+"][ConsumerConfig] Applying expression: "+exp);
				expressionParser.parseExpression(exp).getValue(consumerProps);
			}
		
		ConsumerFactory<String, Object> consumerFactory = new DefaultKafkaConsumerFactory<String, Object>(consumerProps);

		/*
		 * Set consumer topic using SpEL
		 * SpEL example for consumer args:
		 * new String('topic 1'); new String('topic 2');
		 * new TopicPartitionInitialOffset('topic 1', 1, 1, true)
		 */
		List<Object> args = new ArrayList<Object>();
		for(String exp : consumerConProps.split(";")) {
			exp = exp.trim();
			if(exp.isEmpty())
				continue;
			else
				args.add(expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(exp, false)).getValue(Object.class));
		}
			
		ContainerProperties containerProps;
		// Assign to array of TopicPartitionInitialOffset if expresssions contains TopicPartitionInitialOffset
		if(consumerConProps.contains("TopicPartitionInitialOffset")) 
			containerProps = new ContainerProperties(args.toArray(new TopicPartitionInitialOffset[0]));
		// Assign to array of String if expressions contains String
		else if(consumerConProps.contains("String"))
			containerProps = new ContainerProperties(args.toArray(new String[0]));
		// Skip initiation if else
		else {
			LOGGER.error("["+endpointCode+"] Unsupported container properties class in consumer SpEL, stopping endpoint initiation...");
			throw new InvalidContainerPropertiesClassException(consumerConProps);
		}
		
		containerProps.setMessageListener(new MessageListener<String, Object>() {
			
			/*
			 * Every time consumer receive a message, this method is executed(non-Javadoc)
			 * @see org.springframework.kafka.listener.GenericMessageListener#onMessage(java.lang.Object),
			 * Association key is unique per message
			 */
			public void onMessage(ConsumerRecord<String, Object> data) {
				
				// For incoming request fron outside
				if(isReceiver)
					process.receive(data.value(), invocationCode, true);
					
				// For incoming response from outside								
				else
					process.receive(data.value(), invocationCode, false);
			}
		});
		ConcurrentMessageListenerContainer<String, Object> container = new ConcurrentMessageListenerContainer<String, Object>(consumerFactory, containerProps);
		
		// Kafka will create container as many as concurrent value
		container.setConcurrency(kafka.getConcurrent());
		container.start();
		LOGGER.info("["+endpointCode+"] Consumer ID: "+kafka.getId()+", broker: "+consumerServer+", container-props: '"+consumerConProps+"', IS REGISTERED");
		return container;
	}
}
