package com.bifrost.endpoint.kafka;

import org.springframework.web.bind.annotation.RestController;

import com.bifrost.endpoint.access.GenericEndpointAccessSpringRest;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@RestController
public class EndpointAccess extends GenericEndpointAccessSpringRest {

}
