package com.bifrost.endpoint.kafka;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.nucleus8583.core.MessageSerializer;
import org.springframework.stereotype.Component;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.endpoint.packager.AbstractPackageLoader;
import com.bifrost.exception.endpoint.UnsupportedPackagerException;


/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class PackageLoader extends AbstractPackageLoader {
	
	@Override
	public void process(String endpointCode, Entry<String, byte[]> entry) throws UnsupportedPackagerException, JDOMException, IOException {
		/*
		 * File is not created physically, needed only for file name attribute
		 */
		File file = new File(entry.getKey());
		ByteArrayInputStream packageStream = new ByteArrayInputStream(entry.getValue());
		String fileName = file.getName().split("\\.")[0];
		
		if (file.getName().endsWith(PathConstant.Packager.XML_EXT)) {
			Element schema = new SAXBuilder().build(packageStream).getRootElement();
			String parentTagName = schema.getName();
			// Will make packager object as org.jdom2.Element object if first tag is '<schema>'
			if(parentTagName.equalsIgnoreCase(PathConstant.Packager.FIXED_PARENT_TAG))
				packagers.put(fileName, schema);
			
			// Will make packager object as org.nucleus8583.core.Iso8583MessageSerializer object if first tag is '<iso-message>'
			else if(schema.getName().equalsIgnoreCase(PathConstant.Packager.ISO8583_PARENT_TAG))
				packagers.put(fileName, MessageSerializer.create(packageStream));
				
			// Skip packager fetching process if unknown parent tag exists
			else {
				LOGGER.error("[" + endpointCode + "] Unknown parent tag, '<" + parentTagName + ">'");
				throw new UnsupportedPackagerException("Unknown parent tag, '<" + parentTagName + ">'");
			}
		} else {
			LOGGER.error("[" + endpointCode + "] Unsupported extension file, '" + file.getName() + "'");
			throw new UnsupportedPackagerException("Unsupported extension file, '" + file.getName() + "'");
		}
		LOGGER.trace("[" + endpointCode + "] '" + file.getName() + "' is loaded");
		packageStream.close();
	}
}
