package com.bifrost.endpoint.kafka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Element;
import org.nucleus8583.core.MessageSerializer;
import org.springframework.stereotype.Component;

import com.bifrost.endpoint.AbstractCodec;
import com.bifrost.exception.endpoint.InvalidInputClassType;
import com.bifrost.exception.endpoint.InvalidSchemaFieldType;
import com.bifrost.exception.endpoint.InvalidSchemaTagType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class Codec implements AbstractCodec {

	public static final String XML_TYPE_STRING = "str";
	public static final String XML_TYPE_NUMERIC = "num";
	public static final String XML_FIELD = "field";
	public static final String XML_ARRAY = "array";

	public Object encode(Map<String, Object> payload, Object packager) throws InvalidSchemaFieldType, InvalidSchemaTagType, JsonProcessingException {
		// For fixed length type message
		if(packager instanceof Element) {
			String retVal = "";
			Element schema = (Element) packager;
			for(Element child : schema.getChildren()) {
				switch(child.getName()) {
				case XML_FIELD : {
					writeField(child, payload, retVal);
					break;
				}
				case XML_ARRAY : {
					String id = child.getAttributeValue("id").trim();
					int size = (child.getAttributeValue("size")!=null)?Integer.parseInt(child.getAttributeValue("size")):0;
					if(size<1 && child.getAttributeValue("size-ref")!=null)
						size = Integer.parseInt((String)payload.get(child.getAttributeValue("size-ref")));
					if(size<1)
						continue;
					
					@SuppressWarnings("unchecked")
					List<Map<String, Object>> arrayList = (List<Map<String, Object>>) payload.get(id);
					for(int i=0;i<size;i++)
						for(Element children : child.getChildren())
							writeField(children, arrayList.get(i), retVal);
					break;
				}
				default : throw new InvalidSchemaTagType(child.getName());
				}
			}
			return retVal;
		// For ISO8583 type message
		}else if(packager instanceof MessageSerializer) {
			return null;
		}else {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(payload);
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> decode(Object obj, Object packager) throws InvalidInputClassType, InvalidSchemaFieldType, InvalidSchemaTagType {
		// For fixed width type message
		if(packager instanceof Element) {
			String raw;
			Map<String, Object> payload = new HashMap<String, Object>();
			if(obj instanceof String)
				raw = String.valueOf(obj);
			else
				throw new InvalidInputClassType("Object param class is not a String");
			Element schema = (Element) packager;
			for(Element child : schema.getChildren()) {
				switch(child.getName()) {
				case XML_FIELD : {
					parseField(child, raw, payload);
					break;
				}
				case XML_ARRAY : {
					String id = child.getAttributeValue("id").trim();
					int size = (child.getAttributeValue("size")!=null)?Integer.parseInt(child.getAttributeValue("size")):0;
					if(size<1 && child.getAttributeValue("size-ref")!=null)
						size = Integer.parseInt((String)payload.get(child.getAttributeValue("size-ref")));
					if(size<1)
						continue;
					List<Map<String, Object>> arrayList = new ArrayList<Map<String, Object>>();
					for(int i=0;i<size;i++) {
						Map<String, Object> arrayValue = new HashMap<String, Object>(); 
						for(Element children : child.getChildren())
							parseField(children, raw, arrayValue);
						arrayList.add(i, arrayValue);
					}
					payload.put(id, arrayList);
					break;
				}
				default : throw new InvalidSchemaTagType(child.getName());
				}
			}
			return payload;
		// For ISO8583 type message
		}else if(packager instanceof MessageSerializer) {
			return null;
		}else {
			Map<String, Object> payload = new HashMap<String, Object>();
			ObjectMapper mapper = new ObjectMapper();
			payload.putAll((Map<? extends String, ? extends Object>) mapper.convertValue(obj, new TypeReference<Map<String, Object>>(){}));
			return payload;
		}
	}
	
	private static void parseField(Element elm, String raw, Map<String, Object> retVal) throws InvalidSchemaFieldType {
		String id = elm.getAttributeValue("id").trim();
		int length = Integer.parseInt (elm.getAttributeValue("length"));
		String type = elm.getAttributeValue("type").trim().toLowerCase();
		Boolean trim = (elm.getAttributeValue("trim")!=null)?Boolean.valueOf(elm.getAttributeValue("trim")):false;
		String value = raw.substring(0, length);
		raw = raw.substring(length);
		if(trim && type.equals(XML_TYPE_NUMERIC))
			value = value.replaceFirst("^0+(?!$)", "");
		else if(trim && type.equals(XML_TYPE_STRING))
			value = value.replaceAll("\\s+$", "");
		else
			throw new InvalidSchemaFieldType(id);
		retVal.put(id, value);
	}

	private static void writeField(Element elm, Map<String, Object> data, String retVal) throws InvalidSchemaFieldType {
		String id = elm.getAttributeValue("id").trim();
		int length = Integer.parseInt (elm.getAttributeValue("length"));
		String type = elm.getAttributeValue("type").trim().toLowerCase();
		String value = "";
		if(data.get(id)!=null)
			value = String.valueOf(data.get(id));
		if(type.equals(XML_TYPE_NUMERIC))
			retVal += StringUtils.leftPad(value, length, '0').substring(value.length()-length, value.length());
		else if(type.equals(XML_TYPE_STRING))
			retVal += StringUtils.leftPad(value, length, ' ').substring(value.length()-length, value.length());
		else
			throw new InvalidSchemaFieldType(id);
	}

	@Override
	public Object encodeUnregisteredInvocation(Object packager) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
