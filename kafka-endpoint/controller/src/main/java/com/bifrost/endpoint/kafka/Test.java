package com.bifrost.endpoint.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@RestController
public class Test {

	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	@Autowired
	private ThreadPoolTaskExecutor thread;
	
	@RequestMapping(path = "/test")
	public String test() throws InterruptedException {
		for(int i=0;i<50;i++) {
			int x = i;
			thread.createThread(new Runnable() {
				
				@Override
				public void run() {
					IMap<String, Long> map = hazelcastInstance.getMap("test");
					System.out.println(map.size());
					map.put(x+"", new Long(x));
				}
			}).run();
		}
		return "OK";
	}
}
