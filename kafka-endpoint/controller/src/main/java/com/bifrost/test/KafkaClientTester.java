package com.bifrost.test;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import com.bifrost.common.constant.CacheName;
import com.bifrost.common.serialization.ObjectSerializer;
import com.hazelcast.core.HazelcastInstance;

/**
 * @author rosaekapratama@gmail.com
 *
 */
//@Component
public class KafkaClientTester {
	
	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void init() {
		ExpressionParser parser = new SpelExpressionParser();
		StandardEvaluationContext context = new StandardEvaluationContext();
		Object obj = hazelcastInstance.getMap(CacheName.BEAN_LIBRARY_CACHE).get("library-common-component-0.0.1-SNAPSHOT.Test");
		context.setVariable("test", obj);
		Class cls;
		try {
			cls = Class.forName("com.bifrost.library.Test");
			if(obj.getClass().equals(cls)) {
				System.out.println("TINUTINUT");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(parser.parseExpression("#test.testaja('OKAY NIH')").getValue(context));
	}
	/*
	private static KafkaMessageListenerContainer<Integer, String> createContainer(ContainerProperties containerProps) {
		Map<String, Object> props = consumerProps();
		DefaultKafkaConsumerFactory<Integer, String> cf = new DefaultKafkaConsumerFactory<Integer, String>(props);
		KafkaMessageListenerContainer<Integer, String> container = new KafkaMessageListenerContainer<>(cf,
				containerProps);
		return container;
	}
*/
	private static KafkaTemplate<String, Object> createTemplate() {
		Map<String, Object> senderProps = senderProps();
		ProducerFactory<String, Object> pf = new DefaultKafkaProducerFactory<String, Object>(senderProps);
		KafkaTemplate<String, Object> template = new KafkaTemplate<>(pf);
		return template;
	}
/*
	private static Map<String, Object> consumerProps() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "ragnarok");
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "100");
		props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "15000");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		return props;
	}
*/
	private static Map<String, Object> senderProps() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ObjectSerializer.class);
		return props;
	}
	
	public static void main(String[] args) {
	    /*
		ContainerProperties containerProps = new ContainerProperties("topic1", "topic2");
	    final CountDownLatch latch = new CountDownLatch(4);
	    containerProps.setMessageListener(new MessageListener<Integer, String>() {

	        @Override
	        public void onMessage(ConsumerRecord<Integer, String> message) {
	            logger.info("received: " + message);
	            latch.countDown();
	        }

	    });
	    KafkaMessageListenerContainer<String, Object> container = createContainer(containerProps);
	    container.setBeanName("testAuto");
	    container.start();
	    Thread.sleep(1000); // wait a bit for the container to start
	    */
	    KafkaTemplate<String, Object> template = createTemplate();
	    template.setDefaultTopic("test-consume");
	    //BaseRequest request = new BaseRequest();
	    //request.setRetrievalReferenceNumber("123");
	    //request.setTransmissionDateAndTime("12345");
	    //template.send("test-consume", request);
	    template.flush();
	    //assertTrue(latch.await(60, TimeUnit.SECONDS));
	    //container.stop();
	}
}
