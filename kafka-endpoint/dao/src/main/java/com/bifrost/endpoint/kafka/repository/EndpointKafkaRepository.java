package com.bifrost.endpoint.kafka.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.kafka.model.EndpointKafka;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointKafkaRepository extends JpaRepository<EndpointKafka, String>{

	public EndpointKafka findByInvocationCode(String invocationCode);
	public List<EndpointKafka> findByEndpoint_Code(String endpointCode);
	
}
