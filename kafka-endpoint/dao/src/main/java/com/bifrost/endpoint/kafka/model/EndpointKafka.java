package com.bifrost.endpoint.kafka.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bifrost.common.model.GenericModel;
import com.bifrost.endpoint.model.Endpoint;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint_kafka")
public class EndpointKafka implements GenericModel {
	private static final long serialVersionUID = 730290610271711395L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "endpoint_code", nullable = false)
	private Endpoint endpoint;

	@Column(name = "consume_request", nullable = false)
	private Boolean consumeRequest;

	@Column(name = "assoc_key_field", nullable = false, length = 40)
	private String assocKeyField;

	@Column(name = "producer_bootstrap", nullable = false)
	private String producerBootstrap;

	@Column(name = "producer_config", nullable = true)
	private String producerConfig;

	@Column(name = "producer_headers", nullable = false)
	private String producerHeaders;

	@Column(name = "consumer_bootstrap", nullable = false)
	private String consumerBootstrap;

	@Column(name = "consumer_config", nullable = true)
	private String consumerConfig;

	@Column(name = "consumer_container_properties", nullable = false)
	private String consumerContainerProperties;

	@Column(name = "invocation_code", nullable = true, length = 40)
	private String invocationCode;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@Column(name = "hmac_enabled", nullable = false)
	private Boolean hmacEnabled;

	@Column(name = "hmac_key", nullable = true)
	private String hmacKey;

	@Column(name = "concurrent", nullable = true)
	private int concurrent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public Boolean getConsumeRequest() {
		return consumeRequest;
	}

	public void setConsumeRequest(Boolean consumeRequest) {
		this.consumeRequest = consumeRequest;
	}

	public String getAssocKeyField() {
		return assocKeyField;
	}

	public void setAssocKeyField(String assocKeyField) {
		this.assocKeyField = assocKeyField;
	}

	public String getProducerBootstrap() {
		return producerBootstrap;
	}

	public void setProducerBootstrap(String producerBootstrap) {
		this.producerBootstrap = producerBootstrap;
	}

	public String getProducerConfig() {
		return producerConfig;
	}

	public void setProducerConfig(String producerConfig) {
		this.producerConfig = producerConfig;
	}

	public String getProducerHeaders() {
		return producerHeaders;
	}

	public void setProducerHeaders(String producerHeaders) {
		this.producerHeaders = producerHeaders;
	}

	public String getConsumerBootstrap() {
		return consumerBootstrap;
	}

	public void setConsumerBootstrap(String consumerBootstrap) {
		this.consumerBootstrap = consumerBootstrap;
	}

	public String getConsumerConfig() {
		return consumerConfig;
	}

	public void setConsumerConfig(String consumerConfig) {
		this.consumerConfig = consumerConfig;
	}

	public String getConsumerContainerProperties() {
		return consumerContainerProperties;
	}

	public void setConsumerContainerProperties(String consumerContainerProperties) {
		this.consumerContainerProperties = consumerContainerProperties;
	}

	public String getInvocationCode() {
		return invocationCode;
	}

	public void setInvocationCode(String invocationCode) {
		this.invocationCode = invocationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getHmacEnabled() {
		return hmacEnabled;
	}

	public void setHmacEnabled(Boolean hmacEnabled) {
		this.hmacEnabled = hmacEnabled;
	}

	public String getHmacKey() {
		return hmacKey;
	}

	public void setHmacKey(String hmacKey) {
		this.hmacKey = hmacKey;
	}

	public int getConcurrent() {
		return concurrent;
	}

	public void setConcurrent(int concurrent) {
		this.concurrent = concurrent;
	}

	@Override
	public Object getPk() {
		return id;
	}
}
