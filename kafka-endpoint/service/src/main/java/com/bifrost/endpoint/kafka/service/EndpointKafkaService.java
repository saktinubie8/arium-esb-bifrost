package com.bifrost.endpoint.kafka.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.kafka.model.EndpointKafka;
import com.bifrost.endpoint.kafka.repository.EndpointKafkaRepository;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.service.EndpointService;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointKafkaService extends GenericService<EndpointKafkaRepository, EndpointKafka, String>{

	private static final String KEY = "endpoint:kafka";
	
	@Autowired
	private EndpointService endpointService;

	public EndpointKafkaService() {
		super(KEY);
	}

	@Cacheable(cacheNames = KEY+":findByInvocationCode")
	public EndpointKafka findByInvocationCode(String invocationCode) {
		return repository.findByInvocationCode(invocationCode);
	}

	@Cacheable(cacheNames = KEY+":findByEndpointCode")
	public List<EndpointKafka> findByEndpointCode(String endpointCode) {
		return repository.findByEndpoint_Code(endpointCode);
	}

	@Override
	@CachePut(cacheNames = KEY+":findByInvocationCode", key = "#t.invocationCode")
	@CacheEvict(cacheNames = KEY+":findByEndpointCode", key = "#t.endpoint.code")
	public EndpointKafka save(EndpointKafka t) {
		return super.save(t);
	}

	@Override
	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByInvocationCode", key = "#t.invocationCode"),
			@CacheEvict(cacheNames = KEY+":findByEndpointCode", key = "#t.endpoint.code")})
	public void delete(EndpointKafka t) {
		super.delete(t);
	}

	@Override
	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByInvocationCode", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findByEndpointCode", allEntries = true)})
	public void evictAll() {
		super.evictAll();
	}

	@Override
	@PostConstruct
	public void populate() {
		super.populate();
		IMap<String, EndpointKafka> findByInvocationCode = hazelcastInstance.getMap(KEY+":findByInvocationCode");
		IMap<String, List<EndpointKafka> > findByEndpointCode = hazelcastInstance.getMap(KEY+":findByEndpointCode");
		for(EndpointKafka t : findAll())
			findByInvocationCode.put(t.getInvocationCode(), findByInvocationCode(t.getInvocationCode()));
		for(Endpoint t : endpointService.findAll())
			findByEndpointCode.put(t.getCode(), findByEndpointCode(t.getCode()));
	}
	
	
}
