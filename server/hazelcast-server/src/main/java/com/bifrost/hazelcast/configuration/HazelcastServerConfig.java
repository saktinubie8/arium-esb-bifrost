package com.bifrost.hazelcast.configuration;

import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitsofinfo.hazelcast.discovery.docker.swarm.DockerSwarmDiscoveryStrategyFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bifrost.common.constant.HazelcastConstant;
import com.bifrost.common.constant.ParamConstant;
import com.hazelcast.config.AwsConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.DiscoveryConfig;
import com.hazelcast.config.DiscoveryStrategyConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.InterfacesConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.ManagementCenterConfig;
import com.hazelcast.config.MemberAddressProviderConfig;
import com.hazelcast.config.MulticastConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Configuration
public class HazelcastServerConfig {
	
	private static final Log LOGGER = LogFactory.getLog(HazelcastServerConfig.class);

	@Value("${"+ParamConstant.Hazelcast.SERVER_GROUP_NAME+"}")
	private String groupName;

	@Value("${"+ParamConstant.Hazelcast.SOCKET_BIND_ANY+":true}")
	private String bindAny;

	@Value("${"+ParamConstant.Hazelcast.SOCKET_SERVER_BIND_ANY+":null}")
	private String serverBindAny;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_MANAGEMENT_CENTER_ENABLED+"}")
	private String enableManageCenter;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_MANAGEMENT_CENTER_URL+"}")
	private String manageCenterUrl;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_INTERFACE_ENABLED+":false}")
	private Boolean interfaceEnabled;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_INTERFACE_LIST+"}")
	private String interfaceList;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_JOIN_MODE+"}")
	private String joinMode;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_MULTICAST_GROUP+"}")
	private String multicastGroup;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_MULTICAST_PORT+"}")
	private String multicastPort; 
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_TCPIP_MEMBERS+"}")
	private String tcpIpMembers;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_SWARM_DOCKER_NETWORK_NAMES+"}")
	private String swarmDockerNetworkNames;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_SWARM_DOCKER_SERVICE_NAMES+":#{null}}")
	private String swarmDockerServiceNames;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_SWARM_DOCKER_SERVICE_LABELS+":#{null}}")
	private String swarmDockerServiceLabels;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_SWARM_SWARM_MGR_URI+":#{null}}")
	private String swarmMgrUri;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_SWARM_SKIP_VERIFY_SSL+":#{null}}")
	private String swarmSkipVerifySsl;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_SWARM_LOG_ALL_SERVICE_NAMES_ON_FAILED_DISCOVERY+":#{null}}")
	private Boolean swarmLogAllServiceNamesOnFailedDiscovery;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_SWARM_STRICT_SERVICE_NAME_COMPARISON+":#{null}}")
	private Boolean swarmStrictServiceNameComparison;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_NETWORK_PORT+"}")
	private String networkPort;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_NETWORK_PORT_AUTO_INCREMENT+"}")
	private String networkPortAutoIncrement;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_NETWORK_PORT_COUNT+"}")
	private String networkPortCount;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_INSTANCE_NAME+"}")
	private String instanceName;
	
	@Value("${"+ParamConstant.Hazelcast.SERVER_CP_MEMBER_COUNT+":3}")
	private int cpMemberCount;
	
	@Bean
	public HazelcastInstance hazelcastInstance() {
		
		// Group configuration
		GroupConfig groupConfig = new GroupConfig();
		groupConfig.setName(groupName);

		// Management center configuration
		ManagementCenterConfig managementCenterConfig = new ManagementCenterConfig();
		managementCenterConfig.setEnabled(Boolean.parseBoolean(enableManageCenter));
		managementCenterConfig.setUrl(manageCenterUrl);

		// Join configuration
		JoinConfig joinConfig = new JoinConfig();
		MulticastConfig multicastConfig = new MulticastConfig();
		TcpIpConfig tcpIpConfig = new TcpIpConfig();
		AwsConfig awsConfig = new AwsConfig();
		DiscoveryConfig discoveryConfig = new DiscoveryConfig();
		LOGGER.info("[System][Hazelcast] Join mode: "+joinMode);
		if (joinMode.equals(HazelcastConstant.JOIN.SWARM)) {
			multicastConfig.setEnabled(false);
			tcpIpConfig.setEnabled(false);
			awsConfig.setEnabled(false);
			
			DiscoveryStrategyConfig discoveryStrategyConfig = new DiscoveryStrategyConfig(new DockerSwarmDiscoveryStrategyFactory());
			if(swarmDockerServiceNames==null && swarmDockerServiceLabels==null)
				throw new NullPointerException("At least docker-service-names or docker-service-labels must not be null in swarm join mode");
			if(swarmDockerNetworkNames==null)
				throw new NullPointerException("docker-network-names must not be null in swarm join mode");
			else if(swarmDockerServiceNames==null && swarmDockerServiceLabels==null)
				throw new NullPointerException("At least docker-service-names or docker-service-labels must not be null in swarm join mode");
			discoveryStrategyConfig.addProperty("docker-network-names", swarmDockerNetworkNames);
			if(swarmDockerServiceNames!=null)
				discoveryStrategyConfig.addProperty("docker-service-names", swarmDockerServiceNames);
			if(swarmDockerServiceLabels!=null)
				discoveryStrategyConfig.addProperty("docker-service-labels", swarmDockerServiceLabels);
			if(swarmMgrUri!=null)
				discoveryStrategyConfig.addProperty("swarm-mgr-uri", swarmMgrUri);
			if(swarmSkipVerifySsl!=null)
				discoveryStrategyConfig.addProperty("skip-verify-ssl", swarmSkipVerifySsl);
			if(swarmLogAllServiceNamesOnFailedDiscovery!=null)
				discoveryStrategyConfig.addProperty("log-all-service-names-on-failed-discovery", swarmLogAllServiceNamesOnFailedDiscovery);
			if(networkPort!=null)
				discoveryStrategyConfig.addProperty("hazelcast-peer-port", networkPort);
			
			discoveryConfig.addDiscoveryStrategyConfig(discoveryStrategyConfig);
			joinConfig.setDiscoveryConfig(discoveryConfig);
		} else if (joinMode.equals(HazelcastConstant.JOIN.MULTICAST)) {
			multicastConfig.setEnabled(true);
			multicastConfig.setMulticastGroup(multicastGroup);
			multicastConfig.setMulticastPort(Integer.parseInt(multicastPort));
			tcpIpConfig.setEnabled(false);
		} else if (joinMode.equals(HazelcastConstant.JOIN.TCPIP)) {
			tcpIpConfig.setEnabled(true);
			tcpIpConfig.setMembers(Arrays.asList(tcpIpMembers.split(",")));
			multicastConfig.setEnabled(false);
		}
		joinConfig.setMulticastConfig(multicastConfig);
		joinConfig.setTcpIpConfig(tcpIpConfig);
		joinConfig.setAwsConfig(awsConfig);

		// Interface configuration
		InterfacesConfig interfaceConfig = new InterfacesConfig();
		if(interfaceEnabled)
			for(String intrfce : interfaceList.split(","))
				interfaceConfig.addInterface(intrfce.trim());
		
		// Network configuration
		NetworkConfig networkConfig = new NetworkConfig();
		networkConfig.setPort(Integer.parseInt(networkPort));
		networkConfig.setPortAutoIncrement(Boolean.parseBoolean(networkPortAutoIncrement));
		networkConfig.setPortCount(Integer.parseInt(networkPortCount));
		networkConfig.setInterfaces(interfaceConfig);
		networkConfig.setJoin(joinConfig);

		// Member address provider configuration
		if (joinMode.equals(HazelcastConstant.JOIN.SWARM)) {
			MemberAddressProviderConfig memberAddressProviderConfig = new MemberAddressProviderConfig();
			memberAddressProviderConfig.setEnabled(true);
			memberAddressProviderConfig.setClassName("org.bitsofinfo.hazelcast.discovery.docker.swarm.SwarmMemberAddressProvider");
			Properties properties = new Properties();
			if(swarmDockerNetworkNames==null)
				throw new NullPointerException("docker-network-names must not be null in swarm join mode");
			else if(swarmDockerServiceNames==null && swarmDockerServiceLabels==null)
				throw new NullPointerException("At least docker-service-names or docker-service-labels must not be null in swarm join mode");
			properties.put("dockerNetworkNames", swarmDockerNetworkNames);
			if(swarmDockerServiceNames!=null)
				properties.put("dockerServiceNames", swarmDockerServiceNames);
			if(swarmDockerServiceLabels!=null)
				properties.put("dockerServiceLabels", swarmDockerServiceLabels);
            properties.put("hazelcastPeerPort", networkPort);
			if(swarmMgrUri!=null)
				properties.put("swarmMgrUri", swarmMgrUri);
            properties.put("skipVerifySsl", swarmSkipVerifySsl==null?false:swarmMgrUri);
            properties.put("logAllServiceNamesOnFailedDiscovery", swarmLogAllServiceNamesOnFailedDiscovery==null?false:swarmLogAllServiceNamesOnFailedDiscovery);
            properties.put("strictDockerServiceNameComparison", swarmStrictServiceNameComparison==null?false:swarmStrictServiceNameComparison);
            
			memberAddressProviderConfig.setProperties(properties);
			networkConfig.setMemberAddressProviderConfig(memberAddressProviderConfig);
		}

		// Initialize hazelcast instance with configuration above
		Config config = new Config();
		config.setInstanceName(instanceName);
		config.getCPSubsystemConfig().setCPMemberCount(cpMemberCount);
		config.setGroupConfig(groupConfig);
		config.setNetworkConfig(networkConfig);
		if (joinMode.equals(HazelcastConstant.JOIN.SWARM)) {
			config.setProperty("hazelcast.discovery.enabled", "true");
			config.setProperty("hazelcast.shutdownhook.enabled", "true");
			config.setProperty("hazelcast.socket.bind.any", "false");
		}else {
			if(serverBindAny==null)
				config.setProperty("hazelcast.socket.server.bind.any", bindAny);
			else
				config.setProperty("hazelcast.socket.server.bind.any", serverBindAny);
		}
		
		return Hazelcast.newHazelcastInstance(config);
	}
}
