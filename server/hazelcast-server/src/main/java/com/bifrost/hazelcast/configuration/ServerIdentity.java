package com.bifrost.hazelcast.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bifrost.common.banner.ModuleInfo;
import com.bifrost.common.constant.ParamConstant;
import com.bifrost.system.configuration.AbstractSystemIdentity;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class ServerIdentity extends AbstractSystemIdentity {

	@Value("${"+ParamConstant.Bifrost.MODULE_HAZELCAST_SERVER_PORT+":9990}")
	private int port;

	public int getDefaultPort() {
		return port;
	}

	public String getModuleCode() {
		return ModuleInfo.HAZELCAST_SERVER_CODE;
	}
	
}
