package com.bifrost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.bifrost.common.banner.BannerInfo;
import com.bifrost.common.banner.ModuleInfo;

@SpringBootApplication
@EnableJpaRepositories
@EnableConfigServer
public class Application extends BaseApplication {

	public static void main(String[] args) {
		app = new SpringApplication(Application.class);
		app.setBanner(new BannerInfo(ModuleInfo.CONFIG_SERVER_DESC));
		context = app.run(args);
	}
}
