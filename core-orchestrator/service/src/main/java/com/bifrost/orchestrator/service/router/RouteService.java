package com.bifrost.orchestrator.service.router;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.orchestrator.model.router.Route;
import com.bifrost.orchestrator.repository.router.RouteRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class RouteService extends GenericService<RouteRepository, Route, Long>{

	private static final String KEY = "router:route";
	
	public RouteService() {
		super(KEY);
	}
	
	@Cacheable(cacheNames = KEY+":findByCode")
	public Route findByCode(String routeCode) {
		return null;
	}

	@Override
	@CachePut(cacheNames = KEY+":findByCode", key = "#t.code")
	public Route save(Route t) {
		return super.save(t);
	}

	@Override
	@CacheEvict(cacheNames = KEY+":findByCode", key = "#t.code")
	public void delete(Route t) {
		super.delete(t);
	}

	@Override
	@CacheEvict(cacheNames = KEY+":findByCode", allEntries = true)
	public void evictAll() {
		super.evictAll();
	}

	@Override
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, Route> findByCode = hazelcastInstance.getMap(KEY+":findByCode");
		for(Route t : findAll())
			if(findByCode.get(t.getCode())==null)
				findByCode.set(t.getCode(), t);
	}
	
	
	
}
