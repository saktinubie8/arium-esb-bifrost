package com.bifrost.orchestrator.service.router;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.exception.system.InvalidDataException;
import com.bifrost.orchestrator.model.router.Route;
import com.bifrost.orchestrator.model.router.RouteDetail;
import com.bifrost.orchestrator.repository.router.RouteDetailRepository;
import com.hazelcast.core.IMap;

@Service
public class RouteDetailService extends GenericService<RouteDetailRepository, RouteDetail, Long>{

	private static final String KEY = "router:route:detail";
	
	@Autowired
	private RouteService routeService;
	
	public RouteDetailService() {
		super(KEY);
	}

	@Cacheable(cacheNames = KEY+":findByRouteCode")
	public List<RouteDetail> findByRouteCode(String routeCode) {
		return null;
	}

	@Cacheable(cacheNames = KEY+":findFirstRouteByRouteCode")
	public List<RouteDetail> findFirstRouteByRouteCode(String routeCode) {
		return null;
	}

	@Cacheable(cacheNames = KEY+":findNextRouteByRouteCode")
	public List<RouteDetail> findNextRouteByRouteCode(String routeCode, Long lastInvokeId) {
		return null;
	}

	public RouteDetail save(RouteDetail t) {
		RouteDetail retVal = super.save(t);
		String routeCode = retVal.getRoute().getCode();
		Long lastInvokeId = retVal.getLastInvokeId();
		IMap<String, List<RouteDetail>> findByRouteCode = hazelcastInstance.getMap(KEY+":findByRouteCode");
		IMap<String, List<RouteDetail>> findFirstRouteByRouteCode = hazelcastInstance.getMap(KEY+":findFirstRouteByRouteCode");
		IMap<SimpleKey, List<RouteDetail>> findNextRouteByRouteCode = hazelcastInstance.getMap(KEY+":findNextRouteByRouteCode");
		findByRouteCode.set(routeCode, repository.findByRoute_Code(routeCode));
		findFirstRouteByRouteCode.set(routeCode, repository.findByRoute_CodeAndLastInvokeIdIsNullOrderByPriorityDesc(routeCode));
		SimpleKey key = new SimpleKey(routeCode, lastInvokeId);
		findNextRouteByRouteCode.set(key, repository.findByRoute_CodeAndLastInvokeIdOrderByPriorityDesc(routeCode, lastInvokeId));
		return retVal;
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByRouteCode", key = "#t.route.code"),
			@CacheEvict(cacheNames = KEY+":findFirstRouteByRouteCode", key = "#t.route.code"),
			@CacheEvict(cacheNames = KEY+":findNextRouteByRouteCode", key = "new org.springframework.cache.interceptor.SimpleKey(#t.route.code, #t.lastInvokeId)")
	})
	public void delete(RouteDetail t) {
		super.delete(t);
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByRouteCode", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findFirstRouteByRouteCode", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findNextRouteByRouteCode", allEntries = true)
	})
	public void evictAll() {
		super.evictAll();
	}

	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, List<RouteDetail>> findByRouteCode = hazelcastInstance.getMap(KEY+":findByRouteCode");
		IMap<String, List<RouteDetail>> findFirstRouteByRouteCode = hazelcastInstance.getMap(KEY+":findFirstRouteByRouteCode");
		IMap<SimpleKey, List<RouteDetail>> findNextRouteByRouteCode = hazelcastInstance.getMap(KEY+":findNextRouteByRouteCode");		
		for(RouteDetail t : findAll()) {
			if(t.getRoute()==null)
				throw new InvalidDataException("Route detail with ID "+t.getId()+" has null route ID, please set its route ID first");
			Route route = routeService.findById(t.getRoute().getId());
			if(findByRouteCode.get(route.getCode())==null)
				findByRouteCode.set(route.getCode(), repository.findByRoute_Code(route.getCode()));
			if(findFirstRouteByRouteCode.get(route.getCode())==null)
				findFirstRouteByRouteCode.set(route.getCode(), repository.findByRoute_CodeAndLastInvokeIdIsNullOrderByPriorityDesc(route.getCode()));
			SimpleKey key = new SimpleKey(route.getCode(), t.getLastInvokeId());
			if(findNextRouteByRouteCode.get(key)==null)
				findNextRouteByRouteCode.set(key, repository.findByRoute_CodeAndLastInvokeIdOrderByPriorityDesc(route.getCode(), t.getLastInvokeId()));
		}
	}
}
