package com.bifrost.orchestrator.service.modifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.orchestrator.model.modifier.Invocation;
import com.bifrost.orchestrator.repository.modifier.InvocationRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class InvocationService extends GenericService<InvocationRepository, Invocation, Long> {

	private static final String KEY = "modifier:invocation";

	public InvocationService() {
		super(KEY);
	}

	@Cacheable(cacheNames = KEY+":findByEndpointCodeAndInvocationCode")
	public Invocation findByEndpointCodeAndInvocationCode(String endpointCode, String invocationCode) {
		return null;
	}	

	@Cacheable(cacheNames = KEY+":findParentTreeByInvocationId")
	public List<Invocation> findParentTreeByInvocationId(Long invocationId) {
		return null;
	}

	@Override
	@Caching(put = @CachePut(cacheNames = KEY+":findByEndpointCodeAndInvocationCode", key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpointCode, #t.code)"),
			evict = @CacheEvict(cacheNames = KEY+":findParentTreeByInvocationId", key = "#t.id"))
	public Invocation save(Invocation t) {
		Invocation retVal = super.save(t); 
		IMap<Long, List<Invocation>> findParentTreeByInvocationId = hazelcastInstance.getMap(KEY+":findParentTreeByInvocationId");
		populateFindParentTreeByInvocationId(retVal, findParentTreeByInvocationId);
		return retVal;
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndInvocationCode", key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpointCode, #t.code)"),
			@CacheEvict(cacheNames = KEY+":findParentTreeByInvocationId", key = "#t.id")
	})
	public void delete(Invocation t) {
		super.delete(t);
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndInvocationCode", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findParentTreeByInvocationId", allEntries = true)
	})
	public void evictAll() {
		super.evictAll();
	}

	private void populateFindParentTreeByInvocationId(Invocation t, IMap<Long, List<Invocation>> iMap) {
		Long id = t.getId();
		List<Invocation> invocationList = new ArrayList<Invocation>();
		invocationList.add(t);
		while((t = t.getParentInvoke())!=null)
			invocationList.add(findById(t.getId()));
		Collections.reverse(invocationList);
		iMap.setAsync(id, invocationList);
	}
	
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<SimpleKey, Invocation> findByEndpointCodeAndInvocationCode = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndInvocationCode");
		IMap<Long, List<Invocation>> findParentTreeByInvocationId = hazelcastInstance.getMap(KEY+":findParentTreeByInvocationId");
		for(Invocation t : findAll()) {
			SimpleKey key = new SimpleKey(t.getEndpointCode(), t.getCode());
			if(findByEndpointCodeAndInvocationCode.get(key)==null)
				findByEndpointCodeAndInvocationCode.setAsync(key, t);
			if(findParentTreeByInvocationId.get(t.getId())==null)
				populateFindParentTreeByInvocationId(t, findParentTreeByInvocationId);
		}
	}
}
