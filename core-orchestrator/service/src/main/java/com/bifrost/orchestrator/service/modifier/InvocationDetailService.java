package com.bifrost.orchestrator.service.modifier;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.exception.system.InvalidDataException;
import com.bifrost.orchestrator.model.modifier.InvocationDetail;
import com.bifrost.orchestrator.repository.modifier.InvocationDetailRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class InvocationDetailService extends GenericService<InvocationDetailRepository, InvocationDetail, Long> {

	private static final String KEY = "modifier:invocation:detail";

	public InvocationDetailService() {
		super(KEY);
	}

	@Cacheable(cacheNames = KEY+":findByInvocationIdAndGoingOut")
	public List<InvocationDetail> findByInvocationIdAndGoingOut(Long invocationId, Boolean goingOut) {
		return null;
	}	

	public InvocationDetail save(InvocationDetail t) {
		InvocationDetail retVal = super.save(t); 
		long invocationId = retVal.getInvocation().getId();
		Boolean goingOut = retVal.getGoingOut();
		IMap<SimpleKey, List<InvocationDetail>> findByInvocationIdAndGoingOut = hazelcastInstance.getMap(KEY+":findByInvocationIdAndGoingOut");
		findByInvocationIdAndGoingOut.set(new SimpleKey(invocationId, goingOut), repository.findByInvocation_IdAndGoingOutOrderByChainAscIdAsc(invocationId, goingOut));
		return retVal;
	}

	@CacheEvict(cacheNames = KEY+":findByInvocationIdAndGoingOut", key = "new org.springframework.cache.interceptor.SimpleKey(#t.invocation.id, #t.goingOut)")
	public void delete(InvocationDetail t) {
		super.delete(t);
	}

	@CacheEvict(cacheNames = KEY+":findByInvocationIdAndGoingOut", allEntries = true)
	public void evictAll() {
		super.evictAll();
	}

	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<SimpleKey, List<InvocationDetail>> findByInvocationIdAndGoingOut = hazelcastInstance.getMap(KEY+":findByInvocationIdAndGoingOut");
		for(InvocationDetail t : findAll()) {
			if(t.getInvocation()==null)
				throw new InvalidDataException("Invocation detail with ID "+t.getId()+" has null invocation ID, please set its invocation ID first");
			SimpleKey key = new SimpleKey(t.getInvocation().getId(), false);
			if(findByInvocationIdAndGoingOut.get(key)==null)
				findByInvocationIdAndGoingOut.setAsync(key, repository.findByInvocation_IdAndGoingOutOrderByChainAscIdAsc(t.getInvocation().getId(), false));
			key = new SimpleKey(t.getInvocation().getId(), true);
			if(findByInvocationIdAndGoingOut.get(key)==null)
				findByInvocationIdAndGoingOut.setAsync(key, repository.findByInvocation_IdAndGoingOutOrderByChainAscIdAsc(t.getInvocation().getId(), true));
		}
	}
}
