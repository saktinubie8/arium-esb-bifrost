package com.bifrost.orchestrator.service.mapper;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.orchestrator.model.mapper.ResponseCode;
import com.bifrost.orchestrator.repository.mapper.ResponseCodeRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class ResponseCodeService extends GenericService<ResponseCodeRepository, ResponseCode, Long>{

	private static final String KEY = "endpoint:response:code";
	
	public ResponseCodeService() {
		super(KEY);
	}
	
	@Cacheable(cacheNames = KEY+":findByEndpointCodeAndResponseCode")
	public ResponseCode findByEndpointCodeAndResponseCode(String endpointCode, String responseCode) {
		return null;
	}
	

	@Override
	@CachePut(
			cacheNames = KEY+":findByEndpointCodeAndResponseCode", 
			key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpointCode, #t.code)"
	)
	public ResponseCode save(ResponseCode t) {
		return super.save(t);
	}

	@Override
	@CacheEvict(
			cacheNames = KEY+":findByEndpointCodeAndResponseCode", 
			key = "new org.springframework.cache.interceptor.SimpleKey(#t.endpointCode, #t.code)"
	)
	public void delete(ResponseCode t) {
		super.delete(t);
	}
	
	public void populate() throws Exception {
		super.populate();
		IMap<SimpleKey, ResponseCode> findByEndpointCodeAndResponseCode = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndResponseCode");
		for(ResponseCode t : findAll()) {
			SimpleKey key = new SimpleKey(t.getEndpointCode(), t.getCode());
			if(findByEndpointCodeAndResponseCode.get(key)==null)
				if(t.getEndpointCode()==null)
					findByEndpointCodeAndResponseCode.setAsync(key, repository.findByEndpointCodeIsNullAndCode(t.getCode()));
				else
					findByEndpointCodeAndResponseCode.setAsync(key, repository.findByEndpointCodeAndCode(t.getEndpointCode(), t.getCode()));
		}
	}
}
