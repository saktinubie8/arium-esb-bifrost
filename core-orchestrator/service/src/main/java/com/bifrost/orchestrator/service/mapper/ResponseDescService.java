package com.bifrost.orchestrator.service.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.service.GenericService;
import com.bifrost.exception.system.InvalidDataException;
import com.bifrost.orchestrator.model.mapper.ResponseCode;
import com.bifrost.orchestrator.model.mapper.ResponseDesc;
import com.bifrost.orchestrator.repository.mapper.ResponseDescRepository;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class ResponseDescService extends GenericService<ResponseDescRepository, ResponseDesc, Long> { 
	
	@Autowired
	private ResponseCodeService responseCodeService;
	
	private static final String KEY = "endpoint:response:description";
	
	public ResponseDescService() {
		super(KEY);
	}
	
	@Cacheable(cacheNames = KEY+":findByEndpointCodeNullAndResponseCode")
	public ResponseDesc findByEndpointCodeNullAndResponseCode(String responseCode) {
		return null;
	}
	
	@Cacheable(cacheNames = KEY+":findByEndpointCodeAndResponseCode")
	public List<ResponseDesc> findByEndpointCodeAndResponseCode(String endpointCode, String responseCode) {
		return null;
	}

	@Cacheable(cacheNames = KEY+":findTargetBySourceEndpointDescIdAndTargetEndpointCode")
	public List<ResponseDesc> findTargetBySourceEndpointDescIdAndTargetEndpointCode(Long sourceResponseDescId, String targetEndpointCode) {
		return null;
	}
	
	@CachePut(cacheNames = KEY+":findByEndpointCodeNullAndResponseCode", key = "#t.code.code)", unless = "#t.code.endpointCode != null")
	public ResponseDesc save(ResponseDesc t) {
		ResponseDesc retVal = super.save(t);
		String endpointCode = retVal.getCode().getEndpointCode();
		String responseCode = retVal.getCode().getCode();
		IMap<SimpleKey, List<ResponseDesc>> findByEndpointCodeAndResponseCode = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndResponseCode");
		IMap<SimpleKey, List<ResponseDesc>> findTargetBySourceEndpointDescIdAndTargetEndpointCode = hazelcastInstance.getMap(KEY+":findTargetBySourceEndpointDescIdAndTargetEndpointCode");
		findByEndpointCodeAndResponseCode.set(new SimpleKey(endpointCode, responseCode), repository.findByCode_EndpointCodeAndCode_Code(endpointCode, responseCode));
		populateFindTargetBySourceEndpointDescIdAndTargetEndpointCode(t, findTargetBySourceEndpointDescIdAndTargetEndpointCode);
		return retVal;
	}

	@Override
	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeNullAndResponseCode", key = "#t.code.code)", condition = "#t.code.endpointCode == null"),
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndResponseCode", key = "new org.springframework.cache.interceptor.SimpleKey(#t.code.endpointCode, #t.code.code)")
	})
	public void delete(ResponseDesc t) {
		super.delete(t);
		IMap<SimpleKey, List<ResponseDesc>> findTargetBySourceEndpointDescIdAndTargetEndpointCode = hazelcastInstance.getMap(KEY+":findTargetBySourceEndpointDescIdAndTargetEndpointCode");
		for(ResponseDesc s : t.getSource()) {
			String targetEndpointCode = t.getCode().getEndpointCode();
			SimpleKey key = new SimpleKey(s.getId(), targetEndpointCode);
			findTargetBySourceEndpointDescIdAndTargetEndpointCode.delete(key);
		}
	}

	@Override
	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeNullAndResponseCode", key = "#t.code.code)", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndResponseCode", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findTargetBySourceEndpointDescIdAndTargetEndpointCode", allEntries = true)
	})
	public void evictAll() {
		super.evictAll();
	}
	
	private void populateFindTargetBySourceEndpointDescIdAndTargetEndpointCode(ResponseDesc t, IMap<SimpleKey, List<ResponseDesc>> iMap) {
		for(ResponseDesc s : t.getSource()) {
			String targetEndpointCode = t.getCode().getEndpointCode();
			SimpleKey key = new SimpleKey(s.getId(), targetEndpointCode);
			if(iMap.get(key)==null) {
				List<ResponseDesc> result = new ArrayList<ResponseDesc>();
				ResponseDesc desc = repository.findById(s.getId()).get();
				for(ResponseDesc target : desc.getTarget()) {
					target = findById(target.getId());
					ResponseCode targetRc = responseCodeService.findById(target.getCode().getId());
					if(targetRc.getEndpointCode().equals(targetEndpointCode))
						result.add(target);
				}
				iMap.setAsync(key, result);
			}
		}
	}

	@PostConstruct
	public void populate() throws Exception {
		responseCodeService.populate();
		super.populate();
		IMap<String, ResponseDesc> findByEndpointCodeNullAndResponseCode = hazelcastInstance.getMap(KEY+":findByEndpointCodeNullAndResponseCode");
		IMap<SimpleKey, List<ResponseDesc>> findByEndpointCodeAndResponseCode = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndResponseCode");
		IMap<SimpleKey, List<ResponseDesc>> findTargetBySourceEndpointDescIdAndTargetEndpointCode = hazelcastInstance.getMap(KEY+":findTargetBySourceEndpointDescIdAndTargetEndpointCode");
		
		for(ResponseDesc t : findAll()) {
			if(t.getCode()==null)
				throw new InvalidDataException("Response description with ID "+t.getId()+" has null response code, please set its response code first");
			ResponseCode rc = responseCodeService.findById(t.getCode().getId());
			if(findByEndpointCodeNullAndResponseCode.get(rc.getCode()) == null && rc.getEndpointCode() == null)
				findByEndpointCodeNullAndResponseCode.setAsync(rc.getCode(), t);
			SimpleKey key = new SimpleKey(rc.getEndpointCode(), rc.getCode());
			if(findByEndpointCodeAndResponseCode.get(key) == null)
				findByEndpointCodeAndResponseCode.setAsync(key, repository.findByCode_EndpointCodeAndCode_Code(rc.getEndpointCode(), rc.getCode()));
			t.setCode(rc);
			populateFindTargetBySourceEndpointDescIdAndTargetEndpointCode(t, findTargetBySourceEndpointDescIdAndTargetEndpointCode);
		}
		
		ResponseDesc desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.DEFAULT.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.DEFAULT.getCode()), GenericResponse.DEFAULT.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);
		
		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.SUCCESS.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.SUCCESS.getCode()), GenericResponse.SUCCESS.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);

		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.FAILED.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.FAILED.getCode()), GenericResponse.FAILED.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);

		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.ERROR.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.ERROR.getCode()), GenericResponse.ERROR.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);
		
		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.TIMEOUT.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.TIMEOUT.getCode()), GenericResponse.TIMEOUT.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);
		
		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.OFFLINE.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.OFFLINE.getCode()), GenericResponse.OFFLINE.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);

		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.UNAUTHORIZED.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.UNAUTHORIZED.getCode()), GenericResponse.UNAUTHORIZED.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);

		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.UNAVAILBALE_ROUTE.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.UNAVAILBALE_ROUTE.getCode()), GenericResponse.UNAVAILBALE_ROUTE.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);

		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.NO_MATCHING_RESPONSE.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.NO_MATCHING_RESPONSE.getCode()), GenericResponse.NO_MATCHING_RESPONSE.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);

		desc = repository.findByCode_EndpointCodeIsNullAndCode_Code(GenericResponse.UNKNOWN_INVOCATION_CODE.getCode());
		if(desc==null) {
			desc = new ResponseDesc(new ResponseCode(null, GenericResponse.UNKNOWN_INVOCATION_CODE.getCode()), GenericResponse.UNKNOWN_INVOCATION_CODE.getDesc(), null);
			save(desc);
		}
		if(!findByEndpointCodeNullAndResponseCode.containsKey(desc.getCode().getCode()))
			findByEndpointCodeNullAndResponseCode.set(desc.getCode().getCode(), desc);
	}

}
