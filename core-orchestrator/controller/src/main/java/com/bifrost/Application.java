package com.bifrost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bifrost.common.banner.BannerInfo;
import com.bifrost.common.banner.ModuleInfo;
import com.bifrost.orchestrator.controller.ModifierController;

@SpringBootApplication
public class Application extends BaseApplication {
	
	public static void main(String[] args) {
		app = new SpringApplication(Application.class);
		app.setBanner(new BannerInfo(ModuleInfo.CORE_ORCHESTRATOR_DESC));
		context = app.run(args);
		ModifierController controller = context.getBean(ModifierController.class);
		controller.startAll();
	}
}
