package com.bifrost.orchestrator.context;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class DoubleFunction {
	
	private static DoubleFunction obj;
	
	public static DoubleFunction getInstance() {
		if(obj==null)
			obj = new DoubleFunction();
		return obj;
	}

	public String writeAsString(Double source, int decimalPoint) {
		return String.format ("%."+decimalPoint+"f", source);
	}
	
}
