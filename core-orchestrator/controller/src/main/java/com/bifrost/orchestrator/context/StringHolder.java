package com.bifrost.orchestrator.context;

public class StringHolder {

	private String value;
	
	public void set(String value) {
		this.value = value;
	}
	
	public String get() {
		return this.value;
	}
	
}
