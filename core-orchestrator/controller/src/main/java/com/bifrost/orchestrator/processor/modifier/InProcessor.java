package com.bifrost.orchestrator.processor.modifier;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.SpelParseException;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.kafka.listener.MessageListener;

import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.message.ExternalMessage;
import com.bifrost.message.InternalMessage;
import com.bifrost.orchestrator.context.GeneralFunction;
import com.bifrost.orchestrator.context.MapFunction;
import com.bifrost.orchestrator.context.StringHolder;
import com.bifrost.orchestrator.model.modifier.Invocation;
import com.bifrost.orchestrator.model.modifier.InvocationDetail;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class InProcessor extends GenericProcessor implements MessageListener<String, ExternalMessage> {
	
	private static final Log LOGGER = LogFactory.getLog(InProcessor.class);
	
	public InProcessor(Invocation invocation, Map<String, InternalMessage> internalMessages, ApplicationContext context) {
		super(invocation, internalMessages, context);
	}

	@Override
	public void onMessage(ConsumerRecord<String, ExternalMessage> data) {
		ExternalMessage externalMessage = data.value();
		InternalMessage internalMessage;
		String logId = externalMessage.getLogId();
		String invocationCode = invocation.getCode();
		if(externalMessage.isRequest()) {
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Incoming external request message from acquire endpoint: \n"+externalMessage+"\n");
			internalMessage = new InternalMessage(externalMessage);
		}else {
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Incoming external response message from issuer endpoint: \n"+externalMessage+"\n");
			internalMessage = internalMessages.get(externalMessage.getTransactionId());
			internalMessage.setLastTrackResponseCode(externalMessage.getResponseCode());
			internalMessage.setResponse();
		}

		// If system response code exists, send to router and skipping SpEL process
		if(externalMessage.getSystemResponseCode() != null) {
			internalMessage.setSystemResponseCode(externalMessage.getSystemResponseCode());
			internalMessage.setSystemResponseDesc(externalMessage.getSystemResponseDesc());
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] System response code exists: "+externalMessage.getSystemResponseCode()+", skipping expression process");
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Send internal message to router: \n"+internalMessage);
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			producerTemplate.sendBody("direct:router."+internalMessage.getRouteCode()+".in", internalMessage);
			return;
		}

		LOGGER.trace("["+logId+"]["+invocationCode+"][in] Pre-processing\nExternal message: \n"+externalMessage+"\nInternal message: \n"+internalMessage+"\n");
		StandardEvaluationContext context = new StandardEvaluationContext();
		context.setVariable("map", MapFunction.getInstance());
		context.setVariable("emsg", externalMessage);
		context.setVariable("imsg", internalMessage);
		context.setVariable("param", paramHolder);
		context.setVariable("cache", cacheHolder);
		context.setVariable("bean", libraries);
		try {
			context.registerFunction("doNothing", GeneralFunction.class.getDeclaredMethod("doNothing"));
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		}
		
		StringHolder routeHolder = null;
		if(internalMessage.isRequest()) {
			routeHolder = new StringHolder();
			context.setVariable("route", routeHolder);
		}
		
		for(Invocation invocation : invocationService.findParentTreeByInvocationId(invocation.getId())) {
			for(InvocationDetail detail : invocationDetailService.findByInvocationIdAndGoingOut(invocation.getId(), false)) {
				LOGGER.debug("["+logId+"]["+invocationCode+"][in] Process expression \""+detail.getProcess()+"\"");
				if(detail.getProcess()==null || detail.getProcess().isBlank())
					continue;
				try {
					expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(detail.getProcess(), false)).getValue(context);
				}catch(SpelEvaluationException | SpelParseException e) {
					LOGGER.error("["+invocationCode+"][in]["+internalMessage.getTransactionId()+"] Error processing expression '"+detail.getProcess()+"', cause by: "+e.getMessage());
					internalMessage.setSystemResponseCode(GenericResponse.ERROR.getCode());
					internalMessage.setSystemResponseDesc(e.getMessage());
					e.printStackTrace();
				}
			}
		}
		
		if(internalMessage.getRouteCode() != null) {
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Post-processing\nExternal message: \n"+externalMessage+"\nInternal message: \n"+internalMessage+"\n");
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Continuing route: "+internalMessage.getRouteCode());
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Send internal message to router: \n"+internalMessage);
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			producerTemplate.sendBody("direct:router."+internalMessage.getRouteCode()+".in", internalMessage);
		}else if(internalMessage.isRequest() && routeHolder.get() != null) {
			internalMessage.setRouteCode(routeHolder.get());
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Post-processing\nExternal message: \n"+externalMessage+"\nInternal message: \n"+internalMessage+"\n");
			IMap<String, Object> map = hazelcastInstance.getMap("router:route:findByCode");
			if(!map.containsKey(internalMessage.getRouteCode())) {
				internalMessage.setSystemResponseCode(GenericResponse.UNAVAILBALE_ROUTE.getCode());
				internalMessage.setSystemResponseDesc(GenericResponse.UNAVAILBALE_ROUTE.getDesc());
				LOGGER.debug("["+logId+"]["+invocationCode+"][in] Route '"+internalMessage.getRouteCode()+"' is NOT FOUND, replying with response: "+GenericResponse.UNAVAILBALE_ROUTE);
				LOGGER.trace("["+logId+"]["+invocationCode+"][in] Send internal message to response mapper: \n"+internalMessage);
				LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				producerTemplate.sendBody("direct:mapper.response.in", internalMessage);
			}else {
				LOGGER.debug("["+logId+"]["+invocationCode+"][in] Initiating route: "+internalMessage.getRouteCode());
				LOGGER.trace("["+logId+"]["+invocationCode+"][in] Send internal message to router: \n"+internalMessage);
				LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				producerTemplate.sendBody("direct:router."+internalMessage.getRouteCode()+".in", internalMessage);
			}
		}else {
			internalMessage.setSystemResponseCode(GenericResponse.UNAVAILBALE_ROUTE.getCode());
			internalMessage.setSystemResponseDesc(GenericResponse.UNAVAILBALE_ROUTE.getDesc());
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Post-processing\nExternal message: \n"+externalMessage+"\nInternal message: \n"+internalMessage+"\n");
			LOGGER.debug("["+logId+"]["+invocationCode+"][in] Route is not set, replying with response: "+GenericResponse.UNAVAILBALE_ROUTE);
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] Send internal message to response mapper: \n"+internalMessage);
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			producerTemplate.sendBody("direct:mapper.response.in", internalMessage);
		}
	}
}
