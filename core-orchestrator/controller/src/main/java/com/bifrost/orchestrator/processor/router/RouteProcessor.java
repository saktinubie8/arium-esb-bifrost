package com.bifrost.orchestrator.processor.router;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.SpelParseException;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.message.InternalMessage;
import com.bifrost.orchestrator.context.CacheHolder;
import com.bifrost.orchestrator.model.router.Route;
import com.bifrost.orchestrator.model.router.RouteDetail;
import com.bifrost.orchestrator.service.router.RouteDetailService;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class RouteProcessor implements Processor {

	private static final Log LOGGER = LogFactory.getLog(RouteProcessor.class);
	
	private Route route;
	private CacheHolder cacheHolder;
	private ProducerTemplate template;
	private ExpressionParser expressionParser;
	private RouteDetailService routeDetailService;

	public RouteProcessor(Route route, ApplicationContext context) {
		this.route = route;
		
		// Injecting necessery spring bean if application context is exists
		if(context!=null) {
			this.routeDetailService = context.getBean(RouteDetailService.class);
			this.expressionParser = context.getBean(ExpressionParser.class);
			this.template = context.getBean(ProducerTemplate.class);
			this.cacheHolder = context.getBean(CacheHolder.class);
		}
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		InternalMessage internalMessage = exchange.getIn().getBody(InternalMessage.class);
		String logId = internalMessage.getOriginEndpointCode()+':'+internalMessage.getOriginAssocKey();
		LOGGER.trace("["+logId+"]["+route.getCode()+"] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		LOGGER.trace("["+logId+"]["+route.getCode()+"] Incoming internal message from modifier: \n"+internalMessage+"\n");
		List<RouteDetail> routeDetails;
		
		// If no matching route, then send to response mapper with unavailable route response 
		if(internalMessage.routeCompleted()) {
			LOGGER.debug("["+logId+"]["+route.getCode()+"] Route is completed");
			LOGGER.trace("["+logId+"]["+route.getCode()+"] Send internal message to response mapper: \n"+internalMessage);
			LOGGER.trace("["+logId+"]["+route.getCode()+"] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			template.sendBody("direct:mapper.response.in", internalMessage);
			return;
		}
		
		// Find list of route detail based on its last invocation ID
		if(internalMessage.trackIsEmpty()) {
			routeDetails = routeDetailService.findFirstRouteByRouteCode(route.getCode());
		}else {
			routeDetails = routeDetailService.findNextRouteByRouteCode(route.getCode(), internalMessage.getLastTrack().getInvocationId());
		}

		// If list is null or size is empty, then send to response mapper with unavailable route response
		if(routeDetails == null || routeDetails.size()==0) {
			LOGGER.debug("["+logId+"]["+route.getCode()+"] Unavailable next invocation, completing route");
			if(internalMessage.trackIsEmpty()) {
				internalMessage.setSystemResponseCode(GenericResponse.UNAVAILBALE_ROUTE.getCode());
				internalMessage.setSystemResponseDesc(GenericResponse.UNAVAILBALE_ROUTE.getDesc());	
				LOGGER.trace("["+logId+"]["+route.getCode()+"] Set with system response code: "+GenericResponse.UNAVAILBALE_ROUTE);
			}
			internalMessage.completeRoute();
			LOGGER.trace("["+logId+"]["+route.getCode()+"] Send internal message to response mapper: \n"+internalMessage);
			LOGGER.trace("["+logId+"]["+route.getCode()+"] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			template.sendBody("direct:mapper.response.in", internalMessage);
		}else {
			StandardEvaluationContext context = new StandardEvaluationContext();
			context.setVariable("imsg", internalMessage);
			context.setVariable("cache", cacheHolder);

			// Find matching route detail based on its condition, if condition resulting in true then get the corresponding route detail
			RouteDetail nextRoute = null;
			for(RouteDetail detail : routeDetails) {
				try {
					if(detail.getCondition() == null || detail.getCondition().isBlank() || expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(detail.getCondition(), false)).getValue(context, Boolean.class)) {
						LOGGER.trace("["+logId+"]["+route.getCode()+"] Available next invocation ID: "+detail.getNextInvokeId()+", condition: '"+detail.getCondition()+"', status: MATCHED");
						nextRoute = detail;
					}else
						LOGGER.trace("["+logId+"]["+route.getCode()+"] Available next invocation ID: "+detail.getNextInvokeId()+", condition: '"+detail.getCondition()+"', status: NOT MATCHED");
				}catch(SpelEvaluationException | SpelParseException e) {
					LOGGER.error("["+logId+"]["+route.getCode()+"] Error processing condition '"+detail.getCondition()+"', cause by: "+e.getMessage());
					internalMessage.setSystemResponseCode(GenericResponse.ERROR.getCode());
					internalMessage.setSystemResponseDesc(e.getMessage());
					e.printStackTrace();
					break;
				}
			}
				
			// If no matching route, then send to response mapper with unavailable route response 
			if(nextRoute == null) {
				internalMessage.completeRoute();
				LOGGER.debug("["+logId+"]["+route.getCode()+"] No matched next invocation, completing route");
				LOGGER.trace("["+logId+"]["+route.getCode()+"] Send internal message to response mapper: \n"+internalMessage);
				LOGGER.trace("["+logId+"]["+route.getCode()+"] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				template.sendBody("direct:mapper.response.in", internalMessage);
			}else {
				internalMessage.setRequest();
				LOGGER.debug("["+logId+"]["+route.getCode()+"] Found matched next invocation ID: "+nextRoute.getNextInvokeId());
				LOGGER.trace("["+logId+"]["+route.getCode()+"] Send internal message to modifier: \n"+internalMessage);
				LOGGER.trace("["+logId+"]["+route.getCode()+"] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				template.sendBody("direct:modifier."+nextRoute.getNextInvokeId()+".out", internalMessage);
			}
		}	
	}
}
