package com.bifrost.orchestrator.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bifrost.common.constant.PathConstant;
import com.bifrost.common.controller.AbstractCacheController;
import com.bifrost.common.json.response.CacheResponse;
import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.json.response.ModuleResponse;
import com.bifrost.orchestrator.service.mapper.ResponseCodeService;
import com.bifrost.orchestrator.service.mapper.ResponseDescService;
import com.bifrost.orchestrator.service.modifier.InvocationDetailService;
import com.bifrost.orchestrator.service.modifier.InvocationService;
import com.bifrost.orchestrator.service.router.RouteDetailService;
import com.bifrost.orchestrator.service.router.RouteService;
import com.bifrost.system.service.SystemParamService;

@RestController
public class CacheController extends AbstractCacheController {
	
	private static final Log LOGGER = LogFactory.getLog(CacheController.class);
	
	@Autowired
	private ResponseCodeService responseCodeService;
	
	@Autowired
	private ResponseDescService responseDescService;

	@Autowired
	private InvocationService invocationService;
	
	@Autowired
	private InvocationDetailService invocationDetailService;

	@Autowired
	private RouteService routeService;

	@Autowired
	private SystemParamService systemParamService;
	
	@Autowired
	private RouteDetailService routeDetailService;
	
	@Autowired
	private ModifierController modifierController;
	
	@Autowired
	private RouterController routerController;
	
	@Override
	public void evictAll() {
		systemParamService.evictAll();
		evictModifier();
		evictRouter();
		evictMapper();
	}

	@Override
	public void populateAll() {
		try {
			systemParamService.populate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		populateModifier();
		populateRouter();
		populateMapper();
	}

	public void evictModifier() {
		invocationService.evictAll();
		invocationDetailService.evictAll();
	}

	public void populateModifier() {
		try {
			invocationService.populate();
			invocationDetailService.populate();
		} catch (Exception e) {
			LOGGER.error("Failed when trying to populate object modifier cache, cause by: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void evictMapper() {
		responseDescService.evictAll();
		responseCodeService.evictAll();
	}

	public void populateMapper() {
		try {
			responseDescService.populate();
		} catch (Exception e) {
			LOGGER.error("Failed when trying to populate response mapper cache, cause by: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void evictRouter() {
		routeService.evictAll();
		routeDetailService.evictAll();
	}

	public void populateRouter() {
		try {
			routeService.populate();
			routeDetailService.populate();
		} catch (Exception e) {
			LOGGER.error("Failed when trying to populate route logic cache, cause by: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path = PathConstant.Rest.SYSTEM_CACHE+"/modifier/refresh", method = RequestMethod.GET)
	public GenericResponse refreshModifierCache() {
		evictModifier();
		populateModifier();
		return CacheResponse.SUCCESS;
	}
	
	@RequestMapping(path = PathConstant.Rest.SYSTEM_CACHE+"/router/refresh", method = RequestMethod.GET)
	public GenericResponse refreshRouterCache() {
		evictRouter();
		populateRouter();
		return CacheResponse.SUCCESS;
	}
	
	@RequestMapping(path = PathConstant.Rest.SYSTEM_CACHE+"/mapper/refresh", method = RequestMethod.GET)
	public GenericResponse refreshMapperCache() {
		evictMapper();
		populateMapper();
		return CacheResponse.SUCCESS;
	}

	@RequestMapping(path = PathConstant.Rest.SERVICE_MODIFIER_PREFIX+"/reload", method = RequestMethod.GET)
	public GenericResponse reloadModifierModule() {
		modifierController.stopAll();
		evictModifier();
		populateModifier();
		modifierController.init();
		return CacheResponse.SUCCESS;
	}

	@RequestMapping(path = PathConstant.Rest.SERVICE_ROUTER_PREFIX+"/reload", method = RequestMethod.GET)
	public GenericResponse reloadRouterModule() {
		evictRouter();
		populateRouter();
		routerController.init();
		return CacheResponse.SUCCESS;
	}
	
	@RequestMapping(path = PathConstant.Rest.SERVICE_ORCHESTRATOR_PREFIX+"/reload", method = RequestMethod.GET)
	public GenericResponse reloadAll() {
		modifierController.stopAll();
		evictAll();
		populateAll();
		modifierController.init();
		routerController.init();
		return ModuleResponse.SUCCESS;
	}

}
