package com.bifrost.orchestrator.processor.modifier;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.SpelParseException;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.kafka.core.KafkaTemplate;

import com.bifrost.common.constant.CacheName;
import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.logging.model.MessageLog;
import com.bifrost.message.ExternalMessage;
import com.bifrost.message.InternalMessage;
import com.bifrost.orchestrator.context.GeneralFunction;
import com.bifrost.orchestrator.context.MapFunction;
import com.bifrost.orchestrator.model.modifier.Invocation;
import com.bifrost.orchestrator.model.modifier.InvocationDetail;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class OutProcessor extends GenericProcessor implements Processor {

	public static final Log LOGGER = LogFactory.getLog(OutProcessor.class);
	public KafkaTemplate<String, ExternalMessage> kafkaTemplate;
	
	@SuppressWarnings("unchecked")
	public OutProcessor(Invocation invocation, Map<String, InternalMessage> internalMessages, ApplicationContext context) {
		super(invocation, internalMessages, context);
		this.kafkaTemplate = context.getBean(KafkaTemplate.class);
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		InternalMessage internalMessage = exchange.getMessage().getBody(InternalMessage.class);
		String endpointCode = invocation.getEndpointCode();
		String invocationCode = invocation.getCode();
		String logId = internalMessage.getOriginEndpointCode()+":"+internalMessage.getOriginAssocKey();
		if(internalMessage.isRequest()) {
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			LOGGER.trace("["+logId+"]["+invocationCode+"][out] Incoming internal message from router: \n"+internalMessage+"\n");
		}else {
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			LOGGER.trace("["+logId+"]["+invocationCode+"][out] Incoming internal message from response mapper: \n"+internalMessage+"\n");
		}
			
		ExternalMessage externalMessage = new ExternalMessage(endpointCode, invocationCode, internalMessage);
		LOGGER.trace("["+invocationCode+"][out]["+externalMessage.getTransactionId()+"] Pre-processing\nExternal message: \n"+externalMessage+"\nInternal message: \n"+internalMessage+"\n");
		StandardEvaluationContext context = new StandardEvaluationContext();
		context.setVariable("map", MapFunction.getInstance());
		context.setVariable("emsg", externalMessage);
		context.setVariable("imsg", internalMessage);
		context.setVariable("param", paramHolder);
		context.setVariable("cache", cacheHolder);
		context.setVariable("bean", libraries);
		try {
			context.registerFunction("doNothing", GeneralFunction.class.getDeclaredMethod("doNothing"));
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		}
		for(Invocation invocation : invocationService.findParentTreeByInvocationId(invocation.getId())) {
			List<InvocationDetail> details = invocationDetailService.findByInvocationIdAndGoingOut(invocation.getId(), true);
			if(details==null)
				continue;
			for(InvocationDetail detail : invocationDetailService.findByInvocationIdAndGoingOut(invocation.getId(), true)) {
				LOGGER.debug("["+invocationCode+"][out]["+externalMessage.getTransactionId()+"] Process expression \""+detail.getProcess()+"\"");
				if(detail.getProcess()==null || detail.getProcess().isBlank())
					continue;
				try {
					expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(detail.getProcess(), false)).getValue(context);
				}catch(SpelEvaluationException | SpelParseException e) {
					LOGGER.error("["+logId+"]["+invocationCode+"][out] Error processing expression '"+detail.getProcess()+"', cause by: "+e.getMessage());
					e.printStackTrace();
					if(internalMessage.isRequest()) {
						LOGGER.trace("["+logId+"]["+invocationCode+"][out] Sending back to router with response: "+GenericResponse.ERROR);
						internalMessage.setSystemResponseCode(GenericResponse.ERROR.getCode());
						internalMessage.setSystemResponseDesc(e.getMessage());
						internalMessage.setResponse();
						LOGGER.trace("["+invocationCode+"][out]["+externalMessage.getTransactionId()+"] Send internal message to router: \n"+internalMessage);
						LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
						producerTemplate.sendBody("router."+internalMessage.getRouteCode()+".in", internalMessage);
						return;
					}else {
						LOGGER.error("["+logId+"]["+invocationCode+"][out] Dropping message");
						return;
					}
						
				}
			}
		}

		LOGGER.trace("["+logId+"]["+invocationCode+"][out] Post-processing\nExternal message: \n"+externalMessage+"\nInternal message: \n"+internalMessage+"\n");
		/*
		 * If request means send message to issuer endpoint,
		 * back to route logic with system response code set to error if got error processing expression
		 */
		if(externalMessage.isRequest()) {
			try {
				internalMessage.addTrack(endpointCode, identity.getHost(), invocationCode, invocation.getId(), null);
			} catch (UnknownHostException e) {
				e.printStackTrace();
				internalMessage.addTrack(endpointCode, "UnknownHost", invocationCode, invocation.getId(), null);
			}			
			internalMessages.put(internalMessage.getTransactionId(), internalMessage);
			LOGGER.trace("["+logId+"]["+invocationCode+"][out] Send external request message to issuer endpoint: \n"+externalMessage);
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			kafkaTemplate.send(invocation.getEndpointCode()+".out", externalMessage);	

		/*
		 * if response means the message for acquire endpoint, dp nothing if got error processing expression	
		 */
		}else {
			externalMessage.setAssocKey(internalMessage.getOriginAssocKey());
			internalMessages.remove(internalMessage.getTransactionId());
			
			threadPoolTaskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					String transactionId = internalMessage.getTransactionId();
					IMap<String, MessageLog> messageLogs = hazelcastInstance.getMap(CacheName.MESSAGE_LOGS);
					MessageLog messageLog = messageLogs.get(transactionId);
					messageLog.setRouteCode(internalMessage.getRouteCode());
					messageLog.setInvocationCode(internalMessage.getOriginInvocationCode());
					messageLog.setOriginEndpoint(internalMessage.getOriginEndpointCode());
					messageLog.setInternalMessage(internalMessage.toString());
					messageLogs.set(transactionId, messageLog);
				}
			});
			
			LOGGER.trace("["+logId+"]["+invocationCode+"][out] Send external response message to acquire endpoint: \n"+externalMessage);
			LOGGER.trace("["+logId+"]["+invocationCode+"][in] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			kafkaTemplate.send(internalMessage.getOriginEndpointCode()+"."+internalMessage.getOriginNode()+".out", externalMessage);
		}
	}

}
