package com.bifrost.orchestrator.context;

import java.util.List;
import java.util.Map;

import com.bifrost.exception.modifier.SpELProcessException;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public class MapFunction {
	
	private static MapFunction obj;
	
	public static MapFunction getInstance() {
		if(obj==null)
			obj = new MapFunction();
		return obj;
	}

	public Map<String, Object> replaceKey(Map<String, Object> source, String[] oldKeys, String[] newKeys) {
		for(int j=0;j<oldKeys.length;j++) {
			String oldKey = oldKeys[j];
			String newKey;
			try {
				newKey = newKeys[j];
			}catch(ArrayIndexOutOfBoundsException e) {
				continue;
			}
			if(source.get(oldKey)!=null) {
				Object value = source.remove(oldKey);
				source.put(newKey, value);	
			}
		}
		return source;
	}
	
	public List<Map<String, Object>> replaceKeyInList(List<Map<String, Object>> source, String[] oldKeys, String[] newKeys) {
		for(int i=0;i<source.size();i++) {
			Map<String, Object> map = replaceKey(source.get(i), oldKeys, newKeys);
			source.set(i, map);
		}
		return source;
	}
	
	public Map<String, Object> writeValueAsString(Map<String, Object> source, String key, Object... params) throws SpELProcessException {
		Object value = source.get(key);
		if(value!=null && value instanceof Double) {
			if(params[0]==null)
				throw new SpELProcessException("Decimal point cannot be null for value type Double");
			else if(!(params[0] instanceof Integer))
				throw new SpELProcessException("Decimal point must be type of int for value type Double");
			else {
				Double dbl = (Double) value;
				String result = DoubleFunction.getInstance().writeAsString(dbl, (int) params[0]);
				source.put(key, result);
			}
		}
		return source;
	}

	public List<Map<String, Object>> writeValueAsStringInList(List<Map<String, Object>> source, String key, Object... params) throws SpELProcessException {
		for(int i=0;i<source.size();i++) {
			Map<String, Object> map = writeValueAsString(source.get(i), key, params);
			source.set(i, map);
		}
		return source;
	}
}
