package com.bifrost.orchestrator.processor.mapper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Component;

import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.message.ExternalMessage;
import com.bifrost.message.GenericMessage;
import com.bifrost.message.InternalMessage;
import com.bifrost.orchestrator.context.CacheHolder;
import com.bifrost.orchestrator.model.mapper.ResponseDesc;
import com.bifrost.orchestrator.service.mapper.ResponseCodeService;
import com.bifrost.orchestrator.service.mapper.ResponseDescService;
import com.bifrost.system.library.LibraryMap;

@Component
public class ResponseMapperProcessor implements Processor, MessageListener<String, ExternalMessage> {

	private static final Log LOGGER = LogFactory.getLog(ResponseMapperProcessor.class);

	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate;
	
	@Autowired
	private ResponseCodeService responseCodeService;
	
	@Autowired
	private ResponseDescService responseDescService;

	@Autowired
	private ExpressionParser expressionParser;
	
	@Autowired
	private ProducerTemplate producerTemplate;
	
	@Autowired
	private CacheHolder cacheHolder;

	@Autowired
	private LibraryMap libraries;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		ResponseDesc source = null;
		ResponseDesc target = null;
		InternalMessage internalMessage = exchange.getMessage().getBody(InternalMessage.class);
		String logId = internalMessage.getOriginEndpointCode()+":"+internalMessage.getOriginAssocKey();
		LOGGER.trace("["+logId+"][mapper] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		LOGGER.trace("["+logId+"][mapper] Incoming internal message from router: \n"+internalMessage+"\n");
		String sysResponseCode = (String) internalMessage.getSystemResponseCode();
		
		/*
		 * If system response code is exists, then it will find target mapping from system response as its source
		 */
		if(sysResponseCode!=null) {
			source = responseDescService.findByEndpointCodeNullAndResponseCode(sysResponseCode);
			source.setCode(responseCodeService.findById(source.getCode().getId()));
			LOGGER.trace("["+logId+"][mapper] Using system response as source: "+source);
			List<ResponseDesc> targetList = responseDescService.findTargetBySourceEndpointDescIdAndTargetEndpointCode(source.getId(), internalMessage.getOriginEndpointCode());
			target = processTarget(targetList, logId, internalMessage);
		}else {
			String endpointCode = (String) internalMessage.getLastEndpointCode();
			String responseCode = (String) internalMessage.getLastResponseCode();
			List<ResponseDesc> sourceList = responseDescService.findByEndpointCodeAndResponseCode(endpointCode, responseCode);
			if(sourceList==null || sourceList.size()==0) {
				LOGGER.trace("["+logId+"][mapper] Unknown "+endpointCode+" response code: "+responseCode);
				LOGGER.trace("["+logId+"][mapper] Using system response as source: "+GenericResponse.DEFAULT);
				source = responseDescService.findByEndpointCodeNullAndResponseCode(GenericResponse.DEFAULT.getCode());
				source.setCode(responseCodeService.findById(source.getCode().getId()));
				List<ResponseDesc> targetList = responseDescService.findTargetBySourceEndpointDescIdAndTargetEndpointCode(source.getId(), internalMessage.getOriginEndpointCode());
				target = processTarget(targetList, logId, internalMessage);
			}else {
				Set<ResponseDesc> sources = new HashSet<ResponseDesc>(sourceList);
				LOGGER.trace("["+logId+"][mapper] Selecting endpoint "+endpointCode+" with response code "+responseCode+" for source");
				LOGGER.trace("["+logId+"][mapper] Found "+sources.size()+" available response source description: ");
				logSetOfResponseDesc(internalMessage, sources, logId);
				source = findMatchingResponse(internalMessage, sources, logId);
				source.setCode(responseCodeService.findById(source.getCode().getId()));
				LOGGER.trace("["+logId+"][mapper] Using response "+source+" as source");
				List<ResponseDesc> targetList = responseDescService.findTargetBySourceEndpointDescIdAndTargetEndpointCode(source.getId(), internalMessage.getOriginEndpointCode());
				target = processTarget(targetList, logId, internalMessage);
			}
		}

		if(target==null) {
			LOGGER.trace("["+logId+"][mapper] Cannot find matching target response, using "+GenericResponse.NO_MATCHING_RESPONSE+" as target");
			LOGGER.debug("["+logId+"][mapper] Mapping result: "+source+" to "+GenericResponse.NO_MATCHING_RESPONSE);
			internalMessage.setMappedResponseCode(GenericResponse.NO_MATCHING_RESPONSE.getCode());
			internalMessage.setMappedResponseDesc(GenericResponse.NO_MATCHING_RESPONSE.getDesc());
		}else {
			LOGGER.debug("["+logId+"][mapper] Mapping result: "+source+" to "+target);
			internalMessage.setMappedResponseCode(target.getCode().getCode());
			internalMessage.setMappedResponseDesc(target.getDescription());
		}
		internalMessage.setResponse();
		LOGGER.trace("["+logId+"][mapper] Send internal message to modifier: \n"+internalMessage);
		LOGGER.trace("["+logId+"][mapper] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		producerTemplate.sendBody("direct:modifier."+internalMessage.getOriginEndpointCode()+"."+internalMessage.getOriginInvocationCode()+".out", internalMessage);
	}
	
	private ResponseDesc processTarget(List<ResponseDesc> targetList, String logId, GenericMessage message) {
		if(targetList==null) {
			LOGGER.trace("["+logId+"][mapper] Found 0 available endpoint "+message.getOriginEndpointCode()+" response target description: ");
			return null;
		}else {
			Set<ResponseDesc> targets = new HashSet<ResponseDesc>(targetList);
			LOGGER.trace("["+logId+"][mapper] Found "+targets.size()+" available endpoint "+message.getOriginEndpointCode()+" response target description: ");

			if(LOGGER.isTraceEnabled())
				logSetOfResponseDesc(message, targets, logId);
			
			return findMatchingResponse(message, targets, logId);	
		}
	}
	
	private void logSetOfResponseDesc(GenericMessage message, Set<ResponseDesc> descs, String logId) {
		for(ResponseDesc desc : descs) {
			desc.setCode(responseCodeService.findById(desc.getCode().getId()));
			LOGGER.trace("["+logId+"][mapper] "+desc+", condition: '"+desc.getCondition()+"'");
		}
	}
	
	private ResponseDesc findMatchingResponse(GenericMessage message, Set<ResponseDesc> responses, String logId) {
		StandardEvaluationContext context = new StandardEvaluationContext();
		context.setVariable("imsg", message);
		context.setVariable("bean", libraries);
		context.setVariable("cache", cacheHolder);
		for(ResponseDesc response : responses)
			try {
				if(response.getCondition()==null)
					return response;
				else if(expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(response.getCondition(), false)).getValue(context, Boolean.class))
					return response;	
			}catch(SpelEvaluationException e) {
				LOGGER.error("["+logId+"][mapper] Failed processing expression '"+response.getCondition()+"', cause by: "+e.getMessage());
				e.printStackTrace();
				continue;
			}
		return null;
	}

	@Override
	public void onMessage(ConsumerRecord<String, ExternalMessage> data) {
		String logId = null;
		ResponseDesc source = null;
		ResponseDesc target = null;
		ExternalMessage externalMessage = (ExternalMessage) data.value();
		logId = externalMessage.getOriginEndpointCode()+":"+externalMessage.getOriginAssocKey();
		LOGGER.trace("["+logId+"][mapper] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		LOGGER.trace("["+logId+"][mapper] Incoming external message from acquire endpoint: \n"+externalMessage+"\n");
		String sysResponseCode = (String) externalMessage.getSystemResponseCode();
		if(sysResponseCode!=null) {
			source = responseDescService.findByEndpointCodeNullAndResponseCode(sysResponseCode);
			LOGGER.trace("["+logId+"][mapper] Using system response as source: "+source);
			List<ResponseDesc> targetList = responseDescService.findTargetBySourceEndpointDescIdAndTargetEndpointCode(source.getId(), externalMessage.getOriginEndpointCode());
			target = processTarget(targetList, logId, externalMessage);
		}

		if(target==null) {
			LOGGER.trace("["+logId+"][mapper] Cannot find matching target response, using "+GenericResponse.NO_MATCHING_RESPONSE+" as target");
			LOGGER.debug("["+logId+"][mapper] Mapping result: "+source+" to "+GenericResponse.NO_MATCHING_RESPONSE);
			externalMessage.setResponseCode(GenericResponse.NO_MATCHING_RESPONSE.getCode());
			externalMessage.setResponseDesc(GenericResponse.NO_MATCHING_RESPONSE.getDesc());
		}else {
			LOGGER.debug("["+logId+"][mapper] Mapping result: "+source+" to "+target);
			externalMessage.setResponseCode(target.getCode().getCode());
			externalMessage.setResponseDesc(target.getDescription());
		}
		LOGGER.trace("["+logId+"][mapper] Send external message to acquire endpoint: \n"+externalMessage);
		LOGGER.trace("["+logId+"][mapper] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		kafkaTemplate.send(externalMessage.getOriginEndpointCode()+"."+externalMessage.getOriginNode()+".out", externalMessage);
	}
}
