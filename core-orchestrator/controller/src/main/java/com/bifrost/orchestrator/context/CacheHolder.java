package com.bifrost.orchestrator.context;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.HazelcastInstance;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class CacheHolder {

	private static final String BASE = "system:cache:";
	private static final String COMMON = BASE+"common";
	
	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	public Object get(String key) {
		return hazelcastInstance.getMap(COMMON).get(key);
	}
	
	public Object remove(String key) {
		return hazelcastInstance.getMap(COMMON).remove(key);
	}
	
	public void put(String key, Object value) {
		put(key, value, 600000);
	}
	
	public void put(String key, Object value, Integer ttl) {
		hazelcastInstance.getMap(COMMON).setAsync(key, value, ttl, TimeUnit.MILLISECONDS);
	}
	
	public void put(String key, Object value, Long ttl) {
		hazelcastInstance.getMap(COMMON).setAsync(key, value, ttl, TimeUnit.MILLISECONDS);
	}
	
	public Object get(String key, String cacheName) {
		return hazelcastInstance.getMap(BASE+cacheName).get(key);
	}
	
	public Object remove(String key, String cacheName) {
		return hazelcastInstance.getMap(BASE+cacheName).remove(key);
	}
	
	public void put(String key, Object value, String cacheName) {
		put(key, value, cacheName, 600000);
	}
	
	public void put(String key, Object value, String cacheName, Long ttl) {
		hazelcastInstance.getMap(BASE+cacheName).setAsync(key, value, ttl, TimeUnit.MILLISECONDS);
	}
	
	public void put(String key, Object value, String cacheName, Integer ttl) {
		hazelcastInstance.getMap(BASE+cacheName).setAsync(key, value, ttl, TimeUnit.MILLISECONDS);
	}
}
