package com.bifrost.orchestrator.processor.modifier;

import java.util.Map;

import org.apache.camel.ProducerTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.bifrost.message.InternalMessage;
import com.bifrost.orchestrator.context.CacheHolder;
import com.bifrost.orchestrator.context.ParamHolder;
import com.bifrost.orchestrator.model.modifier.Invocation;
import com.bifrost.orchestrator.service.modifier.InvocationDetailService;
import com.bifrost.orchestrator.service.modifier.InvocationService;
import com.bifrost.system.configuration.AbstractSystemIdentity;
import com.bifrost.system.library.LibraryMap;
import com.hazelcast.core.HazelcastInstance;

public class GenericProcessor {

	protected InvocationDetailService invocationDetailService;
	protected Map<String, InternalMessage> internalMessages;
	protected ThreadPoolTaskExecutor threadPoolTaskExecutor;
	protected InvocationService invocationService;
	protected HazelcastInstance hazelcastInstance;
	protected ExpressionParser expressionParser;
	protected ProducerTemplate producerTemplate;
	protected AbstractSystemIdentity identity;
	protected final Invocation invocation;
	protected ParamHolder paramHolder;
	protected CacheHolder cacheHolder;
	protected LibraryMap libraries;
	
	public GenericProcessor(Invocation invocation, Map<String, InternalMessage> internalMessages, ApplicationContext context) {
		this.internalMessages = internalMessages;
		this.invocation = invocation;
		
		// Injecting necessery spring bean if application context is exists
		if(context!=null) {
			this.invocationDetailService = context.getBean(InvocationDetailService.class);
			this.threadPoolTaskExecutor = context.getBean(ThreadPoolTaskExecutor.class);
			this.invocationService = context.getBean(InvocationService.class);
			this.hazelcastInstance = context.getBean(HazelcastInstance.class);
			this.expressionParser = context.getBean(ExpressionParser.class);
			this.producerTemplate = context.getBean(ProducerTemplate.class);
			this.identity = context.getBean(AbstractSystemIdentity.class);
			this.paramHolder = context.getBean(ParamHolder.class);
			this.cacheHolder = context.getBean(CacheHolder.class);
			this.libraries = context.getBean(LibraryMap.class);
		}
	}
}
