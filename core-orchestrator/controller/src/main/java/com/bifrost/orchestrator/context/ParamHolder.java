package com.bifrost.orchestrator.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bifrost.system.service.SystemParamService;

@Component
public class ParamHolder {

	@Autowired
	public SystemParamService paramService;
	
	public String get(String name) {
		return paramService.findByName(name).getValue();
	}
	
}
