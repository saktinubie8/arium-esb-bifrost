package com.bifrost.orchestrator.controller;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.orchestrator.model.router.Route;
import com.bifrost.orchestrator.processor.router.RouteProcessor;
import com.bifrost.orchestrator.service.router.RouteService;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class RouterController {

	private static final Log LOGGER = LogFactory.getLog(RouterController.class);
	
	@Autowired
	private RouteService routeService;
	
	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private ApplicationContext appContext;
	
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;
	
	@Value("${" + ParamConstant.Kafka.ROUTER_KAFKA_CONCURRENCY + ":10}")
	private String kafkaConcurrency;
	
	@PostConstruct
	public void init() {
		if(!camelContext.isStarted())
			camelContext.start();
		
		// Create camel route as many as route records
		for(Route route : routeService.findAll()) {
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {

					String routeCode = route.getCode();
					LOGGER.trace("["+routeCode+"] Initiating router");
					RouteBuilder routeBuilder = new RouteBuilder() {
						
						@Override
						public void configure() throws Exception {
							from("direct:router."+routeCode+".in").process(new RouteProcessor(route, appContext)).setId(String.valueOf(route.getId()));
						}
					};
					
					try {
						camelContext.addRoutes(routeBuilder);
						LOGGER.trace("["+routeCode+"] Router is initiated successfully");
					} catch (Exception e) {
						LOGGER.error("["+routeCode+"] Failed when initiating router, cause by: "+e.getMessage());
						e.printStackTrace();
					}
				}
			});
		}
	}
}
