package com.bifrost.orchestrator.controller;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.message.InternalMessage;
import com.bifrost.orchestrator.model.modifier.Invocation;
import com.bifrost.orchestrator.processor.modifier.InProcessor;
import com.bifrost.orchestrator.processor.modifier.OutProcessor;
import com.bifrost.orchestrator.service.modifier.InvocationService;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class ModifierController {

	private static final Log LOGGER = LogFactory.getLog(ModifierController.class);
	private Map<String, InternalMessage> internalMessages = new ConcurrentHashMap<String, InternalMessage>();
	private Map<Long, ConcurrentMessageListenerContainer<String, Object>> inConsumers = new ConcurrentHashMap<Long, ConcurrentMessageListenerContainer<String, Object>>();
	
	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private InvocationService invocationService;
	
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	private ConsumerFactory<String, Object> consumerFactory;
	
	@Value("${" + ParamConstant.Kafka.MODIFIER_KAFKA_CONCURRENCY + ":10}")
	private String kafkaConcurrency;
	
	@PostConstruct
	public void init() {
		
		// Create kafka consumer as many as invocation records in database
		// If invocation kafka consumer already exist then continue
		for(Invocation invocation : invocationService.findAll()) {
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					String endpointCode = invocation.getEndpointCode();
					String invocationCode = invocation.getCode();
					
					// Going in consumer initiation
					ConcurrentMessageListenerContainer<String, Object> inContainer = inConsumers.get(invocation.getId());
					if(inContainer!=null)
						inContainer.stop();
					
					String topicIn = "modifier."+invocation.getEndpointCode()+"."+invocation.getCode().replaceAll("[^\\dA-Za-z ]", "_")+".in";
					LOGGER.trace("["+endpointCode+"]["+invocationCode+"] Initiating in consumer with topic '"+topicIn+"'");
					ContainerProperties inContainerProps = new ContainerProperties(topicIn);
					inContainerProps.setMissingTopicsFatal(false);
					InProcessor inProcessor = new InProcessor(invocation, internalMessages, context);
					inContainerProps.setMessageListener(inProcessor);
					inContainer = new ConcurrentMessageListenerContainer<String, Object>(consumerFactory, inContainerProps);
					inContainer.setConcurrency(Integer.valueOf(kafkaConcurrency));
					inConsumers.put(invocation.getId(), inContainer);
					
					RouteBuilder outRoute = new RouteBuilder() {
						
						@Override
						public void configure() throws Exception {
							OutProcessor outProcessor = new OutProcessor(invocation, internalMessages, context);
							from("direct:modifier."+invocation.getId()+".out").process(outProcessor).setId(String.valueOf(invocation.getId()+"a"));
							from("direct:modifier."+invocation.getEndpointCode()+"."+invocation.getCode()+".out").process(outProcessor).setId(String.valueOf(invocation.getId()+"b"));
						}
					};
					
					try {
						camelContext.addRoutes(outRoute);
						LOGGER.trace("["+endpointCode+"]["+invocationCode+"] Consumer initiated successfully");	
					} catch (Exception e) {
						LOGGER.error("["+endpointCode+"]["+invocationCode+"] Failed when initiating consumer, cause by: "+e.getMessage());
						e.printStackTrace();
					}
				}
			});
		}		
	}
	
	public void startAll() {
		inConsumers.forEach((k,v)->{
			start(k);
		});
	}
	
	public void stopAll() {
		inConsumers.forEach((k,v)->{
			stop(k);
		});
	}

	public void restartAll() {
		inConsumers.forEach((k,v)->{
			restart(k);
		});
	}
	
	public void start(Long invocationId) {
		ConcurrentMessageListenerContainer<String, Object> consumer = inConsumers.get(invocationId);
		if(!consumer.isRunning())
			consumer.start();
	}
	
	public void stop(Long invocationId) {
		ConcurrentMessageListenerContainer<String, Object> consumer = inConsumers.get(invocationId);
		if(!consumer.isRunning())
			consumer.stop();
	}
	
	public void restart(Long invocationId) {
		stop(invocationId);
		start(invocationId);
	}
}
