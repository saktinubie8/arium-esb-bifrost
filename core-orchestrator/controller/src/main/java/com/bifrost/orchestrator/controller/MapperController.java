package com.bifrost.orchestrator.controller;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.stereotype.Service;

import com.bifrost.common.constant.ParamConstant;
import com.bifrost.orchestrator.processor.mapper.ResponseMapperProcessor;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class MapperController {
	
	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private ResponseMapperProcessor responseMapperProcessor;

	@Autowired
	private ConsumerFactory<String, Object> consumerFactory;
	
	@Value("${" + ParamConstant.Kafka.MAPPER_KAFKA_CONCURRENCY + ":10}")
	private String kafkaConcurrency;
	
	@PostConstruct
	public void init() throws Exception {	
		if(!camelContext.isStarted())
			camelContext.start();
		
		// Response mapper consumer initiation
		ContainerProperties containerProps = new ContainerProperties("mapper.response.in");
		containerProps.setMessageListener(responseMapperProcessor);
		containerProps.setMissingTopicsFatal(false);
		ConcurrentMessageListenerContainer<String, Object> container = new ConcurrentMessageListenerContainer<String, Object>(consumerFactory, containerProps);
		container.setConcurrency(Integer.valueOf(kafkaConcurrency));
		container.start();
		
		RouteBuilder routeBuilder = new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				from("direct:mapper.response.in").process(responseMapperProcessor).setId("mapper.response");;
			}
		};
		
		camelContext.addRoutes(routeBuilder);
	}
}
