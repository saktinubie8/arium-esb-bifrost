package com.bifrost.orchestrator.model.router;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "route_detail")
public class RouteDetail implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "route_id", nullable = false)
	private Route route;

	@Column(name = "last_invocation_id", nullable = true)
	private Long lastInvokeId;

	@Column(name = "condition", nullable = true)
	private String condition;

	@Column(name = "next_invocation_id", nullable = false)
	private Long nextInvokeId;

	@Column(name = "priority", nullable = false, columnDefinition = "integer default 0")
	private int priority;

	@Column(name = "wait_response", nullable = false, columnDefinition = "boolean default true")
	private Boolean waitResponse;

	@Column(name = "in_background", nullable = false, columnDefinition = "boolean default false")
	private Boolean inBackgroud;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Long getLastInvokeId() {
		return lastInvokeId;
	}

	public void setLastInvokeId(Long lastInvokeId) {
		this.lastInvokeId = lastInvokeId;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Long getNextInvokeId() {
		return nextInvokeId;
	}

	public void setNextInvokeId(Long nextInvokeId) {
		this.nextInvokeId = nextInvokeId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Boolean getWaitResponse() {
		return waitResponse;
	}

	public void setWaitResponse(Boolean waitResponse) {
		this.waitResponse = waitResponse;
	}

	public Boolean getInBackgroud() {
		return inBackgroud;
	}

	public void setInBackgroud(Boolean inBackgroud) {
		this.inBackgroud = inBackgroud;
	}

	@Override
	public Object getPk() {
		return id;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeLong(route.getId());
		if(lastInvokeId!=null) {
			out.writeBoolean(true);
			out.writeLong(lastInvokeId);
		}else
			out.writeBoolean(false);

		if(condition!=null) {
			out.writeBoolean(true);
			out.writeUTF(condition);
		}else
			out.writeBoolean(false);
		out.writeLong(nextInvokeId);
		out.writeInt(priority);
		out.writeBoolean(waitResponse);
		out.writeBoolean(inBackgroud);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		if(route==null)
			route = new Route();
		route.setId(in.readLong());
		if(in.readBoolean())
			lastInvokeId = in.readLong();

		if(in.readBoolean())
			condition = in.readUTF();
		nextInvokeId = in.readLong();
		priority = in.readInt();
		waitResponse = in.readBoolean();
		inBackgroud = in.readBoolean();
	}
}
