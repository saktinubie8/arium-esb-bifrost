package com.bifrost.orchestrator.repository.modifier;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.orchestrator.model.modifier.InvocationDetail;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface InvocationDetailRepository extends JpaRepository<InvocationDetail, Long>{

	public List<InvocationDetail> findByInvocation_IdAndGoingOutOrderByChainAscIdAsc(Long invocationId, Boolean goingOut);
	
}
