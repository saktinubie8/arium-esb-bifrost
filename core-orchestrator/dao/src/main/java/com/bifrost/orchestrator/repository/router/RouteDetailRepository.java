package com.bifrost.orchestrator.repository.router;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.orchestrator.model.router.RouteDetail;


/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface RouteDetailRepository extends JpaRepository<RouteDetail, Long> {

	public List<RouteDetail> findByRoute_Code(String routeCode);
	
	public List<RouteDetail> findByRoute_CodeAndLastInvokeIdIsNullOrderByPriorityDesc(String routeCode);
	
	public List<RouteDetail> findByRoute_CodeAndLastInvokeIdOrderByPriorityDesc(String routeCode, Long lastInvokeId);
	
}
