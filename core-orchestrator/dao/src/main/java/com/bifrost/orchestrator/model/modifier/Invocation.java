package com.bifrost.orchestrator.model.modifier;

import java.io.IOException;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "invocation", uniqueConstraints = {@UniqueConstraint(columnNames = {"endpoint_code", "code"})})
public class Invocation implements AbstractModel {

	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "endpoint_code", length = 20)
	private String endpointCode;

	@Column(name = "code", length = 255)
	private String code;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parent_invocation_id")
	private Invocation parentInvoke;

	@OneToMany(mappedBy = "parentInvoke", fetch = FetchType.EAGER)
	private Set<Invocation> childInvoke;

	@OneToMany(mappedBy = "invocation", fetch = FetchType.EAGER)
	@OrderBy("chain ASC, id ASC")
	private Set<InvocationDetail> details;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Invocation getParentInvoke() {
		return this.parentInvoke;
	}

	public void setParentInvoke(Invocation parentInvoke) {
		this.parentInvoke = parentInvoke;
	}

	public void setChildInvoke(Set<Invocation> childInvoke) {
		this.childInvoke = childInvoke;
	}

	public void setDetails(Set<InvocationDetail> details) {
		this.details = details;
	}

	public Object getPk() {
		return id;
	}

	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(endpointCode);
		out.writeUTF(code);
		out.writeUTF(description);
		if(parentInvoke!=null) {
			out.writeBoolean(true);
			out.writeLong(parentInvoke.getId());
		}else
			out.writeBoolean(false);
	}

	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		endpointCode = in.readUTF();
		code = in.readUTF();
		description = in.readUTF();
		if(in.readBoolean()) {
			if(parentInvoke==null)
				parentInvoke = new Invocation();
			parentInvoke.setId(in.readLong());
		}
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	

}