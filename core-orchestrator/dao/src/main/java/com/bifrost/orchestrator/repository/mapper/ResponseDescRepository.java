package com.bifrost.orchestrator.repository.mapper;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.orchestrator.model.mapper.ResponseDesc;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface ResponseDescRepository extends JpaRepository<ResponseDesc, Long> {

	public ResponseDesc findByCode_EndpointCodeIsNullAndCode_Code(String responseCode);
	public List<ResponseDesc> findByCode_EndpointCodeAndCode_Code(String endpointCode, String responseCode);
	
}
