package com.bifrost.orchestrator.repository.router;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.orchestrator.model.router.Route;


/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface RouteRepository extends JpaRepository<Route, Long> {

	public Route findByCode(String routeCode);
	
}
