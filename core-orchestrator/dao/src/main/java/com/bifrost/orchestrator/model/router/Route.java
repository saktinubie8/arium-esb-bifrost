package com.bifrost.orchestrator.model.router;

import java.io.IOException;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "route")
public class Route implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;
	
	@Column(name = "code", nullable = false, unique = true, length = 40)
	private String code;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@OneToMany(mappedBy = "route", fetch = FetchType.EAGER)
	private Set<RouteDetail> details;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<RouteDetail> getDetails() {
		return details;
	}

	public void setDetails(Set<RouteDetail> details) {
		this.details = details;
	}

	@Override
	public Object getPk() {
		return id;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(code);
		out.writeUTF(description);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		code = in.readUTF();
		description = in.readUTF();
	}

}
