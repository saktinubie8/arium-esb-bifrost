package com.bifrost.orchestrator.model.mapper;

import java.io.IOException;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "response_code", uniqueConstraints = { @UniqueConstraint(columnNames = { "endpoint_code", "code" }) })
public class ResponseCode implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "endpoint_code", nullable = true, length = 20)
	private String endpointCode;

	@Column(name = "code", nullable = false, length = 20)
	private String code;

	@OneToMany(mappedBy = "code", fetch = FetchType.EAGER)
	private Set<ResponseDesc> descriptions;

	public ResponseCode() {
	}

	public ResponseCode(String endpointCode, String code) {
		this.endpointCode = endpointCode;
		this.code = code;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEndpointCode() {
		return endpointCode;
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDescriptions(Set<ResponseDesc> descriptions) {
		this.descriptions = descriptions;
	}

	public Object getPk() {
		return id;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(endpointCode);
		out.writeUTF(code);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		endpointCode = in.readUTF();
		code = in.readUTF();
	}

}
