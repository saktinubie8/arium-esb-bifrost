package com.bifrost.orchestrator.repository.modifier;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.orchestrator.model.modifier.Invocation;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface InvocationRepository extends JpaRepository<Invocation, Long> {
	
	public Invocation findByEndpointCodeAndCode(String endpointCode, String invocationCode);
	
}
