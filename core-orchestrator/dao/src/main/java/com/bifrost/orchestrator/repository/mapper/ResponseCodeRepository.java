package com.bifrost.orchestrator.repository.mapper;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.orchestrator.model.mapper.ResponseCode;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface ResponseCodeRepository extends JpaRepository<ResponseCode, Long> {
	
	public ResponseCode findByEndpointCodeAndCode(String endpointCode, String code);
	public ResponseCode findByEndpointCodeIsNullAndCode(String code);

}
