package com.bifrost.orchestrator.model.mapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "response_desc", uniqueConstraints = {@UniqueConstraint(columnNames = {"rc_id", "condition"})})
public class ResponseDesc implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "rc_id", nullable = false)
	private ResponseCode code;

	@Column(name = "priority", nullable = false, columnDefinition = "integer default 0")
	private int priority;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@Column(name = "condition", nullable = true)
	private String condition;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "response_mapping", 
		joinColumns = @JoinColumn(name = "target_id"), 
		inverseJoinColumns = @JoinColumn(name = "source_id"))
	@OrderBy("priority DESC")
	private Set<ResponseDesc> source;

	@ManyToMany(mappedBy = "source", fetch = FetchType.EAGER)
	@OrderBy("priority DESC")
	private Set<ResponseDesc> target;

	public ResponseDesc() {
	}

	public ResponseDesc(ResponseCode responseCode, String description, String condition) {
		this.code = responseCode;
		this.description = description;
		this.condition = condition;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ResponseCode getCode() {
		return code;
	}

	public void setCode(ResponseCode responseCode) {
		this.code = responseCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Set<ResponseDesc> getSource() {
		return source;
	}

	public void setSource(Set<ResponseDesc> source) {
		this.source = source;
	}

	public Set<ResponseDesc> getTarget() {
		return target;
	}

	public void setTarget(Set<ResponseDesc> target) {
		this.target = target;
	}

	public Object getPk() {
		return id;
	}

	@Override
	public String toString() {
		return (code.getEndpointCode()==null?"SYS":code.getEndpointCode())+":"+code.getCode()+":"+description;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeLong(code.getId());
		out.writeInt(priority);
		out.writeUTF(description);
		if(condition!=null) {
			out.writeBoolean(true);
			out.writeUTF(condition);
		}else
			out.writeBoolean(false);
		
		if(source!=null) {
			out.writeBoolean(true);
			writeMapped(source, out);	
		}else
			out.writeBoolean(false);

		if(target!=null) {
			out.writeBoolean(true);
			writeMapped(target, out);
		}else
			out.writeBoolean(false);
	}
	
	private void writeMapped(Set<ResponseDesc> set, ObjectDataOutput out) throws IOException {
		out.writeInt(set.size());
		Iterator<ResponseDesc> iter = set.iterator();
		while(iter.hasNext()) {
			ResponseDesc desc = iter.next();
			out.writeLong(desc.getId());
		}
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		code = new ResponseCode();
		code.setId(in.readLong());
		priority = in.readInt();
		description = in.readUTF();
		if(in.readBoolean())
			condition = in.readUTF();
		if(in.readBoolean())
			source = readMapped(in);
		if(in.readBoolean())
			target = readMapped(in);
	}
	
	private Set<ResponseDesc> readMapped(ObjectDataInput in) throws IOException {
		int size = in.readInt();
		Set<ResponseDesc> retVal = new HashSet<ResponseDesc>();
		for(int i=0;i<size;i++) {
			ResponseDesc desc = new ResponseDesc();
			desc.setId(in.readLong());
			retVal.add(desc);
		}
		return retVal;
	}

}
