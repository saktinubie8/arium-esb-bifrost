package com.bifrost.orchestrator.model.modifier;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bifrost.common.model.AbstractModel;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "invocation_detail")
public class InvocationDetail implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "invocation_id", nullable = false)
	private Invocation invocation;

	@Column(name = "chain", nullable = false, columnDefinition = "integer default 0")
	private int chain;

	@Column(name = "going_out", nullable = false)
	private Boolean goingOut;

	@Column(name = "process", nullable = false, length = 1024)
	private String process;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Invocation getInvocation() {
		return invocation;
	}

	public void setInvocation(Invocation invocation) {
		this.invocation = invocation;
	}

	public int getChain() {
		return chain;
	}

	public void setChain(int chain) {
		this.chain = chain;
	}

	public Boolean getGoingOut() {
		return goingOut;
	}

	public void setGoingOut(Boolean goingOut) {
		this.goingOut = goingOut;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public Object getPk() {
		return id;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeInt(chain);
		out.writeBoolean(goingOut);
		out.writeUTF(process);
		out.writeLong(invocation.getId());
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		chain = in.readInt();
		goingOut = in.readBoolean();
		process = in.readUTF();
		invocation = new Invocation();
		invocation.setId(in.readLong());
	}

}
