package com.bifrost.endpoint.rest.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.rest.model.EndpointRestApi;
import com.bifrost.endpoint.rest.repository.EndpointRestApiRepository;
import com.bifrost.exception.system.InvalidDataException;
import com.hazelcast.core.IMap;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointRestApiService extends GenericService<EndpointRestApiRepository, EndpointRestApi, Long>{

	private static final String KEY = "endpoint:rest:api";
	
	public EndpointRestApiService() {
		super(KEY);
	}

	@SuppressWarnings("unchecked")
	public List<EndpointRestApi> findByEndpointCode(String endpointCode) {
		return (List<EndpointRestApi>) hazelcastInstance.getMap(KEY+":findByEndpointCode").get(endpointCode);
	}

	@Cacheable(cacheNames = KEY+":findByPath")
	public EndpointRestApi findByPath(String path) {
		return (EndpointRestApi) hazelcastInstance.getMap(KEY+":findByPath").get(path);
	}

	@Cacheable(cacheNames = KEY+":findByEndpointCodeAndInvocationCode")
	public EndpointRestApi findByEndpointCodeAndPath(String endpointCode, String path) {
		return (EndpointRestApi) hazelcastInstance.getMap(KEY+":findByEndpointCodeAndInvocationCode").get(new SimpleKey(endpointCode, path));
	}
	
	@CachePut(cacheNames = KEY+":findByEndpointCodeAndInvocationCode", key = "new org.springframework.cache.interceptor.SimpleKey(#t.rest.endpoint.code, #t.invocationCode)")
	public EndpointRestApi save(EndpointRestApi t) {
		String prefix = formatPath(t.getRest().getPathPrefix());
		String path = formatPath(t.getPath());
		t.getRest().setPathPrefix(prefix);
		t.setPath(path);
		EndpointRestApi retVal = super.save(t);
		String endpointCode = retVal.getRest().getEndpointCode();

		IMap<String, List<EndpointRestApi>> findByEndpointCode = hazelcastInstance.getMap(KEY+":findByEndpointCode");
		findByEndpointCode.set(endpointCode, repository.findByRest_Endpoint_Code(endpointCode));
		IMap<String, EndpointRestApi> findByPath = hazelcastInstance.getMap(KEY+":findByPath");
		findByPath.set(retVal.getFullPath(), retVal);
		return retVal;
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndInvocationCode", key = "new org.springframework.cache.interceptor.SimpleKey(#t.rest.endpoint.code, #t.invocationCode)"),
			@CacheEvict(cacheNames = KEY+":findByEndpointCode", key = "#t.rest.endpoint.code")})
	public void delete(EndpointRestApi t) {
		super.delete(t);
		IMap<String, EndpointRestApi> findByPath = hazelcastInstance.getMap(KEY+":findByPath");
		findByPath.delete(t.getFullPath());
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = KEY+":findByEndpointCodeAndInvocationCode", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findByEndpointCode", allEntries = true),
			@CacheEvict(cacheNames = KEY+":findByPath", allEntries = true)})
	public void evictAll() {
		super.evictAll();
	}

	@Override
	@PostConstruct
	public void populate() throws Exception {
		super.populate();
		IMap<String, List<EndpointRestApi>> findByEndpointCode = hazelcastInstance.getMap(KEY+":findByEndpointCode");
		IMap<String, EndpointRestApi> findByPath = hazelcastInstance.getMap(KEY+":findByPath");
		IMap<SimpleKey, EndpointRestApi> findByEndpointCodeAndInvocationCode = hazelcastInstance.getMap(KEY+":findByEndpointCodeAndInvocationCode");
		for(EndpointRestApi t : repository.findAll()) {
			if(t.getRest()==null)
				throw new InvalidDataException("Endpoint REST '"+t.getRest().getEndpointCode()+"' with API ID "+t.getId()+" doesn't exists in table endpoint_rest");
			else if(t.getRest().getEndpoint()==null)
				throw new InvalidDataException("Endpoint REST '"+t.getRest().getEndpointCode()+"' doesn't exists in table endpoint");
			if(findByEndpointCode.get(t.getRest().getEndpoint().getCode())==null)
				findByEndpointCode.set(t.getRest().getEndpoint().getCode(), repository.findByRest_Endpoint_Code(t.getRest().getEndpoint().getCode()));
			if(findByPath.get(t.getFullPath())==null)
				findByPath.set(t.getFullPath(), t);
			SimpleKey key = new SimpleKey(t.getRest().getEndpoint().getCode(), t.getPath());
			if(findByEndpointCodeAndInvocationCode.get(key)==null)
				findByEndpointCodeAndInvocationCode.set(key, t);
		}
	}
	
	/**
	 * @param source path source
	 * @return path with changes '\' to '/' and tyding up start and end char
	 */
	private String formatPath(String source) {
		if(source==null)
			return "";
		else if(source.isEmpty())
			return source;
		else {
			source = source.replace("\\", "/");
			if(!source.startsWith("/"))
				source = "/"+source;
			if(source.endsWith("/"))
				source = source.substring(1);
			return source;
		}
	}
}
