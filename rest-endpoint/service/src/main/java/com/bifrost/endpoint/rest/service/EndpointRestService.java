package com.bifrost.endpoint.rest.service;

import org.springframework.stereotype.Service;

import com.bifrost.common.service.GenericService;
import com.bifrost.endpoint.rest.model.EndpointRest;
import com.bifrost.endpoint.rest.repository.EndpointRestRepository;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Service
public class EndpointRestService extends GenericService<EndpointRestRepository, EndpointRest, String>{

	private static final String KEY = "endpoint:rest";

	public EndpointRestService() {
		super(KEY);
	}
	
}