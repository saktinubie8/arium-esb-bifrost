package com.bifrost.endpoint.rest;


import org.springframework.stereotype.Component;

import com.bifrost.endpoint.packager.GenericPackageLoader;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class PackageLoader extends GenericPackageLoader {

}
