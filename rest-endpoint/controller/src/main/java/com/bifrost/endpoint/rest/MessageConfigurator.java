package com.bifrost.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bifrost.endpoint.AbstractMessageConfigurator;
import com.bifrost.endpoint.rest.model.EndpointRestApi;
import com.bifrost.endpoint.rest.service.EndpointRestApiService;
import com.bifrost.message.ExternalMessage;

@Component
public class MessageConfigurator implements AbstractMessageConfigurator {

	@Autowired
	private EndpointRestApiService service;
	
	@Override
	public void apply(ExternalMessage externalMessage) {
		EndpointRestApi api = service.findByEndpointCodeAndPath(externalMessage.getEndpointCode(), externalMessage.getInvocationCode());
		externalMessage.setResponseCodeInHeaders(api.getRcInHeaders());
		externalMessage.setResponseCodeField(api.getRcFieldName());
		externalMessage.setResponseDescInHeaders(api.getRdInHeaders());
		externalMessage.setResponseDescField(api.getRdFieldName());
		
		if(externalMessage.isResponse()) {
			String responseCode = externalMessage.getResponseCode();
			Object httpCode = externalMessage.getHeader("Http-Code");
			if((responseCode==null || responseCode.isBlank()) && httpCode!=null)
				externalMessage.setResponseCode(String.valueOf(httpCode));
		}
	}

}
