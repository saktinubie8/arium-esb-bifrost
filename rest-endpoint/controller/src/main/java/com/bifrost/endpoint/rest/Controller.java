package com.bifrost.endpoint.rest;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.util.NetworkUtils;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.endpoint.AbstractController;
import com.bifrost.endpoint.EndpointProcess;
import com.bifrost.endpoint.json.response.EndpointServiceResponse;
import com.bifrost.endpoint.model.Endpoint;
import com.bifrost.endpoint.model.EndpointType;
import com.bifrost.endpoint.rest.model.EndpointRest;
import com.bifrost.endpoint.rest.model.EndpointRestApi;
import com.bifrost.endpoint.rest.service.EndpointRestApiService;
import com.bifrost.endpoint.rest.service.EndpointRestService;
import com.bifrost.exception.endpoint.EndpointInitiationException;
import com.bifrost.exception.endpoint.InvalidContainerPropertiesClassException;
import com.bifrost.logging.LoggingClient;
import com.bifrost.logging.model.RouteLog;
import com.bifrost.message.ExternalMessage;
import com.bifrost.serialization.KryoService;
import com.bifrost.system.model.EtcFile;

/**
 * @author rosaekapratama@gmail.com
 *
 */

@Component
public class Controller extends AbstractController {

	private static final Map<Long, Boolean> status = new ConcurrentHashMap<Long, Boolean>();
	private static final Map<String, EndpointProcess> processes = new ConcurrentHashMap<String, EndpointProcess>();
	private static final Log LOGGER = LogFactory.getLog(Controller.class);

	@Autowired
	private Dispatcher dispatcher;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private LoggingClient loggingClient;
	
	@Autowired	
	protected CamelContext camelContext;
	
	@Autowired
	private PackageLoader packageLoader;
	
	@Autowired
	private ReceiverLogger receiverLogger;
	  
	@Autowired
	private EndpointRestService restService;
	
	@Autowired
	private CacheController cacheController;
	
	@Autowired
	private EndpointRestApiService apiService;
	
	@Autowired
	private RestTemplateFactory restTemplateFactory;
	
	@PostConstruct
	public void init() throws UnknownHostException {
		node = systemIdentity.getHost();
		if(camelContext.getStatus().isStopped())
			camelContext.start();
		super.init();
	}
	
	public EndpointServiceResponse start(String endpointCode) {
		EndpointRest rest = restService.findById(endpointCode);
		if(rest==null) {
			endpointStatusManager.setOffline(endpointCode, node, "No service can be found");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", no service can be found"));
		}else {
			for(EndpointRestApi api : apiService.findByEndpointCode(rest.getEndpointCode()))
				status.put(api.getId(), true);
			endpointStatusManager.setOnline(endpointCode, node, "Endpoint started successfully");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());
		}
	}

	public EndpointServiceResponse stop(String endpointCode) {
		EndpointRest rest = restService.findById(endpointCode);
		if(rest==null) {
			endpointStatusManager.setOffline(endpointCode, node, "No service can be found");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), ", no service can be found");
		}else {
			for(EndpointRestApi api : apiService.findByEndpointCode(rest.getEndpointCode()))
				status.put(api.getId(), false);
			endpointStatusManager.setOffline(endpointCode, node, "Endpoint stopped successfully");
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());
		}
	}

	public EndpointServiceResponse restart(String endpointCode) {
		EndpointServiceResponse response = stop(endpointCode);
		if(response.getCode().equals(GenericResponse.SUCCESS.getCode()))
			response = start(endpointCode);
		else
			return response;
		return response;
	}

	public EndpointServiceResponse reload(String endpointCode) {
		EndpointServiceResponse response = stop(endpointCode);
		if(!response.getCode().equals(GenericResponse.SUCCESS.getCode()))
			return response;
		
		// Stopping internal kafka endpoint consumer
		EndpointProcess process = processes.get(endpointCode); 
		if(process!=null)
			process.stop();	
		
		// refresh all cache
		cacheController.refreshAll();
		
		/*
		 * Remove route from camel context,
		 * to make sure no message consumed and be processed
		 */
		String key = null;
		EndpointRest rest = restService.findById(endpointCode);
		for(EndpointRestApi api : apiService.findByEndpointCode(rest.getEndpointCode())) {
			try {
				status.remove(api.getId());
				key = constructKey(endpointCode, api.getId());
				camelContext.removeRoute(key);
			} catch (Exception e) {
				LOGGER.error("["+endpointCode+"] Error when trying to remove route"+key+", cause by: "+e.getMessage());
				endpointStatusManager.setError(endpointCode, node, "Error when trying to remove route"+key+", cause by: "+e.getMessage());
				e.printStackTrace();
				return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", cause by: "+e.getMessage()));
			}
		}
		
		// Load packager
		try {
			packageLoader.load(endpointCode);
		} catch (Exception e) {
			LOGGER.error("["+endpointCode+"] Error when trying to load packager, cause by: "+e.getMessage());
			endpointStatusManager.setError(endpointCode, node, "Error when trying to load packager, cause by: "+e.getMessage());
			e.printStackTrace();
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", cause by: "+e.getMessage()));
		}
		
		// Load endpoint configuration
		try {
			loadEndpoint(endpointCode);
		} catch (Exception e) {
			for(EndpointRestApi api : apiService.findByEndpointCode(rest.getEndpointCode()))
				if(status.containsKey(api.getId()))
					status.put(api.getId(), false);
			LOGGER.error("["+endpointCode+"] Error when trying to load configuration, cause by: "+e.getMessage());
			endpointStatusManager.setError(endpointCode, node, "Error when trying to reload configuration, cause by: "+e.getMessage());
			e.printStackTrace();
			return new EndpointServiceResponse(endpointCode, node, GenericResponse.ERROR.getCode(), GenericResponse.ERROR.getDesc().concat(", cause by: "+e.getMessage()));
		}
		return new EndpointServiceResponse(endpointCode, node, GenericResponse.SUCCESS.getCode(), GenericResponse.SUCCESS.getDesc());
	}
	
	public List<EndpointServiceResponse> startAll() {
		CountDownLatch latch = new CountDownLatch(getAllEndpoint().size());
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(start(endpoint.getCode()));
					latch.countDown();
				}
			});
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return listResponse;
	}

	public List<EndpointServiceResponse> stopAll() {
		CountDownLatch latch = new CountDownLatch(getAllEndpoint().size());
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(stop(endpoint.getCode()));
					latch.countDown();
				}
			});
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return listResponse;
	}

	public List<EndpointServiceResponse> restartAll() {
		CountDownLatch latch = new CountDownLatch(getAllEndpoint().size());
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(restart(endpoint.getCode()));
					latch.countDown();
				}
			});
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return listResponse;
	}

	public List<EndpointServiceResponse> reloadAll() {
		CountDownLatch latch = new CountDownLatch(getAllEndpoint().size());
		List<EndpointServiceResponse> listResponse = new ArrayList<EndpointServiceResponse>();
		for(Endpoint endpoint : getAllEndpoint())
			taskExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
					listResponse.add(reload(endpoint.getCode()));
					latch.countDown();
				}
			});
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return listResponse;
	}
	
	private void loadEndpoint(String endpointCode) throws InvalidContainerPropertiesClassException, EndpointInitiationException, InterruptedException {
		EndpointRest rest = restService.findById(endpointCode);
		rest.setEndpoint(endpointService.findById(endpointCode));
		EndpointProcess process = new EndpointProcess(rest.getEndpoint(), node, context);
		process.setSyncDispatcher(dispatcher);
		processes.put(endpointCode, process);
		while(!process.isRunning())
			Thread.sleep(1000L);
		if(rest.isClient() && (rest.getHost()==null || rest.getHost().isEmpty()))
			throw new EndpointInitiationException("Host cannot be null if endpoint is a client");
		try {
			for(EndpointRestApi api : apiService.findByEndpointCode(rest.getEndpointCode())) {
				api.setRest(rest);
				loadApi(api, process);
				status.put(api.getId(), true);
			}
			if(rest.isClient() && (!rest.isSslEnabled() || (rest.isSslEnabled() && rest.getCert()==null)))
				restTemplateFactory.createTrustAllRestTemplate(endpointCode, rest.getMaxCon(), rest.getKeepAliveMillis(), rest.getCloseIdleMillis(), rest.getReqTOMillis(), rest.getConTOMillis(), rest.getSocTOMillis());
			else if(rest.isClient() && rest.isSslEnabled()) {
				EtcFile cert = fileService.findById(rest.getCert().getId());
				restTemplateFactory.createTrustCertRestTemplate(endpointCode, cert.getData(), rest.getMaxCon(), rest.getKeepAliveMillis(), rest.getCloseIdleMillis(), rest.getReqTOMillis(), rest.getConTOMillis(), rest.getSocTOMillis());
			}
			endpointStatusManager.setOnline(endpointCode, node, "Endpoint loaded and started successfully");	
			LOGGER.trace("["+endpointCode+"] ============================================================================================================================================");
			LOGGER.info("["+endpointCode+"] Endpoint IS REGISTERED successfully");
			LOGGER.trace("["+endpointCode+"] ============================================================================================================================================");
		} catch (Exception e) {
			stop(endpointCode);
			LOGGER.error("["+endpointCode+"] Error when trying to load configuration, cause by: "+e.getMessage());
			e.printStackTrace();
			endpointStatusManager.setError(endpointCode, node, "Error when trying to load configuration, cause by: "+e.getMessage());
			LOGGER.trace("["+endpointCode+"] ============================================================================================================================================");
			LOGGER.info("["+endpointCode+"] FAILED to register endpoint");
			LOGGER.trace("["+endpointCode+"] ============================================================================================================================================");
		}
	}
	
	public void loadApi(final EndpointRestApi api, final EndpointProcess process) throws Exception {
		EndpointRest endpointRest = api.getRest();
		Boolean isServer = endpointRest.isServer();
		final String endpointCode = endpointRest.getEndpointCode();
		final String restWhiteList = endpointRest.getWhiteList();
		
		/*
		 * SpEL example for http headers
		 * set(HttpHeaders.CONTENT_TYPECONTENT_TYPE, 'application/json');
		 * set(HttpHeaders.AUTHORIZATION, 'Basic: XXXXXXXXXXXX');
		 * deilimited by semicolon
		 */
		final HttpHeaders httpHeaders = new HttpHeaders();
		if(api.getHeaders()!=null)
			for (String exp : api.getHeaders().split(";"))
				if(exp != null && !exp.trim().isEmpty())
					expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(exp, true)).getValue(httpHeaders);
		
		final String contentType = httpHeaders.getFirst(HttpHeaders.CONTENT_TYPE);
		final Long apiId = api.getId();
		final String path = api.getFullPath();
		final String apiWhiteList = api.getWhiteList();
		final String invocationCode = api.getPath();
		final String httpMethod = api.getHttpMethod().toUpperCase();
		if(isServer) {
			RouteBuilder routeBuilder = new RouteBuilder() {
				
				@Override
				public void configure() throws Exception {
					restConfiguration("servlet");
					
					rest()
						.verb(httpMethod.toLowerCase(), path)
						.consumes((contentType==null || contentType.isBlank())?MediaType.TEXT_PLAIN_VALUE:contentType)
						.consumes((contentType==null || contentType.isBlank())?MediaType.TEXT_PLAIN_VALUE:contentType)
						.route()
						.routeId(constructKey(endpointCode, api.getId()))
						.convertBodyTo(byte[].class)
						.choice()
						.when(new Predicate() {
						
							public boolean matches(Exchange exchange) {
								exchange.getMessage().setHeader(RECEIVED_TIME, new Date());
								
								// Checking is service status ONLINE or OFFLINE
								Boolean result = status.get(apiId);
								if(result==null || !result || KryoService.poolIsNotExist()) {
									// Return service offline
									exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 503);
									return result;	
								}

								// Checking remote IP within white list or not
								result = false;
								if(restWhiteList==null || restWhiteList.isEmpty() || restWhiteList.equals("*"))
									result = true;
								else if(apiWhiteList==null || apiWhiteList.isEmpty() || apiWhiteList.equals("*"))
									result = true;
								else {
									HttpServletRequest message = exchange.getMessage().getHeader(Exchange.HTTP_SERVLET_REQUEST, HttpServletRequest.class);
									if(message.getRemoteAddr()==null)
										// Should not be null
										result = false;
									else if(!result)
										result = NetworkUtils.validateIp(message.getRemoteAddr(), restWhiteList);
									else if(!result)
										result = NetworkUtils.validateIp(message.getRemoteAddr(), apiWhiteList);
								}
							
								if(!result) {
									// Return unauthorized
									exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 401);
									return result;
								}

								// Checking is content type supported or not
								if(httpHeaders.getContentType()==null)
									result = true;
								else {
									MediaType mediaTypeRequest = MediaType.parseMediaType((String) exchange.getMessage().getHeader(Exchange.CONTENT_TYPE));
									MediaType mediaTypeService = httpHeaders.getContentType();
									result = mediaTypeService.includes(mediaTypeRequest);
								}
							
								if(!result) {
									// Return unsupported content type
									exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 415);
								}
								return result;
							}
						})
							// Process message
							.process(new Processor() {
					
								public void process(Exchange exchange) throws Exception {
									Date receivedTime = (Date) exchange.getMessage().removeHeader(RECEIVED_TIME);
									org.apache.camel.Message responseMessage = (org.apache.camel.Message) process.receive(exchange.getMessage(), invocationCode, receivedTime, true);
									if(responseMessage == null)
										exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
									else if(responseMessage.getHeader(Exchange.HTTP_RESPONSE_CODE)==null) {
										LOGGER.info("["+endpointCode+"] Http response code is null, set with 200");
										responseMessage.setHeader(Exchange.HTTP_RESPONSE_CODE, 200);
										exchange.setMessage(responseMessage);
									}else {
										try {
											Object httpResponseCode = responseMessage.getHeader(Exchange.HTTP_RESPONSE_CODE);
											if(httpResponseCode instanceof String)
												Integer.valueOf((String) httpResponseCode);
											exchange.setMessage(responseMessage);
										}catch(NumberFormatException e) {	
											LOGGER.info("["+endpointCode+"] Http response code '"+responseMessage.getHeader(Exchange.HTTP_RESPONSE_CODE)+"' is not a number, cannot reply!");
										}
									}
								}
							})
						// Write failed message route log
						.otherwise()
							.process(new Processor() {
								public void process(Exchange exchange) throws Exception {
									writeFailedLog(endpointCode, api, exchange);
								}
							})
					.endRest();
				}
			};
			camelContext.addRoutes(routeBuilder);
		}
	}
	
	private String constructKey(String endpointCode, Long apiId) {
		return endpointCode+".api."+apiId;
	}
	
	private void writeFailedLog(String endpointCode, EndpointRestApi api, Exchange exchange) {
		Date receivedTime = (Date) exchange.getMessage().removeHeader(RECEIVED_TIME);
		String trxId;
		try {
			trxId = ExternalMessage.constructTrxId(endpointCode, systemIdentity.getHost(), exchange.getExchangeId());
		} catch (UnknownHostException e) {
			trxId = ExternalMessage.constructTrxId(endpointCode, "UnknownHost", exchange.getExchangeId());
			e.printStackTrace();
		}

		RouteLog logIn = new RouteLog();
		logIn.setTransactionId(trxId);
		logIn.setDateTime(receivedTime);
		logIn.setEndpointCode(endpointCode);
		logIn.setInvocationCode(api.getPath());
		logIn.setRequest();
		logIn.setRaw(receiverLogger.writeAsString(exchange.getMessage()));
		loggingClient.write(logIn);
		
		RouteLog logOut = new RouteLog();
		logOut.setTransactionId(trxId);
		logOut.setDateTime(new Date());
		logOut.setEndpointCode(endpointCode);
		logOut.setInvocationCode(api.getPath());
		logOut.setResponse();
		logOut.setRaw(receiverLogger.writeAsString(exchange.getMessage()));
		loggingClient.write(logOut);
	}

	@Override
	public List<Endpoint> getAllEndpoint() {
		return endpointService.findByType(EndpointType.REST);
	}

}
