package com.bifrost.endpoint.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.bifrost.endpoint.access.GenericCamelRestCacheController;
import com.bifrost.endpoint.rest.service.EndpointRestApiService;
import com.bifrost.endpoint.rest.service.EndpointRestService;

@RestController
public class CacheController extends GenericCamelRestCacheController {

	private static final Log LOGGER = LogFactory.getLog(CacheController.class);
	
	@Autowired
	private EndpointRestService restService;
	
	@Autowired
	private EndpointRestApiService apiService;

	@Override
	public void evictAll() {
		super.evictAll();
		restService.evictAll();
		apiService.evictAll();
	}

	@Override
	public void populateAll() {
		super.populateAll();
		try {
			restService.populate();
			apiService.populate();
		} catch (Exception e) {
			LOGGER.error("Failed when trying to populate endpoint cache, cause by: "+e.getMessage());
			e.printStackTrace();
		}
	}
}