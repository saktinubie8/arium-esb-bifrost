package com.bifrost.endpoint.rest;

import java.net.URI;
import java.util.Map.Entry;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.support.DefaultExchange;
import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.bifrost.common.json.response.GenericResponse;
import com.bifrost.common.util.SpELUtils;
import com.bifrost.endpoint.AbstractCodec;
import com.bifrost.endpoint.rest.model.EndpointRestApi;
import com.bifrost.endpoint.rest.service.EndpointRestApiService;
import com.bifrost.endpoint.rest.service.EndpointRestService;
import com.bifrost.exception.modifier.UnknownInvocationException;
import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class Codec extends AbstractCodec {

	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private EndpointRestService restService;
	
	@Autowired
	private EndpointRestApiService apiService;
	
	@Autowired
	private ExpressionParser expressionParser;
	
	public ExternalMessage decode(Object obj, Object packager) throws Exception {
		ExternalMessage result = new ExternalMessage();
		byte[] body = null;
		if(obj instanceof Message) {
			Message message = (Message) obj;
			result.putHeaders(message.getHeaders());
			body = (byte[]) message.getBody();
		}else if(obj instanceof ResponseEntity<?>) {
			ResponseEntity<?> responseEntity = (ResponseEntity<?>) obj;
			result.putHeader("Http-Code", responseEntity.getStatusCodeValue());
			if(responseEntity.getHeaders()!=null) {
				HttpHeaders headers = responseEntity.getHeaders();
				for(String key : headers.keySet())
					result.putHeader(key, headers.get(key));
			}
			if(responseEntity.getBody()!=null)
				body = (byte[]) responseEntity.getBody();
		}
		if(packager==null && body!=null && body.length>0)
			result.putBody(decodeJson(body));
		else if(packager!=null && body!=null && body.length>0 && obj instanceof Message)
			result.putBody(decodeFixedLength(body, (Element) packager, true));
		else if(packager!=null && body!=null && body.length>0 && obj instanceof ResponseEntity<?>)
			result.putBody(decodeFixedLength(body, (Element) packager, false));
		return result;	
	}

	public Object encode(ExternalMessage externalMessage, Object packager) throws Exception {
		if(externalMessage.isRequest()) {
			EndpointRestApi api = apiService.findByEndpointCodeAndPath(externalMessage.getEndpointCode(), externalMessage.getInvocationCode());
			if(api==null)
				throw new UnknownInvocationException(GenericResponse.UNKNOWN_INVOCATION_CODE.getDesc()+", perhaps invocation '"+externalMessage.getInvocationCode()+"' is not registered in rest api records?");
			
			api.setRest(restService.findById(api.getRest().getEndpointCode()));
			String[] fragments = api.getFullPath().split("/");
			StringBuilder address = new StringBuilder();
			if(api.getRest().isSslEnabled()!=null && api.getRest().isSslEnabled())
				address.append("https://");
			else
				address.append("http://");
			address.append(api.getRest().getHost().trim());
			for(String fragment : fragments)
				if(fragment.startsWith("{") && fragment.endsWith("}"))
					address.append("/"+externalMessage.getRoot(fragment.substring(1).replace("}", "")));
				else
					address.append("/"+fragment);
			
			HttpMethod httpMethod = HttpMethod.resolve(api.getHttpMethod().trim());
			HttpHeaders httpHeaders = new HttpHeaders();
			for(Entry<String, Object> header: externalMessage.getHeaders().entrySet())
				httpHeaders.set(header.getKey(), String.valueOf(header.getValue()));
			if(api.getHeaders()!=null)
				for (String exp : api.getHeaders().split(";"))
					if(exp != null && !exp.trim().isEmpty())
						expressionParser.parseExpression(SpELUtils.addPackagerOnSpEL(exp, true)).getValue(httpHeaders);

			RequestEntity<String> requestEntity;
			if(packager==null)
				requestEntity = new RequestEntity<String>((String) encodeJson(externalMessage.getBody()), httpHeaders, httpMethod, URI.create(address.toString()));
			else
				requestEntity = new RequestEntity<String>((String) encodeFixedLength(externalMessage.getBody(), (Element) packager, true), httpHeaders, httpMethod, URI.create(address.toString()));
			return requestEntity;
		}else {
			Exchange exchange = new DefaultExchange(camelContext);
			Message result = exchange.getMessage();
			result.setHeaders(externalMessage.getHeaders());
			if(packager==null)
				result.setBody(encodeJson(externalMessage.getBody()));
			else
				result.setBody(encodeFixedLength(externalMessage.getBody(), (Element) packager, false));
			if(externalMessage.getHeader("Http-Code")!=null)
				result.setHeader(Exchange.HTTP_RESPONSE_CODE, externalMessage.getHeader("Http-Code"));
			else
				result.setHeader(Exchange.HTTP_RESPONSE_CODE, 200);
			return result;
		}
	}

	public Object encodeUnknownInvocation(Object packager) throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		Message result = exchange.getMessage();
		result.setHeader(Exchange.HTTP_RESPONSE_CODE, 501);
		return result;
	}

}
