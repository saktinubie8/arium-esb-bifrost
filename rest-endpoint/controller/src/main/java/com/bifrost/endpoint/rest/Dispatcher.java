package com.bifrost.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.bifrost.endpoint.dispatcher.SynchronousDispatcher;
import com.bifrost.endpoint.rest.model.EndpointRest;
import com.bifrost.endpoint.rest.service.EndpointRestService;
import com.bifrost.exception.endpoint.EndpointProcessException;
import com.bifrost.message.ExternalMessage;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class Dispatcher implements SynchronousDispatcher {
	
	@Autowired
	private EndpointRestService restService;

	@Autowired
	private RestTemplateFactory restTemplateContainer;
	
	public Object send(Object obj, ExternalMessage externalMessage) throws Exception {
		RequestEntity<?> requestEntity = (RequestEntity<?>) obj;
		EndpointRest rest = restService.findById(externalMessage.getEndpointCode());
		RestTemplate restTemplate = restTemplateContainer.get(rest.getPk());
		if(restTemplate==null)
			throw new EndpointProcessException(rest.getPk()+" has null RestTemplate");
		ResponseEntity<byte[]> responseEntity = null;
		try {
			responseEntity = restTemplate.exchange(requestEntity, byte[].class);
		}catch(ResourceAccessException e) {
			HttpHeaders headers = new HttpHeaders(); 
			headers.add("Exception", e.getMessage());
			responseEntity = new ResponseEntity<>(headers, HttpStatus.SERVICE_UNAVAILABLE);
		}catch(HttpClientErrorException e) {
			responseEntity = new ResponseEntity<byte[]>(e.getResponseBodyAsByteArray(), e.getResponseHeaders(), e.getStatusCode());
		}catch(HttpServerErrorException e) {
			responseEntity = new ResponseEntity<byte[]>(e.getResponseBodyAsByteArray(), e.getResponseHeaders(), e.getStatusCode());
		}
		return responseEntity;
	}
}
