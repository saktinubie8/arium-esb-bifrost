package com.bifrost.endpoint.rest;

import org.springframework.stereotype.Component;

import com.bifrost.endpoint.access.GenericCamelRestEndpointAccess;


/**
 * @author rosaekapratama@gmail.com
 *
 */
@Component
public class EndpointAccess extends GenericCamelRestEndpointAccess {
	
}
