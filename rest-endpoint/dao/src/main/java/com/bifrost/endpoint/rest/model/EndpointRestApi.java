package com.bifrost.endpoint.rest.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bifrost.common.model.AbstractModel;
import com.bifrost.common.util.StringUtils;
import com.bifrost.endpoint.model.Endpoint;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

/**
 * @author rosaekapratama@gmail.com
 *
 */
@Entity
@Table(name = "endpoint_rest_api", uniqueConstraints = @UniqueConstraint(columnNames = { "endpoint_code", "path" }))
public class EndpointRestApi implements AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "endpoint_code", nullable = false)
	private EndpointRest rest;

	@Column(name = "headers", nullable = true)
	private String headers;

	@Column(name = "path", nullable = false, length = 255)
	private String path;

	@Column(name = "http_method", nullable = false, length = 10, columnDefinition = "varchar(10) default 'POST'")
	private String httpMethod;

	@Column(name = "rc_field_name", nullable = true, length = 40)
	private String rcFieldName;

	@Column(name = "rc_in_headers", nullable = true)
	private Boolean rcInHeaders;

	@Column(name = "rd_field_name", nullable = true, length = 40)
	private String rdFieldName;

	@Column(name = "rd_in_headers", nullable = true)
	private Boolean rdInHeaders;

	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@Column(name = "white_list", nullable = true)
	private String whiteList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EndpointRest getRest() {
		return rest;
	}

	public void setRest(EndpointRest rest) {
		this.rest = rest;
	}

	public String getFullPath() {
		return this.rest.getPathPrefix() + this.getPath();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWhiteList() {
		return StringUtils.setBlankIfNull(whiteList);
	}

	public void setWhiteList(String whiteList) {
		this.whiteList = whiteList;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getRcFieldName() {
		return rcFieldName;
	}

	public void setRcFieldName(String rcFieldName) {
		this.rcFieldName = rcFieldName;
	}

	public Boolean getRcInHeaders() {
		return rcInHeaders;
	}

	public void setRcInHeaders(Boolean rcInHeaders) {
		this.rcInHeaders = rcInHeaders;
	}

	public String getRdFieldName() {
		return rdFieldName;
	}

	public void setRdFieldName(String rdFieldName) {
		this.rdFieldName = rdFieldName;
	}

	public Boolean getRdInHeaders() {
		return rdInHeaders;
	}

	public void setRdInHeaders(Boolean rdInHeaders) {
		this.rdInHeaders = rdInHeaders;
	}

	public Object getPk() {
		return this.id;
	}
	
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(id);
		out.writeUTF(rest.getEndpointCode());
		out.writeUTF(rest.getPathPrefix());
		if(headers!=null) {
			out.writeBoolean(true);
			out.writeUTF(headers);
		}else
			out.writeBoolean(false);
		out.writeUTF(path);
		out.writeUTF(httpMethod);
		if(rcFieldName!=null) {
			out.writeBoolean(true);
			out.writeUTF(rcFieldName);
		}else
			out.writeBoolean(false);
		if(rcInHeaders!=null) {
			out.writeBoolean(true);
			out.writeBoolean(rcInHeaders);
		}else
			out.writeBoolean(false);
		if(rdFieldName!=null) {
			out.writeBoolean(true);
			out.writeUTF(rdFieldName);
		}else
			out.writeBoolean(false);
		if(rdInHeaders!=null) {
			out.writeBoolean(true);
			out.writeBoolean(rdInHeaders);
		}else
			out.writeBoolean(false);
		out.writeUTF(description);
		if(whiteList!=null) {
			out.writeBoolean(true);
			out.writeUTF(whiteList);
		}else
			out.writeBoolean(false);
	}

	public void readData(ObjectDataInput in) throws IOException {
		id = in.readLong();
		if(rest==null)
			rest = new EndpointRest();
		rest.setEndpointCode(in.readUTF());
		if(rest.getEndpoint()==null) {
			Endpoint endpoint = new Endpoint();
			endpoint.setCode(rest.getEndpointCode());
			rest.setEndpoint(endpoint);
		}
		rest.setPathPrefix(in.readUTF());
		if(in.readBoolean())
			headers = in.readUTF();
		path = in.readUTF();
		httpMethod = in.readUTF();
		if(in.readBoolean())
			rcFieldName = in.readUTF();
		if(in.readBoolean())
			rcInHeaders = in.readBoolean();
		if(in.readBoolean())
			rdFieldName = in.readUTF();
		if(in.readBoolean())
			rdInHeaders = in.readBoolean();
		description = in.readUTF();
		if(in.readBoolean())
			whiteList = in.readUTF();
	}

}
