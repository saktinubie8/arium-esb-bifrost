package com.bifrost.endpoint.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.rest.model.EndpointRestApi;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointRestApiRepository extends JpaRepository<EndpointRestApi, Long> {

	public List<EndpointRestApi> findByRest_Endpoint_Code(String endpointCode);
	public List<EndpointRestApi> findByPathLike(String path);
	public EndpointRestApi findByRest_Endpoint_CodeAndPath(String endpointCode, String path);
	
}
