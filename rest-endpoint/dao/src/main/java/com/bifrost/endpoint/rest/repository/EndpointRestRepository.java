package com.bifrost.endpoint.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bifrost.endpoint.rest.model.EndpointRest;

/**
 * @author rosaekapratama@gmail.com
 *
 */
public interface EndpointRestRepository extends JpaRepository<EndpointRest, String> {
	
}
